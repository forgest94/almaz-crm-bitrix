<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Личный кабинет");
$APPLICATION->SetPageProperty("body_additional_class", "body-right");
if ($USER->IsAuthorized()) {
    $arGroups = $USER->GetUserGroupArray();
    $isGroup = array_intersect($arGroups, LK_IDS);
    $idUrl = $isGroup[key($isGroup)];
    if(!empty($idUrl))
    LocalRedirect('/'.LK_URLS[$idUrl].'/');
}else{
?>
<? $APPLICATION->IncludeComponent("letsrock:auth",
    ".default",
    ["REDIRECT"=>""]
);
}?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>