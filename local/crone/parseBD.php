<?php
$_SERVER['DOCUMENT_ROOT'] = "/var/www/almaz_panel/data/www/my.almaztd.ru/";
//$_SERVER['DOCUMENT_ROOT'] = "/var/www/almaz/data/www/almaz.test.letsrock.pro/";
require_once($_SERVER['DOCUMENT_ROOT'] . "/bitrix/modules/main/include/prolog_before.php");
\Bitrix\Main\Loader::includeModule('letsrock.lib');

use Letsrock\Lib\Models\Element;
use Letsrock\Lib\Models\HLBlock;

$dbh = new PDO('mysql:host=localhost;dbname=almaz_old;charset=utf8', "almaz_old", "9K2v5Q7b");
$infoDealer = [];
foreach ($dbh->query('SELECT * from _tst_company') as $row) {
    if ($row["company_name"] == 'Название' || $row["company_name"] == 1) continue;
    $rsUsers = \Bitrix\Main\UserTable::getList([
        'select' => [
            '*',
            'UF_*'
        ],
        'filter' => [
            "UF_PARSE_NAME" => $row["company_name"]
        ],
        'order' => ['ID' => 'asc'],
    ]);

    if ($reg = $rsUsers->fetch()) {
        $infoDealer[] = [
            "id_bitrix" => $reg["ID"],
            "id_old" => $row["company_id"]
        ];
    }
}
$infoBuyer = [];
if (!empty($infoDealer)) {
    $elemIbl = new Element;
    foreach ($infoDealer as $dealer) {
        foreach ($dbh->query('SELECT * from _tst_owner WHERE owner_company="' . $dealer["id_old"] . '"') as $row) {
            $arBuyers_db = $elemIbl->GetList(IB_BUYERS, ["NAME", "ID", "IBLOCK_ID", "PROPERTY_*"],
                [
                    "PROPERTY_DILER" => $dealer["id_bitrix"],
                    "PROPERTY_INN" => $row["owner_inn"]
                ],
                ["ID" => "desc"]);
            if (!empty($arBuyers_db["ELEMS"][0])) {
                $infoBuyer[] = [
                    "id_bitrix" => $arBuyers_db["ELEMS"][0]["ID"],
                    "id_old" => $row["owner_id"],
                    "dealer" => $dealer
                ];
            } else {
                if (!empty($row["owner_region"])) {
                    $infoRegion = $elemIbl->GetList(IB_REGION, ["ID"], ["NAME" => $row["owner_region"]])["ELEMS"][0];
                    if (!empty($infoRegion)) {
                        $regionBuyer = $infoRegion["ID"];
                    } else {
                        $regionBuyer = 0;
                    }
                } else {
                    $regionBuyer = 0;
                }
                $arFields['PROPERTY'] = [
                    'DILER' => $dealer["id_bitrix"],
                    'INN' => $row["owner_inn"],
                    'REGION' => $regionBuyer,
                    'DISTRICT' => 0,
                    'ADDRESS' => $row["owner_addr"],
                    'PHONE' => $row["owner_phone"],
                    'EMAIL' => $row["owner_email"],
                    'CONTACT_PEOPLE' => $row["owner_person"],
                    'POSITION' => '',
                ];
                $addBuyer = $elemIbl->add(IB_BUYERS, $row["owner_name"], $arFields, true);
                if ($addBuyer) {
                    $infoBuyer[] = [
                        "id_bitrix" => $addBuyer,
                        "id_old" => $row["owner_id"],
                        "dealer" => $dealer
                    ];
                } else {
                    echo 'no-buyer-add';
                    file_put_contents($_SERVER["DOCUMENT_ROOT"].'/csv/no_buyer_add_dealer.txt', print_r($dealer, true), FILE_APPEND);
                    file_put_contents($_SERVER["DOCUMENT_ROOT"].'/csv/no_buyer_add.txt', print_r($row, true), FILE_APPEND);
                }
            }
        }
    }
}
if (!empty($infoBuyer)) {
    $elemIbl2 = new Element;
    $typeTechHl = new HLBlock(HL_EQUIPMENT);
    foreach ($infoBuyer as $buyer) {
        $company = $buyer["dealer"]["id_old"];
        $owner = $buyer["id_old"];
        foreach ($dbh->query("SELECT * from _tst_tech WHERE tech_company='$company' AND tech_owner='$owner'") as $row) {
            $infoEx = $elemIbl->GetList(IB_EXPLOITATION, ["ID"], [
                "PROPERTY_NUMBER" => $row["tech_sn"],
                "PROPERTY_DATE" => date('d.m.Y', strtotime($row["tech_date"])),
                "PROPERTY_BUYER" => $buyer["id_bitrix"],
                "PROPERTY_DEALER" => $buyer["dealer"]["id_bitrix"],
            ])["ELEMS"][0];
            if (empty($infoEx)) {
                $arFields['PROPERTY'] = [
                    'NUMBER' => $row["tech_sn"],
                    'DATE' => date('d.m.Y', strtotime($row["tech_date"])),
                    'BUYER' => $buyer["id_bitrix"],
                    'DEALER' => $buyer["dealer"]["id_bitrix"],
                    'STATUS' => 15,
                    'STATUS_ID' => 2
                ];
                $infoModels = $elemIbl2->getList(IB_DIRECTORY);
                foreach ($infoModels["ELEMS"] as $model) {
                    if (!strpos($row["tech_name"], $model["NAME"]) === false) {
                        $arFields['PROPERTY']["MODEL"] = $model["ID"];
                    } else {
                        continue;
                    }
                }

                $infoTypes = $typeTechHl->get([]);
                foreach ($infoTypes as $type) {
                    $exType = explode(' ', $type["UF_NAME"]);
                    if (!strpos($row["tech_name"], $exType[count($exType) - 1]) === false) {
                        $arFields['PROPERTY']["EQUIPMENT"] = $type["ID"];
                    } else {
                        continue;
                    }
                }
                $result = $elemIbl2->add(IB_EXPLOITATION, $row["tech_name"], $arFields, true);
                if (!$result) {
                    echo 'no-tech-add';
                    file_put_contents($_SERVER["DOCUMENT_ROOT"].'/csv/no_tech_add.txt', print_r($row, true), FILE_APPEND);
                }
            } else {
                echo 'no-tech';
                file_put_contents($_SERVER["DOCUMENT_ROOT"].'/csv/no_tech.txt', print_r($row, true), FILE_APPEND);
            }
        }
    }
}
// /opt/php72/bin/php /var/www/almaz_panel/data/www/my.almaztd.ru/local/crone/parseBD.php
// /opt/php73/bin/php /var/www/almaz/data/www/almaz.test.letsrock.pro/local/crone/parseBD.php