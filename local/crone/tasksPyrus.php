<?
require_once($_SERVER['DOCUMENT_ROOT'] . "/bitrix/modules/main/include/prolog_before.php");
\Bitrix\Main\Loader::includeModule('letsrock.lib');

use Letsrock\Lib\Controllers\MailController;
use Letsrock\Lib\Controllers\PyrusController;
use Letsrock\Lib\Models\Element;

$arTask = [];
$arIdsDealer = [];
$infoDealers = [];
$usersForManger = CUser::GetList(($by = "ID"), ($order = "ASC"),
    [
        "GROUPS_ID" => [ID_DEALER]
    ], array("SELECT" => array("UF_*")));
while ($arUser = $usersForManger->Fetch()) {
    $arIdsDealer[] = $arUser['ID'];
    $infoDealers[$arUser['ID']] = $arUser;
}
if (!empty($arIdsDealer)) {
    $elemIbl = new Element;
    $arReclamationTech_db = $elemIbl->GetList(IB_RECLAMATION_TECH,
        ["NAME", "ID", "IBLOCK_ID", "DATE_CREATE", "PROPERTY_*"],
        [
            "PROPERTY_DEALER" => $arIdsDealer,
            [
                "LOGIC" => "OR",
                ["PROPERTY_STATUS_VALUE" => "Отправлено"],
                ["PROPERTY_STATUS_VALUE" => "В работе"],
            ]
        ],
        ["created_date" => "desc"]
    );
    foreach ($arReclamationTech_db["ELEMS"] as $tech) {
        if (strpos('---- ' . $tech["NAME"], 'RT') === false &&
            strpos('---- ' . $tech["NAME"], 'RP') === false) {
            $propsTech = $arReclamationTech_db["PROPS"][$tech["ID"]];
            $arTask[] = [
                "id_pyrus" => $tech["NAME"],
                "id" => $tech["ID"],
                "iblock" => $tech["IBLOCK_ID"],
                "dealer" => $propsTech["DEALER"]["VALUE"]
            ];
        }
    }
    $arReclamationParts_db = $elemIbl->GetList(IB_RECLAMATION_PARTS,
        ["NAME", "ID", "IBLOCK_ID", "DATE_CREATE", "PROPERTY_*"],
        [
            "PROPERTY_DEALER" => $arIdsDealer,
            [
                "LOGIC" => "OR",
                ["PROPERTY_STATUS_VALUE" => "Отправлено"],
                ["PROPERTY_STATUS_VALUE" => "В работе"],
            ],
        ],
        ["created_date" => "desc"]
    );
    foreach ($arReclamationParts_db["ELEMS"] as $parts) {
        if (strpos('---- ' . $parts["NAME"], 'RP') === false &&
            strpos('---- ' . $parts["NAME"], 'RT') === false) {
            $propsParts = $arReclamationParts_db["PROPS"][$parts["ID"]];
            $arTask[] = [
                "id_pyrus" => $parts["NAME"],
                "id" => $parts["ID"],
                "iblock" => $parts["IBLOCK_ID"],
                "dealer" => $propsParts["DEALER"]["VALUE"]
            ];
        }
    }
}
if (!empty($arTask)) {
    $pyrus = new PyrusController;
    foreach ($arTask as $item) {
        $statusMess = '';
        $arMail = [];
        $resp_pyrus = $pyrus->getTask($item["id_pyrus"]);
        if ($resp_pyrus["success"]) {
            /*
            [0] => Не выбрано
            [1] => Принята в работу
            [2] => Отказано в удовлетворении требований
            [3] => Требования удовлетворены
            [4] => Рекламация закрыта
            */
            switch ($resp_pyrus["status"]) {
                case 2:
                    if ($item["iblock"] == IB_RECLAMATION_PARTS) {
                        CIBlockElement::SetPropertyValueCode($item["id"], "STATUS", 24);
                    } else if ($item["iblock"] == IB_RECLAMATION_TECH) {
                        CIBlockElement::SetPropertyValueCode($item["id"], "STATUS", 20);
                    }
                    $statusMess = 'отклонена';
                    break;
                case 3:
                    if ($item["iblock"] == IB_RECLAMATION_PARTS) {
                        CIBlockElement::SetPropertyValueCode($item["id"], "STATUS", 23);
                    } else if ($item["iblock"] == IB_RECLAMATION_TECH) {
                        CIBlockElement::SetPropertyValueCode($item["id"], "STATUS", 19);
                    }
                    $statusMess = 'одобрена';
                    break;
            }
            if (!empty($statusMess) && !empty($infoDealers)) {
                $infoElem = $elemIbl->getById($item["iblock"], $item["id"], ["*"], true);
                if (strpos('---- ' . $infoElem["NAME"], 'RP') === false &&
                    strpos('---- ' . $infoElem["NAME"], 'RT') === false) {
                    $numberRecl = $infoElem["NAME"];
                } else {
                    if ($item["iblock"] == IB_RECLAMATION_PARTS) {
                        $numberRecl = 'RP' . $infoElem["ID"];
                    } else {
                        $numberRecl = 'RT' . $infoElem["ID"];
                    }
                }
                $arMail["subject"] = "Личный кабинет дилера Алмаз";
                $arMail["to_mail"] = $infoDealers[$item["dealer"]]["EMAIL"];
                $arMail["html"] = "
                                        <p>
                                            Рекламация номер " . $numberRecl . " " . $statusMess . "<br/>
                                            Дата подачи рекламации: " . $infoElem["PROPERTY"]["DATE"]["VALUE"] . "
                                        </p>
                                ";
            }
            if (!empty($arMail)) {
                $mailer = new MailController();
                $mailto = $mailer->send($arMail);
            }
        }
    }
}
?>