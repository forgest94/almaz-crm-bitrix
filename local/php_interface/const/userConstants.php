<?php
/**
 * Пользовательские константы
 */

const ASSETS_PATH = '/local/assets/';
const DAY_EXECUTION = '+ 5 day';


/**
 * Highload блоки
 */

const HL_EQUIPMENT = 2;
const HL_HISTORY = 5;
const HL_MARKPROPS = 6;
const HL_POPSDOP = 7;

/**
 * Инфоблоков
 */
const IB_PROPS = 5;
const IB_DIRECTORY = 6;
const IB_BUYERS = 7;
const IB_EXPLOITATION = 8;
const IB_REGION = 9;
const IB_AREA = 10;
const IB_COUNTRY = 16;
const IB_WARNING = 11;
const IB_MARKETING = 12;
const IB_SERVCOMP = 13;
const IB_RECLAMATION_TECH = 14;
const IB_RECLAMATION_PARTS = 15;

/**
 * ID групп пользователей
 */

const ID_DEALER = 8;
const ID_MANAGER_SERVICES = 7;
const ID_MANAGER_PARTS = 10;
const ID_MANAGER_TECH = 11;
const ID_MANAGER_MARKETING = 12;
const ID_ADMIN = 9;
const LK_URLS = [
    ID_MANAGER_SERVICES => 'manager',
    ID_MANAGER_PARTS => 'manager',
    ID_MANAGER_TECH => 'manager',
    ID_MANAGER_MARKETING => 'manager',
    ID_DEALER => 'dealer',
    ID_ADMIN => 'administrator'
];
const LK_IDS = [
    ID_MANAGER_SERVICES,
    ID_MANAGER_PARTS,
    ID_MANAGER_TECH,
    ID_MANAGER_MARKETING,
    ID_DEALER,
    ID_ADMIN
];


