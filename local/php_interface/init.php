<?
include $_SERVER["DOCUMENT_ROOT"] . '/local/php_interface/const/userConstants.php';
require $_SERVER["DOCUMENT_ROOT"] . '/local/vendor/autoload.php';
\Bitrix\Main\Loader::includeModule('letsrock.lib');

use Letsrock\Lib\Controllers\PyrusController;
use Letsrock\Lib\Models\Element;

function pr($arr)
{
    echo '<pre>';
    print_r($arr);
    echo '</pre>';
}

/*$pyrus = new PyrusController;
    $resp_pyrus = $pyrus->getTask(51226122);*/
/*
AddEventHandler("main", "OnAfterUserLogin", Array("AfterLogin", "OnAfterUserLoginHandler"));

class AfterLogin
{
    function OnAfterUserLoginHandler(&$fields)
    {
        // если логин успешен то
        if ($fields['USER_ID'] > 0) {
            $infoGroup = CUser::GetUserGroup($fields['USER_ID']);
            if ($infoGroup) {
                $arTask = [];
                $arIdsDealer = [];
                if (in_array(ID_DEALER, $infoGroup)) {
                    $arIdsDealer = $fields['USER_ID'];
                } else if (
                    in_array(ID_MANAGER_SERVICES, $infoGroup) ||
                    in_array(ID_MANAGER_PARTS, $infoGroup) ||
                    in_array(ID_MANAGER_TECH, $infoGroup) ||
                    in_array(ID_MANAGER_MARKETING, $infoGroup)) {
                    $usersForManger = CUser::GetList(($by = "ID"), ($order = "ASC"),
                        [
                            "UF_MANAGER_NEW" => array($fields['USER_ID'])
                        ], array("SELECT" => array("UF_*")));
                    while ($arUser = $usersForManger->Fetch()) {
                        $arIdsDealer[] = $arUser["ID"];
                    }
                } else if (in_array(ID_ADMIN, $infoGroup)) {
                    $usersForManger = CUser::GetList(($by = "ID"), ($order = "ASC"),
                        [
                            "UF_ADMIN_ID" => $fields['USER_ID']
                        ], array("SELECT" => array("UF_*")));
                    while ($arUser = $usersForManger->Fetch()) {
                        if ($arUser["UF_ADMIN_ID"]) {
                            $usersForManger2 = CUser::GetList(($by = "ID"), ($order = "ASC"), ["UF_MANAGER_NEW" => $arUser["ID"]], array("SELECT" => array("UF_*")));
                            while ($arUser2 = $usersForManger2->Fetch()) {
                                $arIdsDealer[] = $arUser2["ID"];
                            }
                        }
                    }
                }
                if ($arIdsDealer) {
                    $elemIbl = new Element;
                    $arReclamationTech_db = $elemIbl->GetList(IB_RECLAMATION_TECH,
                        ["NAME", "ID", "IBLOCK_ID", "DATE_CREATE", "PROPERTY_*"],
                        [
                            "PROPERTY_DEALER" => $arIdsDealer,
                            [
                                "LOGIC" => "OR",
                                ["PROPERTY_STATUS_VALUE" => "Отправлено"],
                                ["PROPERTY_STATUS_VALUE" => "В работе"],
                            ]
                        ],
                        ["created_date" => "desc"]
                    );
                    foreach ($arReclamationTech_db["ELEMS"] as $tech) {
                        if (strpos('---- ' . $tech["NAME"], 'RT') === false &&
                            strpos('---- ' . $tech["NAME"], 'RP') === false)
                            $arTask[] = ["id_pyrus" => $tech["NAME"], "id" => $tech["ID"], "iblock" => $tech["IBLOCK_ID"]];
                    }
                    $arReclamationParts_db = $elemIbl->GetList(IB_RECLAMATION_PARTS,
                        ["NAME", "ID", "IBLOCK_ID", "DATE_CREATE", "PROPERTY_*"],
                        [
                            "PROPERTY_DEALER" => $arIdsDealer,
                            [
                                "LOGIC" => "OR",
                                ["PROPERTY_STATUS_VALUE" => "Отправлено"],
                                ["PROPERTY_STATUS_VALUE" => "В работе"],
                            ],
                        ],
                        ["created_date" => "desc"]
                    );
                    foreach ($arReclamationParts_db["ELEMS"] as $parts) {
                        if (strpos('---- ' . $parts["NAME"], 'RP') === false &&
                            strpos('---- ' . $parts["NAME"], 'RT') === false)
                            $arTask[] = ["id_pyrus" => $parts["NAME"], "id" => $parts["ID"], "iblock" => $parts["IBLOCK_ID"]];
                    }
                }
                if (!empty($arTask)) {
                    $pyrus = new PyrusController;
                    foreach ($arTask as $item) {
                        $resp_pyrus = $pyrus->getTask($item["id_pyrus"]);
                        if ($resp_pyrus["success"]) {
                            /*
                            [0] => Не выбрано
                            [1] => Принята в работу
                            [2] => Отказано в удовлетворении требований
                            [3] => Требования удовлетворены
                            [4] => Рекламация закрыта
                            *
                            switch ($resp_pyrus["status"]) {
                                case 2:
                                    if ($item["iblock"] == IB_RECLAMATION_PARTS) {
                                        CIBlockElement::SetPropertyValueCode($item["id"], "STATUS", 24);
                                    } else if ($item["iblock"] == IB_RECLAMATION_TECH) {
                                        CIBlockElement::SetPropertyValueCode($item["id"], "STATUS", 20);
                                    }
                                    break;
                                case 3:
                                    if ($item["iblock"] == IB_RECLAMATION_PARTS) {
                                        CIBlockElement::SetPropertyValueCode($item["id"], "STATUS", 23);
                                    } else if ($item["iblock"] == IB_RECLAMATION_TECH) {
                                        CIBlockElement::SetPropertyValueCode($item["id"], "STATUS", 19);
                                    }
                                    break;
                            }
                        }
                    }
                }
            }
        }
    }
}*/