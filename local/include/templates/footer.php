<footer class="footer">
    <div class="container">
        <div class="row">
            <div class="col-4 col-md-6 col-sm-15">
                <p>© ТД Алмаз 2019. Алтайский край, г. Барнаул, ул. Северо-Западная 2а, оф. 301</p>
                <p><a href="tel:+73852503703">+7 (3852) 503-703</a></p>
                <p><a href="mailto:almaztd@almaztd.ru">almaztd@almaztd.ru</a></p>
                <p></p>
            </div>
            <div class="col-2 col-md-3"></div>
            <div class="col-4 col-md-6">
                <p><a href="https://www.facebook.com/almaztd/" class="footer__soc f">Facebook</a></p>
                <p><a href="http://www.youtube.com/channel/UCYZsFu6eCgyzu55kp-clC8Q" class="footer__soc y">YouTube</a></p>
                <p><a href="https://www.instagram.com/almaztd_official/" class="footer__soc i">Instagram</a></p>
            </div>
        </div>
        <div class="footer__bot">
            <div class="copy"><a href="#">Пользовательское соглашение</a></div>
            <div class="country">
                <a target="_blank" href="https://letsrock.pro/">
                    Разработка сайта —
                </a>
                <a target="_blank" href="https://letsrock.pro/">
                    <img src="<?=ASSETS_PATH?>img/letsrock-logo.svg" alt="">
                </a>
            </div>
        </div>
    </div>
</footer>
</div>
<!-- page-content -->

<div class="menu">
    <div class="menu__exit"><img src="<?=ASSETS_PATH?>img/exit.svg" alt=""></div>
    <div class="menu__inner">
        <div class="container">
            <div class="menu-blocks">
                <div class="menu-block">
                    <h4 class="menu-block__title"><a href="http://almaztd.ru/equipment/">Техника</a></h4>
                    <h4 class="menu-block__title"><a href="http://almaztd.ru/spare_parts/">Запчасти</a></h4>
                    <h4 class="menu-block__title"><a href="http://almaztd.ru/service/">Сервис</a></h4>
                    <h4 class="menu-block__title"><a href="http://almaztd.ru/company/">Компания</a></h4>
                    <h4 class="menu-block__title"><a href="http://almaztd.ru/purchase/">Покупка</a></h4>
                    <h4 class="menu-block__title"><a href="http://almaztd.ru/press_center/">Пресс-центр</a></h4>
                    <h4 class="menu-block__title"><a href="http://almaztd.ru/contacts/">Контакты</a></h4>
                </div>
            </div>
            <div class="menu-phone">
                <h3><a href="tel:88007005008">8 800 700 50 08</a></h3>
                <p>Бесплатные звонки по России</p>
            </div>
        </div>
    </div>
</div>

<div class="regions">
    <div class="regions__exit"><img src="<?=ASSETS_PATH?>img/exit.svg" alt=""></div>
    <div class="regions__inner">
        <div class="container">
            <div class="regions__search">
                <form action="#">
                    <input type="text" class="regions__search-input" placeholder="Поиск по регионам">
                    <label for="" class="regions__search-btn">
                        <svg width="28" height="28" viewBox="0 0 28 28" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path fill-rule="evenodd" clip-rule="evenodd"
                                  d="M15.7696 16.5659L22.1337 22.9298L22.1335 20.1016L17.1023 15.0702C17.9442 13.8312 18.4306 12.3323 18.4164 10.721C18.3793 6.50816 14.934 3.12301 10.7211 3.16009C6.50825 3.19717 3.1231 6.64245 3.16018 10.8553C3.19727 15.0682 6.64254 18.4534 10.8554 18.4163C12.735 18.3997 14.4499 17.7048 15.7696 16.5659ZM10.8378 16.4164C7.72947 16.4437 5.18747 13.9461 5.16011 10.8377C5.13275 7.72937 7.63038 5.18737 10.7387 5.16001C13.8471 5.13265 16.3891 7.63029 16.4165 10.7386C16.4438 13.847 13.9462 16.389 10.8378 16.4164Z">
                        </svg>
                        <input type="submit" value="">
                    </label>
                </form>
            </div>
            <div class="regions__help"></div>
            <ul class="regions__country">
                <li class="regions__country-item">Азербайджан</li>
                <li class="regions__country-item">Армения</li>
                <li class="regions__country-item">Грузия</li>
                <li class="regions__country-item">Кыргызская Республика</li>
                <li class="regions__country-item">Монголия</li>
                <li class="regions__country-item">Республика Беларусь</li>
                <li class="regions__country-item">Республика Казахстан</li>
                <li class="regions__country-item">Республика Молдова</li>
                <li class="regions__country-item">Таджикестан</li>
                <li class="regions__country-item">Узбескистан</li>
            </ul>
            <div class="regions__russia">
                <span class="regions__country-item">Россия</span>
                <div class="regions__russia-items">
                    <div class="regions__russia-item">
                        <div class="regions__russia-sum">А</div>
                        <span class="regions__russia-item_child">Адыгея</span>
                        <span class="regions__russia-item_child">Алтай, Республика</span>
                        <span class="regions__russia-item_child">Алтайский край</span>
                        <span class="regions__russia-item_child">Амурская область</span>
                        <span class="regions__russia-item_child">Архангельская область</span>
                        <span class="regions__russia-item_child">Астраханская область</span>
                    </div>
                    <div class="regions__russia-item">
                        <div class="regions__russia-sum">Б</div>
                        <span class="regions__russia-item_child">Башкортостан</span>
                        <span class="regions__russia-item_child">Белгородская область</span>
                        <span class="regions__russia-item_child">Брянская область</span>
                        <span class="regions__russia-item_child">Бурятия</span>
                    </div>
                    <div class="regions__russia-item">
                        <div class="regions__russia-sum">В</div>
                        <span class="regions__russia-item_child">Владимирская область</span>
                        <span class="regions__russia-item_child">Волгоградская область</span>
                        <span class="regions__russia-item_child">Вологодская область</span>
                        <span class="regions__russia-item_child">Воронежская область</span>
                    </div>
                    <div class="regions__russia-item">
                        <div class="regions__russia-sum">Д</div>
                        <span class="regions__russia-item_child">Дагестан</span>
                    </div>
                    <div class="regions__russia-item">
                        <div class="regions__russia-sum">Е</div>
                        <span class="regions__russia-item_child">Еврейская автономная область</span>
                    </div>
                    <div class="regions__russia-item">
                        <div class="regions__russia-sum">З</div>
                        <span class="regions__russia-item_child">Забайкальский край</span>
                    </div>
                    <div class="regions__russia-item">
                        <div class="regions__russia-sum">и</div>
                        <span class="regions__russia-item_child">Ивановская область</span>
                        <span class="regions__russia-item_child">Ингушетия</span>
                        <span class="regions__russia-item_child">Иркутская область</span>
                    </div>
                    <div class="regions__russia-item">
                        <div class="regions__russia-sum">к</div>
                        <span class="regions__russia-item_child">Кабардино-Балкарская Республика</span>
                        <span class="regions__russia-item_child">Калининградская область</span>
                        <span class="regions__russia-item_child">Калмыкия</span>
                        <span class="regions__russia-item_child">Калужская область</span>
                        <span class="regions__russia-item_child">Камчатский край</span>
                        <span class="regions__russia-item_child">Карачаево-Черкессия</span>
                        <span class="regions__russia-item_child">Карелия</span>
                        <span class="regions__russia-item_child">Кемеровская область</span>
                        <span class="regions__russia-item_child">Кировская область</span>
                        <span class="regions__russia-item_child">Коми</span>
                        <span class="regions__russia-item_child">Костромская область</span>
                        <span class="regions__russia-item_child">Краснодарский край</span>
                        <span class="regions__russia-item_child">Красноярский край</span>
                        <span class="regions__russia-item_child">Крым</span>
                        <span class="regions__russia-item_child">Курганская область</span>
                        <span class="regions__russia-item_child">Курская область</span>
                    </div>
                    <div class="regions__russia-item">
                        <div class="regions__russia-sum">л</div>
                        <span class="regions__russia-item_child">Ленинградская область</span>
                        <span class="regions__russia-item_child">Липецкая область</span>
                    </div>
                    <div class="regions__russia-item">
                        <div class="regions__russia-sum">м</div>
                        <span class="regions__russia-item_child">Магаданская область</span>
                        <span class="regions__russia-item_child">Марий Эл</span>
                        <span class="regions__russia-item_child">Мордовия</span>
                        <span class="regions__russia-item_child">Москва</span>
                        <span class="regions__russia-item_child">Московская область</span>
                        <span class="regions__russia-item_child">Мурманская область</span>
                    </div>
                    <div class="regions__russia-item">
                        <div class="regions__russia-sum">н</div>
                        <span class="regions__russia-item_child">Ненецкий автономный округ</span>
                        <span class="regions__russia-item_child">Нижегородская область</span>
                        <span class="regions__russia-item_child">Новгородская область</span>
                        <span class="regions__russia-item_child">Новосибирская область</span>
                    </div>
                    <div class="regions__russia-item">
                        <div class="regions__russia-sum">о</div>
                        <span class="regions__russia-item_child">Омская область</span>
                        <span class="regions__russia-item_child">Оренбургская область</span>
                        <span class="regions__russia-item_child">Орловская область</span>
                    </div>
                    <div class="regions__russia-item">
                        <div class="regions__russia-sum">п</div>
                        <span class="regions__russia-item_child">Пензенская область</span>
                        <span class="regions__russia-item_child">Пермский край</span>
                        <span class="regions__russia-item_child">Приморский край</span>
                        <span class="regions__russia-item_child">Псковская область</span>
                    </div>
                    <div class="regions__russia-item">
                        <div class="regions__russia-sum">р</div>
                        <span class="regions__russia-item_child">Ростовская область</span>
                        <span class="regions__russia-item_child">Рязанская область</span>
                    </div>
                    <div class="regions__russia-item">
                        <div class="regions__russia-sum">с</div>
                        <span class="regions__russia-item_child">Самарская область</span>
                        <span class="regions__russia-item_child">Санкт-Петербург</span>
                        <span class="regions__russia-item_child">Саратовская область</span>
                        <span class="regions__russia-item_child">Саха (Якутия)</span>
                        <span class="regions__russia-item_child">Сахалинская область</span>
                        <span class="regions__russia-item_child">Свердловская область</span>
                        <span class="regions__russia-item_child">Северная Осетия (Алания)</span>
                        <span class="regions__russia-item_child">Смоленская область</span>
                        <span class="regions__russia-item_child">Ставропольский край</span>
                    </div>
                    <div class="regions__russia-item">
                        <div class="regions__russia-sum">т</div>
                        <span class="regions__russia-item_child">Тамбовская область</span>
                        <span class="regions__russia-item_child">Татарстан</span>
                        <span class="regions__russia-item_child">Тверская область</span>
                        <span class="regions__russia-item_child">Томская область</span>
                        <span class="regions__russia-item_child">Тульская область</span>
                        <span class="regions__russia-item_child">Тыва (Тува)</span>
                        <span class="regions__russia-item_child">Тюменская область</span>
                    </div>
                    <div class="regions__russia-item">
                        <div class="regions__russia-sum">у</div>
                        <span class="regions__russia-item_child">Удмуртия</span>
                        <span class="regions__russia-item_child">Ульяновская область</span>
                    </div>
                    <div class="regions__russia-item">
                        <div class="regions__russia-sum">х</div>
                        <span class="regions__russia-item_child">Хабаровский край</span>
                        <span class="regions__russia-item_child">Хакасия</span>
                        <span class="regions__russia-item_child">Ханты-Мансийский автономный округ</span>
                    </div>
                    <div class="regions__russia-item">
                        <div class="regions__russia-sum">ч</div>
                        <span class="regions__russia-item_child">Челябинская область</span>
                        <span class="regions__russia-item_child">Чечня</span>
                        <span class="regions__russia-item_child">Чувашия</span>
                        <span class="regions__russia-item_child">Чукотский автономный округ</span>
                    </div>
                    <div class="regions__russia-item">
                        <div class="regions__russia-sum">я</div>
                        <span class="regions__russia-item_child">Ямало-Ненецкий автономный округ</span>
                        <span class="regions__russia-item_child">Ярославская область</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="hint-region">
    <div class="hint-region__text">&nbsp;</div>
</div>

<div class="hint-region-ask">
    <span class="hint-region-ask__text">Ваш регион Алтайский край?</span>
    <span class="hint-region-ask__btn">ДА</span>
    <span class="hint-region-ask__btn">НЕТ</span>
</div>

<div class="cube-loader-wrap js-cube-loader">
    <div class="cube-loader-inner">
        <div class="caption">
            <div class="cube-loader">
                <div class="cube loader-1"></div>
                <div class="cube loader-2"></div>
                <div class="cube loader-4"></div>
                <div class="cube loader-3"></div>
            </div>
        </div>
    </div>
</div>

<div class="popup-list">
    <div class="popup popup__result js-modal" data-modal-type="result">
        <div class="popup__inner">
            <div class="close popup__close js-modal-close"></div>
            <div class="popup__head js-modal-text">
                <div class="popup__title">Действие совершено успешно</div>
            </div>
            <div class="popup__content js-modal-content">
                <div class="popup__btn-wrap">
                    <div class="fildModal no-elem">
                        <label class="field field-textarea">
                            <textarea class="field__input" name="text_modal" type="text" style="height: 150px;"> </textarea>
                        </label>
                        <br/>
                    </div>
                    <a class="btn popup__btn link-href js-link-href js-modal-close" href="javascript:void(0)">Ок</a>
                    <span class="mess"></span>
                    <?/*js-modal-close*/?>
                </div>
            </div>
        </div>
    </div>
</div>
</body>

</html>