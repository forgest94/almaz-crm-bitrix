<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <?

    use Bitrix\Main\Page\Asset;
    use Letsrock\Lib\Models\Property;

    CUtil::InitJSCore();
    CJSCore::Init(['ajax', 'json', 'ls', 'session', 'jquery', 'popup', 'pull']);
    Asset::getInstance()->addString('<link rel="icon" type="image/png" href="' . ASSETS_PATH . 'img/favicons/favicon.png">');

    //Подключаем статику
    Asset::getInstance()->addCss(ASSETS_PATH . "css/main.css");
    Asset::getInstance()->addJs(ASSETS_PATH . "js/main.js");
    ?>
    <? $APPLICATION->ShowHead(); ?>
    <title><? $APPLICATION->ShowTitle() ?></title>
    <meta name="theme-color" content="#c9e0e04d">
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-4175596-31"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-4175596-31');
    </script>
</head>
<body class="<? $APPLICATION->ShowProperty("body_additional_class") ?>">
<!-- Yandex.Metrika counter --> <script type="text/javascript" > (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)}; m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)}) (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym"); ym(2148148, "init", { clickmap:true, trackLinks:true, accurateTrackBounce:true }); </script> <noscript><div><img src="https://mc.yandex.ru/watch/2148148" style="position:absolute; left:-9999px;" alt="" /></div></noscript> <!-- /Yandex.Metrika counter -->
<? $APPLICATION->ShowPanel() ?>
<div class="page-nav">
    <a href="/" class="page-nav__logo">
        <svg width="69" height="69" viewBox="0 0 69 69" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path class="page-nav__logo-path"
                  d="M20.129 19.5272L29.9355 30.1901V46.1202L33.0323 48.6896V29.1624L20.129 15.2878V19.5272ZM20.129 27.1069L24.6452 32.1172V41.8808L27.7419 44.4501V31.2179L20 22.9959V27.1069H20.129ZM20.129 38.2837L22.5806 40.2107V33.4018L20.129 30.5755V38.2837ZM40.3871 44.4501L43.4839 41.8808V32.1172L48 27.1069V22.8674L40.2581 31.0894V44.4501H40.3871ZM35.0968 48.6896L38.1935 46.1202V30.1901L48 19.5272V15.2878L35.0968 29.1624V48.6896ZM45.6774 40.2107L48.129 38.2837V30.5755L45.6774 33.4018V40.2107ZM40.3871 0.385406L34.0645 7.06578L27.871 0.385406V4.75334L34.0645 11.5622L40.3871 4.62487V0.385406ZM34.0645 19.5272L48.129 4.23947V0L34.0645 14.9024L20.129 0V4.23947L34.0645 19.5272ZM20.129 11.8191L34.0645 27.1069L48.129 11.8191V7.70812L34.0645 22.6105L20.129 7.70812V11.8191Z" />
            <path
                    d="M65.4485 61.6038L68.6195 57.7736V55H57.8382L58.8529 58.1698H64.8143L61.2629 61.7358L64.5607 65.566H58.9798L57.7114 67.283L54.7941 55H45.7886L46.5496 57.5094L44.1397 67.5472L41.8566 57.7736L43.7592 55H39.5735L36.2757 60.6792L32.8511 55L28.4118 55L30.1875 57.9057L27.9044 67.283L24.9871 55H15.8548L16.7427 57.5094L14.7132 65.6981H13.5717L11.0349 55H2.02942L2.79045 57.5094L0 69H3.67831L4.43934 65.566H8.37133L7.48346 62.9245H5.07353L6.21508 57.9057H7.99081L10.4007 69H11.5423H14.4596H17.6305L20.1673 57.9057H21.943L24.3529 69H27.5239H28.2849H31.329L32.7243 62.1321L35.8952 67.4151L39.0662 62.1321L40.4614 69H43.7592H44.5202H47.5643L48.3254 65.566H52.1305L51.3695 62.9245H48.8327L49.9743 57.9057H51.8768L54.1599 69H56.443H58.2188H69V66.4906L65.4485 61.6038Z"
                    fill="black" />
        </svg>
    </a>
    <a href="#" class="page-nav__menu">
        <svg width="15" height="10" viewBox="0 0 15 10" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path fill-rule="evenodd" clip-rule="evenodd"
                  d="M15 2L0 2L0 -0.00012207L15 -0.00012207V2ZM15 6.00012L0 6.00012L0 4L15 4V6.00012Z" fill="black" />
            <path d="M0 10H15V8L0 8L0 10Z" fill="black" />
        </svg>
        <span>меню</span>
    </a>
    <div class="page-nav__exit">
        <img src="<?=ASSETS_PATH?>img/exit.svg" alt="">
    </div>
    <div class="page-nav__bot">
        <div class="page-nav__bot-main">
            <a href="#" class="page-nav__search">
                <svg width="29" height="29" viewBox="0 0 29 29" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path fill-rule="evenodd" clip-rule="evenodd"
                          d="M16.3697 17.4849L22.7338 23.8488L22.7336 21.0206L17.7024 15.9892C18.5443 14.7502 19.0307 13.2513 19.0165 11.64C18.9794 7.42717 15.5341 4.04202 11.3212 4.0791C7.10835 4.11618 3.7232 7.56146 3.76028 11.7743C3.79736 15.9872 7.24264 19.3724 11.4555 19.3353C13.3351 19.3187 15.05 18.6238 16.3697 17.4849ZM11.4379 17.3354C8.32956 17.3627 5.78756 14.8651 5.7602 11.7567C5.73284 8.64838 8.23048 6.10638 11.3388 6.07902C14.4472 6.05166 16.9892 8.5493 17.0166 11.6577C17.0439 14.766 14.5463 17.308 11.4379 17.3354Z"
                          fill="black" />
                </svg>
            </a>
            <a href="#" class="page-nav__lc">
                <svg width="18" height="21" viewBox="0 0 18 21" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M0 14.25L1.875 21H16.125L18 14.25L10.5 10.875H7.5L0 14.25Z" fill="black" />
                    <path
                            d="M12.7501 9C12.0001 10.5 10.5001 11.25 10.5001 11.25L9.00006 12L7.50006 11.25C7.50006 11.25 6.00007 10.5 5.25007 9C4.77572 8.0513 2.25002 0 9.00006 0C15.7501 0 13.1555 8.18915 12.7501 9Z"
                            fill="black" />
                </svg>
            </a>
            <a href="#" class="page-nav__location">
                <svg width="28" height="28" viewBox="0 0 28 28" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M0.429824 13.4212L9.78979 17.3159L12 27.21L22.2099 6.89416L0.429824 13.4212Z" fill="black" />
                </svg>
            </a>
        </div>
        <div class="page-nav__bot-menu">
            <a href="#" class="page-nav__search">
                <img src="<?=ASSETS_PATH?>img/f-b.svg" alt="">
            </a>
            <a href="#" class="page-nav__lc">
                <img src="<?=ASSETS_PATH?>img/y-b.svg" alt="">
            </a>
            <a href="#" class="page-nav__location">
                <img src="<?=ASSETS_PATH?>img/i-b.svg" alt="">
            </a>
        </div>
    </div>
</div>
<div class="page-content">
<header class="header">
    <div class="container">
        <div class="header__inner">
            <div class="header__logo">
                <a href="/">
                    <svg width="69" height="23" viewBox="0 0 69 23" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path
                                d="M0.059633 9.22427L4.59174 14.2612V21.7863L6.02294 23V13.7757L0.059633 7.22164V9.22427ZM0.059633 12.8047L2.14679 15.1715V19.7836L3.57798 20.9974V14.7467L0 10.8628V12.8047H0.059633ZM0.059633 18.0844L1.19266 18.9947V15.7784L0.059633 14.4433V18.0844ZM9.42202 20.9974L10.8532 19.7836V15.1715L12.9404 12.8047V10.8021L9.36239 14.686V20.9974H9.42202ZM6.97706 23L8.40826 21.7863V14.2612L12.9404 9.22427V7.22164L6.97706 13.7757V23ZM11.867 18.9947L13 18.0844V14.4433L11.867 15.7784V18.9947ZM9.42202 0.182058L6.5 3.33773L3.63761 0.182058V2.24538L6.5 5.46174L9.42202 2.1847V0.182058ZM6.5 9.22427L13 2.00264V0L6.5 7.03958L0.059633 0V2.00264L6.5 9.22427ZM0.059633 5.58311L6.5 12.8047L13 5.58311V3.64116L6.5 10.6807L0.059633 3.64116V5.58311Z"
                                fill="#FBBA00" />
                        <path
                                d="M66.4265 10.717L68.7243 7.98113V6L60.9118 6L61.6471 8.26415H65.9669L63.3934 10.8113L65.7831 13.5472H61.739L60.8199 14.7736L58.7059 6L52.1801 6L52.7316 7.79245L50.9853 14.9623L49.3309 7.98113L50.7096 6H47.6765L45.2868 10.0566L42.8051 6H39.5882L40.875 8.07547L39.2206 14.7736L37.1066 6L30.489 6L31.1324 7.79245L29.6618 13.6415H28.8346L26.9963 6L20.4706 6L21.0221 7.79245L19 16H21.6654L22.2169 13.5472H25.0662L24.4228 11.6604H22.6765L23.5037 8.07547H24.7904L26.5368 16H27.364H29.4779H31.7757L33.614 8.07547H34.9007L36.6471 16H38.9449H39.4963L41.7022 16L42.7132 11.0943L45.011 14.8679L47.3088 11.0943L48.3199 16H50.7096H51.261H53.4669L54.0184 13.5472L56.7757 13.5472L56.2243 11.6604L54.386 11.6604L55.2132 8.07547H56.5919L58.2463 16H59.9007H61.1875H69V14.2075L66.4265 10.717Z"
                                fill="black" />
                    </svg>
                </a>
            </div>
            <ul class="header__menu">
                <li><a href="http://almaztd.ru/equipment/">Техника</a></li>
                <li><a href="http://almaztd.ru/spare_parts/">Запчасти</a></li>
                <li><a href="http://almaztd.ru/service/">Сервис</a></li>
                <li><a href="http://almaztd.ru/company/">Компания</a></li>
                <li><a href="http://almaztd.ru/purchase/">Покупка</a></li>
                <li><a href="http://almaztd.ru/press_center/">Пресс-центр</a></li>
                <li><a href="http://almaztd.ru/contacts/">Контакты</a></li>
            </ul>
            <div class="header__phone">
                <a href="tel:88007005008">8 800 700 50 08</a>
            </div>
            <div class="header__mobile">
                <div class="header__burger">
                    <svg width="15" height="10" viewBox="0 0 15 10" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd" clip-rule="evenodd"
                              d="M15 2L0 2L0 -0.00012207L15 -0.00012207V2ZM15 6.00012L0 6.00012L0 4L15 4V6.00012Z" fill="black" />
                        <path d="M0 10H15V8L0 8L0 10Z" fill="black" />
                    </svg>
                </div>
            </div>
        </div>
    </div>
</header>
<div class="skybox"></div>