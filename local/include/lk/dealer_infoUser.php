    <div class="lk__tabs-content-inner js-tabs-content-inner active">
        <ul class="list-dotted">
            <li class="list-dotted__row">
                <span class="list-dotted__name">Название организации:</span>
                <span class="list-dotted__line"></span>
                <span class="list-dotted__value"><?=$workUser["WORK_COMPANY"];?></span>
            </li>
            <li class="list-dotted__row">
                <span class="list-dotted__name">Адрес:</span>
                <span class="list-dotted__line"></span>
                <span class="list-dotted__value"><?=$workUser["WORK_STATE"];?>, <?=$workUser["WORK_CITY"];?>, <?=$workUser["WORK_STREET"];?></span>
            </li>
            <li class="list-dotted__row">
                <span class="list-dotted__name">Закрепленные за дилером регионы:</span>
                <span class="list-dotted__line"></span>
                <span class="list-dotted__value">Регион 1, Регион 2</span>
            </li>
            <li class="list-dotted__row">
                <span class="list-dotted__name">Контактное лицо:</span>
                <span class="list-dotted__line"></span>
                <span class="list-dotted__value"><?=$workUser["LAST_NAME"];?> <?=$workUser["NAME"];?> <?=$workUser["SECOND_NAME"];?></span>
            </li>
            <li class="list-dotted__row">
                <span class="list-dotted__name">ИНН:</span>
                <span class="list-dotted__line"></span>
                <span class="list-dotted__value"><?=$workUser["WORK_ZIP"];?></span>
            </li>
            <li class="list-dotted__row">
                <span class="list-dotted__name">Телефон:</span>
                <span class="list-dotted__line"></span>
                <span class="list-dotted__value">
                            <a href="tel:<?=preg_replace('/[^0-9]/', '', $workUser["PERSONAL_PHONE"]);?>"><?=$workUser["PERSONAL_PHONE"];?></a>
                          </span>
            </li>
            <li class="list-dotted__row">
                <span class="list-dotted__name">Электронная почта:</span>
                <span class="list-dotted__line"></span>
                <span class="list-dotted__value">
                            <a href="<?=$workUser["EMAIL"];?>"><?=$workUser["EMAIL"];?></a>
                          </span>
            </li>
        </ul>
        <a class="btn-border js-tabs-show-form" href="javascript:void(0)">Изменить данные</a>
    </div>
    <div class="lk__tabs-content-form js-tabs-form">
        <a class="btn-arrow js-tabs-back" href="javascript:void(0)">
            <i class="icon-back"></i>
            <span>Назад</span>
        </a>
        <form action="" method="POST">
            <div class="lk__form-section">
                <div class="lk__form-block lk__form-edit">
                    <div class="lk__form-title">
                        Редактирование личной информации
                    </div>
                    <div class="lk__form-row">
                        <div class="lk__form-field required">
                            <label class="field <?=$workUser["WORK_COMPANY"]?'focused':'';?>">
                                <span class="field__title field__required">Название организации</span>
                                <input class="field__input" type="text" name="name" value="<?=$workUser["WORK_COMPANY"];?>">
                            </label>
                        </div>
                        <div class="hint">
                            <div class="hint__message">
                                Lorem, ipsum dolor sit amet consectetur adipisicing elit. Modi, repellat necessitatibus quam
                                quia
                                libero expedita? Reprehenderit vitae minima illum excepturi quis obcaecati in laborum eius quos
                                veritatis quibusdam, aliquid natus.
                            </div>
                        </div>
                        <div class="tooltip">
                            Название не должно быть пустым
                        </div>
                    </div>
                    <div class="lk__form-row">
                        <div class="lk__form-field required">
                            <label class="field <?=$workUser["WORK_STATE"]?'focused':'';?>">
                                <span class="field__title field__required">Адрес</span>
                                <input class="field__input" type="text" name="address" value="<?=$workUser["WORK_STATE"];?>, <?=$workUser["WORK_CITY"];?>, <?=$workUser["WORK_STREET"];?>">
                            </label>
                        </div>
                        <div class="hint">
                            <div class="hint__message">
                                Lorem, ipsum dolor sit amet consectetur adipisicing elit. Modi, repellat necessitatibus quam
                                quia
                                libero expedita? Reprehenderit vitae minima illum excepturi quis obcaecati in laborum eius quos
                                veritatis quibusdam, aliquid natus.
                            </div>
                        </div>
                        <div class="tooltip">
                            Адрес не должн быть пустым
                        </div>
                    </div>
                    <div class="lk__form-row">
                        <div class="lk__form-field required">
                            <!-- Цитата из axshare: При введении нового покупателя должен быть выбор регионов с закреплением определенных регионов за конкретным дилером и выбор районов в регионе (т.е. нужна база районов). -->
                            <select class="select js-select-search" name="region[]">
                                <option disabled selected hidden>Выбор региона</option>
                                <option value="1">Москва</option>
                                <option value="2">Алтайский край</option>
                                <option value="3">Регион 3</option>
                                <option value="4">Тест регион 4</option>
                                <option value="5">Тест регион 5</option>
                            </select>
                        </div>
                        <div class="hint">
                            <div class="hint__message">
                                Lorem, ipsum dolor sit amet consectetur adipisicing elit. Modi, repellat necessitatibus quam
                                quia
                                libero expedita? Reprehenderit vitae minima illum excepturi quis obcaecati in laborum eius quos
                                veritatis quibusdam, aliquid natus.
                            </div>
                        </div>
                    </div>
                    <div class="lk__form-row">
                        <a class="lk__form-add js-lk-row-add" href="javascript:void(0)">Еще</a>
                    </div>
                    <div class="lk__form-row">
                        <div class="lk__form-field required">
                            <label class="field <?=$workUser["NAME"]?'focused':'';?>">
                                <span class="field__title field__required">Контактное лицо</span>
                                <input class="field__input" type="text" name="contact_person" value="<?=$workUser["LAST_NAME"];?> <?=$workUser["NAME"];?> <?=$workUser["SECOND_NAME"];?>">
                            </label>
                        </div>
                        <div class="hint">
                            <div class="hint__message">
                                Lorem, ipsum dolor sit amet consectetur adipisicing elit. Modi, repellat necessitatibus quam
                                quia
                                libero expedita? Reprehenderit vitae minima illum excepturi quis obcaecati in laborum eius quos
                                veritatis quibusdam, aliquid natus.
                            </div>
                        </div>
                        <div class="tooltip">
                            Поле не должно быть пустым
                        </div>
                    </div>
                    <div class="lk__form-row">
                        <div class="lk__form-field required">
                            <label class="field <?=$workUser["WORK_ZIP"]?'focused':'';?>">
                                <span class="field__title field__required">ИНН</span>
                                <input class="field__input" type="text" name="inn" value="<?=$workUser["WORK_ZIP"];?>">
                            </label>
                        </div>
                        <div class="hint">
                            <div class="hint__message">
                                Lorem, ipsum dolor sit amet consectetur adipisicing elit. Modi, repellat necessitatibus quam
                                quia
                                libero expedita? Reprehenderit vitae minima illum excepturi quis obcaecati in laborum eius quos
                                veritatis quibusdam, aliquid natus.
                            </div>
                        </div>
                        <div class="tooltip">
                            ИНН не должно быть пустым
                        </div>
                    </div>
                    <div class="lk__form-row">
                        <div class="lk__form-field required">
                            <label class="field <?=$workUser["PERSONAL_PHONE"]?'focused':'';?>">
                                <span class="field__title field__required">Телефон</span>
                                <input class="field__input js-mask-phone" type="text" name="phone" value="<?=$workUser["PERSONAL_PHONE"];?>">
                            </label>
                        </div>
                        <div class="hint">
                            <div class="hint__message">
                                Lorem, ipsum dolor sit amet consectetur adipisicing elit. Modi, repellat necessitatibus quam
                                quia
                                libero expedita? Reprehenderit vitae minima illum excepturi quis obcaecati in laborum eius quos
                                veritatis quibusdam, aliquid natus.
                            </div>
                        </div>
                        <div class="tooltip">
                            Телефон не должн быть пустым
                        </div>
                    </div>
                    <div class="lk__form-row">
                        <div class="lk__form-field required">
                            <label class="field <?=$workUser["EMAIL"]?'focused':'';?>">
                                <span class="field__title field__required">Электронная почта</span>
                                <input class="field__input" type="text" name="email" value="<?=$workUser["EMAIL"];?>">
                            </label>
                        </div>
                        <div class="hint">
                            <div class="hint__message">
                                Lorem, ipsum dolor sit amet consectetur adipisicing elit. Modi, repellat necessitatibus quam
                                quia
                                libero expedita? Reprehenderit vitae minima illum excepturi quis obcaecati in laborum eius quos
                                veritatis quibusdam, aliquid natus.
                            </div>
                        </div>
                        <div class="tooltip">
                            Электронная почта не должна быть пустой
                        </div>
                    </div>
                </div>
                <div class="lk__form-req-text">
                    <b>*</b> – поля, обязательные для заполнения
                </div>
                <button class="btn lk__form-submit" type="submit">Сохранить</button>
            </div>
        </form>
    </div>