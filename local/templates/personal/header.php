<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
require_once(dirname(__FILE__) . '/../../include/templates/header.php');
\Bitrix\Main\Loader::includeModule('letsrock.lib');
global $USER, $workUser;

$type_menu = [];
$idUrl = 0;

if (!$USER->IsAuthorized()) {
    LocalRedirect('/login/');
}else{
    $workUser = \Letsrock\Lib\Models\User::getWork();
    $arGroups = CUser::GetUserGroup($USER->GetID());
    $isGroup = array_intersect($arGroups, LK_IDS);
    $idUrl = $isGroup[key($isGroup)];
}
?>
<input type="hidden" name="urlPage" value="<?=$APPLICATION->GetCurPage();?>" />
    <section class="lk">
        <div class="container">
            <div class="lk__head">
                <h2 class="lk__title"><?=$APPLICATION->ShowTitle(false);?></h2>
                <h3 class="lk__subtitle"><?=$workUser['WORK_COMPANY']?$workUser['WORK_COMPANY']:$workUser['NAME'];?></h3>
            </div>
            <div class="lk__block">
                <div class="lk__sidebar">
                    <? $APPLICATION->IncludeComponent("bitrix:menu", "personal", [
                            "ROOT_MENU_TYPE" => LK_URLS[$idUrl],
                            "MAX_LEVEL" => "1",
                            "CHILD_MENU_TYPE" => LK_URLS[$idUrl],
                            "USE_EXT" => "Y",
                            "DELAY" => "N",
                            "ALLOW_MULTI_SELECT" => "Y",
                            "MENU_CACHE_TYPE" => "N",
                            "MENU_CACHE_TIME" => "3600",
                            "MENU_CACHE_USE_GROUPS" => "Y",
                            "MENU_CACHE_GET_VARS" => "",
                            "IS_TASK"=>$_SESSION["newTask"]
                        ]
                    ); ?>
                    <? $APPLICATION->IncludeComponent("letsrock:manager",
                        ".default",
                        []
                    ); ?>
                    <a href="<?= $APPLICATION->GetCurPageParam("logout=yes", [
                        "login",
                        "logout",
                        "register",
                        "forgot_password",
                        "change_password"
                    ]); ?>" class="lk__logout">Выход</a>
                </div>

                <div class="lk__content">
                    <div class="lk__file-link-block">
                        <a href="/documents/" class="lk__file-link">Документы</a>
                        <a href="/spravochnaya-informatsiya/" class="lk__file-link lk__file-link_info"> Справочная информация</a>
                    </div>