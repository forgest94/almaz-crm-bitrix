<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if (!empty($arResult)):?>
<ul class="lk__menu">
<?
foreach($arResult as $arItem):
	if($arParams["MAX_LEVEL"] == 1 && $arItem["DEPTH_LEVEL"] > 1) 
		continue;
    $select = '';
    if($APPLICATION->GetCurPage() == $arItem["LINK"]){
        $select = 'Y';
    }/*elseif($arItem["SELECTED"]){
        $select = 'Y';
    }*/
    $taskIsClass='';
    if($arParams["IS_TASK"] && ($arItem["LINK"] == '/manager/' || $arItem["LINK"] == '/administrator/')){
        $taskIsClass='warning';
    }
?>
	<?if($select):?>
		<li class="active <?=$taskIsClass;?>"><a href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a></li>
	<?else:?>
		<li class="<?=$taskIsClass;?>"><a href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a></li>
	<?endif?>
<?endforeach?>
</ul>
<?endif?>