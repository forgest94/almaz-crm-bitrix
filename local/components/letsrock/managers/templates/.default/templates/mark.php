<div class="lk__table-row-wrap js-table-detail <?=$item["error"]?'failed':'';?>">
    <div class="lk__table-row">
        <div class="lk__table-col">
            <div class="lk__table-col-head">
                Дата и время
            </div>
            <span>
                            <?=$item["date"];?>
                        </span>
        </div>
        <div class="lk__table-col">
            <div class="lk__table-col-head">
                Номер
            </div>
            <?=$item["number"];?>
        </div>
        <div class="lk__table-col">
            <div class="lk__table-col-head">
                Инициатор
            </div>
            <span>
                               Дилер <br>
                              <b><?=$item["dealer"]["WORK_COMPANY"];?></b>
                            </span>
        </div>
        <div class="lk__table-col">
            <div class="lk__table-col-head">
                Задача
            </div>
            <?=$item["name"];?>
        </div>
        <div class="lk__table-col">
            <div class="lk__table-col-head">
                Срок выполнения
            </div>
            <?=$item["due_date"];?>
        </div>
        <div class="lk__table-col">
            <a class="lk__link js-table-detail-link" href="javascript:void(0)" data-show-text="Свернуть">
                Подробнее
            </a>
        </div>
    </div>
    <div class="lk__table-row-data js-table-detail-data">
        <div class="lk__data">
            <?if(!empty($item["phone"])):?>
                <div class="lk__data-row">
                    <div class="lk__data-label">
                        Контактный номер телефона
                    </div>
                    <div class="lk__data-value">
                        <div class="lk__data-box">
                            <?=$item["phone"]?>
                        </div>
                    </div>
                </div>
            <?endif;?>
            <?if(!empty($item["files"])):?>
                <?foreach ($item["files"] as $file):?>
                    <?if(!empty($file["info"][0])):?>
                        <div class="lk__data-separate"></div>
                    <?endif;?>
                    <div class="lk__data-row">
                        <div class="lk__data-label">
                            <?=$file["name"];?>
                        </div>
                        <div class="lk__data-value">
                            <div class="lk__data-files">
                                <?if(!empty($file["info"][0])):?>
                                    <?foreach ($file["info"] as $infoFile):?>
                                        <a class="link-file" href="<?=$infoFile["SRC"];?>" download><?=$infoFile["ORIGINAL_NAME"];?></a>
                                    <?endforeach;?>
                                <?else:?>
                                    <a class="link-file" href="<?=$file["info"]["SRC"];?>" download><?=$file["info"]["ORIGINAL_NAME"];?></a>
                                <?endif;?>
                            </div>
                        </div>
                    </div>
                <?endforeach;?>
            <?endif;?>
            <div class="lk__data-row">
                <div class="lk__data-label">
                </div>
                <div class="lk__data-value">
                    <div class="btn-group">
                        <a class="btn btn-black" href="javascript:void(0);"
                           data-editTask="<?=$item["id"];?>"
                           data-dealer="<?=$item["dealer"]["ID"];?>"
                           data-status="<?=$item["status_arr"]["reject_id"];?>">Отклонить</a>
                        <a class="btn" href="javascript:void(0);"
                           data-editTask="<?=$item["id"];?>"
                           data-dealer="<?=$item["dealer"]["ID"];?>"
                           data-status="<?=$item["status_arr"]["confirm_id"];?>">Подтвердить</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>