<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
$pathComponent = $_SERVER["DOCUMENT_ROOT"].$this->GetFolder();
if(!empty($arParams["DETAIL_ID"])){
    include($pathComponent.'/element.php');
}else{
    include($pathComponent.'/list_elems.php');
}
?>