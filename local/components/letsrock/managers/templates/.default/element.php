<div class="lk__content-box">
    <section class="lk__tabs-section">
        <div class="lk__tabs-header">
            <div class="lk__tabs-title"><?=$arResult["manager"]["LAST_NAME"];?> <?=$arResult["manager"]["NAME"];?> <?=$arResult["manager"]["SECOND_NAME"];?></div>
            <a class="lk__tabs-link js-modal-open" data-modal-type="manager-data" href="javascript:void(0)">Данные менеджера</a>
            <div class="popup popup__dealer-data js-modal" data-modal-type="manager-data">
                <div class="popup__inner">
                    <div class="close popup__close js-modal-close"></div>
                    <div class="popup__content">
                        <ul class="list-dotted js-modal-copy-area">
                            <li class="list-dotted__row">
                                <span class="list-dotted__name">ФИО:</span>
                                <span class="list-dotted__line"></span>
                                <span class="list-dotted__value"><?=$arResult["manager"]["LAST_NAME"];?> <?=$arResult["manager"]["NAME"];?> <?=$arResult["manager"]["SECOND_NAME"];?></span>
                            </li>
                            <li class="list-dotted__row">
                                <span class="list-dotted__name">Телефон:</span>
                                <span class="list-dotted__line"></span>
                                <span class="list-dotted__value">
                                   <a href="tel:<?=$arResult["manager"]["PERSONAL_PHONE"];?>"><?=$arResult["manager"]["PERSONAL_PHONE"];?></a>
                                </span>
                            </li>
                            <li class="list-dotted__row">
                                <span class="list-dotted__name">Адрес электронной почты:</span>
                                <span class="list-dotted__line"></span>
                                <span class="list-dotted__value"><a href="<?=$arResult["manager"]["EMAIL"];?>"><?=$arResult["manager"]["EMAIL"];?></a></span>
                            </li>
                        </ul>
                        <div class="popup__btn-wrap popup__btn-wrap-m-t">
                            <a class="btn popup__btn js-modal-close" href="javascript:void(0)">Закрыть</a>
                            <a class="btn btn-black popup__btn js-modal-copy" data-copy-message="Скопировано" href="javascript:void(0)">Скопировать</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <nav class="lk__tabs-nav js-tabs-nav">
            <a class="lk__tabs-nav-link active" data-tab='tasks' href="javascript:void(0)">
                <?if($arResult["checkTask"] > 0):?>
                    <i class="icon-tasks"></i>
                <?endif;?>
                <span>Задачи менеджера</span>
            </a>
            <a class="lk__tabs-nav-link" data-tab='dealers' href="javascript:void(0)">
                <i class="icon-buyers"></i>
                <span>Дилеры</span>
            </a>
        </nav>
        <section class="lk__tabs-content js-tabs-content">
            <article class="lk__tabs-content-item active" data-tab-content='tasks'>
                <div class="lk__table tasks">
                    <div class="lk__table-head">
                        <div class="lk__table-row">
                            <div class="lk__table-col">
                                Дата подачи
                            </div>
                            <div class="lk__table-col">
                                Номер
                            </div>
                            <div class="lk__table-col">
                                Инициатор
                            </div>
                            <div class="lk__table-col">
                                Задача
                            </div>
                            <div class="lk__table-col">
                                Срок выполнения
                            </div>
                            <div class="lk__table-col">
                            </div>
                        </div>
                    </div>
                    <div class="lk__table-body">
                        <?if(!empty($arResult["tasks"]["arTasks"])):?>
                            <?
                            foreach ($arResult["tasks"]["arTasks"] as $item){
                                $vid = $pathComponent.'/templates/'.$item["vid"].'.php';
                                include($vid);
                            }
                            ?>
                        <?endif;?>
                    </div>
                </div>
                <?$APPLICATION->IncludeComponent("letsrock:paginations",
                    ".default",
                    [
                        "CACHE_TIME" => 0,
                        "ELEMS_PAGE"=>$arResult["tasks"]["pagination"],
                        "GET_PAGE"=>"page_task",
                        "AJAX_BLOCK"=>"",
                        "PAGEN_HASH"=>""
                    ]
                );?>
            </article>
            <article class="lk__tabs-content-item" data-tab-content='dealers'>
                <div class="lk__table manager-dealer">
                    <div class="lk__table-head">
                        <div class="lk__table-row">
                            <div class="lk__table-col">
                                Название
                            </div>
                            <div class="lk__table-col">
                                Регион
                            </div>
                            <div class="lk__table-col">
                                ИНН
                            </div>
                            <div class="lk__table-col">
                            </div>
                        </div>
                    </div>
                    <div class="lk__table-body">
                        <?if(!empty($arResult["arDealers"])):?>
                            <?foreach ($arResult["arDealers"] as $dealer):?>
                                <div class="lk__table-row-wrap js-table-detail">
                                    <div class="lk__table-row">
                                        <div class="lk__table-col">
                                            <div class="lk__table-col-head">
                                                Название
                                            </div>
                                            <?=$dealer["WORK_COMPANY"];?>
                                        </div>
                                        <div class="lk__table-col">
                                            <div class="lk__table-col-head">
                                                Регион
                                            </div>
                                            <?=$dealer["regions_text"];?>
                                        </div>
                                        <div class="lk__table-col">
                                            <div class="lk__table-col-head">
                                                ИНН
                                            </div>
                                            <?=$dealer["WORK_ZIP"];?>
                                        </div>
                                        <div class="lk__table-col">
                                            <a class="lk__link js-table-detail-link" href="javascript:void(0)" data-show-text="Свернуть">
                                                Подробнее
                                            </a>
                                        </div>
                                    </div>
                                    <div class="lk__table-row-data js-table-detail-data">
                                        <div class="lk__data">
                                            <div class="lk__data-row">
                                                <div class="lk__data-label">
                                                    Информация о дилере
                                                </div>
                                                <div class="lk__data-value">
                                                    <ul class="list-dotted">
                                                        <li class="list-dotted__row">
                                                            <span class="list-dotted__name">Название организации:</span>
                                                            <span class="list-dotted__line"></span>
                                                            <span class="list-dotted__value"><?=$dealer["WORK_COMPANY"];?></span>
                                                        </li>
                                                        <li class="list-dotted__row">
                                                            <span class="list-dotted__name">Адрес:</span>
                                                            <span class="list-dotted__line"></span>
                                                            <span class="list-dotted__value"><?=$dealer["WORK_STATE"];?>, <?=$dealer["WORK_CITY"];?>, <?=$dealer["WORK_STREET"];?></span>
                                                        </li>
                                                        <li class="list-dotted__row">
                                                            <span class="list-dotted__name">Закрепленные за дилером регионы:</span>
                                                            <span class="list-dotted__line"></span>
                                                            <span class="list-dotted__value"><?=$dealer["regions_text"];?></span>
                                                        </li>
                                                        <li class="list-dotted__row">
                                                            <span class="list-dotted__name">Контактное лицо:</span>
                                                            <span class="list-dotted__line"></span>
                                                            <span class="list-dotted__value">
                                                                <?=$dealer["LAST_NAME"];?> <?=$dealer["NAME"];?> <?=$dealer["SECOND_NAME"];?>
                                                            </span>
                                                        </li>
                                                        <li class="list-dotted__row">
                                                            <span class="list-dotted__name">ИНН:</span>
                                                            <span class="list-dotted__line"></span>
                                                            <span class="list-dotted__value"><?=$dealer["WORK_ZIP"];?></span>
                                                        </li>
                                                        <li class="list-dotted__row">
                                                            <span class="list-dotted__name">Телефон:</span>
                                                            <span class="list-dotted__line"></span>
                                                            <span class="list-dotted__value">
                                                <a href="tel:<?=$dealer["PERSONAL_PHONE"];?>"><?=$dealer["PERSONAL_PHONE"];?></a>
                                              </span>
                                                        </li>
                                                        <li class="list-dotted__row">
                                                            <span class="list-dotted__name">Электронная почта:</span>
                                                            <span class="list-dotted__line"></span>
                                                            <span class="list-dotted__value">
                                                <a href="mailto:<?=$dealer["EMAIL"];?>"><?=$dealer["EMAIL"];?></a>
                                              </span>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?endforeach;?>
                        <?endif;?>
                    </div>
                </div>
            </article>
        </section>
    </section>
</div>