<div class="lk__content-box">
    <label class="field field-search">
        <input class="field__input" type="text" name="search_manager" data-search="<?=$APPLICATION->GetCurPage();?>?search_manager=" data-has="manager" placeholder="Поиск по менеджеру" value="<?=$_REQUEST["search_manager"];?>">
    </label>
    <div class="lk__table manager-list">
        <div class="lk__table-head">
            <div class="lk__table-row">
                <a class="lk__table-col"href="?sort_name=<?=$_REQUEST["sort_name"]=='asc'?'desc':'asc';?>">
                    ФИО
                    <?if(empty($_REQUEST["sort_position"]) || !empty($_REQUEST["sort_name"])):?>
                        <i class = "icon icon-sort-arrow js-lk-sort <?=$_REQUEST["sort_name"]=='asc'?'active':'';?>"> </i>
                    <?endif;?>
                </a>
                <a class="lk__table-col" href="?sort_position=<?=$_REQUEST["sort_position"]=='asc'?'desc':'asc';?>">
                    Должность
                    <?if(!empty($_REQUEST["sort_position"])):?>
                        <i class = "icon icon-sort-arrow js-lk-sort <?=$_REQUEST["sort_position"]=='asc'?'active':'';?>"> </i>
                    <?endif;?>
                </a>
                <div class="lk__table-col">
                </div>
            </div>
        </div>
        <div class="lk__table-body" data-blockajax="manager">
            <?if(!empty($arResult["persons"])):?>
                <!--startAjaxPagemanager-->
                <?foreach ($arResult["persons"] as $id => $person):?>
                    <div class="lk__table-row-wrap">
                        <div class="lk__table-row">
                            <div class="lk__table-col">
                                <div class="lk__table-col-head">
                                    ФИО
                                </div>
                                <?=$person["fio"];?>
                            </div>
                            <div class="lk__table-col">
                                <div class="lk__table-col-head">
                                    Должность
                                </div>
                                <?=$person["position"];?>
                            </div>
                            <div class="lk__table-col">
                                <a class="lk__link" href="?person=<?=$person["id"];?>">
                                    Подробнее
                                </a>
                            </div>
                        </div>
                    </div>
                <?endforeach;?>
                <!--endAjaxPagemanager-->
                <?$APPLICATION->IncludeComponent("letsrock:paginations",
                    ".default",
                    [
                        "CACHE_TIME" => 0,
                        "ELEMS_PAGE"=>$arResult["pagination"],
                        "GET_PAGE"=>"page_manager",
                        "AJAX_BLOCK"=>"manager",
                        "PAGEN_HASH"=>"manager"
                    ]
                );?>
            <?endif;?>
        </div>
    </div>
</div>