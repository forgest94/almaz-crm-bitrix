<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}

use Letsrock\Lib\Models\User;
use Letsrock\Lib\Models\Element;
use Letsrock\Lib\Models\HLBlock;

\Bitrix\Main\Loader::includeModule('letsrock.lib');

class ManagersComponent extends CBitrixComponent
{
    public function onPrepareComponentParams($arParams)
    {
        $result = $arParams;

        return $result;
    }

    public function getUser($id){
        if($id){
            return CUser::GetByID($id)->Fetch();
        }
    }

    public function isTasks($id_manager, $type_vid, $request=false)
    {
        global $USER;
        $isTask = 0;
        $arTasks = [];
        $id_dealer = [];
        $visibleTask = [IB_EXPLOITATION, IB_SERVCOMP, IB_RECLAMATION_PARTS, IB_RECLAMATION_TECH, IB_MARKETING];

        $usersForManger = CUser::GetList(($by="ID"), ($order="ASC"),
            [
                "ID"=>$id_manager
            ], array("SELECT"=>array("UF_*")));
        while ($arUser = $usersForManger->Fetch()) {
            if ($arUser["UF_ADMIN_NEW"]) {
                $usersForManger2 = CUser::GetList(($by = "ID"), ($order = "ASC"), ["UF_MANAGER_NEW" => $arUser["ID"]], array("SELECT" => array("UF_*")));
                while ($arUser2 = $usersForManger2->Fetch()) {
                    $id_dealer[] = $arUser2["ID"];
                }
            }
        }

        if(!empty($visibleTask)){
            $elemIbl = new Element;
            /**
             * Ввод в эксплуатацию
             */
            if(in_array(IB_EXPLOITATION, $visibleTask)) {
                $arExplotations_db = $elemIbl->GetList(IB_EXPLOITATION,
                    ["NAME", "ID", "IBLOCK_ID", "DATE_CREATE", "PROPERTY_*"],
                    [
                        "!PROPERTY_DEALER" => false,
                        "PROPERTY_DEALER" => $id_dealer,
                        "!NAME" => "Техника не введена в эксплуатацию"
                    ],
                    ["created_date" => "desc"]
                );
                $typeTechHl = new HLBlock(HL_EQUIPMENT);
                foreach ($arExplotations_db["ELEMS"] as $item){
                    $arProps = $arExplotations_db["PROPS"][$item["ID"]];
                    $status = $arProps["STATUS"]["VALUE"];
                    $isNew = '';
                    if($status == 'На рассмотрении'){
                        $isTask++;
                        $isNew = 'Y';
                    }
                    if($type_vid == "detail"){
                        $infoBuyer = $elemIbl->GetList(IB_BUYERS, ["NAME", "ID", "IBLOCK_ID", "PROPERTY_*"], ["ID" => $arProps["BUYER"]["VALUE"], "!PROPERTY_TYPE_VALUE" => false]);
                        $fildsBuyer = $infoBuyer["ELEMS"][0];
                        $porpsBuyer = $infoBuyer["PROPS"][$fildsBuyer["ID"]];
                        $infoModel = $elemIbl->getById(IB_DIRECTORY, $arProps["MODEL"]["VALUE"]);
                        $infoType = $typeTechHl->get(["UF_XML_ID" => $arProps["EQUIPMENT"]["VALUE"]])[0];
                        $infoRegionBuyer = $elemIbl->getById(IB_REGION, $porpsBuyer["REGION"]["VALUE"]);
                        $infoDistrictBuyer = $elemIbl->getById(IB_AREA, $porpsBuyer["DISTRICT"]["VALUE"]);
                        $akt_parse = [];
                        if(!empty($arProps["FILE"]["VALUE"])){
                            $akt = CFile::GetFileArray($arProps["FILE"]["VALUE"]);
                            if($akt)
                                $akt_parse = ["name"=>$akt["ORIGINAL_NAME"], "src"=>$akt["SRC"]];
                        }
                        $arPhoto = [];
                        if(!empty($arProps["PHOTO_TECH"]["VALUE"])){
                            foreach ($arProps["PHOTO_TECH"]["VALUE"] as $itemP){
                                $photo = CFile::GetFileArray($itemP);
                                if($photo)
                                    $arPhoto[] = ["name"=>$photo["ORIGINAL_NAME"], "src"=>$photo["SRC"]];
                            }
                        }
                        if(!empty($arProps["DATE_EXECUTION"]["VALUE"])){
                            $due_date = $arProps["DATE_EXECUTION"]["VALUE"];
                        }else{
                            $deteProp = explode(' ', $item["DATE_CREATE"])[0];
                            $due_date = date('d.m.Y' ,strtotime($deteProp.DAY_EXECUTION));
                        }
                        $error = '';
                        if((strtotime(date("d.m.Y"))>strtotime($due_date)) && !empty($isNew)){
                            $error = 'Y';
                        }
                        $arTasks[] = [
                            "id"=> $item["ID"],
                            "vid"=>"expl",
                            "error"=>$error,
                            "type"=>$infoType["UF_NAME"],
                            "model"=>$infoModel["NAME"],
                            "date"=>$item["DATE_CREATE"],
                            "date_sort" => strtotime($item["DATE_CREATE"]),
                            "date_expl" => $arProps["DATE"]["VALUE"],
                            "status"=>$status,
                            "status_arr"=>["reject_id"=>14, "confirm_id"=>15],
                            "akt"=>$akt_parse,
                            "photo"=>$arPhoto,
                            "phone" => $arProps["PHONE_PERSONAL"]["VALUE"],
                            "name"=>$item["NAME"],
                            "due_date"=>$due_date,
                            "new"=>$isNew,
                            "status_id"=>$arProps["STATUS_ID"]["VALUE"],
                            "number"=>$arProps["NUMBER"]["VALUE"],
                            "dealer"=>$this::getUser($arProps["DEALER"]["VALUE"]),
                            "buyer"=>[
                                "name"=>$item["NAME"],
                                "contact_people"=>$porpsBuyer["CONTACT_PEOPLE"]["VALUE"],
                                "phone"=>$porpsBuyer["PHONE"]["VALUE"],
                                "inn"=>(!empty($porpsBuyer["INN"]["VALUE"]) && $porpsBuyer["INN"]["VALUE"] != 0)?$porpsBuyer["INN"]["VALUE"]:'',
                                "bin"=>(!empty($porpsBuyer["BIN"]["VALUE"]) && $porpsBuyer["BIN"]["VALUE"] != 0)?$porpsBuyer["BIN"]["VALUE"]:'',
                                "position"=>$porpsBuyer["POSITION"]["VALUE"],
                                "region"=>$infoRegionBuyer["NAME"],
                                "district"=>$infoDistrictBuyer["NAME"],
                                "address"=>$porpsBuyer["ADDRESS"]["VALUE"],
                                "mail"=>$porpsBuyer["EMAIL"]["VALUE"],
                                "arProps"=>$porpsBuyer
                            ]
                        ];
                    }
                }
            }
            /**
             * Рекламации "Техника"
             */
            if(in_array(IB_RECLAMATION_TECH, $visibleTask)) {
                $arReclamationTech_db = $elemIbl->GetList(IB_RECLAMATION_TECH,
                    ["NAME", "ID", "IBLOCK_ID", "DATE_CREATE", "PROPERTY_*"],
                    [
                        "PROPERTY_DEALER" => $id_dealer,
                    ],
                    ["created_date" => "desc"]
                );
                foreach ($arReclamationTech_db["ELEMS"] as $tech){
                    $arProps = $arReclamationTech_db["PROPS"][$tech["ID"]];
                    $status = $arProps["STATUS"]["VALUE"];
                    $isNew = '';
                    if($status == 'Отправлено' || $status == 'В работе'){
                        $isTask++;
                        $isNew = 'Y';
                    }

                    if($type_vid == "detail"){
                        $arBuyerInfo = [];
                        if ($arProps["SERIAL_NUMBER"]["VALUE"]) {
                            $infoExpl = $elemIbl->GetList(IB_EXPLOITATION, ["NAME", "ID", "IBLOCK_ID", "DATE_CREATE", "PROPERTY_*"], ["PROPERTY_DEALER" => $id_dealer, "ID" => $arProps["SERIAL_NUMBER"]], array("created_date" => "desc"));
                            $arPropsEx = $infoExpl["PROPS"][$infoExpl["ELEMS"][0]["ID"]];
                            if (!$arProps["NOT_OPERATION"]["VALUE"]) {
                                $infoBuyer = $elemIbl->GetList(IB_BUYERS, ["NAME", "ID", "IBLOCK_ID", "PROPERTY_*"], ["ID" => $arPropsEx["BUYER"]["VALUE"], "!PROPERTY_TYPE_VALUE" => false]);
                                $fildsBuyer = $infoBuyer["ELEMS"][0];
                                $porpsBuyer = $infoBuyer["PROPS"][$fildsBuyer["ID"]];
                                $infoRegionBuyer = $elemIbl->getById(IB_REGION, $porpsBuyer["REGION"]["VALUE"]);
                                $infoDistrictBuyer = $elemIbl->getById(IB_AREA, $porpsBuyer["DISTRICT"]["VALUE"]);
                                $arBuyerInfo = [
                                    "name" => $infoExpl["ELEMS"][0]["NAME"],
                                    "contact_people" => $porpsBuyer["CONTACT_PEOPLE"]["VALUE"],
                                    "phone" => $porpsBuyer["PHONE"]["VALUE"],
                                    "inn"=>(!empty($porpsBuyer["INN"]["VALUE"]) && $porpsBuyer["INN"]["VALUE"] != 0)?$porpsBuyer["INN"]["VALUE"]:'',
                                    "bin"=>(!empty($porpsBuyer["BIN"]["VALUE"]) && $porpsBuyer["BIN"]["VALUE"] != 0)?$porpsBuyer["BIN"]["VALUE"]:'',
                                    "position" => $porpsBuyer["POSITION"]["VALUE"],
                                    "region" => $infoRegionBuyer["NAME"],
                                    "district" => $infoDistrictBuyer["NAME"],
                                    "address" => $porpsBuyer["ADDRESS"]["VALUE"],
                                    "mail" => $porpsBuyer["EMAIL"]["VALUE"],
                                    "arProps" => $porpsBuyer
                                ];
                            }
                        } else {
                            continue;
                        }


                        /*$akt_parse = [];
                    if (!empty($arProps["AKT"]["VALUE"])) {
                        $akt = CFile::GetFileArray($arProps["AKT"]["VALUE"]);
                        if ($akt)
                            $akt_parse = ["name" => $akt["ORIGINAL_NAME"], "src" => $akt["SRC"]];
                    }*/
                        $akt_parse = [];
                        if (!empty($arProps["AKT"]["VALUE"])) {
                            foreach ($arProps["AKT"]["VALUE"] as $akt2) {
                                $arFile = CFile::GetFileArray($akt2);
                                if ($arFile)
                                    $akt_parse[] = ["name" => $arFile["ORIGINAL_NAME"], "src" => $arFile["SRC"]];
                            }
                        }
                        $arPritenzy = [];
                        if (!empty($arProps["PRITENZY"]["VALUE"])) {
                            foreach ($arProps["PRITENZY"]["VALUE"] as $photo) {
                                $arFile = CFile::GetFileArray($photo);
                                if ($arFile)
                                    $arPritenzy[] = ["name" => $arFile["ORIGINAL_NAME"], "src" => $arFile["SRC"]];
                            }
                        }
                        $arPhoto = [];
                        if (!empty($arProps["PHOTO"]["VALUE"])) {
                            foreach ($arProps["PHOTO"]["VALUE"] as $photo) {
                                $pare_phot = CFile::GetFileArray($photo);
                                if ($pare_phot)
                                    $arPhoto[] = ["name" => $pare_phot["ORIGINAL_NAME"], "src" => $pare_phot["SRC"]];
                            }
                        }

                        if(!empty($arProps["DATE_EXECUTION"]["VALUE"])){
                            $due_date = $arProps["DATE_EXECUTION"]["VALUE"];
                        }else{
                            $deteProp = explode(' ', $tech["DATE_CREATE"])[0];
                            $due_date = date('d.m.Y' ,strtotime($deteProp.DAY_EXECUTION));
                        }
                        $error = '';
                        if((strtotime(date("d.m.Y"))>strtotime($due_date)) && !empty($isNew)){
                            $error = 'Y';
                        }
                        if(strpos('---- '.$tech["NAME"], 'RT') === false){
                            $numberTechRecl = $tech["NAME"];
                        }else{
                            $numberTechRecl = 'RT'.$tech["ID"];
                        }
                        $arTasks[] = [
                            "id" => $tech["ID"],
                            "vid" => "tech",
                            "error" => $error,
                            "date" => $tech["DATE_CREATE"],
                            "date_sort" => strtotime($tech["DATE_CREATE"]),
                            "status" => $status,
                            "status_id"=>$arProps["STATUS_ID"]["VALUE"],
                            "status_arr" => ["reject_id" => 20, "confirm_id" => 19],
                            "akt" => $akt_parse,
                            "pritenzy" => $arPritenzy,
                            "photo" => $arPhoto,
                            "phone" => $arProps["PHONE_PERSONAL"]["VALUE"],
                            "new" => $isNew,
                            "name" => "Рекламации - техника",
                            "reason" => $arProps["REASON"]["VALUE"]["TEXT"],
                            "wishes" => $arProps["WISHES"]["VALUE"]["TEXT"],
                            "due_date" => $due_date,
                            "number" => $numberTechRecl,
                            "dealer" => $this::getUser($arProps["DEALER"]["VALUE"]),
                            "buyer" => $arBuyerInfo
                        ];
                    }
                }
            }
            /**
             * Рекламации "Запчасти"
             */
            if(in_array(IB_RECLAMATION_PARTS, $visibleTask)) {
                $arReclamationParts_db = $elemIbl->GetList(IB_RECLAMATION_PARTS,
                    ["NAME", "ID", "IBLOCK_ID", "DATE_CREATE", "PROPERTY_*"],
                    [
                        "PROPERTY_DEALER" => $id_dealer,
                    ],
                    ["created_date" => "desc"]
                );
                foreach ($arReclamationParts_db["ELEMS"] as $parts){
                    $arProps = $arReclamationParts_db["PROPS"][$parts["ID"]];
                    $status = $arProps["STATUS"]["VALUE"];
                    $isNew = '';
                    if($status == 'Отправлено' || $status == 'В работе'){
                        $isTask++;
                        $isNew = 'Y';
                    }

                    if($type_vid == "detail"){
                        $akt_parse = [];
                        if (!empty($arProps["AKT"]["VALUE"])) {
                            $akt = CFile::GetFileArray($arProps["AKT"]["VALUE"]);
                            if ($akt)
                                $akt_parse = ["name" => $akt["ORIGINAL_NAME"], "src" => $akt["SRC"]];
                        }
                        $arPhoto = [];
                        if (!empty($arProps["PHOTO"]["VALUE"])) {
                            foreach ($arProps["PHOTO"]["VALUE"] as $photo) {
                                $pare_phot = CFile::GetFileArray($photo);
                                if ($pare_phot)
                                    $arPhoto[] = ["name" => $pare_phot["ORIGINAL_NAME"], "src" => $pare_phot["SRC"]];
                            }
                        }

                        if(!empty($arProps["DATE_EXECUTION"]["VALUE"])){
                            $due_date = $arProps["DATE_EXECUTION"]["VALUE"];
                        }else{
                            $deteProp = explode(' ', $parts["DATE_CREATE"])[0];
                            $due_date = date('d.m.Y' ,strtotime($deteProp.DAY_EXECUTION));
                        }
                        $error = '';
                        if((strtotime(date("d.m.Y"))>strtotime($due_date)) && !empty($isNew)){
                            $error = 'Y';
                        }
                        if(strpos('---- '.$parts["NAME"], 'RP') === false){
                            $numberParts = $parts["NAME"];
                        }else{
                            $numberParts = 'RP'.$parts["ID"];
                        }
                        $arTasks[] = [
                            "id" => $parts["ID"],
                            "vid" => "parts",
                            "error" => $error,
                            "date" => $parts["DATE_CREATE"],
                            "date_sort" => strtotime($parts["DATE_CREATE"]),
                            "status" => $status,
                            "status_id"=>$arProps["STATUS_ID"]["VALUE"],
                            "status_arr" => ["reject_id" => 24, "confirm_id" => 23],
                            "akt" => $akt_parse,
                            "photo" => $arPhoto,
                            "phone" => $arProps["PHONE_PERSONAL"]["VALUE"],
                            "new" => $isNew,
                            "name" => "Рекламации - запчасти",
                            "invoice" => $arProps["INVOICE"]["VALUE"],
                            "consignment" => $arProps["CONSIGNMENT"]["VALUE"],
                            "reason" => $arProps["REASON"]["VALUE"]["TEXT"],
                            "wishes" => $arProps["WISHES"]["VALUE"]["TEXT"],
                            "due_date" => $due_date,
                            "number" => $numberParts,
                            "dealer" => $this::getUser($arProps["DEALER"]["VALUE"]),
                        ];
                    }
                }
            }
            /**
             * Сервисные компенсации
             */
            if(in_array(IB_SERVCOMP, $visibleTask)) {
                $typeTechHl = new HLBlock(HL_EQUIPMENT);
                $infoServices = $elemIbl->GetList(IB_SERVCOMP,
                    ["ID", "IBLOCK_ID", "NAME", "DATE_CREATE", "CODE", "DATE_ACTIVE_FROM", "DATE_CREATE", "PROPERTY_*"],
                    [
                        "ACTIVE_DATE" => "Y",
                        "ACTIVE" => "Y",
                        "PROPERTY_DILER" => $id_dealer,
                    ],
                    ["ID" => "desc"]
                );
                foreach ($infoServices["ELEMS"] as $service){
                    $arProps = $infoServices["PROPS"][$service["ID"]];
                    $status = $arProps["STATUS"]["~VALUE"];
                    $isNew = '';
                    if($status == 'Отправлено' || $status == 'В работе'){
                        $isTask++;
                        $isNew = 'Y';
                    }

                    if($type_vid == "detail"){
                        $type = $arProps["TYPE_SERVICE"]["~VALUE"];
                        unset($arProps["TYPE_SERVICE"]);
                        unset($arProps["STATUS"]);
                        $arFiles = [];
                        foreach ($arProps as $arProp) {
                            if ($arProp["PROPERTY_TYPE"] == 'F') {
                                if(empty($arProp["VALUE"]))continue;
                                $src = [];
                                if (is_array($arProp["VALUE"])) {
                                    foreach ($arProp["VALUE"] as $id) {
                                        $fileFind = CFile::GetFileArray($id);
                                        if(!empty($fileFind))
                                            $src[] = $fileFind;
                                    }
                                } else {
                                    $fileFind2 = CFile::GetFileArray($arProp["VALUE"]);
                                    if(!empty($fileFind2))
                                        $src = $fileFind2;
                                }
                                if(!empty($src))
                                    $arFiles[] = ["info" => $src, "name" => $arProp["NAME"]];
                            }
                        }
                        $arTech = [];
                        if(!empty($arProps["SERIAL_NUMBER"]["VALUE"])){
                            foreach ($arProps["SERIAL_NUMBER"]["VALUE"] as $serialElem) {
                                if(!empty((int)$serialElem)) {
                                    $infoTech = $elemIbl->GetList(IB_EXPLOITATION, ["NAME", "ID", "PROPERTY_MODEL", "PROPERTY_EQUIPMENT", "PROPERTY_NUMBER"], ["ID" => (int)$serialElem, "!EQUIPMENT_VALUE" => false])["ELEMS"][0];
                                    if ($infoTech) {
                                        $infoModel = $elemIbl->getById(IB_DIRECTORY, $infoTech["PROPERTY_MODEL_VALUE"]);
                                        $infoType = $typeTechHl->get(["UF_XML_ID" => $infoTech["PROPERTY_EQUIPMENT_VALUE"]])[0];
                                        $numberTech = $infoTech["PROPERTY_NUMBER_VALUE"];
                                        $arTech[$infoTech["ID"]] = ["number" => $numberTech, "model" => $infoModel["NAME"], "type" => $infoType["UF_NAME"]];
                                    }
                                }
                            }
                        }
                        $arReclamationTech = [];
                        if(!empty($arProps["RECLAMATION_TECH"]["VALUE"])){
                            foreach ($arProps["RECLAMATION_TECH"]["VALUE"] as $reclElem) {
                                if(!empty((int)$reclElem)){
                                    $infoRecl = $elemIbl->GetList(IB_RECLAMATION_TECH, ["NAME", "ID", "IBLOCK_ID", "PROPERTY_*"], ["ID" => $reclElem]);
                                    $elem = $infoRecl["ELEMS"][0];
                                    $propsRecl = $infoRecl["PROPS"][$elem["ID"]];
                                    $infoTechRecl = [];
                                    if(!empty($propsRecl["SERIAL_NUMBER"]["VALUE"])){
                                        if(is_array($propsRecl["SERIAL_NUMBER"]["VALUE"])){
                                            foreach ($propsRecl["SERIAL_NUMBER"]["VALUE"] as $itemIdsTech){
                                                $infoTechRecl = $arTech[$itemIdsTech];
                                            }
                                        }else{
                                            $infoTechRecl = $arTech[$propsRecl["SERIAL_NUMBER"]["VALUE"]];
                                        }
                                    }
                                    $akt_parse = [];
                                    if(!empty($propsRecl["AKT"]["VALUE"])){
                                        foreach ($propsRecl["AKT"]["VALUE"] as $akt2) {
                                            $arFile = CFile::GetFileArray($akt2);
                                            if($arFile)
                                                $akt_parse[] = ["name"=>$arFile["ORIGINAL_NAME"], "src"=>$arFile["SRC"]];
                                        }
                                    }
                                    $arPhoto = [];
                                    if(!empty($propsRecl["PHOTO"]["VALUE"])){
                                        foreach ($propsRecl["PHOTO"]["VALUE"] as $photo) {
                                            $arFile = CFile::GetFileArray($photo);
                                            if($arFile)
                                                $arPhoto[] = ["name"=>$arFile["ORIGINAL_NAME"], "src"=>$arFile["SRC"]];
                                        }
                                    }
                                    $arReclamationTech[] = [
                                        "number"=>$elem["NAME"],
                                        "id"=>$elem["ID"],
                                        "infoTech"=>$infoTechRecl,
                                        "reason" => $propsRecl["REASON"]["VALUE"]["TEXT"],
                                        "wishes" => $propsRecl["WISHES"]["VALUE"]["TEXT"],
                                        "akt" => $akt_parse,
                                        "photo" => $arPhoto
                                    ];
                                }
                            }
                        }
                        if(!empty($arReclamationTech))
                            $arTech = [];

                        if(!empty($arProps["DATE_EXECUTION"]["VALUE"])){
                            $due_date = $arProps["DATE_EXECUTION"]["VALUE"];
                        }else{
                            $deteProp = explode(' ', $service["DATE_CREATE"])[0];
                            $due_date = date('d.m.Y' ,strtotime($deteProp.DAY_EXECUTION));
                        }
                        $error = '';
                        if((strtotime(date("d.m.Y"))>strtotime($due_date)) && !empty($isNew)){
                            $error = 'Y';
                        }
                        $arTasks[] = [
                            "id" => $service["ID"],
                            "vid" => "services",
                            "error" => $error,
                            "date" => $service["DATE_CREATE"],
                            "date_sort" => strtotime($service["DATE_CREATE"]),
                            "due_date" => $due_date,
                            "number"=>'S'.$service["ID"],
                            "type"=>$type,
                            "status"=>$status,
                            "phone" => $arProps["PHONE_PERSONAL"]["VALUE"],
                            "status_id"=>$arProps["STATUS_ID"]["VALUE"],
                            "new" => $isNew,
                            "status_arr" => ["reject_id" => 26, "confirm_id" => 12],
                            "name" => "Сервисные компенсации",
                            "files"=>$arFiles,
                            "dealer" => $this::getUser($arProps["DILER"]["VALUE"]),
                            "arTech" => $arTech,
                            "arReclamationTech" => $arReclamationTech
                        ];
                    }
                }
            }
            /**
             * Маркетинговые компенсации
             */
            if(in_array(IB_MARKETING, $visibleTask)) {
                $elemIbl = new Element;
                $infoMarketing = $elemIbl->GetList(IB_MARKETING,
                    ["ID", "IBLOCK_ID", "NAME", "CODE", "DATE_CREATE", "DATE_ACTIVE_FROM", "PROPERTY_*"],
                    [
                        "ACTIVE_DATE" => "Y",
                        "ACTIVE" => "Y",
                        "PROPERTY_DILER" => $id_dealer,
                    ],
                    ["ID" => "desc"]
                );
                foreach ($infoMarketing["ELEMS"] as $info){
                    $arProps = $infoMarketing["PROPS"][$info["ID"]];
                    $status = $arProps["STATUS"]["~VALUE"];
                    $isNew = '';
                    if($status == 'Отправлено' || $status == 'В работе'){
                        $isTask++;
                        $isNew = 'Y';
                    }

                    if($type_vid == "detail"){
                        $type = $arProps["TYPE_KOMP"]["~VALUE"];
                        unset($arProps["TYPE_KOMP"]);
                        unset($arProps["STATUS"]);
                        $arFiles = [];
                        foreach ($arProps as $arProp) {
                            if ($arProp["PROPERTY_TYPE"] == 'F') {
                                if(empty($arProp["VALUE"]))continue;
                                $src = [];
                                if (is_array($arProp["VALUE"])) {
                                    foreach ($arProp["VALUE"] as $id) {
                                        $fileFind = CFile::GetFileArray($id);
                                        if(!empty($fileFind))
                                            $src[] = $fileFind;
                                    }
                                } else {
                                    $fileFind2 = CFile::GetFileArray($arProp["VALUE"]);
                                    if(!empty($fileFind2))
                                        $src = $fileFind2;
                                }
                                if(!empty($src))
                                    $arFiles[] = ["info" => $src, "name" => $arProp["NAME"]];
                            }
                        }

                        if(!empty($arProps["DATE_EXECUTION"]["VALUE"])){
                            $due_date = $arProps["DATE_EXECUTION"]["VALUE"];
                        }else{
                            $deteProp = explode(' ', $info["DATE_CREATE"])[0];
                            $due_date = date('d.m.Y' ,strtotime($deteProp.DAY_EXECUTION));
                        }
                        $error = '';
                        if((strtotime(date("d.m.Y"))>strtotime($due_date)) && !empty($isNew)){
                            $error = 'Y';
                        }
                        $arTasks[] = [
                            "id" => $info["ID"],
                            "vid" => "mark",
                            "error" => $error,
                            "date" => $info["DATE_CREATE"],
                            "date_sort" => strtotime($info["DATE_CREATE"]),
                            "due_date" => $due_date,
                            "number"=>'M'.$info["ID"],
                            "type"=>$type,
                            "status"=>$status,
                            "phone" => $arProps["PHONE_PERSONAL"]["VALUE"],
                            "status_id"=>$arProps["STATUS_ID"]["VALUE"],
                            "new" => $isNew,
                            "status_arr" => ["reject_id" => 25, "confirm_id" => 3],
                            "name" => "Маркетинговые компенсации",
                            "files"=>$arFiles,
                            "dealer" => $this::getUser($arProps["DILER"]["VALUE"]),
                        ];
                    }
                }
            }
        }
            //usort($arTasks, "cmp");
            $arTasks = sort_nested_arrays( $arTasks, array('status_id'=>'asc', 'date_sort' => 'desc') );
            $pagen = [];
            $arPage = [];
            if(!empty($arTasks) && !empty($this->arParams["PAGE_ELEMS"])){
                $arPage = array_chunk($arTasks, $this->arParams["PAGE_ELEMS"]);
                if(count($arPage) > 1){
                    for ($i = 1; $i <= count($arPage); $i++) {
                        $pagen[] = $i;
                    }
                }
            }
        return ["check_task"=>$isTask, "tasks"=>["arTasks"=>$arPage[$request["page_task"]?$request["page_task"]-1:0], "pagination"=>$pagen]];
    }

    /**
     * @return array|mixed
     * @throws \Bitrix\Main\LoaderException
     */
    public function executeComponent()
    {
        function sort_nested_arrays( $array, $args = array('votes' => 'desc') ){
            usort( $array, function( $a, $b ) use ( $args ){
                $res = 0;

                $a = (object) $a;
                $b = (object) $b;

                foreach( $args as $k => $v ){
                    if( $a->$k == $b->$k ) continue;

                    $res = ( $a->$k < $b->$k ) ? -1 : 1;
                    if( $v=='desc' ) $res= -$res;
                    break;
                }

                return $res;
            } );

            return $array;
        }
        global $USER;
        if ($this->startResultCache($this->arParams["CACHE_TIME"]) && $USER->GetID()) {
            if(!empty($this->arParams["DETAIL_ID"])){
                $this->arResult["tasks"] = $this::isTasks($this->arParams["DETAIL_ID"], "detail", $_REQUEST)["tasks"];
                $this->arResult["checkTask"] = $this::isTasks($this->arParams["DETAIL_ID"], "detail", $_REQUEST)["check_task"];
                $this->arResult["manager"] = $this::getUser($this->arParams["DETAIL_ID"]);
                $arDealers = [];
                $elemIbl = new Element;
                $usersForManger = CUser::GetList(($by="ID"), ($order="ASC"),
                    [
                        "ID"=>$this->arParams["DETAIL_ID"]
                    ], array("SELECT"=>array("UF_*")));
                while ($arUser = $usersForManger->Fetch()) {
                    if ($arUser["UF_ADMIN_NEW"]) {
                        $usersForManger2 = CUser::GetList(($by = "ID"), ($order = "ASC"), ["UF_MANAGER_NEW" => $arUser["ID"]], array("SELECT" => array("UF_*")));
                        while ($arUser2 = $usersForManger2->Fetch()) {
                            $regions = [];
                            if(!empty($arUser2["UF_REGIONS_USER"])){
                                foreach ($arUser2["UF_REGIONS_USER"] as $regionId){
                                    $fildRegion = $elemIbl->getById(IB_REGION, $regionId);
                                    $regions[] = $fildRegion["NAME"];
                                }
                            }
                            $arUser2["regions_text"]=implode(',', $regions);
                            $arDealers[] = $arUser2;
                        }
                    }
                }
                $this->arResult["arDealers"] = $arDealers;
            }else{
                $arInfo = [];
                $arIdsDealer = [];
                $elemIbl = new Element;

                $filterDealer = [];
                if(!empty($_REQUEST["search_manager"])){
                    $filterDealer = [
                        [
                            "LOGIC" => "OR",
                            "?LAST_NAME" => $_REQUEST["search_manager"],
                            "?NAME" => $_REQUEST["search_manager"],
                            "?SECOND_NAME" => $_REQUEST["search_manager"]
                        ]
                    ];
                }
                $filterDealer["UF_ADMIN_NEW"] = $USER->GetID();
                $rsManagers = \Bitrix\Main\UserTable::getList([
                    'select' => [
                        'ID',
                        'UF_ADMIN_NEW',
                        'LAST_NAME',
                        'NAME',
                        'SECOND_NAME',
                    ],
                    'filter' => $filterDealer,
                    'order' => ['ID' => 'asc'],
                ]);
                while ($arUser = $rsManagers->fetch()) {
                        /*$usersForManger = CUser::GetList(($by="ID"), ($order="ASC"),
                            [
                                "UF_ADMIN_ID"=>$USER->GetID()
                            ], array("SELECT"=>array("UF_*")));
                        while ($arUser = $usersForManger->Fetch()) {*/
                            if($arUser["UF_ADMIN_NEW"]) {
                                $countTask = 0;
                                $usersForManger2 = CUser::GetList(($by = "ID"), ($order = "ASC"), ["UF_MANAGER_NEW" => $arUser["ID"]], array("SELECT" => array("UF_*")));
                                while ($arUser2 = $usersForManger2->Fetch()) {
                                    $arIdsDealer[] = $arUser2["ID"];
                                    //$countTask += $this::isTasks($arUser2["ID"], "list")["check_task"];
                                }
                                $position = '';
                                $groupManager = CUser::GetUserGroup($arUser["ID"]);
                                if(in_array(ID_MANAGER_SERVICES, $groupManager)){
                                    $position = 'Сервисный специалист';
                                }
                                if(in_array(ID_MANAGER_PARTS, $groupManager)){
                                    $position = 'Менеджер по запчастям';
                                }
                                if(in_array(ID_MANAGER_TECH, $groupManager)){
                                    $position = 'Менеджер по технике';
                                }
                                if(in_array(ID_MANAGER_MARKETING, $groupManager)){
                                    $position = 'Менеджер по маркетингу';
                                }
                                $arInfo[$arUser["ID"]] = [
                                    "fio" => $arUser["LAST_NAME"].' '.$arUser["NAME"].' '.$arUser["SECOND_NAME"],
                                    "position" => $position,
                                    "id" => $arUser["ID"]
                                    //"tasks" => $countTask
                                ];
                            }
                        }
                if(!empty($_REQUEST["sort_name"])){
                    $arInfo = sort_nested_arrays($arInfo, array('fio'=>$_REQUEST["sort_name"]));
                }elseif(!empty($_REQUEST["sort_position"])){
                    $arInfo = sort_nested_arrays($arInfo, array('position'=>$_REQUEST["sort_position"]));
                }else{
                    $arInfo = sort_nested_arrays($arInfo, array('fio'=>'desc'));
                }
                $pagenManager = [];
                $arPageManager = [];
                if(!empty($arInfo) && !empty($this->arParams["PAGE_ELEMS"])){
                    $arPageManager = array_chunk($arInfo, $this->arParams["PAGE_ELEMS"]);
                    if(count($arPageManager) > 1){
                        for ($i = 1; $i <= count($arPageManager); $i++) {
                            $pagenManager[] = $i;
                        }
                    }
                }
                $this->arResult = [
                    "persons"=>$arPageManager[$_REQUEST["page_manager"]?$_REQUEST["page_manager"]-1:0],
                    "pagination"=>$pagenManager
                ];
            }
            $this->includeComponentTemplate();
        }
        return $this->arResult;
    }
} ?>