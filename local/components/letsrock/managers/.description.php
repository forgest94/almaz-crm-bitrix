<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
$arComponentDescription = array(
    "NAME" => 'Список менеджеров',
    "DESCRIPTION" => 'Список менеджеров',
    "PATH" => array(
        "ID" => "dv_components",
        "CHILD" => array(
            "ID" => "managers",
            "NAME" => "Список менеджеров"
        )
    ),
);
?>