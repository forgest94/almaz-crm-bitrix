<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}

if (!$arResult) {
    return null;
}
?>
<div class="lk__card">
    <div class="lk__card-title">Возникли вопросы?</div>
    <div class="lk__card-avatar" style="background-image: url(<?=$arResult['PHOTO']['src']?>);background-size: cover;">
        <?/*<img src="<?= $arResult['PHOTO']['src'] ?>" alt="">*/?>
    </div>
    <div class="lk__card-name">
        <?= $arResult['NAME'] . ' ' . $arResult['LAST_NAME'] ?>
    </div>
    <div class="lk__card-position">
        Персональный менеджер
    </div>
    <a href="tel:<?= $arResult['PERSONAL_PHONE'] ?>" class="lk__card-phone"><?= $arResult['PERSONAL_PHONE'] ?></a>
    <a href="mailto:<?= $arResult['EMAIL'] ?>" class="lk__card-mail"><?= $arResult['EMAIL'] ?></a>
</div>