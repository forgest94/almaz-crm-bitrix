<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}

use Letsrock\Lib\Models\User;

\Bitrix\Main\Loader::includeModule('letsrock.lib');

class ManagerComponent extends CBitrixComponent
{
    public function onPrepareComponentParams($arParams)
    {
        $result = [
            "CACHE_TYPE" => $arParams["CACHE_TYPE"],
            "CACHE_TIME" => 0,
        ];

        return $result;
    }

    /**
     * @return array|mixed
     * @throws \Bitrix\Main\LoaderException
     */
    public function executeComponent()
    {
        global $USER;

        if ($this->startResultCache($this->arParams["CACHE_TIME"], $USER->GetID())) {
            $this->arResult = User::getManager();

            $this->includeComponentTemplate();
        }

        return $this->arResult;
    }
} ?>