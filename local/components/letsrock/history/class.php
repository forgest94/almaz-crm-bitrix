<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}

use Letsrock\Lib\Models\User;
use Letsrock\Lib\Models\Element;
use Letsrock\Lib\Models\HLBlock;

\Bitrix\Main\Loader::includeModule('letsrock.lib');

class BuyersComponent extends CBitrixComponent
{
    public function onPrepareComponentParams($arParams)
    {
        $result = $arParams;

        return $result;
    }

    /**
     * @return array|mixed
     * @throws \Bitrix\Main\LoaderException
     */
    public function executeComponent()
    {
        global $USER;
        if ($this->startResultCache($this->arParams["CACHE_TIME"]) && $USER->GetID()) {
            switch ($this->arParams["PANEL"]){
                case 'dealer':
                    $id_manager = '';
                    $id_admin = '';
                    $usersForManger = CUser::GetList(($by="ID"), ($order="ASC"), ["ID"=>$USER->GetID()], array("SELECT"=>array("UF_*")));
                    if ($arUser = $usersForManger->Fetch()) {
                        if(!empty($arUser["UF_MANAGER_NEW"])){
                            $id_manager = $arUser["UF_MANAGER_NEW"];
                            $usersForManger2 = CUser::GetList(($by="ID"), ($order="ASC"), ["ID"=>$arUser["UF_MANAGER_NEW"]], array("SELECT"=>array("UF_*")));
                            if ($arUser2 = $usersForManger2->Fetch()) {
                                $id_admin = $arUser2["UF_ADMIN_NEW"];
                            }
                        }
                    }

                    $historyHl = new HLBlock(HL_HISTORY);
                    $arHistory_db = $historyHl->get(
                        [
                            [
                                "LOGIC" => "OR",
                                ["UF_WHO_USER"=>$id_manager, "UF_THAT_USER"=>$USER->GetID()],
                                ["UF_WHO_USER"=>$id_admin, "UF_THAT_USER"=>$USER->GetID()],
                                ["UF_WHO_USER"=>$USER->GetID(), "UF_THAT_USER"=>$USER->GetID()]
                            ]
                        ]
                    );
                    $arHistory = [];
                    foreach ($arHistory_db as $item){
                        $arWhoInfo = [];
                        $infoGroupWho = CUser::GetUserGroup($item["UF_WHO_USER"]);
                        $infoUserWho = CUser::GetByID($item["UF_WHO_USER"])->Fetch();
                        if(in_array(ID_DEALER, $infoGroupWho)){
                            $arWhoInfo = ["name"=>$infoUserWho["WORK_COMPANY"], "type"=>"Дилер"];
                        }else if(
                            in_array(ID_MANAGER_SERVICES, $infoGroupWho) ||
                            in_array(ID_MANAGER_PARTS, $infoGroupWho) ||
                            in_array(ID_MANAGER_TECH, $infoGroupWho) ||
                            in_array(ID_MANAGER_MARKETING, $infoGroupWho)){
                            $name_person = '';
                            $secondName_person = '';
                            if($infoUserWho["NAME"])
                                $name_person = mb_substr($infoUserWho["NAME"],0,1).". ";
                            if($infoUserWho["SECOND_NAME"])
                                $secondName_person = mb_substr($infoUserWho["SECOND_NAME"],0,1).".";

                            $name_parse = $infoUserWho["LAST_NAME"]." ".$name_person.$secondName_person;
                            $arWhoInfo = ["name"=>$name_parse, "type"=>"Менеджер"];
                        }else if(in_array(ID_ADMIN, $infoGroupWho)){
                            $arWhoInfo = ["name"=>$infoUserWho["NAME"], "type"=>"Администратор"];
                        }
                        $item["UF_WHO_USER"] = $arWhoInfo;
                        $arHistory[] = $item;
                    }
                    $arPage = array_chunk($arHistory, $this->arParams["PAGE_ELEMS"]);

                    $pagen = [];
                    if(count($arPage) > 1){
                        for ($i = 1; $i <= count($arPage); $i++) {
                            $pagen[] = $i;
                        }
                    }
                    $this->arResult = ["elems"=>$arPage[$_REQUEST["page"]?$_REQUEST["page"]-1:0], "pagination"=>$pagen];
                    $this->includeComponentTemplate();
                    break;
                case 'manager':
                    $visibleTask=[];
                    $groupManager = $USER->GetUserGroupArray();
                    if(in_array(ID_MANAGER_SERVICES, $groupManager)){
                        $visibleTask[]=IB_EXPLOITATION;
                        $visibleTask[]=IB_SERVCOMP;
                    }
                    if(in_array(ID_MANAGER_PARTS, $groupManager)){
                        $visibleTask[]=IB_RECLAMATION_PARTS;
                    }
                    if(in_array(ID_MANAGER_TECH, $groupManager)){
                        $visibleTask[]=IB_RECLAMATION_TECH;
                    }
                    if(in_array(ID_MANAGER_MARKETING, $groupManager)){
                        $visibleTask[]=IB_MARKETING;
                    }
                    if(empty($visibleTask)){
                        $this->arResult = ["elems"=>[], "pagination"=>[]];
                        $this->includeComponentTemplate();
                        return $this->arResult;
                    }

                    $historyHl = new HLBlock(HL_HISTORY);
                    $arIdsDealer = [];
                    $arInfo = [];
                    $usersForManger = CUser::GetList(($by="ID"), ($order="ASC"), [
                        "UF_MANAGER_NEW"=>$USER->GetID()
                    ], array("SELECT"=>array("UF_*")));
                    while ($arUser = $usersForManger->Fetch()) {
                        $arIdsDealer[] = $arUser["ID"];
                        $arInfo[$arUser["ID"]] = $arUser;
                    }
                    $infoThisUser = \Letsrock\Lib\Models\User::getWork();
                    $arHistory_db = $historyHl->get(
                        [
                            "UF_ID_RAZ"=>$visibleTask,
                            [
                                "LOGIC" => "OR",
                                ["UF_WHO_USER"=>$USER->GetID(), "UF_THAT_USER"=>$arIdsDealer],
                                ["UF_WHO_USER"=>$infoThisUser["UF_ADMIN_NEW"], "UF_THAT_USER"=>$arIdsDealer],
                                ["UF_WHO_USER"=>$arIdsDealer, "UF_THAT_USER"=>$arIdsDealer],
                            ]
                        ]
                    );
                    $arHistory = [];
                    foreach ($arHistory_db as $item){
                        $arWhoInfo = [];
                        $infoGroupWho = CUser::GetUserGroup($item["UF_WHO_USER"]);
                        $infoUserWho = CUser::GetByID($item["UF_WHO_USER"])->Fetch();
                        if(in_array(ID_DEALER, $infoGroupWho)){
                            $arWhoInfo = ["name"=>$infoUserWho["WORK_COMPANY"], "type"=>"Дилер"];
                        }else if(
                            in_array(ID_MANAGER_SERVICES, $infoGroupWho) ||
                            in_array(ID_MANAGER_PARTS, $infoGroupWho) ||
                            in_array(ID_MANAGER_TECH, $infoGroupWho) ||
                            in_array(ID_MANAGER_MARKETING, $infoGroupWho)){
                            $name_person = '';
                            $secondName_person = '';
                            if($infoUserWho["NAME"])
                                $name_person = mb_substr($infoUserWho["NAME"],0,1).". ";
                            if($infoUserWho["SECOND_NAME"])
                                $secondName_person = mb_substr($infoUserWho["SECOND_NAME"],0,1).".";

                            $name_parse = $infoUserWho["LAST_NAME"]." ".$name_person.$secondName_person;
                            $arWhoInfo = ["name"=>$name_parse, "type"=>"Менеджер"];
                        }else if(in_array(ID_ADMIN, $infoGroupWho)){
                            $arWhoInfo = ["name"=>$infoUserWho["NAME"], "type"=>"Администратор"];
                        }
                        $item["UF_WHO_USER"] = $arWhoInfo;
                        $arHistory[] = $item;
                    }
                    $arPage = array_chunk($arHistory, $this->arParams["PAGE_ELEMS"]);

                    $pagen = [];
                    if(count($arPage) > 1){
                        for ($i = 1; $i <= count($arPage); $i++) {
                            $pagen[] = $i;
                        }
                    }

                    $this->arResult = ["elems"=>$arPage[$_REQUEST["page"]?$_REQUEST["page"]-1:0], "pagination"=>$pagen];
                    $this->includeComponentTemplate();
                    break;
                case 'admin':
                    $historyHl = new HLBlock(HL_HISTORY);
                    $arIdsDealer = [];
                    $arManagerIds = [];
                    $usersForManger = CUser::GetList(($by="ID"), ($order="ASC"), [
                        "UF_ADMIN_NEW"=>$USER->GetID()
                    ], array("SELECT"=>array("UF_*")));
                    while ($arUser = $usersForManger->Fetch()) {
                        if($arUser["UF_ADMIN_NEW"]){
                            $usersForManger2 = CUser::GetList(($by="ID"), ($order="ASC"), ["UF_MANAGER_NEW"=>$arUser["ID"]], array("SELECT"=>array("UF_*")));
                            while ($arUser2 = $usersForManger2->Fetch()) {
                                $arIdsDealer[] = $arUser2["ID"];
                            }
                            $arManagerIds[] = $arUser["ID"];
                        }
                    }
                    $arHistory_db = $historyHl->get(
                        [
                            [
                                "LOGIC" => "OR",
                                ["UF_WHO_USER"=>$arManagerIds, "UF_THAT_USER"=>$arIdsDealer],
                                ["UF_WHO_USER"=>$USER->GetID(), "UF_THAT_USER"=>$arIdsDealer],
                                ["UF_WHO_USER"=>$arIdsDealer, "UF_THAT_USER"=>$arIdsDealer]
                            ]
                        ]
                    );
                    $arHistory = [];
                    foreach ($arHistory_db as $item){
                        $arWhoInfo = [];
                        $infoGroupWho = CUser::GetUserGroup($item["UF_WHO_USER"]);
                        $infoUserWho = CUser::GetByID($item["UF_WHO_USER"])->Fetch();

                        if(in_array(ID_DEALER, $infoGroupWho)){
                            $arWhoInfo = ["name"=>$infoUserWho["WORK_COMPANY"], "type"=>"Дилер"];
                        }else if(
                            in_array(ID_MANAGER_SERVICES, $infoGroupWho) ||
                            in_array(ID_MANAGER_PARTS, $infoGroupWho) ||
                            in_array(ID_MANAGER_TECH, $infoGroupWho) ||
                            in_array(ID_MANAGER_MARKETING, $infoGroupWho)){
                            $name_person = '';
                            $secondName_person = '';
                            if($infoUserWho["NAME"])
                                $name_person = mb_substr($infoUserWho["NAME"],0,1).". ";
                            if($infoUserWho["SECOND_NAME"])
                                $secondName_person = mb_substr($infoUserWho["SECOND_NAME"],0,1).".";

                            $name_parse = $infoUserWho["LAST_NAME"]." ".$name_person.$secondName_person;
                            $arWhoInfo = ["name"=>$name_parse, "type"=>"Менеджер"];
                        }else if(in_array(ID_ADMIN, $infoGroupWho)){
                            $arWhoInfo = ["name"=>$infoUserWho["NAME"], "type"=>"Администратор"];
                        }
                        $item["UF_WHO_USER"] = $arWhoInfo;
                        $arHistory[] = $item;
                    }
                    $pagen = [];
                    $arPage = [];
                    if(!empty($arHistory)){
                        $arPage = array_chunk($arHistory, $this->arParams["PAGE_ELEMS"]);
                        if(count($arPage) > 1){
                            for ($i = 1; $i <= count($arPage); $i++) {
                                $pagen[] = $i;
                            }
                        }
                    }

                    $this->arResult = ["elems"=>$arPage[$_REQUEST["page"]?$_REQUEST["page"]-1:0], "pagination"=>$pagen];
                    $this->includeComponentTemplate();
                    break;
            }
        }
        return $this->arResult;
    }
} ?>