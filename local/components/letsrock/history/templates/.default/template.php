<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
?>
<div class="lk__content-box">
    <div class="lk__table history">
        <div class="lk__table-head">
            <div class="lk__table-row">
                <div class="lk__table-col">
                    Дата и время
                </div>
                <div class="lk__table-col">
                    Кто совершил
                </div>
                <div class="lk__table-col">
                    Раздел
                </div>
                <div class="lk__table-col">
                    Действие
                </div>
            </div>
        </div>
        <?if(!empty($arResult["elems"])):?>
        <div class="lk__table-body">
            <?foreach ($arResult["elems"] as $item):?>
            <div class="lk__table-row-wrap">
                <div class="lk__table-row">
                    <div class="lk__table-col">
                        <div class="lk__table-col-head">
                            Дата и время
                        </div>
                        <span>
                            <?
                            $date = explode(' ', $item["UF_DATE_HISTORY"])
                            ?>
                          <?=$date[0];?><br/><?=$date[1];?>
                        </span>
                    </div>
                    <div class="lk__table-col">
                        <div class="lk__table-col-head">
                            Кто совершил
                        </div>
                        <span>
                           <?=$item["UF_WHO_USER"]["type"];?> <br>
                        <b><?=$item["UF_WHO_USER"]["name"];?></b>
                        </span>
                    </div>
                    <div class="lk__table-col">
                        <div class="lk__table-col-head">
                            Раздел
                        </div>
                        <?=$item["UF_SECTION"];?>
                    </div>
                    <div class="lk__table-col">
                        <div class="lk__table-col-head">
                            Действие
                        </div>
                        <span><?=$item["UF_ACTION"];?></span>
                    </div>
                </div>
            </div>
            <?endforeach;?>
        </div>
        <?endif;?>
    </div>
    <?$APPLICATION->IncludeComponent("letsrock:paginations",
        ".default",
        [
            "CACHE_TIME" => 0,
            "ELEMS_PAGE"=>$arResult["pagination"],
            "GET_PAGE"=>"page",
            "AJAX_BLOCK"=>""
        ]
    );?>
</div>