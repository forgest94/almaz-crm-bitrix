<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
$arComponentDescription = array(
    "NAME" => 'Сервисные компенсации',
    "DESCRIPTION" => 'Сервисные компенсации',
    "PATH" => array(
        "ID" => "dv_components",
        "CHILD" => array(
            "ID" => "service",
            "NAME" => "Сервисные компенсации"
        )
    ),
);
?>