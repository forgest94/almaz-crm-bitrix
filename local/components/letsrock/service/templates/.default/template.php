<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}

if (!$arResult) {
    return null;
}
?>
<div class="lk__content-box">
    <section class="lk__tabs-section">
        <article class="lk__tabs-content-item active" data-tab-content>
            <div class="lk__tabs-content-inner js-tabs-content-inner <?= $arParams["EDTI_ID"] ? '' : 'active'; ?>">
                <div class="lk__tabs-top">
                    <a class="btn btn_small lk__tabs-top-btn js-tabs-show-form <?= $arParams["EDTI_ID"] ? 'hide' : ''; ?>"
                       href="javascript:void(0)">Подать документы</a>
                </div>
                <div class="lk__table service">
                    <div class="lk__table-head">
                        <div class="lk__table-row">
                            <div class="lk__table-col">
                                Название
                            </div>
                            <div class="lk__table-col">
                                Дата создания
                            </div>
                            <div class="lk__table-col">
                                Тип
                            </div>
                            <div class="lk__table-col">
                                Статус
                            </div>
                            <div class="lk__table-col">
                            </div>
                        </div>
                    </div>
                    <? if (!empty($arResult["arServices"])): ?>
                        <div class="lk__table-body">
                            <? foreach ($arResult["arServices"] as $itemS): ?>
                                <div class="lk__table-row-wrap js-table-detail">
                                    <div class="lk__table-row">
                                        <div class="lk__table-col">
                                            <div class="lk__table-col-head">
                                                Название
                                            </div>
                                            <?= $itemS["number"]; ?>
                                        </div>
                                        <div class="lk__table-col">
                                            <div class="lk__table-col-head">
                                                Дата создания
                                            </div>
                                            <?= $itemS["date"]; ?>
                                        </div>
                                        <div class="lk__table-col">
                                            <div class="lk__table-col-head">
                                                Тип
                                            </div>
                                            <?= $itemS["type"]; ?>
                                        </div>
                                        <div class="lk__table-col">
                                            <div class="lk__table-col-head">
                                                Статус
                                            </div>
                                            <?= $itemS["status"]; ?>
                                        </div>
                                        <div class="lk__table-col">
                                            <a class="lk__link js-table-detail-link" href="javascript:void(0)"
                                               data-show-text="Свернуть">
                                                Подробнее
                                            </a>
                                        </div>
                                    </div>
                                    <div class="lk__table-row-data js-table-detail-data">
                                        <div class="lk__data">
                                            <? if (!empty($itemS["files"])): ?>
                                                <? foreach ($itemS["files"] as $file): ?>
                                                    <div class="lk__data-row">
                                                        <div class="lk__data-label">
                                                            <?= $file["name"]; ?>
                                                        </div>
                                                        <div class="lk__data-value">
                                                            <div class="lk__data-files">
                                                                <? if (!empty($file["info"][0])): ?>
                                                                    <? foreach ($file["info"] as $infoFile): ?>
                                                                        <a class="link-file"
                                                                           href="<?= $infoFile["SRC"]; ?>"
                                                                           download><?= $infoFile["ORIGINAL_NAME"]; ?></a>
                                                                    <? endforeach; ?>
                                                                <? else: ?>
                                                                    <a class="link-file"
                                                                       href="<?= $file["info"]["SRC"]; ?>"
                                                                       download><?= $file["info"]["ORIGINAL_NAME"]; ?></a>
                                                                <? endif; ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                <? endforeach; ?>
                                                <div class="lk__data-separate"></div>
                                            <? endif; ?>
                                            <? if (!empty($itemS["arReclamationTech"])): ?>
                                                <? foreach ($itemS["arReclamationTech"] as $recl): ?>
                                                    <div class="lk__data-block-box js-table-block">
                                                        <div class="lk__data-row lk__data-row--margin">
                                                            <div class="lk__data-label">
                                                                Указанная рекламация <br>
                                                                № <b><?= $recl["number"]; ?></b>
                                                            </div>
                                                            <div class="lk__data-value">
                                                                <a class="lk__link js-table-block-link"
                                                                   href="javascript:void(0)" data-show-text="Свернуть">
                                                                    Развернуть
                                                                </a>
                                                            </div>
                                                        </div>
                                                        <div class="lk__data-block hide js-table-block-data"
                                                             style="display: none;">
                                                            <div class="lk__data-row">
                                                                <div class="lk__data-label">
                                                                    Указанная техника
                                                                </div>
                                                                <div class="lk__data-value">
                                                                    <ul class="lk__data-grid">
                                                                        <li>
                                                                            <b><?= $recl["infoTech"]["number"]; ?></b> <?= $recl["infoTech"]["type"]; ?>
                                                                            / <?= $recl["infoTech"]["model"]; ?>
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                            <? if (!empty($recl["reason"])): ?>
                                                                <div class="lk__data-row">
                                                                    <div class="lk__data-label">
                                                                        Причина обращения
                                                                    </div>
                                                                    <div class="lk__data-value">
                                                                        <div class="lk__data-box">
                                                                            <?= $recl["reason"]; ?>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            <? endif; ?>
                                                            <? if (!empty($recl["wishes"])): ?>
                                                                <div class="lk__data-row">
                                                                    <div class="lk__data-label">
                                                                        Пожелания по решению рекламации
                                                                    </div>
                                                                    <div class="lk__data-value">
                                                                        <div class="lk__data-box">
                                                                            <?= $recl["wishes"]; ?>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            <? endif; ?>
                                                            <? if (!empty($recl["akt"])): ?>
                                                                <div class="lk__data-row">
                                                                    <div class="lk__data-label">
                                                                        Акт ввода в эксплуатацию
                                                                    </div>
                                                                    <div class="lk__data-value">
                                                                        <div class="lk__data-files">
                                                                            <? foreach ($recl["akt"] as $akt): ?>
                                                                                <a class="link-file"
                                                                                   href="<?= $akt["src"]; ?>"
                                                                                   download><?= $akt["name"]; ?></a>
                                                                            <? endforeach; ?>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            <? endif; ?>
                                                            <? if (!empty($recl["photo"])): ?>
                                                                <div class="lk__data-row">
                                                                    <div class="lk__data-label">
                                                                        Фото
                                                                    </div>
                                                                    <div class="lk__data-value">
                                                                        <div class="lk__data-files">
                                                                            <? foreach ($recl["photo"] as $photo): ?>
                                                                                <a class="link-file"
                                                                                   href="<?= $photo["src"]; ?>"
                                                                                   download><?= $photo["name"]; ?></a>
                                                                            <? endforeach; ?>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            <? endif; ?>
                                                            <div class="lk__data-separate"></div>
                                                        </div>
                                                    </div>
                                                <? endforeach; ?>
                                            <? endif; ?>
                                            <? /*<div class="lk__data-row">
                                                    <div class="lk__data-label">
                                                        Указанная рекламация
                                                    </div>
                                                    <div class="lk__data-value">
                                                        <ul class="lk__data-grid">
                                                            <?foreach ($itemS["arReclamationTech"] as $recl):?>
                                                                <li>
                                                                    № <b><?=$recl["number"];?></b>
                                                                </li>
                                                            <?endforeach;?>
                                                        </ul>
                                                    </div>
                                                </div>*/ ?>
                                            <? if (!empty($itemS["arTech"])): ?>
                                                <div class="lk__data-row">
                                                    <div class="lk__data-label">
                                                        Указанная техника
                                                    </div>
                                                    <div class="lk__data-value">
                                                        <ul class="lk__data-grid">
                                                            <? foreach ($itemS["arTech"] as $tech): ?>
                                                                <li>
                                                                    <b><?= $tech["number"]; ?></b> <?= $tech["type"]; ?>
                                                                    / <?= $tech["model"]; ?>
                                                                </li>
                                                            <? endforeach; ?>
                                                        </ul>
                                                    </div>
                                                </div>
                                            <? endif; ?>
                                            <? if ($itemS["status_id"] == 26): ?>
                                                <div class="lk__data-row">
                                                    <div class="lk__data-label">
                                                        <? if (!empty($itemS["refusal"])): ?>Причина отказа<? endif; ?>
                                                    </div>
                                                    <div class="lk__data-value">
                                                        <? if (!empty($itemS["refusal"])): ?>
                                                            <div class="lk__data-value">
                                                                <div class="lk__data-box">
                                                                    <?= $itemS["refusal"]; ?>
                                                                </div>
                                                            </div>
                                                        <? endif; ?>
                                                        <div class="btn-group">
                                                            <a class="btn"
                                                               href="/dealer/service/?edit_service=<?= $itemS["id"]; ?>"
                                                               data-taskid="<?= $itemS["id"]; ?>" data-click="editTask">Редактировать</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            <? endif; ?>
                                        </div>
                                    </div>
                                </div>
                            <? endforeach; ?>
                        </div>
                    <? endif; ?>
                </div>
                <? $APPLICATION->IncludeComponent("letsrock:paginations",
                    ".default",
                    [
                        "CACHE_TIME" => 0,
                        "ELEMS_PAGE" => $arResult["pagination"],
                        "GET_PAGE" => "page",
                        "AJAX_BLOCK" => ""
                    ]
                ); ?>
            </div>
            <?
            $editElement = [];
            if (!empty($arParams["EDTI_ID"])) {
                $editElement = $arResult["arServices"][0];
            }
            ?>
            <div class="lk__tabs-content-form js-tabs-form <?= $arParams["EDTI_ID"] ? 'active' : ''; ?>">
                <a class="btn-arrow js-tabs-back" href="javascript:void(0)">
                    <i class="icon-back"></i>
                    <span>Назад</span>
                </a>
                <form action="/ajax/dealer/addService/" method="POST" class="js-form-marketing">
                    <div class="lk__form-section">
                        <div class="lk__form-box">
                            <div class="lk__form-title">
                                Выберите <?= $arResult["arTextServices"]["TYPE_SERVICE"]["name"]; ?>
                            </div>
                            <div class="lk__form-row js-field">
                                <div class="lk__form-field <?= $arResult["arTextServices"]["TYPE_SERVICE"]["req"]; ?>">
                                    <select class="select js-select js-tabs-select" name="TYPE_SERVICE"
                                            <?= $arResult["arTextServices"]["TYPE_SERVICE"]["req"]; ?>>
                                        <option disabled selected hidden>Тип сервисной компенсации</option>
                                        <? foreach ($arResult["listSelect"] as $option): ?>
                                            <? if (!empty($editElement) && $option["ID"] == $editElement["type_id"]): ?>
                                                <option value="<?= $option["ID"]; ?>"
                                                        selected><?= $option["VALUE"]; ?></option>
                                            <? else: ?>
                                                <option value="<?= $option["ID"]; ?>"><?= $option["VALUE"]; ?></option>
                                            <? endif; ?>
                                        <? endforeach; ?>
                                    </select>
                                </div>
                                <? if ($arResult["arTextServices"]["TYPE_SERVICE"]["text"]): ?>
                                    <div class="hint">
                                        <div class="hint__message">
                                            <?= $arResult["arTextServices"]["TYPE_SERVICE"]["text"]; ?>
                                        </div>
                                    </div>
                                <? endif; ?>
                                <? if ($arResult["arTextServices"]["TYPE_SERVICE"]["req"]): ?>
                                    <div class="tooltip js-error-tooltip">
                                        Поле <?= $arResult["arTextServices"]["TYPE_SERVICE"]["name"]; ?> не может быть
                                        пустым
                                    </div>
                                <? endif; ?>
                            </div>
                        </div>
                        <br/>
                        <div class="lk__form-row js-field">
                            <div class="lk__form-field <?= $arResult["arTextServices"]["PHONE_PERSONAL"]["req"]; ?>">
                                <label class="field <?= $editElement['PHONE_PERSONAL'] ? 'focused' : ''; ?>">
                                    <span class="field__title <?= $arResult["arTextServices"]["PHONE_PERSONAL"]["req2"]; ?>"><?= $arResult["arTextServices"]["PHONE_PERSONAL"]["name"]; ?></span>
                                    <input class="field__input js-mask-phone" type="text" name="PHONE_PERSONAL"
                                           <?= $arResult["arTextServices"]["PHONE_PERSONAL"]["req"]; ?>
                                           value="<?=!empty($editElement["PHONE_PERSONAL"])?$editElement["PHONE_PERSONAL"]:'';?>">
                                </label>
                            </div>
                            <? if ($arResult["arTextServices"]["PHONE_PERSONAL"]["text"]): ?>
                                <div class="hint">
                                    <div class="hint__message">
                                        <?= $arResult["arTextServices"]["PHONE_PERSONAL"]["text"]; ?>
                                    </div>
                                </div>
                            <? endif; ?>
                            <? if ($arResult["arTextServices"]["PHONE_PERSONAL"]["req"]): ?>
                                <div class="tooltip js-error-tooltip">
                                    Поле <?= $arResult["arTextServices"]["PHONE_PERSONAL"]["name"]; ?> не может
                                    быть пустым
                                </div>
                            <? endif; ?>
                        </div>
                    </div>
                    <div class="lk__form-fieldset-list js-tabs-select-content">
                        <fieldset <?= $editElement["type_id"] == 8 ? '' : 'disabled'; ?> class="lk__form-fieldset"
                                  data-form-fieldset="8">
                            <div class="lk__form-section"
                                 data-req="<?= $arResult["arTextServices"]["SERIAL_NUMBER"]["req"] ? 'Y' : 'N'; ?>">
                                <div class="lk__form-title">
                                    Укажите технику:
                                </div>
                                <? if (!empty($editElement["arTech"])): ?>
                                    <? foreach ($editElement["arTech"] as $tech): ?>
                                        <div class="lk__form-box">
                                            <div class="lk__form-row lk__form-row_long js-field">
                                                <div class="lk__form-field <?= $arResult["arTextServices"]["SERIAL_NUMBER"]["req"]; ?>">
                                                    <label class="field focused">
                                                        <span class="field__title <?= $arResult["arTextServices"]["SERIAL_NUMBER"]["req2"]; ?>">Введите <?= $arResult["arTextServices"]["SERIAL_NUMBER"]["name"]; ?></span>
                                                        <input class="field__input js-autocomplete" type="text"
                                                               name="SERIAL_NUMBER[]" value="<?= $tech["number"]; ?>"
                                                               <?= $editElement["type_id"] == 8 ? $arResult["arTextServices"]["SERIAL_NUMBER"]["req"] : ''; ?>
                                                               data-path="/ajax/dealer/getExplotationSerial/">
                                                    </label>
                                                </div>
                                                <? if ($arResult["arTextServices"]["SERIAL_NUMBER"]["text"]): ?>
                                                    <div class="hint">
                                                        <div class="hint__message">
                                                            <?= $arResult["arTextServices"]["SERIAL_NUMBER"]["text"]; ?>
                                                        </div>
                                                    </div>
                                                <? endif; ?>
                                                <? if ($arResult["arTextServices"]["SERIAL_NUMBER"]["req"]): ?>
                                                    <div class="tooltip js-error-tooltip">
                                                        Поле <?= $arResult["arTextServices"]["SERIAL_NUMBER"]["name"]; ?>
                                                        не может быть пустым
                                                    </div>
                                                <? endif; ?>
                                            </div>
                                            <div class="lk__form-desc js-autocomplete-text"
                                                 data-def-text="Название техники (<span>появится автоматически после ввода заводского номера</span>)">
                                                <?= $tech["number"]; ?> <?= $tech["type"]; ?> <?= $tech["model"]; ?>
                                            </div>
                                            <input type="hidden" class="js-autocomplete-hidden-input"
                                                   name="id_explotation[]" value="<?= $tech["id"]; ?>"
                                                   <?= $editElement["type_id"] == 8 ? $arResult["arTextServices"]["SERIAL_NUMBER"]["req"] : ''; ?>/>
                                        </div>
                                    <? endforeach; ?>
                                <? else: ?>
                                    <div class="lk__form-box">
                                        <div class="lk__form-row lk__form-row_long js-field">
                                            <div class="lk__form-field <?= $arResult["arTextServices"]["SERIAL_NUMBER"]["req"]; ?>">
                                                <label class="field">
                                                    <span class="field__title <?= $arResult["arTextServices"]["SERIAL_NUMBER"]["req2"]; ?>">Введите <?= $arResult["arTextServices"]["SERIAL_NUMBER"]["name"]; ?></span>
                                                    <input class="field__input js-autocomplete" type="text"
                                                           name="SERIAL_NUMBER[]"
                                                           <?= $editElement["type_id"] == 8 ? $arResult["arTextServices"]["SERIAL_NUMBER"]["req"] : ''; ?>
                                                           data-path="/ajax/dealer/getExplotationSerial/">
                                                </label>
                                            </div>
                                            <? if ($arResult["arTextServices"]["SERIAL_NUMBER"]["text"]): ?>
                                                <div class="hint">
                                                    <div class="hint__message">
                                                        <?= $arResult["arTextServices"]["SERIAL_NUMBER"]["text"]; ?>
                                                    </div>
                                                </div>
                                            <? endif; ?>
                                            <? if ($arResult["arTextServices"]["SERIAL_NUMBER"]["req"]): ?>
                                                <div class="tooltip js-error-tooltip">
                                                    Поле <?= $arResult["arTextServices"]["SERIAL_NUMBER"]["name"]; ?> не
                                                    может быть пустым
                                                </div>
                                            <? endif; ?>
                                        </div>
                                        <div class="lk__form-desc js-autocomplete-text"
                                             data-def-text="Название техники (<span>появится автоматически после ввода заводского номера</span>)">
                                            Название техники
                                            (<span>появится автоматически после ввода заводского номера</span>)
                                        </div>
                                        <input type="hidden" class="js-autocomplete-hidden-input"
                                               name="id_explotation[]" value=""
                                               <?= $editElement["type_id"] == 8 ? $arResult["arTextServices"]["SERIAL_NUMBER"]["req"] : ''; ?>/>
                                    </div>
                                <? endif; ?>
                                <div class="lk__form-row">
                                    <a class="lk__form-add js-lk-row-add" href="javascript:void(0)">Еще</a>
                                </div>
                            </div>
                            <div class="lk__form-section">
                                <div class="lk__form-title">
                                    Прикрепите необходимые документы
                                </div>
                                <div class="lk__form-row js-field"
                                     data-req="<?= $arResult["arTextServices"]["AKT"]["req"] ? 'Y' : 'N'; ?>">
                                    <div class="lk__form-field <?= $arResult["arTextServices"]["AKT"]["req"]; ?>">
                                        <? if (!empty($editElement["files"]["AKT"])): ?>
                                            <div class="lk__form-file selected">
                                                <div class="lk__download">
                                                    <div class="lk__download-title"
                                                         data-default-title="<?= $arResult["arTextServices"]["AKT"]["name"]; ?>">
                                                        <?= $editElement["files"]["AKT"]["info"]['FILE_NAME']; ?>
                                                    </div>
                                                    <? if (!empty($arResult["arTextServices"]["AKT"]["dop_filds"]["UF_SAMPLE"])): ?>
                                                        <a href="<?= CFile::GetPath($arResult["arTextServices"]["AKT"]["dop_filds"]["UF_SAMPLE"]); ?>"
                                                           class="lk__download-link" download>
                                                            Скачать форму
                                                        </a>
                                                    <? endif; ?>
                                                </div>
                                                <label class="field-file">
                                                    <input class="field-file__input js-file" type="file" name="AKT"
                                                           <?= $editElement["type_id"] == 8 ? $arResult["arTextServices"]["AKT"]["req"] : ''; ?>
                                                           data-type="<?= $arResult["arTextServices"]["AKT"]["file_type"]; ?>">
                                                    <span class="field-file__title" data-default-title="Загрузить">Удалить файл</span>
                                                </label>
                                                <input type="hidden" name="akt_id" data-editfile
                                                       value="<?= $editElement["files"]["AKT"]["info"]['ID']; ?>"/>
                                            </div>
                                        <? else: ?>
                                            <div class="lk__form-file">
                                                <div class="lk__download">
                                                    <div class="lk__download-title <?= $arResult["arTextServices"]["AKT"]["req2"]; ?>">
                                                        <?= $arResult["arTextServices"]["AKT"]["name"]; ?>
                                                    </div>
                                                    <? if (!empty($arResult["arTextServices"]["AKT"]["dop_filds"]["UF_SAMPLE"])): ?>
                                                        <a href="<?= CFile::GetPath($arResult["arTextServices"]["AKT"]["dop_filds"]["UF_SAMPLE"]); ?>"
                                                           class="lk__download-link" download>
                                                            Скачать форму
                                                        </a>
                                                    <? endif; ?>
                                                </div>
                                                <label class='field-file'>
                                                    <input class="field-file__input js-file" type="file" name="AKT"
                                                           <?= $editElement["type_id"] == 8 ? $arResult["arTextServices"]["AKT"]["req"] : ''; ?>
                                                           data-type="<?= $arResult["arTextServices"]["AKT"]["file_type"]; ?>">
                                                    <span class="field-file__title">Загрузить</span>
                                                </label>
                                            </div>
                                        <? endif; ?>
                                    </div>
                                    <? if ($arResult["arTextServices"]["AKT"]["text"]): ?>
                                        <div class="hint">
                                            <div class="hint__message">
                                                <?= $arResult["arTextServices"]["AKT"]["text"]; ?>
                                            </div>
                                        </div>
                                    <? endif; ?>
                                    <? if ($arResult["arTextServices"]["AKT"]["req"]): ?>
                                        <div class="tooltip js-error-tooltip">
                                            Поле <?= $arResult["arTextServices"]["AKT"]["name"]; ?> не может быть пустым
                                        </div>
                                    <? endif; ?>
                                </div>
                                <div class="lk__form-row js-field"
                                     data-req="<?= $arResult["arTextServices"]["INVOICE"]["req"] ? 'Y' : 'N'; ?>">
                                    <div class="lk__form-field <?= $arResult["arTextServices"]["INVOICE"]["req"]; ?>">
                                        <? if (!empty($editElement["files"]["INVOICE"])): ?>
                                            <div class="lk__form-file selected">
                                                <div class="lk__download">
                                                    <div class="lk__download-title"
                                                         data-default-title="<?= $arResult["arTextServices"]["INVOICE"]["name"]; ?>">
                                                        <?= $editElement["files"]["INVOICE"]["info"]['FILE_NAME']; ?>
                                                    </div>
                                                    <? if (!empty($arResult["arTextServices"]["INVOICE"]["dop_filds"]["UF_SAMPLE"])): ?>
                                                        <a href="<?= CFile::GetPath($arResult["arTextServices"]["INVOICE"]["dop_filds"]["UF_SAMPLE"]); ?>"
                                                           class="lk__download-link" download>
                                                            Скачать форму
                                                        </a>
                                                    <? endif; ?>
                                                </div>
                                                <label class="field-file">
                                                    <input class="field-file__input js-file" type="file" name="INVOICE"
                                                           <?= $editElement["type_id"] == 8 ? $arResult["arTextServices"]["INVOICE"]["req"] : ''; ?>
                                                           data-type="<?= $arResult["arTextServices"]["INVOICE"]["file_type"]; ?>">
                                                    <span class="field-file__title" data-default-title="Загрузить">Удалить файл</span>
                                                </label>
                                                <input type="hidden" name="invoice_id" data-editfile
                                                       value="<?= $editElement["files"]["INVOICE"]["info"]['ID']; ?>"/>
                                            </div>
                                        <? else: ?>
                                            <div class="lk__form-file">
                                                <div class="lk__download">
                                                    <div class="lk__download-title <?= $arResult["arTextServices"]["INVOICE"]["req2"]; ?>">
                                                        <?= $arResult["arTextServices"]["INVOICE"]["name"]; ?>
                                                    </div>
                                                    <? if (!empty($arResult["arTextServices"]["INVOICE"]["dop_filds"]["UF_SAMPLE"])): ?>
                                                        <a href="<?= CFile::GetPath($arResult["arTextServices"]["INVOICE"]["dop_filds"]["UF_SAMPLE"]); ?>"
                                                           class="lk__download-link" download>
                                                            Скачать форму
                                                        </a>
                                                    <? endif; ?>
                                                </div>
                                                <label class='field-file'>
                                                    <input class="field-file__input js-file" type="file" name="INVOICE"
                                                           <?= $editElement["type_id"] == 8 ? $arResult["arTextServices"]["INVOICE"]["req"] : ''; ?>
                                                           data-type="<?= $arResult["arTextServices"]["INVOICE"]["file_type"]; ?>">
                                                    <span class="field-file__title">Загрузить</span>
                                                </label>
                                            </div>
                                        <? endif; ?>
                                    </div>
                                    <? if ($arResult["arTextServices"]["INVOICE"]["text"]): ?>
                                        <div class="hint">
                                            <div class="hint__message">
                                                <?= $arResult["arTextServices"]["INVOICE"]["text"]; ?>
                                            </div>
                                        </div>
                                    <? endif; ?>
                                    <? if ($arResult["arTextServices"]["INVOICE"]["req"]): ?>
                                        <div class="tooltip js-error-tooltip">
                                            Поле <?= $arResult["arTextServices"]["INVOICE"]["name"]; ?> не может быть
                                            пустым
                                        </div>
                                    <? endif; ?>
                                </div>
                                <? if (!empty($editElement["files"]["INVOICE_PAYMENT"]["info"])): ?>
                                    <? foreach ($editElement["files"]["INVOICE_PAYMENT"]["info"] as $file): ?>
                                        <div class="lk__form-row js-field"
                                             data-req="<?= $arResult["arTextServices"]["INVOICE_PAYMENT"]["req"] ? 'Y' : 'N'; ?>">
                                            <div class="lk__form-field <?= $arResult["arTextServices"]["INVOICE_PAYMENT"]["req"]; ?>">
                                                <div class="lk__form-file selected">
                                                    <div class="lk__download">
                                                        <div class="lk__download-title"
                                                             data-default-title="<?= $arResult["arTextServices"]["INVOICE_PAYMENT"]["name"]; ?>">
                                                            <?= $file['FILE_NAME']; ?>
                                                        </div>
                                                        <? if (!empty($arResult["arTextServices"]["INVOICE_PAYMENT"]["dop_filds"]["UF_SAMPLE"])): ?>
                                                            <a href="<?= CFile::GetPath($arResult["arTextServices"]["INVOICE_PAYMENT"]["dop_filds"]["UF_SAMPLE"]); ?>"
                                                               class="lk__download-link" download>
                                                                Скачать форму
                                                            </a>
                                                        <? endif; ?>
                                                    </div>
                                                    <label class="field-file">
                                                        <input class="field-file__input js-file" type="file"
                                                               name="INVOICE_PAYMENT[]"
                                                               <?= $editElement["type_id"] == 8 ? $arResult["arTextServices"]["INVOICE_PAYMENT"]["req"] : ''; ?>
                                                               data-type="<?= $arResult["arTextServices"]["INVOICE_PAYMENT"]["file_type"]; ?>">
                                                        <span class="field-file__title" data-default-title="Загрузить">Удалить файл</span>
                                                    </label>
                                                    <input type="hidden" name="invoice_payment_id[]" data-editfile
                                                           value="<?= $file['ID']; ?>"/>
                                                </div>
                                            </div>
                                            <? if ($arResult["arTextServices"]["INVOICE_PAYMENT"]["text"]): ?>
                                                <div class="hint">
                                                    <div class="hint__message">
                                                        <?= $arResult["arTextServices"]["INVOICE_PAYMENT"]["text"]; ?>
                                                    </div>
                                                </div>
                                            <? endif; ?>
                                            <? if ($arResult["arTextServices"]["INVOICE_PAYMENT"]["req"]): ?>
                                                <div class="tooltip js-error-tooltip">
                                                    Поле <?= $arResult["arTextServices"]["INVOICE_PAYMENT"]["name"]; ?>
                                                    не может быть пустым
                                                </div>
                                            <? endif; ?>
                                        </div>
                                    <? endforeach; ?>
                                <? else: ?>
                                    <div class="lk__form-row js-field"
                                         data-req="<?= $arResult["arTextServices"]["INVOICE_PAYMENT"]["req"] ? 'Y' : 'N'; ?>">
                                        <div class="lk__form-field <?= $arResult["arTextServices"]["INVOICE_PAYMENT"]["req"]; ?>">
                                            <div class="lk__form-file">
                                                <div class="lk__download">
                                                    <div class="lk__download-title <?= $arResult["arTextServices"]["INVOICE_PAYMENT"]["req2"]; ?>">
                                                        <?= $arResult["arTextServices"]["INVOICE_PAYMENT"]["name"]; ?>
                                                    </div>
                                                    <? if (!empty($arResult["arTextServices"]["INVOICE_PAYMENT"]["dop_filds"]["UF_SAMPLE"])): ?>
                                                        <a href="<?= CFile::GetPath($arResult["arTextServices"]["INVOICE_PAYMENT"]["dop_filds"]["UF_SAMPLE"]); ?>"
                                                           class="lk__download-link" download>
                                                            Скачать форму
                                                        </a>
                                                    <? endif; ?>
                                                </div>
                                                <label class='field-file'>
                                                    <input class="field-file__input" type="file"
                                                           name="INVOICE_PAYMENT[]"
                                                           <?= $editElement["type_id"] == 8 ? $arResult["arTextServices"]["INVOICE_PAYMENT"]["req"] : ''; ?>
                                                           data-type="<?= $arResult["arTextServices"]["INVOICE_PAYMENT"]["file_type"]; ?>">
                                                    <span class="field-file__title">Загрузить</span>
                                                </label>
                                            </div>
                                        </div>
                                        <? if ($arResult["arTextServices"]["INVOICE_PAYMENT"]["text"]): ?>
                                            <div class="hint">
                                                <div class="hint__message">
                                                    <?= $arResult["arTextServices"]["INVOICE_PAYMENT"]["text"]; ?>
                                                </div>
                                            </div>
                                        <? endif; ?>
                                        <? if ($arResult["arTextServices"]["INVOICE_PAYMENT"]["req"]): ?>
                                            <div class="tooltip js-error-tooltip">
                                                Поле <?= $arResult["arTextServices"]["INVOICE_PAYMENT"]["name"]; ?> не
                                                может быть пустым
                                            </div>
                                        <? endif; ?>
                                    </div>
                                <? endif; ?>
                                <div class="lk__form-row">
                                    <a class="lk__form-add js-lk-row-add" href="javascript:void(0)">Еще</a>
                                </div>
                                <div class="lk__form-req-text">
                                    <b>*</b> – поля, обязательные для заполнения
                                </div>
                                <button class="btn lk__form-submit" type="submit" data-click="upHref"
                                        data-url="/dealer/service/">Отправить
                                </button>
                                <span class="mess"></span>
                            </div>
                        </fieldset>
                    </div>
                    <div class="lk__form-fieldset-list js-tabs-select-content">
                        <fieldset <?= $editElement["type_id"] == 9 ? '' : 'disabled'; ?> class="lk__form-fieldset"
                                  data-form-fieldset="9">
                            <div class="lk__form-section"
                                 data-req="<?= $arResult["arTextServices"]["SERIAL_NUMBER"]["req"] ? 'Y' : 'N'; ?>">
                                <div class="lk__form-title">
                                    Укажите рекламацию (по номеру техники):
                                </div>
                                <? if (!empty($editElement["arReclamationTech"])): ?>
                                    <? foreach ($editElement["arReclamationTech"] as $recl): ?>
                                        <div class="lk__form-box">
                                            <div class="lk__form-row lk__form-row_long js-field">
                                                <div class="lk__form-field <?= $arResult["arTextServices"]["SERIAL_NUMBER"]["req"]; ?>">
                                                    <label class="field focused">
                                                        <span class="field__title <?= $arResult["arTextServices"]["SERIAL_NUMBER"]["req2"]; ?>">Введите <?= $arResult["arTextServices"]["SERIAL_NUMBER"]["name"]; ?></span>
                                                        <input class="field__input js-autocomplete" type="text"
                                                               value="<?= $recl["number"]; ?>" name="SERIAL_NUMBER[]"
                                                               <?= $editElement["type_id"] == 9 ? $arResult["arTextServices"]["SERIAL_NUMBER"]["req"] : ''; ?>
                                                               data-path="/ajax/dealer/getExplotationSerialRecl/">
                                                    </label>
                                                </div>
                                                <? if ($arResult["arTextServices"]["SERIAL_NUMBER"]["text"]): ?>
                                                    <div class="hint">
                                                        <div class="hint__message">
                                                            <?= $arResult["arTextServices"]["SERIAL_NUMBER"]["text"]; ?>
                                                        </div>
                                                    </div>
                                                <? endif; ?>
                                                <? if ($arResult["arTextServices"]["SERIAL_NUMBER"]["req"]): ?>
                                                    <div class="tooltip js-error-tooltip">
                                                        Поле <?= $arResult["arTextServices"]["SERIAL_NUMBER"]["name"]; ?>
                                                        не может быть пустым
                                                    </div>
                                                <? endif; ?>
                                            </div>
                                            <? if (!empty($recl["infoTech"])): ?>
                                                <div class="lk__form-desc js-autocomplete-text"
                                                     data-def-text="Название техники (<span>появится автоматически после ввода заводского номера</span>)">
                                                    <?= $recl["number"]; ?> <?= $recl["infoTech"]["type"]; ?> <?= $recl["infoTech"]["model"]; ?>
                                                </div>
                                                <input type="hidden" class="js-autocomplete-hidden-input"
                                                       name="id_explotation[]" value="<?= $recl["infoTech"]["id"]; ?>"
                                                       <?= $editElement["type_id"] == 9 ? $arResult["arTextServices"]["SERIAL_NUMBER"]["req"] : ''; ?>/>
                                            <? else: ?>
                                                <div class="lk__form-desc js-autocomplete-text"
                                                     data-def-text="Название техники (<span>появится автоматически после ввода заводского номера</span>)">
                                                    Название рекламации (<span>появится автоматически после ввода заводского номера</span>)
                                                </div>
                                                <input type="hidden" class="js-autocomplete-hidden-input"
                                                       name="id_explotation[]" value=""
                                                       <?= $editElement["type_id"] == 9 ? $arResult["arTextServices"]["SERIAL_NUMBER"]["req"] : ''; ?>/>
                                            <? endif; ?>
                                            <input type="hidden" class="js-autocomplete-hidden-input"
                                                   name="id_reclamation[]" value="<?= $recl["id"]; ?>"
                                                   <?= $editElement["type_id"] == 9 ? $arResult["arTextServices"]["RECLAMATION_TECH"]["req"] : ''; ?>/>
                                        </div>
                                    <? endforeach; ?>
                                <? else: ?>
                                    <div class="lk__form-box">
                                        <div class="lk__form-row lk__form-row_long js-field">
                                            <div class="lk__form-field <?= $arResult["arTextServices"]["SERIAL_NUMBER"]["req"]; ?>">
                                                <label class="field">
                                                    <span class="field__title <?= $arResult["arTextServices"]["SERIAL_NUMBER"]["req2"]; ?>">Введите <?= $arResult["arTextServices"]["SERIAL_NUMBER"]["name"]; ?></span>
                                                    <input class="field__input js-autocomplete" type="text"
                                                           name="SERIAL_NUMBER[]"
                                                           <?= $editElement["type_id"] == 9 ? $arResult["arTextServices"]["SERIAL_NUMBER"]["req"] : ''; ?>
                                                           data-path="/ajax/dealer/getExplotationSerialRecl/">
                                                </label>
                                            </div>
                                            <? if ($arResult["arTextServices"]["SERIAL_NUMBER"]["text"]): ?>
                                                <div class="hint">
                                                    <div class="hint__message">
                                                        <?= $arResult["arTextServices"]["SERIAL_NUMBER"]["text"]; ?>
                                                    </div>
                                                </div>
                                            <? endif; ?>
                                            <? if ($arResult["arTextServices"]["SERIAL_NUMBER"]["req"]): ?>
                                                <div class="tooltip js-error-tooltip">
                                                    Поле <?= $arResult["arTextServices"]["SERIAL_NUMBER"]["name"]; ?> не
                                                    может быть пустым
                                                </div>
                                            <? endif; ?>
                                        </div>
                                        <div class="lk__form-desc js-autocomplete-text"
                                             data-def-text="Название техники (<span>появится автоматически после ввода заводского номера</span>)">
                                            Название рекламации (<span>появится автоматически после ввода заводского номера</span>)
                                        </div>
                                        <input type="hidden" class="js-autocomplete-hidden-input"
                                               name="id_explotation[]" value=""
                                               <?= $editElement["type_id"] == 9 ? $arResult["arTextServices"]["SERIAL_NUMBER"]["req"] : ''; ?>/>
                                        <input type="hidden" class="js-autocomplete-hidden-input"
                                               name="id_reclamation[]" value=""
                                               <?= $editElement["type_id"] == 9 ? $arResult["arTextServices"]["RECLAMATION_TECH"]["req"] : ''; ?>/>
                                    </div>
                                <? endif; ?>
                                <div class="lk__form-row">
                                    <a class="lk__form-add js-lk-row-add" href="javascript:void(0)">Еще</a>
                                </div>
                            </div>
                            <div class="lk__form-section">
                                <div class="lk__form-title">
                                    Прикрепите необходимые документы
                                </div>
                                <div class="lk__form-row js-field"
                                     data-req="<?= $arResult["arTextServices"]["AKT"]["req"] ? 'Y' : 'N'; ?>">
                                    <div class="lk__form-field <?= $arResult["arTextServices"]["AKT"]["req"]; ?>">
                                        <? if (!empty($editElement["files"]["AKT"])): ?>
                                            <div class="lk__form-file selected">
                                                <div class="lk__download">
                                                    <div class="lk__download-title"
                                                         data-default-title="<?= $arResult["arTextServices"]["AKT"]["name"]; ?>">
                                                        <?= $editElement["files"]["AKT"]["info"]['FILE_NAME']; ?>
                                                    </div>
                                                    <? if (!empty($arResult["arTextServices"]["AKT"]["dop_filds"]["UF_SAMPLE"])): ?>
                                                        <a href="<?= CFile::GetPath($arResult["arTextServices"]["AKT"]["dop_filds"]["UF_SAMPLE"]); ?>"
                                                           class="lk__download-link" download>
                                                            Скачать форму
                                                        </a>
                                                    <? endif; ?>
                                                </div>
                                                <label class="field-file">
                                                    <input class="field-file__input js-file" type="file" name="AKT"
                                                           <?= $editElement["type_id"] == 9 ? $arResult["arTextServices"]["AKT"]["req"] : ''; ?>
                                                           data-type="<?= $arResult["arTextServices"]["AKT"]["file_type"]; ?>">
                                                    <span class="field-file__title" data-default-title="Загрузить">Удалить файл</span>
                                                </label>
                                                <input type="hidden" name="akt_id" data-editfile
                                                       value="<?= $editElement["files"]["AKT"]["info"]['ID']; ?>"/>
                                            </div>
                                        <? else: ?>
                                            <div class="lk__form-file">
                                                <div class="lk__download">
                                                    <div class="lk__download-title <?= $arResult["arTextServices"]["AKT"]["req2"]; ?>">
                                                        <?= $arResult["arTextServices"]["AKT"]["name"]; ?>
                                                    </div>
                                                    <? if (!empty($arResult["arTextServices"]["AKT"]["dop_filds"]["UF_SAMPLE"])): ?>
                                                        <a href="<?= CFile::GetPath($arResult["arTextServices"]["AKT"]["dop_filds"]["UF_SAMPLE"]); ?>"
                                                           class="lk__download-link" download>
                                                            Скачать форму
                                                        </a>
                                                    <? endif; ?>
                                                </div>
                                                <label class='field-file'>
                                                    <input class="field-file__input js-file" type="file" name="AKT"
                                                           <?= $editElement["type_id"] == 9 ? $arResult["arTextServices"]["AKT"]["req"] : ''; ?>
                                                           data-type="<?= $arResult["arTextServices"]["AKT"]["file_type"]; ?>">
                                                    <span class="field-file__title">Загрузить</span>
                                                </label>
                                            </div>
                                        <? endif; ?>
                                    </div>
                                    <? if ($arResult["arTextServices"]["AKT"]["text"]): ?>
                                        <div class="hint">
                                            <div class="hint__message">
                                                <?= $arResult["arTextServices"]["AKT"]["text"]; ?>
                                            </div>
                                        </div>
                                    <? endif; ?>
                                    <? if ($arResult["arTextServices"]["AKT"]["req"]): ?>
                                        <div class="tooltip js-error-tooltip">
                                            Поле <?= $arResult["arTextServices"]["AKT"]["name"]; ?> не может быть пустым
                                        </div>
                                    <? endif; ?>
                                </div>
                                <div class="lk__form-row js-field"
                                     data-req="<?= $arResult["arTextServices"]["INVOICE"]["req"] ? 'Y' : 'N'; ?>">
                                    <div class="lk__form-field <?= $arResult["arTextServices"]["INVOICE"]["req"]; ?>">
                                        <? if (!empty($editElement["files"]["INVOICE"])): ?>
                                            <div class="lk__form-file selected">
                                                <div class="lk__download">
                                                    <div class="lk__download-title"
                                                         data-default-title="<?= $arResult["arTextServices"]["INVOICE"]["name"]; ?>">
                                                        <?= $editElement["files"]["INVOICE"]["info"]['FILE_NAME']; ?>
                                                    </div>
                                                    <? if (!empty($arResult["arTextServices"]["INVOICE"]["dop_filds"]["UF_SAMPLE"])): ?>
                                                        <a href="<?= CFile::GetPath($arResult["arTextServices"]["INVOICE"]["dop_filds"]["UF_SAMPLE"]); ?>"
                                                           class="lk__download-link" download>
                                                            Скачать форму
                                                        </a>
                                                    <? endif; ?>
                                                </div>
                                                <label class="field-file">
                                                    <input class="field-file__input js-file" type="file" name="INVOICE"
                                                           <?= $editElement["type_id"] == 9 ? $arResult["arTextServices"]["INVOICE"]["req"] : ''; ?>
                                                           data-type="<?= $arResult["arTextServices"]["INVOICE"]["file_type"]; ?>">
                                                    <span class="field-file__title" data-default-title="Загрузить">Удалить файл</span>
                                                </label>
                                                <input type="hidden" name="invoice_id" data-editfile
                                                       value="<?= $editElement["files"]["INVOICE"]["info"]['ID']; ?>"/>
                                            </div>
                                        <? else: ?>
                                            <div class="lk__form-file">
                                                <div class="lk__download">
                                                    <div class="lk__download-title <?= $arResult["arTextServices"]["INVOICE"]["req2"]; ?>">
                                                        <?= $arResult["arTextServices"]["INVOICE"]["name"]; ?>
                                                    </div>
                                                    <? if (!empty($arResult["arTextServices"]["INVOICE"]["dop_filds"]["UF_SAMPLE"])): ?>
                                                        <a href="<?= CFile::GetPath($arResult["arTextServices"]["INVOICE"]["dop_filds"]["UF_SAMPLE"]); ?>"
                                                           class="lk__download-link" download>
                                                            Скачать форму
                                                        </a>
                                                    <? endif; ?>
                                                </div>
                                                <label class='field-file'>
                                                    <input class="field-file__input js-file" type="file" name="INVOICE"
                                                           <?= $editElement["type_id"] == 9 ? $arResult["arTextServices"]["INVOICE"]["req"] : ''; ?>
                                                           data-type="<?= $arResult["arTextServices"]["INVOICE"]["file_type"]; ?>">
                                                    <span class="field-file__title">Загрузить</span>
                                                </label>
                                            </div>
                                        <? endif; ?>
                                    </div>
                                    <? if ($arResult["arTextServices"]["INVOICE"]["text"]): ?>
                                        <div class="hint">
                                            <div class="hint__message">
                                                <?= $arResult["arTextServices"]["INVOICE"]["text"]; ?>
                                            </div>
                                        </div>
                                    <? endif; ?>
                                    <? if ($arResult["arTextServices"]["INVOICE"]["req"]): ?>
                                        <div class="tooltip js-error-tooltip">
                                            Поле <?= $arResult["arTextServices"]["INVOICE"]["name"]; ?> не может быть
                                            пустым
                                        </div>
                                    <? endif; ?>
                                </div>
                                <? if (!empty($editElement["files"]["INVOICE_PAYMENT"]["info"])): ?>
                                    <? foreach ($editElement["files"]["INVOICE_PAYMENT"]["info"] as $file): ?>
                                        <div class="lk__form-row js-field"
                                             data-req="<?= $arResult["arTextServices"]["INVOICE_PAYMENT"]["req"] ? 'Y' : 'N'; ?>">
                                            <div class="lk__form-field <?= $arResult["arTextServices"]["INVOICE_PAYMENT"]["req"]; ?>">
                                                <div class="lk__form-file selected">
                                                    <div class="lk__download">
                                                        <div class="lk__download-title"
                                                             data-default-title="<?= $arResult["arTextServices"]["INVOICE_PAYMENT"]["name"]; ?>">
                                                            <?= $file['FILE_NAME']; ?>
                                                        </div>
                                                        <? if (!empty($arResult["arTextServices"]["INVOICE_PAYMENT"]["dop_filds"]["UF_SAMPLE"])): ?>
                                                            <a href="<?= CFile::GetPath($arResult["arTextServices"]["INVOICE_PAYMENT"]["dop_filds"]["UF_SAMPLE"]); ?>"
                                                               class="lk__download-link" download>
                                                                Скачать форму
                                                            </a>
                                                        <? endif; ?>
                                                    </div>
                                                    <label class="field-file">
                                                        <input class="field-file__input js-file" type="file"
                                                               name="INVOICE_PAYMENT[]"
                                                               <?= $editElement["type_id"] == 9 ? $arResult["arTextServices"]["INVOICE_PAYMENT"]["req"] : ''; ?>
                                                               data-type="<?= $arResult["arTextServices"]["INVOICE_PAYMENT"]["file_type"]; ?>">
                                                        <span class="field-file__title" data-default-title="Загрузить">Удалить файл</span>
                                                    </label>
                                                    <input type="hidden" name="invoice_payment_id[]" data-editfile
                                                           value="<?= $file['ID']; ?>"/>
                                                </div>
                                            </div>
                                            <? if ($arResult["arTextServices"]["INVOICE_PAYMENT"]["text"]): ?>
                                                <div class="hint">
                                                    <div class="hint__message">
                                                        <?= $arResult["arTextServices"]["INVOICE_PAYMENT"]["text"]; ?>
                                                    </div>
                                                </div>
                                            <? endif; ?>
                                            <? if ($arResult["arTextServices"]["INVOICE_PAYMENT"]["req"]): ?>
                                                <div class="tooltip js-error-tooltip">
                                                    Поле <?= $arResult["arTextServices"]["INVOICE_PAYMENT"]["name"]; ?>
                                                    не может быть пустым
                                                </div>
                                            <? endif; ?>
                                        </div>
                                    <? endforeach; ?>
                                <? else: ?>
                                    <div class="lk__form-row js-field"
                                         data-req="<?= $arResult["arTextServices"]["INVOICE_PAYMENT"]["req"] ? 'Y' : 'N'; ?>">
                                        <div class="lk__form-field <?= $arResult["arTextServices"]["INVOICE_PAYMENT"]["req"]; ?>">
                                            <div class="lk__form-file">
                                                <div class="lk__download">
                                                    <div class="lk__download-title <?= $arResult["arTextServices"]["INVOICE_PAYMENT"]["req2"]; ?>">
                                                        <?= $arResult["arTextServices"]["INVOICE_PAYMENT"]["name"]; ?>
                                                    </div>
                                                    <? if (!empty($arResult["arTextServices"]["INVOICE_PAYMENT"]["dop_filds"]["UF_SAMPLE"])): ?>
                                                        <a href="<?= CFile::GetPath($arResult["arTextServices"]["INVOICE_PAYMENT"]["dop_filds"]["UF_SAMPLE"]); ?>"
                                                           class="lk__download-link" download>
                                                            Скачать форму
                                                        </a>
                                                    <? endif; ?>
                                                </div>
                                                <label class='field-file'>
                                                    <input class="field-file__input" type="file"
                                                           name="INVOICE_PAYMENT[]"
                                                           <?= $editElement["type_id"] == 9 ? $arResult["arTextServices"]["INVOICE_PAYMENT"]["req"] : ''; ?>
                                                           data-type="<?= $arResult["arTextServices"]["INVOICE_PAYMENT"]["file_type"]; ?>">
                                                    <span class="field-file__title">Загрузить</span>
                                                </label>
                                            </div>
                                        </div>
                                        <? if ($arResult["arTextServices"]["INVOICE_PAYMENT"]["text"]): ?>
                                            <div class="hint">
                                                <div class="hint__message">
                                                    <?= $arResult["arTextServices"]["INVOICE_PAYMENT"]["text"]; ?>
                                                </div>
                                            </div>
                                        <? endif; ?>
                                        <? if ($arResult["arTextServices"]["INVOICE_PAYMENT"]["req"]): ?>
                                            <div class="tooltip js-error-tooltip">
                                                Поле <?= $arResult["arTextServices"]["INVOICE_PAYMENT"]["name"]; ?> не
                                                может быть пустым
                                            </div>
                                        <? endif; ?>
                                    </div>
                                <? endif; ?>
                                <div class="lk__form-row">
                                    <a class="lk__form-add js-lk-row-add" href="javascript:void(0)">Еще</a>
                                </div>
                            </div>
                            <div class="lk__form-section">
                                <div class="lk__form-row js-field"
                                     data-req="<?= $arResult["arTextServices"]["PUT_LIST"]["req"] ? 'Y' : 'N'; ?>">
                                    <div class="lk__form-field <?= $arResult["arTextServices"]["PUT_LIST"]["req"]; ?>">
                                        <? if (!empty($editElement["files"]["PUT_LIST"])): ?>
                                            <div class="lk__form-file selected">
                                                <div class="lk__download">
                                                    <div class="lk__download-title"
                                                         data-default-title="<?= $arResult["arTextServices"]["PUT_LIST"]["name"]; ?>">
                                                        <?= $editElement["files"]["PUT_LIST"]["info"]['FILE_NAME']; ?>
                                                    </div>
                                                    <? if (!empty($arResult["arTextServices"]["PUT_LIST"]["dop_filds"]["UF_SAMPLE"])): ?>
                                                        <a href="<?= CFile::GetPath($arResult["arTextServices"]["PUT_LIST"]["dop_filds"]["UF_SAMPLE"]); ?>"
                                                           class="lk__download-link" download>
                                                            Скачать форму
                                                        </a>
                                                    <? endif; ?>
                                                </div>
                                                <label class="field-file">
                                                    <input class="field-file__input js-file" type="file" name="PUT_LIST"
                                                           <?= $editElement["type_id"] == 9 ? $arResult["arTextServices"]["PUT_LIST"]["req"] : ''; ?>
                                                           data-type="<?= $arResult["arTextServices"]["PUT_LIST"]["file_type"]; ?>">
                                                    <span class="field-file__title" data-default-title="Загрузить">Удалить файл</span>
                                                </label>
                                                <input type="hidden" name="put_list_id" data-editfile
                                                       value="<?= $editElement["files"]["PUT_LIST"]["info"]['ID']; ?>"/>
                                            </div>
                                        <? else: ?>
                                            <div class="lk__form-file">
                                                <div class="lk__download">
                                                    <div class="lk__download-title <?= $arResult["arTextServices"]["PUT_LIST"]["req2"]; ?>">
                                                        <?= $arResult["arTextServices"]["PUT_LIST"]["name"]; ?>
                                                    </div>
                                                    <? if (!empty($arResult["arTextServices"]["PUT_LIST"]["dop_filds"]["UF_SAMPLE"])): ?>
                                                        <a href="<?= CFile::GetPath($arResult["arTextServices"]["PUT_LIST"]["dop_filds"]["UF_SAMPLE"]); ?>"
                                                           class="lk__download-link" download>
                                                            Скачать форму
                                                        </a>
                                                    <? endif; ?>
                                                </div>
                                                <label class='field-file'>
                                                    <input class="field-file__input" type="file" name="PUT_LIST"
                                                           <?= $editElement["type_id"] == 9 ? $arResult["arTextServices"]["PUT_LIST"]["req"] : ''; ?>
                                                           data-type="<?= $arResult["arTextServices"]["PUT_LIST"]["file_type"]; ?>">
                                                    <span class="field-file__title">Загрузить</span>
                                                </label>
                                            </div>
                                        <? endif; ?>
                                    </div>
                                    <? if ($arResult["arTextServices"]["PUT_LIST"]["text"]): ?>
                                        <div class="hint">
                                            <div class="hint__message">
                                                <?= $arResult["arTextServices"]["PUT_LIST"]["text"]; ?>
                                            </div>
                                        </div>
                                    <? endif; ?>
                                    <? if ($arResult["arTextServices"]["PUT_LIST"]["req"]): ?>
                                        <div class="tooltip js-error-tooltip">
                                            Поле <?= $arResult["arTextServices"]["PUT_LIST"]["name"]; ?> не может быть
                                            пустым
                                        </div>
                                    <? endif; ?>
                                </div>
                            </div>
                            <div class="lk__form-req-text">
                                <b>*</b> – поля, обязательные для заполнения
                            </div>
                            <button class="btn lk__form-submit" type="submit" data-click="upHref"
                                    data-url="/dealer/service/">Отправить
                            </button>
                            <span class="mess"></span>
                    </div>
                    </fieldset>
            </div>
            <? if (!empty($editElement)): ?>
                <input type="hidden" name="edit" value="<?= $editElement['id']; ?>">
            <? endif; ?>
            </form>
</div>
</article>
</section>
</div>