<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}

use Letsrock\Lib\Models\User;
use Letsrock\Lib\Models\Element;
use Letsrock\Lib\Models\HLBlock;

\Bitrix\Main\Loader::includeModule('letsrock.lib');

class ServiceComponent extends CBitrixComponent
{
    public function propIsDop($code, $iblock)
    {
        $prMarkHl = new HLBlock(HL_POPSDOP);
        $arMarkProp_db = $prMarkHl->get(["UF_CODE_PROP" => $code, "UF_IBLOCK_ID"=>$iblock]);
        if (!empty($arMarkProp_db)) {
            return $arMarkProp_db[0];
        } else {
            return [];
        }
    }

    public function onPrepareComponentParams($arParams)
    {
        $result = $arParams;

        return $result;
    }

    /**
     * @return array|mixed
     * @throws \Bitrix\Main\LoaderException
     */
    public function executeComponent()
    {
        global $USER;
        if ($this->startResultCache($this->arParams["CACHE_TIME"]) && $USER->GetID()) {
            $elemIbl = new Element;
            $typeTechHl = new HLBlock(HL_EQUIPMENT);
            if(!empty($this->arParams["EDTI_ID"])){
                $infoServices = $elemIbl->GetList(IB_SERVCOMP,
                    ["ID", "IBLOCK_ID", "NAME", "CODE", "DATE_ACTIVE_FROM", "DATE_CREATE", "PROPERTY_*"],
                    [
                        "ID" => $this->arParams["EDTI_ID"],
                        "ACTIVE_DATE" => "Y",
                        "ACTIVE" => "Y",
                        "PROPERTY_DILER" => $USER->GetID()
                    ],
                    ["ID"=>"desc"]
                );
            }else{
                $infoServices = $elemIbl->GetList(IB_SERVCOMP,
                    ["ID", "IBLOCK_ID", "NAME", "CODE", "DATE_ACTIVE_FROM", "DATE_CREATE", "PROPERTY_*"],
                    [
                        "ACTIVE_DATE" => "Y",
                        "ACTIVE" => "Y",
                        "PROPERTY_DILER" => $USER->GetID()
                    ],
                    ["ID"=>"desc"]
                );
            }
            $arServices = [];
            foreach ($infoServices["ELEMS"] as $key => $service) {
                $arProps = $infoServices["PROPS"][$service["ID"]];
                $type = $arProps["TYPE_SERVICE"]["~VALUE"];
                $type_id = $arProps["TYPE_SERVICE"]["VALUE_ENUM_ID"];
                $status = $arProps["STATUS"]["~VALUE"];
                $status_id = $arProps["STATUS"]["VALUE_ENUM_ID"];
                unset($arProps["TYPE_SERVICE"]);
                unset($arProps["STATUS"]);
                $arFiles = [];
                foreach ($arProps as $arProp) {
                    if ($arProp["PROPERTY_TYPE"] == 'F') {
                        if(empty($arProp["VALUE"]))continue;
                        $src = [];
                        if (is_array($arProp["VALUE"])) {
                            foreach ($arProp["VALUE"] as $id) {
                                $file = CFile::GetFileArray($id);
                                if(!empty($file))
                                $src[] = $file;
                            }
                        } else {
                            $file_one = CFile::GetFileArray($arProp["VALUE"]);
                            if(!empty($file_one))
                            $src = $file_one;
                        }
                        if(!empty($src))
                        $arFiles[$arProp["CODE"]] = ["info" => $src, "name" => $arProp["NAME"], "code"=>$arProp["CODE"]];
                    }
                }

                $arTech = [];
                if(!empty($arProps["SERIAL_NUMBER"]["VALUE"])){
                    foreach ($arProps["SERIAL_NUMBER"]["VALUE"] as $serialElem) {
                        if(!empty((int)$serialElem)) {
                            $infoTech = $elemIbl->GetList(IB_EXPLOITATION, ["NAME", "ID", "PROPERTY_MODEL", "PROPERTY_EQUIPMENT", "PROPERTY_NUMBER"], ["ID" => (int)$serialElem, "!EQUIPMENT_VALUE" => false])["ELEMS"][0];
                            if ($infoTech) {
                                $infoModel = $elemIbl->getById(IB_DIRECTORY, $infoTech["PROPERTY_MODEL_VALUE"]);
                                $infoType = $typeTechHl->get(["UF_XML_ID" => $infoTech["PROPERTY_EQUIPMENT_VALUE"]])[0];
                                $numberTech = $infoTech["PROPERTY_NUMBER_VALUE"];
                                $arTech[$infoTech["ID"]] = ["number" => $numberTech, "model" => $infoModel["NAME"], "type" => $infoType["UF_NAME"], "id"=>$infoTech["ID"]];
                            }
                        }
                    }
                }
                $arReclamationTech = [];
                if(!empty($arProps["RECLAMATION_TECH"]["VALUE"])){
                    foreach ($arProps["RECLAMATION_TECH"]["VALUE"] as $reclElem) {
                        if(!empty((int)$reclElem)){
                            $infoRecl = $elemIbl->GetList(IB_RECLAMATION_TECH, ["NAME", "ID", "IBLOCK_ID", "PROPERTY_*"], ["ID" => $reclElem]);
                            $elem = $infoRecl["ELEMS"][0];
                            $propsRecl = $infoRecl["PROPS"][$elem["ID"]];
                            $infoTechRecl = [];
                            if(!empty($propsRecl["SERIAL_NUMBER"]["VALUE"])){
                                if(is_array($propsRecl["SERIAL_NUMBER"]["VALUE"])){
                                    foreach ($propsRecl["SERIAL_NUMBER"]["VALUE"] as $itemIdsTech){
                                        $infoTechRecl = $arTech[$itemIdsTech];
                                    }
                                }else{
                                    $infoTechRecl = $arTech[$propsRecl["SERIAL_NUMBER"]["VALUE"]];
                                }
                            }
                            $akt_parse = [];
                            if(!empty($propsRecl["AKT"]["VALUE"])){
                                foreach ($propsRecl["AKT"]["VALUE"] as $akt2) {
                                    $arFile = CFile::GetFileArray($akt2);
                                    if($arFile)
                                        $akt_parse[] = ["name"=>$arFile["ORIGINAL_NAME"], "src"=>$arFile["SRC"]];
                                }
                            }
                            $arPhoto = [];
                            if(!empty($propsRecl["PHOTO"]["VALUE"])){
                                foreach ($propsRecl["PHOTO"]["VALUE"] as $photo) {
                                    $arFile = CFile::GetFileArray($photo);
                                    if($arFile)
                                        $arPhoto[] = ["name"=>$arFile["ORIGINAL_NAME"], "src"=>$arFile["SRC"]];
                                }
                            }
                            $arReclamationTech[] = [
                                "number"=>$elem["NAME"],
                                "id"=>$elem["ID"],
                                "infoTech"=>$infoTechRecl,
                                "refusal" => $arProps["REFUSAL"]["VALUE"]["TEXT"],
                                "reason" => $propsRecl["REASON"]["VALUE"]["TEXT"],
                                "wishes" => $propsRecl["WISHES"]["VALUE"]["TEXT"],
                                "akt" => $akt_parse,
                                "photo" => $arPhoto
                            ];
                        }
                    }
                }
                if(!empty($arReclamationTech))
                    $arTech = [];

                $arServices[] = [
                    "id" => $service["ID"],
                    "number" => 'S'.$service["ID"],
                    "type" => $type,
                    "type_id" => $type_id,
                    "date" => explode(' ', $service["DATE_CREATE"])[0],
                    "PHONE_PERSONAL" => $arProps["PHONE_PERSONAL"]["VALUE"],
                    "status" => $status,
                    "status_id"=>$status_id,
                    "files" => $arFiles,
                    "arTech" => $arTech,
                    "arReclamationTech" => $arReclamationTech
                ];
            }
            $listSelect = [];
            $db_enum_list = CIBlockProperty::GetPropertyEnum("TYPE_SERVICE", Array(), Array("IBLOCK_ID" => IB_SERVCOMP));
            while ($ar_enum_list = $db_enum_list->GetNext()) {
                $listSelect[] = $ar_enum_list;
            }

            $properties = CIBlockProperty::GetList(Array("sort" => "asc", "name" => "asc"), Array("ACTIVE" => "Y", "IBLOCK_ID" => IB_SERVCOMP));
            $arTextServices = [];
            while ($prop_fields = $properties->GetNext()) {
                if ($prop_fields["CODE"] == "STATUS")
                    unset($prop_fields);
                $req_class = '';
                $req_class2 = '';
                if ($prop_fields["IS_REQUIRED"] == 'Y') {
                    $req_class = 'required';
                    $req_class2 = 'field__required';
                }
                $arTextServices[$prop_fields["CODE"]] = [
                    "name" => $prop_fields["NAME"],
                    "file_type" => $prop_fields["FILE_TYPE"],
                    "text" => $prop_fields["HINT"],
                    "req" => $req_class,
                    "req2" => $req_class2,
                    "dop_filds" => $this->propIsDop($prop_fields["CODE"], IB_SERVCOMP),
                ];
            }
            $pagen = [];
            $arPage = [];
            if(!empty($arServices)){
                $arPage = array_chunk($arServices, $this->arParams["PAGE_ELEMS"]);
                if(count($arPage) > 1){
                    for ($i = 1; $i <= count($arPage); $i++) {
                        $pagen[] = $i;
                    }
                }
            }

            $arFullParams = [
                "listSelect" => $listSelect,
                "arTextServices" => $arTextServices,
                "arServices" => $arPage[$_REQUEST["page"]?$_REQUEST["page"]-1:0],
                "pagination"=>$pagen
            ];
            $this->arResult = $arFullParams;
            $this->includeComponentTemplate();
        }

        return $this->arResult;
    }
} ?>