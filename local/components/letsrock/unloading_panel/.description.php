<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
$arComponentDescription = array(
    "NAME" => 'Выгрузка',
    "DESCRIPTION" => 'Выгрузка',
    "PATH" => array(
        "ID" => "dv_components",
        "CHILD" => array(
            "ID" => "unloading_panel",
            "NAME" => "Выгрузка"
        )
    ),
);
?>