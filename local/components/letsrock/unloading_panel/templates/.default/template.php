<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
?>
<form action="/ajax/unloading/" method="POST" class="lk__form js-form-marketing">
    <div class="lk__form-section">
        <div class="lk__form-title">
            Выберите регион, данные которого будут выгружены (один или несколько)
        </div>
        <div class="lk__form-row-search required">
            <select class="select js-select-search js-lk-tag-select" name="region" required>
                <option disabled selected hidden>Начните вводить название региона </option>
                <?foreach ($arResult["REGIONS"] as $region):?>
                    <option value="<?=$region["ID"];?>"><?=$region["NAME"];?></option>
                <?endforeach;?>
            </select>
            <a class="btn lk__form-search-btn js-lk-tag-select-btn" href="javascript:void(0)">Выбрать</a>
        </div>
        <div class="lk__form-row">
            <div class="lk__form-tags-empty js-lk-tag-empty active">
                <span>Здесь будет выводится список выбранных для выгрузки регионов</span>
            </div>
            <div class="lk__form-tags-wrap js-lk-tag-wrap">
                <div class="lk__form-title">
                    Выбрано:
                </div>
                <ul class="lk__form-tags js-lk-tag-list" data-name="regions[]">
                </ul>
            </div>
        </div>
    </div>
    <div class="lk__form-section">
        <div class="lk__form-title">
            Укажите даты выгрузки для выбранных регионов:
        </div>
        <div class="lk__form-date-row">
            <div class="lk__form-date required">
                <label class="field field_date">
                    <span class="field__title field__required">дд.мм.гггг</span>
                    <input class="field__input js-datepicker" type="text" name="date_from" required>
                </label>
            </div>
            <div class="lk__form-date required">
                <label class="field field_date">
                    <span class="field__title field__required">дд.мм.гггг</span>
                    <input class="field__input js-datepicker" type="text" name="date_to" required>
                </label>
            </div>
        </div>
        <button class="btn lk__form-unloading-submit" type="submit">Выгрузить данные</button>
        <span class="mess"></span>
    </div>
</form>