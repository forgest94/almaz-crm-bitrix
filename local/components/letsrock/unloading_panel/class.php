<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}

use Letsrock\Lib\Models\User;
use Letsrock\Lib\Models\Element;
use Letsrock\Lib\Models\HLBlock;

\Bitrix\Main\Loader::includeModule('letsrock.lib');

class UnloadingPanelComponent extends CBitrixComponent
{
    public function onPrepareComponentParams($arParams)
    {
        $result = $arParams;

        return $result;
    }

    /**
     * @return array|mixed
     * @throws \Bitrix\Main\LoaderException
     */
    public function executeComponent()
    {
        global $USER;
        if ($this->startResultCache($this->arParams["CACHE_TIME"]) && $USER->GetID()) {
            $arRegions[] = [
                "ID" => 0,
                "NAME" => "Все регионы"
            ];
            $regions_db = Element::getList(
                IB_REGION,
                ['ID', 'NAME'],
                [
                    'ACTIVE' => 'Y',
                ],
                [
                    'NAME' => 'ASC'
                ]
            )["ELEMS"];
            foreach ($regions_db as $item) {
                $arRegions[] = $item;
            }
            $this->arResult['REGIONS'] = $arRegions;

            $this->includeComponentTemplate();
        }
        return $this->arResult;
    }
} ?>