<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}

use Letsrock\Lib\Models\User;
use Letsrock\Lib\Models\Element;
use Letsrock\Lib\Models\HLBlock;
use Letsrock\Lib\Models\Property;

\Bitrix\Main\Loader::includeModule('letsrock.lib');

class PersonalPageComponent extends CBitrixComponent
{
    public function onPrepareComponentParams($arParams)
    {
        $result = $arParams;

        return $result;
    }

    /**
     * @return array|mixed
     * @throws \Bitrix\Main\LoaderException
     */
    public function executeComponent()
    {
        global $USER;
        if ($this->startResultCache($this->arParams["CACHE_TIME"]) && $USER->GetID()) {
            $workUser = \Letsrock\Lib\Models\User::getWork();
            $arRegionsUser = [];
            if (!empty($workUser["UF_REGIONS_USER"])) {
                foreach ($workUser["UF_REGIONS_USER"] as $region) {
                    $infoRegion = CIBlockElement::GetByID($region)->GetNext();
                    if ($infoRegion) {
                        $arRegionsUser[] = $infoRegion["NAME"];
                    }
                }
            }
            $countryUser = [];
            /*if (!empty($workUser["UF_COUNTRY"])) {
                $infoCountry = CIBlockElement::GetByID($workUser["UF_COUNTRY"])->GetNext();
                $countryUser = $infoCountry;
            }*/
            $arRegions = Element::getList(
                IB_REGION,
                ['ID', 'NAME'],
                [
                    'ACTIVE' => 'Y',
                ],
                [
                    'NAME' => 'ASC'
                ]
            )["ELEMS"];
            $arCountry = []; /*Element::getList(
                IB_COUNTRY,
                ['ID', 'NAME'],
                [
                    'ACTIVE' => 'Y',
                ],
                [
                    'NAME' => 'ASC'
                ]
            )["ELEMS"];*/

            $this->arResult = ["user" => $workUser, "arRegions" => $arRegions, "arCountry" => $arCountry, "CountryUser"=>$countryUser, "arRegionsUser" => $arRegionsUser];
            $this->arResult['FIELD_NAMES']['WORKNAME'] = Property::getText('FIELD_NAMES_WORKNAME');
            $this->arResult['FIELD_NAMES']['INN'] = Property::getText('FIELD_NAMES_INN');
            $this->arResult['FIELD_NAMES']['REGION'] = Property::getText('FIELD_NAMES_REGION');
            $this->arResult['FIELD_NAMES']['BUYER'] = Property::getText('BUYER');
            $this->arResult['FIELD_NAMES']['WARRANTY'] = Property::getText('WARRANTY');
            $this->arResult['FIELD_NAMES']['NUMBER'] = Property::getText('NUMBER');
            $this->arResult['FIELD_NAMES']['TITLE'] = Property::getText('TITLE');
            $this->arResult['FIELD_NAMES']['ADDRESS'] = Property::getText('FIELD_NAMES_ADDRESS');
            $this->arResult['FIELD_NAMES']['PHONE'] = Property::getText('FIELD_NAMES_PHONE');
            $this->arResult['FIELD_NAMES']['EMAIL'] = Property::getText('FIELD_NAMES_EMAIL');
            $this->arResult['FIELD_NAMES']['CONTACT_PERSON'] = Property::getText('FIELD_NAMES_CONTACT_PERSON');
            $this->includeComponentTemplate();

        }
        return $this->arResult;
    }
} ?>