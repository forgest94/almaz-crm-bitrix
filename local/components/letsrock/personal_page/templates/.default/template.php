<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}

if (!$arResult) {
    return null;
}
?>
<div class="lk__tabs-content-inner js-tabs-content-inner active">
    <ul class="list-dotted">
        <li class="list-dotted__row">
            <span class="list-dotted__name">Название организации:</span>
            <span class="list-dotted__line"></span>
            <span class="list-dotted__value"><?= $arResult["user"]["WORK_COMPANY"]; ?></span>
        </li>

        <li class="list-dotted__row">
            <span class="list-dotted__name">Адрес:</span>
            <span class="list-dotted__line"></span>
            <span class="list-dotted__value"><?= $arResult["user"]["WORK_STATE"]; ?>, <?= $arResult["user"]["WORK_CITY"]; ?>, <?= $arResult["user"]["WORK_STREET"]; ?></span>
        </li>
        <? if (!empty($arResult["CountryUser"])): ?>
            <li class="list-dotted__row">
                <span class="list-dotted__name">Страна:</span>
                <span class="list-dotted__line"></span>
                <span class="list-dotted__value"><?= $arResult["CountryUser"]["NAME"]; ?></span>
            </li>
        <? endif; ?>
        <? if (!empty($arResult["arRegionsUser"])): ?>
            <li class="list-dotted__row">
                <span class="list-dotted__name">Закрепленные за дилером регионы:</span>
                <span class="list-dotted__line"></span>
                <span class="list-dotted__value"><?= implode(", ", $arResult["arRegionsUser"]); ?></span>
            </li>
        <? endif; ?>
        <?if(!empty($arResult["user"]["WORK_ZIP"])):?>
            <li class="list-dotted__row">
                <span class="list-dotted__name">ИНН:</span>
                <span class="list-dotted__line"></span>
                <span class="list-dotted__value"><?= $arResult["user"]["WORK_ZIP"]; ?></span>
            </li>
        <?endif;?>
        <?if(!empty($arResult["user"]["UF_BIN"])):?>
            <li class="list-dotted__row">
                <span class="list-dotted__name">БИН:</span>
                <span class="list-dotted__line"></span>
                <span class="list-dotted__value"><?= $arResult["user"]["UF_BIN"]; ?></span>
            </li>
        <?endif;?>
        <li class="list-dotted__row">
            <span class="list-dotted__name">Контактное лицо:</span>
            <span class="list-dotted__line"></span>
            <span class="list-dotted__value"><?= $arResult["user"]["LAST_NAME"]; ?> <?= $arResult["user"]["NAME"]; ?> <?= $arResult["user"]["SECOND_NAME"]; ?></span>
        </li>
        <li class="list-dotted__row">
            <span class="list-dotted__name">Телефон:</span>
            <span class="list-dotted__line"></span>
            <span class="list-dotted__value">
                            <a href="tel:<?= preg_replace('/[^0-9]/', '', $arResult["user"]["PERSONAL_PHONE"]); ?>"><?= $arResult["user"]["PERSONAL_PHONE"]; ?></a>
                          </span>
        </li>
        <li class="list-dotted__row">
            <span class="list-dotted__name">Электронная почта:</span>
            <span class="list-dotted__line"></span>
            <span class="list-dotted__value">
                            <a href="<?= $arResult["user"]["EMAIL"]; ?>"><?= $arResult["user"]["EMAIL"]; ?></a>
                          </span>
        </li>
    </ul>
    <a class="btn-border js-tabs-show-form" href="javascript:void(0)">Изменить данные</a>
</div>
<div class="lk__tabs-content-form js-tabs-form">
    <a class="btn-arrow js-tabs-back" href="javascript:void(0)">
        <i class="icon-back"></i>
        <span>Назад</span>
    </a>
    <form action="/ajax/dealer/editPersonalPage" method="POST" class="js-form-marketing">
        <div class="lk__form-section">
            <div class="lk__form-block lk__form-edit">
                <div class="lk__form-title">
                    Редактирование личной информации
                </div>
                <div class="lk__form-row js-field">
                    <div class="lk__form-field required">
                        <label class="field <?= $arResult["user"]["WORK_COMPANY"] ? 'focused' : ''; ?>">
                            <span class="field__title field__required">Название организации</span>
                            <input class="field__input" type="text" name="WORK_COMPANY"
                                   value="<?= $arResult["user"]["WORK_COMPANY"]; ?>" required>
                        </label>
                    </div>
                    <? if (!empty($arResult['FIELD_NAMES']['WORKNAME'])): ?>
                        <div class="hint">
                            <div class="hint__message">
                                <?= $arResult['FIELD_NAMES']['WORKNAME']; ?>
                            </div>
                        </div>
                    <? endif; ?>
                    <div class="tooltip">
                        Название не должно быть пустым
                    </div>
                </div>

                <div class="lk__form-row js-field">
                    <div class="lk__form-field required">
                        <label class="field <?= $arResult["user"]["WORK_STATE"] ? 'focused' : ''; ?>">
                            <span class="field__title field__required">Адрес</span>
                            <input required class="field__input" type="text" name="WORK_STATE"
                                   value="<?= !empty($arResult["user"]["WORK_STATE"]) ? $arResult["user"]["WORK_STATE"] . ', ' : ''; ?><?= !empty($arResult["user"]["WORK_CITY"]) ? $arResult["user"]["WORK_CITY"] . ', ' : ''; ?><?= !empty($arResult["user"]["WORK_STREET"]) ? $arResult["user"]["WORK_STREET"] : ''; ?>">
                        </label>
                    </div>
                    <? if (!empty($arResult['FIELD_NAMES']['ADDRESS'])): ?>
                        <div class="hint">
                            <div class="hint__message">
                                <?= $arResult['FIELD_NAMES']['ADDRESS']; ?>
                            </div>
                        </div>
                    <? endif; ?>
                    <div class="tooltip">
                        Адрес не должн быть пустым
                    </div>
                </div>
                <?if(!empty($arResult["arCountry"])):?>
                    <div class="lk__form-row">
                        <div class="lk__form-field">
                            <select class="select js-select js-tabs-select" name="country">
                                <option disabled selected hidden>Выбор страны</option>
                                <? foreach ($arResult["arCountry"] as $reg): ?>
                                    <? if (!empty($arResult["CountryUser"])): ?>
                                        <option <?= $arResult["CountryUser"]["ID"] === $reg["ID"] ? 'selected' : ''; ?>
                                                value="<?= $reg["ID"]; ?>"><?= $reg["NAME"]; ?></option>
                                    <? else: ?>
                                        <option value="<?= $reg["ID"]; ?>"><?= $reg["NAME"]; ?></option>
                                    <? endif; ?>
                                <? endforeach; ?>
                            </select>
                        </div>
                        <? if (!empty($arResult['FIELD_NAMES']['COUNTRY'])): ?>
                            <div class="hint">
                                <div class="hint__message">
                                    <?= $arResult['FIELD_NAMES']['COUNTRY']; ?>
                                </div>
                            </div>
                        <? endif; ?>
                    </div>
                <?endif;?>
                <? if (!empty($arResult["arRegionsUser"])): ?>
                    <? foreach ($arResult["arRegionsUser"] as $usRegion): ?>
                        <div class="lk__form-row">
                            <div class="lk__form-field">
                                <select class="select js-select-search" name="region[]">
                                    <? foreach ($arResult["arRegions"] as $reg): ?>
                                        <option <?= $usRegion === $reg["NAME"] ? 'selected' : ''; ?>
                                                value="<?= $reg["ID"]; ?>"><?= $reg["NAME"]; ?></option>
                                    <? endforeach; ?>
                                </select>
                            </div>
                            <? if (!empty($arResult['FIELD_NAMES']['REGION'])): ?>
                                <div class="hint">
                                    <div class="hint__message">
                                        <?= $arResult['FIELD_NAMES']['REGION']; ?>
                                    </div>
                                </div>
                            <? endif; ?>
                        </div>
                    <? endforeach; ?>
                <? else: ?>
                    <div class="lk__form-row">
                        <div class="lk__form-field">
                            <select class="select js-select-search" name="region[]">
                                <option disabled selected hidden>Выбор региона</option>
                                <? foreach ($arResult["arRegions"] as $reg): ?>
                                    <option value="<?= $reg["ID"]; ?>"><?= $reg["NAME"]; ?></option>
                                <? endforeach; ?>
                            </select>
                        </div>
                        <? if (!empty($arResult['FIELD_NAMES']['REGION'])): ?>
                            <div class="hint">
                                <div class="hint__message">
                                    <?= $arResult['FIELD_NAMES']['REGION']; ?>
                                </div>
                            </div>
                        <? endif; ?>
                    </div>
                <? endif; ?>
                <div class="lk__form-row">
                    <a class="lk__form-add js-lk-row-add" href="javascript:void(0)">Еще</a>
                </div>
                <br/>
                <div class="lk__form-fieldset-list js-tabs-select-content">
                    <fieldset <?= ($arResult["CountryUser"]["ID"] == 2666 || empty($arResult["CountryUser"])) ? '' : 'disabled'; ?>
                              class="lk__form-fieldset" data-form-fieldset="2666">
                    <div class="lk__form-row js-field">
                        <div class="lk__form-field required">
                            <label class="field <?= $arResult["user"]["WORK_ZIP"] ? 'focused' : ''; ?>">
                                <span class="field__title field__required">ИНН</span>
                                <input required class="field__input js-inn" type="text" name="WORK_ZIP"
                                       value="<?= $arResult["user"]["WORK_ZIP"]; ?>">
                            </label>
                        </div>
                        <? if (!empty($arResult['FIELD_NAMES']['INN'])): ?>
                            <div class="hint">
                                <div class="hint__message">
                                    <?= $arResult['FIELD_NAMES']['INN']; ?>
                                </div>
                            </div>
                        <? endif; ?>
                        <div class="tooltip js-error-tooltip">
                            ИНН имеет длинну 10 или 12 цифр
                        </div>
                    </div>
                    </fieldset>
                    <fieldset <?= $arResult["CountryUser"]["ID"] == 2667 ? '' : 'disabled'; ?>
                              class="lk__form-fieldset" data-form-fieldset="2667">
                        <div class="lk__form-row js-field">
                            <div class="lk__form-field required">
                                <label class="field <?= $arResult["user"]["UF_BIN"] ? 'focused' : ''; ?>">
                                    <span class="field__title field__required">БИН</span>
                                    <input required class="field__input" type="text" name="UF_BIN"
                                           value="<?= $arResult["user"]["UF_BIN"]; ?>">
                                </label>
                            </div>
                            <? if (!empty($arResult['FIELD_NAMES']['BIN'])): ?>
                                <div class="hint">
                                    <div class="hint__message">
                                        <?= $arResult['FIELD_NAMES']['BIN']; ?>
                                    </div>
                                </div>
                            <? endif; ?>
                        </div>
                    </fieldset>
                </div>
                <br/>
                <div class="lk__form-row js-field">
                    <div class="lk__form-field required">
                        <label class="field <?= $arResult["user"]["NAME"] ? 'focused' : ''; ?>">
                            <span class="field__title field__required">Контактное лицо</span>
                            <input required class="field__input" type="text" name="NAME"
                                   value="<?= $arResult["user"]["LAST_NAME"]; ?> <?= $arResult["user"]["NAME"]; ?> <?= $arResult["user"]["SECOND_NAME"]; ?>">
                        </label>
                    </div>
                    <? if (!empty($arResult['FIELD_NAMES']['CONTACT_PERSON'])): ?>
                        <div class="hint">
                            <div class="hint__message">
                                <?= $arResult['FIELD_NAMES']['CONTACT_PERSON']; ?>
                            </div>
                        </div>
                    <? endif; ?>
                    <div class="tooltip">
                        Поле не должно быть пустым
                    </div>
                </div>
                <div class="lk__form-row js-field">
                    <div class="lk__form-field required">
                        <label class="field <?= $arResult["user"]["PERSONAL_PHONE"] ? 'focused' : ''; ?>">
                            <span class="field__title field__required">Телефон</span>
                            <input required class="field__input js-mask-phone" type="text" name="PERSONAL_PHONE"
                                   value="<?= $arResult["user"]["PERSONAL_PHONE"]; ?>">
                        </label>
                    </div>
                    <? if (!empty($arResult['FIELD_NAMES']['PHONE'])): ?>
                        <div class="hint">
                            <div class="hint__message">
                                <?= $arResult['FIELD_NAMES']['PHONE']; ?>
                            </div>
                        </div>
                    <? endif; ?>
                    <div class="tooltip">
                        Телефон не должн быть пустым
                    </div>
                </div>
                <div class="lk__form-row js-field">
                    <div class="lk__form-field required">
                        <label class="field <?= $arResult["user"]["EMAIL"] ? 'focused' : ''; ?>">
                            <span class="field__title field__required">Электронная почта</span>
                            <input required class="field__input" type="text" name="EMAIL"
                                   value="<?= $arResult["user"]["EMAIL"]; ?>">
                        </label>
                    </div>
                    <? if (!empty($arResult['FIELD_NAMES']['EMAIL'])): ?>
                        <div class="hint">
                            <div class="hint__message">
                                <?= $arResult['FIELD_NAMES']['EMAIL']; ?>
                            </div>
                        </div>
                    <? endif; ?>
                    <div class="tooltip">
                        Электронная почта не должна быть пустой
                    </div>
                </div>
            </div>
            <div class="lk__form-req-text">
                <b>*</b> – поля, обязательные для заполнения
            </div>
            <button class="btn lk__form-submit" type="submit" data-click="upHref" data-url="/dealer/#details">
                Сохранить
            </button>
        </div>
    </form>
</div>