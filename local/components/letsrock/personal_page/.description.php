<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
$arComponentDescription = array(
    "NAME" => 'Информация пользователя',
    "DESCRIPTION" => 'Информация пользователя',
    "PATH" => array(
        "ID" => "dv_components",
        "CHILD" => array(
            "ID" => "Информация пользователя",
            "NAME" => "Информация пользователя"
        )
    ),
);
?>