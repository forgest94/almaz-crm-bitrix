<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
$arComponentDescription = array(
    "NAME" => 'Рекламации техники',
    "DESCRIPTION" => 'Рекламации техники',
    "PATH" => array(
        "ID" => "dv_components",
        "CHILD" => array(
            "ID" => "reclamation_tech",
            "NAME" => "Рекламации техники"
        )
    ),
);
?>