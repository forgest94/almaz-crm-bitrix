<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}

use Letsrock\Lib\Models\User;
use Letsrock\Lib\Models\Element;
use Letsrock\Lib\Models\HLBlock;
use Letsrock\Lib\Models\Property;

\Bitrix\Main\Loader::includeModule('letsrock.lib');

class ReclamationTechComponent extends CBitrixComponent
{
    public function propIsDop($code, $iblock)
    {
        $prMarkHl = new HLBlock(HL_POPSDOP);
        $arMarkProp_db = $prMarkHl->get(["UF_CODE_PROP" => $code, "UF_IBLOCK_ID"=>$iblock]);
        if (!empty($arMarkProp_db)) {
            return $arMarkProp_db[0];
        } else {
            return [];
        }
    }

    public function onPrepareComponentParams($arParams)
    {
        $result = $arParams;

        return $result;
    }

    /**
     * @return array|mixed
     * @throws \Bitrix\Main\LoaderException
     */
    public function executeComponent()
    {
        global $USER;
        if ($this->startResultCache($this->arParams["CACHE_TIME"]) && $USER->GetID()) {
            $elemIbl = new Element;
            /*startTech*/
            $typeTechHl = new HLBlock(HL_EQUIPMENT);
            $arReclamation_Tech_db = $elemIbl->GetList(IB_RECLAMATION_TECH,
                ["NAME", "ID", "IBLOCK_ID", "PROPERTY_*"],
                ["PROPERTY_DEALER" => $USER->GetID()],
                ["ID" => "desc"]
            );
            $arTech = [];
            foreach ($arReclamation_Tech_db["ELEMS"] as $item) {
                $arProps = $arReclamation_Tech_db["PROPS"][$item["ID"]];
                $infoModel = [];
                $infoType = [];
                $numberTech = '';
                $arBuyer = [];
                $infoTech = $elemIbl->GetList(IB_EXPLOITATION,
                    ["NAME", "PROPERTY_MODEL", "PROPERTY_EQUIPMENT", "PROPERTY_NUMBER", "PROPERTY_BUYER"],
                    ["ID" => $arProps["SERIAL_NUMBER"]["VALUE"], "PROPERTY_DEALER" => $USER->GetID(), "!EQUIPMENT_VALUE" => false],
                    ["ID" => "desc"]
                )["ELEMS"][0];
                if (!empty($infoTech["PROPERTY_EQUIPMENT_VALUE"])) {
                    $infoModel = $elemIbl->getById(IB_DIRECTORY, $infoTech["PROPERTY_MODEL_VALUE"]);
                    $infoType = $typeTechHl->get(["UF_XML_ID" => $infoTech["PROPERTY_EQUIPMENT_VALUE"]])[0];
                    $numberTech = $infoTech["PROPERTY_NUMBER_VALUE"];
                    if ($infoTech["PROPERTY_BUYER_VALUE"]) {
                        $infoBuyer = $elemIbl->GetList(IB_BUYERS, ["NAME", "PROPERTY_*", "IBLOCK_ID", "ID"], ["ID" => $infoTech["PROPERTY_BUYER_VALUE"]]);
                        $arPropsBuyer = $infoBuyer["PROPS"][$infoBuyer["ELEMS"][0]["ID"]];
                        $arBuyer = [
                            "name" => $infoBuyer["ELEMS"][0]["NAME"],
                            "id" => $infoBuyer["ELEMS"][0]["ID"],
                            "inn" => $arPropsBuyer["INN"]["VALUE"],
                            "contacts_people" => $arPropsBuyer["CONTACT_PEOPLE"]["VALUE"],
                            "phone" => $arPropsBuyer["PHONE"]["VALUE"]
                        ];
                    }
                } else {
                    continue;
                }
                $akt_parse = [];
                if (!empty($arProps["AKT"]["VALUE"])) {
                    foreach ($arProps["AKT"]["VALUE"] as $akt2) {
                        $arFile = CFile::GetFileArray($akt2);
                        if ($arFile)
                            $akt_parse[] = ["name" => $arFile["ORIGINAL_NAME"], "src" => $arFile["SRC"]];
                    }
                    /*$akt = CFile::GetFileArray($arProps["AKT"]["VALUE"]);
                    if($akt)
                    $akt_parse = ["name"=>$akt["ORIGINAL_NAME"], "src"=>$akt["SRC"]];*/
                }
                $arPritenzy = [];
                if (!empty($arProps["PRITENZY"]["VALUE"])) {
                    foreach ($arProps["PRITENZY"]["VALUE"] as $photo) {
                        $arFile = CFile::GetFileArray($photo);
                        if ($arFile)
                            $arPritenzy[] = ["name" => $arFile["ORIGINAL_NAME"], "src" => $arFile["SRC"]];
                    }
                }
                $arPhoto = [];
                if (!empty($arProps["PHOTO"]["VALUE"])) {
                    foreach ($arProps["PHOTO"]["VALUE"] as $photo) {
                        $arFile = CFile::GetFileArray($photo);
                        if ($arFile)
                            $arPhoto[] = ["name" => $arFile["ORIGINAL_NAME"], "src" => $arFile["SRC"]];
                    }
                }

                if (strpos('---- ' . $item["NAME"], 'RT') === false) {
                    $numberParts = $item["NAME"];
                } else {
                    $numberParts = 'RT' . $item["ID"];
                }

                $arTech[] = [
                    "number" => $numberParts,
                    "id" => $item["ID"],
                    "date" => $arProps["DATE"]["VALUE"],
                    "status" => $arProps["STATUS"]["VALUE"],
                    "status_id" => $arProps["STATUS"]["VALUE_ENUM_ID"],
                    "serial_number" => $numberTech,
                    "model" => $infoModel["NAME"],
                    "type" => $infoType["UF_NAME"],
                    "akt" => $akt_parse,
                    "refusal" => $arProps["REFUSAL"]["VALUE"]["TEXT"],
                    "pritenzy" => $arPritenzy,
                    "photo" => $arPhoto,
                    "reason" => $arProps["REASON"]["VALUE"]["TEXT"],
                    "wishes" => $arProps["WISHES"]["VALUE"]["TEXT"],
                    "buyer" => $arBuyer
                ];
            }
            $propertiesTech = CIBlockProperty::GetList(Array("sort" => "asc", "name" => "asc"), Array("ACTIVE" => "Y", "IBLOCK_ID" => IB_RECLAMATION_TECH));
            $arTextTech = [];
            while ($prop_fields = $propertiesTech->GetNext()) {
                if ($prop_fields["CODE"] == "STATUS")
                    unset($prop_fields);
                $req_class = '';
                $req_class2 = '';
                if ($prop_fields["IS_REQUIRED"] == 'Y') {
                    $req_class = 'required';
                    $req_class2 = 'field__required';
                }
                $arTextTech[$prop_fields["CODE"]] = [
                    "name" => $prop_fields["NAME"],
                    "file_type" => $prop_fields["FILE_TYPE"],
                    "text" => $prop_fields["HINT"],
                    "req" => $req_class, "req2" => $req_class2,
                    "dop_filds" => $this->propIsDop($prop_fields["CODE"], IB_RECLAMATION_TECH),
                ];
            }
            $this->arParams['FIELD_NAMES']['TYPES'] = Property::getText('FIELD_NAMES_TYPES');
            $this->arParams['FIELD_NAMES']['MODEL'] = Property::getText('FIELD_NAMES_MODEL');
            $this->arParams['FIELD_NAMES']['NUMBER'] = Property::getText('FIELD_NAMES_NUMBER');

            $arTextTech["T"] = Property::getText('FIELD_NAMES_TYPES');
            $model = new HLBlock(HL_EQUIPMENT);
            $types_form = $model->get();

            /*endTech*/

            $pagen = [];
            $arPage = [];
            if (!empty($arTech)) {
                $arPage = array_chunk($arTech, $this->arParams["PAGE_ELEMS"]);
                if (count($arPage) > 1) {
                    for ($i = 1; $i <= count($arPage); $i++) {
                        $pagen[] = $i;
                    }
                }
            }

            $infoEdit = [];
            if (!empty($this->arParams["EDTI_ID"])) {
                $arReclamation_Tech_db2 = $elemIbl->GetList(IB_RECLAMATION_TECH,
                    ["NAME", "ID", "IBLOCK_ID", "PROPERTY_*"],
                    [
                        "PROPERTY_DEALER" => $USER->GetID(),
                        "ID" => $this->arParams["EDTI_ID"]
                    ],
                    ["ID" => "desc"]
                );
                foreach ($arReclamation_Tech_db2["ELEMS"] as $item) {
                    $arProps = $arReclamation_Tech_db["PROPS"][$item["ID"]];
                    $serialNumb = '';
                    $infoModel = [];
                    $infoType = [];
                    $infoTech = $elemIbl->GetList(IB_EXPLOITATION,
                        ["NAME", "PROPERTY_MODEL", "PROPERTY_EQUIPMENT", "PROPERTY_NUMBER", "PROPERTY_BUYER"],
                        ["ID" => $arProps["SERIAL_NUMBER"]["VALUE"], "PROPERTY_DEALER" => $USER->GetID(), "!EQUIPMENT_VALUE" => false],
                        ["ID" => "desc"]
                    )["ELEMS"][0];
                    if ($infoTech) {
                        $serialNumb = $infoTech["PROPERTY_NUMBER_VALUE"];
                        $infoModel = $elemIbl->getById(IB_DIRECTORY, $infoTech["PROPERTY_MODEL_VALUE"]);
                        $infoType = $typeTechHl->get(["UF_XML_ID" => $infoTech["PROPERTY_EQUIPMENT_VALUE"]])[0];
                    }
                    /*$aktInfo = [];
                    if(!empty($arProps["AKT"]["VALUE"])){
                        $aktInfo = CFile::GetFileArray($arProps["AKT"]["VALUE"]);
                        $aktInfo = ["name"=>$aktInfo["ORIGINAL_NAME"], "src"=>$aktInfo["SRC"], "id"=>$arProps["AKT"]["VALUE"]];
                    }*/
                    $aktInfo = [];
                    if (!empty($arProps["AKT"]["VALUE"])) {
                        $fileInfo = '';
                        foreach ($arProps["AKT"]["VALUE"] as $file) {
                            $fileInfo = CFile::GetFileArray($file);
                            $aktInfo[] = ["name" => $fileInfo["ORIGINAL_NAME"], "src" => $fileInfo["SRC"], "id" => $file];
                        }
                    }
                    $pritenzyInfo = [];
                    if (!empty($arProps["PRITENZY"]["VALUE"])) {
                        $fileInfo = '';
                        foreach ($arProps["PRITENZY"]["VALUE"] as $file) {
                            $fileInfo = CFile::GetFileArray($file);
                            $pritenzyInfo[] = ["name" => $fileInfo["ORIGINAL_NAME"], "src" => $fileInfo["SRC"], "id" => $file];
                        }
                    }
                    $photoInfo = [];
                    if (!empty($arProps["PHOTO"]["VALUE"])) {
                        $fileInfo = '';
                        foreach ($arProps["PHOTO"]["VALUE"] as $file) {
                            $fileInfo = CFile::GetFileArray($file);
                            $photoInfo[] = ["name" => $fileInfo["ORIGINAL_NAME"], "src" => $fileInfo["SRC"], "id" => $file];
                        }
                    }
                    $infoEdit = [
                        "id" => $item["ID"],
                        "not_operation" => $arProps["NOT_OPERATION"]["VALUE"],
                        "serial_numb_id" => $arProps["SERIAL_NUMBER"]["VALUE"],
                        "reason" => $arProps["REASON"]["VALUE"]["TEXT"],
                        "wishes" => $arProps["WISHES"]["VALUE"]["TEXT"],
                        "PHONE_PERSONAL" => $arProps["PHONE_PERSONAL"]["VALUE"],
                        "akt" => $aktInfo,
                        "photo" => $photoInfo,
                        "pritenzy" => $pritenzyInfo
                    ];
                    if ($arProps["NOT_OPERATION"]["VALUE"]) {
                        $infoModels = $elemIbl->GetList(IB_DIRECTORY,
                            ["NAME", "ID", "IBLOCK_ID", "PROPERTY_*"],
                            [
                                "PROPERTY_TYPE" => $infoType["UF_XML_ID"],
                            ]
                        )["ELEMS"];
                        $infoEdit["nTech"]["model"] = ["id" => $infoModel["ID"], "name" => $infoModel["NAME"]];
                        $infoEdit["nTech"]["models"] = $infoModels;
                        $infoEdit["nTech"]["type"] = ["id" => $infoType["UF_XML_ID"], "name" => $infoType["UF_NAME"]];
                        $infoEdit["nTech"]["serial_numb"] = $serialNumb;
                    } else {
                        $infoEdit["tech"]["model"] = ["id" => $infoModel["ID"], "name" => $infoModel["NAME"]];
                        $infoEdit["tech"]["type"] = ["id" => $infoType["UF_XML_ID"], "name" => $infoType["UF_NAME"]];
                        $infoEdit["tech"]["serial_numb"] = $serialNumb;
                    }
                }
            }
            $arFullParams = [
                "arTech" => $arPage[$this->arParams["page"] ? $this->arParams["page"] - 1 : 0],
                "formLabelTech" => $arTextTech,
                "types_form" => $types_form,
                "pagination" => $pagen,
                "infoEdit" => $infoEdit
            ];
            $this->arResult = $arFullParams;
            $this->includeComponentTemplate();
        }
        return $this->arResult;
    }
} ?>