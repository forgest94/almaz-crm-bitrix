<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
?>
<a class="btn btn_small lk__tabs-content-btn js-tabs-show-form <?= $arParams["EDTI_ID"] ? 'hide' : ''; ?>"
   href="javascript:void(0)">Подать рекламацию на технику</a>
<div class="lk__tabs-content-inner js-tabs-content-inner <?= $arParams["EDTI_ID"] ? '' : 'active'; ?>">
    <div class="lk__table technics">
        <div class="lk__table-head">
            <div class="lk__table-row">
                <div class="lk__table-col">
                    Номер
                </div>
                <div class="lk__table-col">
                    Дата подачи
                </div>
                <div class="lk__table-col">
                    Зав. номер
                </div>
                <div class="lk__table-col">
                    Техника
                </div>
                <div class="lk__table-col">
                    Статус
                </div>
                <div class="lk__table-col">
                </div>
            </div>
        </div>
        <div class="lk__table-body">
            <? if (!empty($arResult["arTech"])): ?>
                <? foreach ($arResult["arTech"] as $itemT): ?>
                    <div class="lk__table-row-wrap js-table-detail">
                        <div class="lk__table-row">
                            <div class="lk__table-col">
                                <div class="lk__table-col-head">
                                    Номер
                                </div>
                                <?= $itemT["number"]; ?>
                            </div>
                            <div class="lk__table-col">
                                <div class="lk__table-col-head">
                                    Дата подачи
                                </div>
                                <?= $itemT["date"]; ?>
                            </div>
                            <div class="lk__table-col">
                                <div class="lk__table-col-head">
                                    Зав. номер
                                </div>
                                <?= $itemT["serial_number"]; ?>
                            </div>
                            <div class="lk__table-col">
                                <div class="lk__table-col-head">
                                    Техника
                                </div>
                                <?= $itemT["type"]; ?> / <?= $itemT["model"]; ?>
                            </div>
                            <div class="lk__table-col">
                                <div class="lk__table-col-head">
                                    Статус
                                </div>
                                <?= $itemT["status"]; ?>
                            </div>
                            <div class="lk__table-col">
                                <a class="lk__link js-table-detail-link" href="javascript:void(0)"
                                   data-show-text="Свернуть">
                                    Подробнее
                                </a>
                            </div>
                        </div>
                        <div class="lk__table-row-data js-table-detail-data">
                            <div class="lk__data">
                                <? if (!empty($itemT["reason"])): ?>
                                    <div class="lk__data-row">
                                        <div class="lk__data-label">
                                            Причина обращения
                                        </div>
                                        <div class="lk__data-value">
                                            <div class="lk__data-box">
                                                <?= $itemT["reason"]; ?>
                                            </div>
                                        </div>
                                    </div>
                                <? endif; ?>
                                <? if (!empty($itemT["wishes"])): ?>
                                    <div class="lk__data-row">
                                        <div class="lk__data-label">
                                            Предложения по решению
                                        </div>
                                        <div class="lk__data-value">
                                            <div class="lk__data-box">
                                                <?= $itemT["wishes"]; ?>
                                            </div>
                                        </div>
                                    </div>
                                <? endif; ?>
                                <? /*if(!empty($itemT["akt"]["src"])):?>
                                                <div class="lk__data-row">
                                                    <div class="lk__data-label">
                                                        Акт выхода из строя
                                                    </div>
                                                    <div class="lk__data-value">
                                                        <div class="lk__data-files">
                                                            <a class="link-file" href="<?=$itemT["akt"]["src"];?>" download><?=$itemT["akt"]["name"];?></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            <?endif;*/ ?>
                                <? if (!empty($itemT["akt"])): ?>
                                    <div class="lk__data-row">
                                        <div class="lk__data-label">
                                            Рекламация
                                        </div>
                                        <div class="lk__data-value">
                                            <div class="lk__data-files">
                                                <? foreach ($itemT["akt"] as $photo): ?>
                                                    <a class="link-file" href="<?= $photo["src"]; ?>"
                                                       download><?= $photo["name"]; ?></a>
                                                <? endforeach; ?>
                                            </div>
                                        </div>
                                    </div>
                                <? endif; ?>
                                <? if (!empty($itemT["pritenzy"])): ?>
                                    <div class="lk__data-row">
                                        <div class="lk__data-label">
                                            Рекламация
                                        </div>
                                        <div class="lk__data-value">
                                            <div class="lk__data-files">
                                                <? foreach ($itemT["pritenzy"] as $photo): ?>
                                                    <a class="link-file" href="<?= $photo["src"]; ?>"
                                                       download><?= $photo["name"]; ?></a>
                                                <? endforeach; ?>
                                            </div>
                                        </div>
                                    </div>
                                <? endif; ?>
                                <? if (!empty($itemT["photo"])): ?>
                                    <div class="lk__data-row">
                                        <div class="lk__data-label">
                                            Фото
                                        </div>
                                        <div class="lk__data-value">
                                            <div class="lk__data-files">
                                                <? foreach ($itemT["photo"] as $photo): ?>
                                                    <a class="link-file" href="<?= $photo["src"]; ?>"
                                                       download><?= $photo["name"]; ?></a>
                                                <? endforeach; ?>
                                            </div>
                                        </div>
                                    </div>
                                <? endif; ?>
                                <? if (!empty($itemT["buyer"])): ?>
                                    <div class="lk__data-row">
                                        <div class="lk__data-label">
                                            Информация о покупателе
                                        </div>
                                        <div class="lk__data-value">
                                            <ul class="list-dotted">
                                                <li class="list-dotted__row">
                                                    <span class="list-dotted__name">Название организации:</span>
                                                    <span class="list-dotted__line"></span>
                                                    <span class="list-dotted__value"><?= $itemT["buyer"]["name"]; ?></span>
                                                </li>
                                                <li class="list-dotted__row">
                                                    <span class="list-dotted__name">Контактное лицо:</span>
                                                    <span class="list-dotted__line"></span>
                                                    <span class="list-dotted__value"><?= $itemT["buyer"]["contacts_people"]; ?></span>
                                                </li>
                                                <li class="list-dotted__row">
                                                    <span class="list-dotted__name">Телефон:</span>
                                                    <span class="list-dotted__line"></span>
                                                    <span class="list-dotted__value">
                                                                <a href="tel:<?= $itemT["buyer"]["phone"]; ?>"><?= $itemT["buyer"]["phone"]; ?></a>
                                                            </span>
                                                </li>
                                                <li class="list-dotted__row">
                                                    <span class="list-dotted__name">ИНН:</span>
                                                    <span class="list-dotted__line"></span>
                                                    <span class="list-dotted__value"><?= $itemT["buyer"]["inn"]; ?></span>
                                                </li>
                                            </ul>
                                            <a class="btn-border"
                                               href="/dealer/?vids_buyer=<?= $itemT["buyer"]["id"]; ?>">Вся информация о
                                                покупателе</a>
                                        </div>
                                    </div>
                                <? endif; ?>
                                <? if ($itemT["status_id"] == 20): ?>
                                    <div class="lk__data-row">
                                        <div class="lk__data-label">
                                            <? if (!empty($itemT["refusal"])): ?>Причина отказа<? endif; ?>
                                        </div>
                                        <div class="lk__data-value">
                                            <? if (!empty($itemT["refusal"])): ?>
                                                <div class="lk__data-value">
                                                    <div class="lk__data-box">
                                                        <?= $itemT["refusal"]; ?>
                                                    </div>
                                                </div>
                                            <? endif; ?>
                                            <div class="btn-group">
                                                <a class="btn"
                                                   href="/dealer/reclamation/?edit_rect=<?= $itemT["id"]; ?>#technics"
                                                   data-taskid="<?= $itemT["id"]; ?>" data-click="editTask">Редактировать</a>
                                            </div>
                                        </div>
                                    </div>
                                <? endif; ?>
                            </div>
                        </div>
                    </div>
                <? endforeach; ?>
            <? endif; ?>
        </div>
    </div>
    <? $APPLICATION->IncludeComponent("letsrock:paginations",
        ".default",
        [
            "CACHE_TIME" => 0,
            "ELEMS_PAGE" => $arResult["pagination"],
            "GET_PAGE" => "page_tech",
            "AJAX_BLOCK" => "reclamation_tech"
        ]
    ); ?>
</div>
<div class="lk__tabs-content-form js-tabs-form <?= $arParams["EDTI_ID"] ? 'active' : ''; ?>">
    <a class="btn-arrow js-tabs-back" href="javascript:void(0)">
        <i class="icon-back"></i>
        <span>Назад</span>
    </a>
    <form action="/ajax/dealer/addReclamation/tech" method="POST" class="js-form-marketing" novalidate="novalidate">
        <div class="lk__form-section">
            <div class="lk__form-box-wrap js-lk-form-box <?= $arResult["infoEdit"]["not_operation"] ? '' : 'active'; ?>">
                <div class="lk__form-title">
                    Укажите технику:
                </div>
                <div class="lk__form-row lk__form-row_long js-field">
                    <div class="lk__form-field <?= $arResult["formLabelTech"]["SERIAL_NUMBER"]["req"]; ?>">
                        <label class="field <?= $arResult["infoEdit"]["tech"]["serial_numb"] ? 'focused' : ''; ?>">
                            <span class="field__title <?= $arResult["formLabelTech"]["SERIAL_NUMBER"]["req2"]; ?>">Введите заводской номер введенной в эксплуатацию техники</span>
                            <input
                                    class="field__input js-autocomplete"
                                    type="text"
                                    name="SERIAL_NUMBER[]"
                                    data-min-len="2"
                                    <?= $arResult["formLabelTech"]["SERIAL_NUMBER"]["req"]; ?>
                                    data-path="/ajax/dealer/getExplotationSerial/"
                                    <?= $arResult["infoEdit"]["tech"]["serial_numb"] ? 'value="' . $arResult["infoEdit"]["tech"]["serial_numb"] . '"' : ''; ?>/>
                        </label>
                    </div>
                    <? if ($arResult["formLabelTech"]["SERIAL_NUMBER"]["text"]): ?>
                        <div class="hint">
                            <div class="hint__message">
                                <?= $arResult["formLabelTech"]["SERIAL_NUMBER"]["text"]; ?>
                            </div>
                        </div>
                    <? endif; ?>
                    <? if ($arResult["formLabelTech"]["SERIAL_NUMBER"]["req"]): ?>
                        <div class="tooltip js-error-tooltip">
                            Поле <?= $arResult["formLabelTech"]["SERIAL_NUMBER"]["name"]; ?> не может быть пустым
                        </div>
                    <? endif; ?>
                </div>
                <div class="lk__form-desc js-autocomplete-text"
                     data-def-text="Название техники (<span>появится автоматически после ввода заводского номера</span>)">
                    <? if (!empty($arResult["infoEdit"]["tech"]["model"]["name"])): ?>
                        <?= $arResult["infoEdit"]["tech"]["serial_numb"]; ?> <?= $arResult["infoEdit"]["tech"]["type"]["name"]; ?> <?= $arResult["infoEdit"]["tech"]["model"]["name"]; ?>
                    <? else: ?>
                        Название техники (<span>появится автоматически после ввода заводского номера</span>)
                    <? endif; ?>
                </div>
                <input type="hidden" class="js-autocomplete-hidden-input" name="id_explotation[]"
                       value="<?= $arResult["infoEdit"]["serial_numb_id"]; ?>"
                       <?= $arResult["formLabelTech"]["SERIAL_NUMBER"]["req"]; ?>/>
                <div class="lk__form-box">
                    <div class="lk__form-row">
                        <a class="btn-border js-lk-form-box-show" href="javascript:void(0)" data-req="Y"
                           data-block-visible="no-tech">Техника не введена в эксплуатацию</a>
                        <? if ($arResult["formLabelTech"]["NOT_OPERATION"]["text"]): ?>
                            <div class="hint">
                                <div class="hint__message">
                                    <?= $arResult["formLabelTech"]["NOT_OPERATION"]["text"]; ?>
                                </div>
                            </div>
                        <? endif; ?>
                    </div>
                </div>
            </div>
            <div class="lk__form-box-wrap js-lk-form-box <?= $arResult["infoEdit"]["not_operation"] ? 'active' : ''; ?>">
                <div class="lk__form-box">
                    <div class="lk__form-title">
                        Укажите технику:
                    </div>
                    <div class="lk__form-row js-field">
                        <div class="lk__form-field">
                            <select class="select js-select js-select-linked"
                                    name="TYPE_TECH"
                                    data-path="/ajax/equipments/get/"
                                    data-linked=".js-select-models">
                                <option disabled selected hidden>Выберите тип техники</option>
                                <? foreach ($arResult['types_form'] as $type): ?>
                                    <? if (!empty($arResult["infoEdit"]['nTech']["type"]["id"]) && $arResult["infoEdit"]['nTech']["type"]["id"] == $type['UF_XML_ID']): ?>
                                        <option value="<?= $type['UF_XML_ID'] ?>"
                                                selected><?= $type['UF_NAME'] ?></option>
                                    <? else: ?>
                                        <option value="<?= $type['UF_XML_ID'] ?>"><?= $type['UF_NAME'] ?></option>
                                    <? endif; ?>
                                <? endforeach; ?>
                            </select>
                        </div>
                        <? if ($arParams['FIELD_NAMES']['TYPES']): ?>
                            <div class="hint">
                                <div class="hint__message">
                                    <?= $arParams['FIELD_NAMES']['TYPES']; ?>
                                </div>
                            </div>
                        <? endif; ?>
                        <div class="tooltip js-error-tooltip">
                            Поле тип техники не может быть пустым
                        </div>
                    </div>
                    <div class="lk__form-row js-field">
                        <div class="lk__form-field">
                            <select class="select js-select js-select-models" name="MODELS_TECH">
                                <option disabled selected hidden>Выберите модель</option>
                                <? if (!empty($arResult['infoEdit']['nTech']['models']) && !empty($arResult['infoEdit']['nTech']["model"]["id"])): ?>
                                    <? foreach ($arResult['infoEdit']['nTech']['models'] as $model): ?>
                                        <? if (!empty($arResult['infoEdit']['nTech']["model"]['id']) && $arResult['infoEdit']['nTech']["model"]['id'] == $model['ID']): ?>
                                            <option value="<?= $model['ID'] ?>" selected><?= $model['NAME'] ?></option>
                                        <? else: ?>
                                            <option value="<?= $model['ID'] ?>"><?= $model['NAME'] ?></option>
                                        <? endif; ?>
                                    <? endforeach; ?>
                                <? endif; ?>
                            </select>
                        </div>
                        <? if ($arParams['FIELD_NAMES']['MODEL']): ?>
                            <div class="hint">
                                <div class="hint__message">
                                    <?= $arParams['FIELD_NAMES']['MODEL']; ?>
                                </div>
                            </div>
                        <? endif; ?>
                        <div class="tooltip js-error-tooltip">
                            Поле модель не может быть пустым
                        </div>
                    </div>
                    <div class="lk__form-row js-field">
                        <div class="lk__form-field">
                            <label class="field <?= $arResult["infoEdit"]["nTech"]["serial_numb"] ? 'focused' : ''; ?>">
                                <span class="field__title">Введите заводской номер</span>
                                <input class="field__input js-number" type="text" name="NUMBER_TECH"
                                       value="<?= $arResult["infoEdit"]["nTech"]["serial_numb"]; ?>">
                            </label>
                        </div>
                        <? if ($arParams['FIELD_NAMES']['NUMBER']): ?>
                            <div class="hint">
                                <div class="hint__message">
                                    <?= $arParams['FIELD_NAMES']['NUMBER']; ?>
                                </div>
                            </div>
                        <? endif; ?>
                        <div class="tooltip">
                            Номер введен неверно
                        </div>
                    </div>
                    <div class="lk__form-row">
                        <a class="lk__link js-lk-form-box-hide" href="javascript:void(0)" data-req="Y"
                           data-block-visible="on-tech">Техника введена в эксплуатацию</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="lk__form-section">
            <div class="lk__form-box">
                <div class="lk__form-title">
                    Укажите причину обращения
                </div>
                <div class="lk__form-row">
                    <label class="field field-textarea <?= $arResult["infoEdit"]["reason"] ? 'focused' : ''; ?>">
                        <span class="field__title <?= $arResult["formLabelTech"]["REASON"]["req2"]; ?>">Кратко опишите проблему</span>
                        <textarea class="field__input" type="text" name="REASON"
                                  <?= $arResult["formLabelTech"]["REASON"]["req"]; ?>><?= $arResult["infoEdit"]["reason"]; ?></textarea>
                    </label>
                    <? if ($arResult["formLabelTech"]["REASON"]["text"]): ?>
                        <div class="hint">
                            <div class="hint__message">
                                <?= $arResult["formLabelTech"]["REASON"]["text"]; ?>
                            </div>
                        </div>
                    <? endif; ?>
                    <? if ($arResult["formLabelTech"]["REASON"]["req"]): ?>
                        <div class="tooltip js-error-tooltip">
                            Поле <?= $arResult["formLabelTech"]["REASON"]["name"]; ?> не может быть пустым
                        </div>
                    <? endif; ?>
                </div>
            </div>
            <div class="lk__form-box">
                <div class="lk__form-title">
                    Ваши пожелания по решению рекламации
                </div>
                <div class="lk__form-row">
                    <label class="field field-textarea <?= $arResult["infoEdit"]["wishes"] ? 'focused' : ''; ?>">
                        <span class="field__title <?= $arResult["formLabelTech"]["WISHES"]["req2"]; ?>">Допоставка, возврат средств и т.д.</span>
                        <textarea class="field__input" type="text" name="WISHES"
                                  <?= $arResult["formLabelTech"]["WISHES"]["req"]; ?>><?= $arResult["infoEdit"]["wishes"]; ?></textarea>
                    </label>
                    <? if ($arResult["formLabelTech"]["WISHES"]["text"]): ?>
                        <div class="hint">
                            <div class="hint__message">
                                <?= $arResult["formLabelTech"]["WISHES"]["text"]; ?>
                            </div>
                        </div>
                    <? endif; ?>
                    <? if ($arResult["formLabelTech"]["WISHES"]["req"]): ?>
                        <div class="tooltip js-error-tooltip">
                            Поле <?= $arResult["formLabelTech"]["WISHES"]["name"]; ?> не может быть пустым
                        </div>
                    <? endif; ?>
                </div>
            </div>
        </div>
        <div class="lk__form-section">
            <div class="lk__form-title">
                Прикрепите необходимые документы
            </div>
            <? /*<div class="lk__form-row js-field">
                                <div class="lk__form-field <?=$arResult["formLabelTech"]["AKT"]["req"];?>">
                                    <?if(!empty($arResult['infoEdit']['akt']['name'])):?>
                                        <div class="lk__form-file selected">
                                            <div class="lk__download">
                                                <div class="lk__download-title" data-default-title="Акт ввода в эксплуатацию">
                                                    <?=$arResult['infoEdit']['akt']['name'];?>
                                                </div>
                                                <?if(!empty($arResult["formLabelTech"]["AKT"]["dop_filds"]["UF_SAMPLE"])):?>
                                                    <a href="<?=CFile::GetPath($arResult["formLabelTech"]["AKT"]["dop_filds"]["UF_SAMPLE"]);?>" class="lk__download-link" download>
                                                        Скачать форму
                                                    </a>
                                                <?endif;?>
                                            </div>
                                            <label class="field-file">
                                                <input class="field-file__input js-file" type="file" name="AKT" <?=$arResult["formLabelTech"]["AKT"]["req"];?> data-type="<?=$arResult["formLabelTech"]["AKT"]["file_type"];?>">
                                                <span class="field-file__title" data-default-title="Загрузить">Удалить файл</span>
                                            </label>
                                            <input type="hidden" name="akt_id" data-editfile value="<?=$arResult['infoEdit']['akt']['id'];?>" />
                                        </div>
                                    <?else:?>
                                        <div class="lk__form-file">
                                            <div class="lk__download">
                                                <div class="lk__download-title <?=$arResult["formLabelTech"]["AKT"]["req2"];?>">
                                                    Акт выхода из строя
                                                </div>
                                                <?if(!empty($arResult["formLabelTech"]["AKT"]["dop_filds"]["UF_SAMPLE"])):?>
                                                    <a href="<?=CFile::GetPath($arResult["formLabelTech"]["AKT"]["dop_filds"]["UF_SAMPLE"]);?>" class="lk__download-link" download>
                                                        Скачать форму
                                                    </a>
                                                <?endif;?>
                                            </div>
                                            <label class="field-file">
                                                <input class="field-file__input js-file" type="file" name="AKT" <?=$arResult["formLabelTech"]["AKT"]["req"];?> data-type="<?=$arResult["formLabelTech"]["AKT"]["file_type"];?>">
                                                <span class="field-file__title">Загрузить</span>
                                            </label>
                                        </div>
                                    <?endif;?>
                                </div>
                                <?if($arResult["formLabelTech"]["AKT"]["text"]):?>
                                    <div class="hint">
                                        <div class="hint__message">
                                            <?=$arResult["formLabelTech"]["AKT"]["text"];?>
                                        </div>
                                    </div>
                                <?endif;?>
                                <?if($arResult["formLabelTech"]["AKT"]["req"]):?>
                                    <div class="tooltip js-error-tooltip">
                                        Поле <?=$arResult["formLabelTech"]["AKT"]["name"];?> не может быть пустым
                                    </div>
                                <?endif;?>
                            </div>*/ ?>
            <div data-block="on-tech">
                <? if (!empty($arResult['infoEdit']['akt'])): ?>
                    <? foreach ($arResult['infoEdit']['akt'] as $photo): ?>
                        <div class="lk__form-row js-field"
                             <?= $arResult["formLabelTech"]["AKT"]["req"] ? 'data-req="Y"' : ''; ?>>
                            <div class="lk__form-field <?= $arResult["formLabelTech"]["AKT"]["req"]; ?>">
                                <div class="lk__form-file selected">
                                    <div class="lk__download">
                                        <div class="lk__download-title <?= $arResult["formLabelTech"]["AKT"]["req2"]; ?>"
                                             data-default-title="Рекламация">
                                            <?= $photo['name']; ?>
                                        </div>
                                        <? if (!empty($arResult["formLabelTech"]["AKT"]["dop_filds"]["UF_SAMPLE"])): ?>
                                            <a href="<?= CFile::GetPath($arResult["formLabelTech"]["AKT"]["dop_filds"]["UF_SAMPLE"]); ?>"
                                               class="lk__download-link" download>
                                                Скачать форму
                                            </a>
                                        <? endif; ?>
                                    </div>
                                    <label class="field-file">
                                        <input class="field-file__input js-file" type="file" name="AKT[]"
                                               <?= $arResult["formLabelTech"]["AKT"]["req"]; ?>
                                               data-type="<?= $arResult["formLabelTech"]["AKT"]["file_type"]; ?>">
                                        <span class="field-file__title"
                                              data-default-title="Загрузить">Удалить файл</span>
                                    </label>
                                    <input type="hidden" name="akt_id[]" data-editfile value="<?= $photo['id']; ?>"/>
                                </div>

                            </div>
                            <? if ($arResult["formLabelTech"]["AKT"]["text"]): ?>
                                <div class="hint">
                                    <div class="hint__message">
                                        <?= $arResult["formLabelTech"]["AKT"]["text"]; ?>
                                    </div>
                                </div>
                            <? endif; ?>
                            <? if ($arResult["formLabelTech"]["AKT"]["req"]): ?>
                                <div class="tooltip js-error-tooltip">
                                    Поле <?= $arResult["formLabelTech"]["AKT"]["name"]; ?> не может быть пустым
                                </div>
                            <? endif; ?>
                        </div>
                    <? endforeach; ?>
                <? else: ?>
                    <div class="lk__form-row js-field"
                         <?= $arResult["formLabelTech"]["AKT"]["req"] ? 'data-req="Y"' : ''; ?>>
                        <div class="lk__form-field <?= $arResult["formLabelTech"]["AKT"]["req"]; ?>">
                            <div class="lk__form-file">
                                <div class="lk__download">
                                    <div class="lk__download-title <?= $arResult["formLabelTech"]["AKT"]["req2"]; ?>">
                                        Рекламация
                                    </div>
                                    <? if (!empty($arResult["formLabelTech"]["AKT"]["dop_filds"]["UF_SAMPLE"])): ?>
                                        <a href="<?= CFile::GetPath($arResult["formLabelTech"]["AKT"]["dop_filds"]["UF_SAMPLE"]); ?>"
                                           class="lk__download-link" download>
                                            Скачать форму
                                        </a>
                                    <? endif; ?>
                                </div>
                                <label class="field-file">
                                    <input class="field-file__input js-file" type="file" name="AKT[]"
                                           <?= $arResult["formLabelTech"]["AKT"]["req"]; ?>
                                           data-type="<?= $arResult["formLabelTech"]["AKT"]["file_type"]; ?>">
                                    <span class="field-file__title">Загрузить</span>
                                </label>
                            </div>
                        </div>
                        <? if ($arResult["formLabelTech"]["AKT"]["text"]): ?>
                            <div class="hint">
                                <div class="hint__message">
                                    <?= $arResult["formLabelTech"]["AKT"]["text"]; ?>
                                </div>
                            </div>
                        <? endif; ?>
                        <? if ($arResult["formLabelTech"]["AKT"]["req"]): ?>
                            <div class="tooltip js-error-tooltip">
                                Поле <?= $arResult["formLabelTech"]["AKT"]["name"]; ?> не может быть пустым
                            </div>
                        <? endif; ?>
                    </div>
                <? endif; ?>
                <div class="lk__form-row">
                    <a class="lk__form-add js-lk-row-add" href="javascript:void(0)">Еще</a>
                </div>
            </div>
            <div class="no-elem" data-block="no-tech">
                <? if (!empty($arResult['infoEdit']['pritenzy'])): ?>
                    <? foreach ($arResult['infoEdit']['pritenzy'] as $photo): ?>
                        <div class="lk__form-row js-field"
                             <?= $arResult["formLabelTech"]["PRITENZY"]["req"] ? 'data-req="Y"' : ''; ?>>
                            <div class="lk__form-field">
                                <div class="lk__form-file selected">
                                    <div class="lk__download">
                                        <div class="lk__download-title" data-default-title="Рекламация">
                                            <?= $photo['name']; ?>
                                        </div>
                                        <? if (!empty($arResult["formLabelTech"]["PRITENZY"]["dop_filds"]["UF_SAMPLE"])): ?>
                                            <a href="<?= CFile::GetPath($arResult["formLabelTech"]["PRITENZY"]["dop_filds"]["UF_SAMPLE"]); ?>"
                                               class="lk__download-link" download>
                                                Скачать форму
                                            </a>
                                        <? endif; ?>
                                    </div>
                                    <label class="field-file">
                                        <input class="field-file__input js-file" type="file" name="PRITENZY[]"
                                               data-type="<?= $arResult["formLabelTech"]["PRITENZY"]["file_type"]; ?>">
                                        <span class="field-file__title"
                                              data-default-title="Загрузить">Удалить файл</span>
                                    </label>
                                    <input type="hidden" name="pritenzy_id[]" data-editfile
                                           value="<?= $photo['id']; ?>"/>
                                </div>

                            </div>
                            <? if ($arResult["formLabelTech"]["PRITENZY"]["text"]): ?>
                                <div class="hint">
                                    <div class="hint__message">
                                        <?= $arResult["formLabelTech"]["PRITENZY"]["text"]; ?>
                                    </div>
                                </div>
                            <? endif; ?>
                            <? if ($arResult["formLabelTech"]["PRITENZY"]["req"]): ?>
                                <div class="tooltip js-error-tooltip">
                                    Поле <?= $arResult["formLabelTech"]["PRITENZY"]["name"]; ?> не может быть пустым
                                </div>
                            <? endif; ?>
                        </div>
                    <? endforeach; ?>
                <? else: ?>
                    <div class="lk__form-row js-field"
                         <?= $arResult["formLabelTech"]["PRITENZY"]["req"] ? 'data-req="Y"' : ''; ?>>
                        <div class="lk__form-field">
                            <div class="lk__form-file">
                                <div class="lk__download">
                                    <div class="lk__download-title">
                                        Рекламация
                                    </div>
                                    <? if (!empty($arResult["formLabelTech"]["PRITENZY"]["dop_filds"]["UF_SAMPLE"])): ?>
                                        <a href="<?= CFile::GetPath($arResult["formLabelTech"]["PRITENZY"]["dop_filds"]["UF_SAMPLE"]); ?>"
                                           class="lk__download-link" download>
                                            Скачать форму
                                        </a>
                                    <? endif; ?>
                                </div>
                                <label class="field-file">
                                    <input class="field-file__input js-file" type="file" name="PRITENZY[]"
                                           data-type="<?= $arResult["formLabelTech"]["PRITENZY"]["file_type"]; ?>">
                                    <span class="field-file__title">Загрузить</span>
                                </label>
                            </div>
                        </div>
                        <? if ($arResult["formLabelTech"]["PRITENZY"]["text"]): ?>
                            <div class="hint">
                                <div class="hint__message">
                                    <?= $arResult["formLabelTech"]["PRITENZY"]["text"]; ?>
                                </div>
                            </div>
                        <? endif; ?>
                        <? if ($arResult["formLabelTech"]["AKT"]["req"]): ?>
                            <div class="tooltip js-error-tooltip">
                                Поле <?= $arResult["formLabelTech"]["PRITENZY"]["name"]; ?> не может быть пустым
                            </div>
                        <? endif; ?>
                    </div>
                <? endif; ?>
                <div class="lk__form-row">
                    <a class="lk__form-add js-lk-row-add" href="javascript:void(0)">Еще</a>
                </div>
            </div>
            <br/>
            <? if (!empty($arResult['infoEdit']['photo'])): ?>
                <? foreach ($arResult['infoEdit']['photo'] as $photo): ?>
                    <div class="lk__form-row js-field">
                        <div class="lk__form-field <?= $arResult["formLabelTech"]["PHOTO"]["req"]; ?>">
                            <div class="lk__form-file selected">
                                <div class="lk__download">
                                    <div class="lk__download-title <?= $arResult["formLabelTech"]["PHOTO"]["req2"]; ?>"
                                         data-default-title="Фото">
                                        <?= $photo['name']; ?>
                                    </div>
                                    <? if (!empty($arResult["formLabelTech"]["PHOTO"]["dop_filds"]["UF_SAMPLE"])): ?>
                                        <a href="<?= CFile::GetPath($arResult["formLabelTech"]["PHOTO"]["dop_filds"]["UF_SAMPLE"]); ?>"
                                           class="lk__download-link" download>
                                            Скачать форму
                                        </a>
                                    <? endif; ?>
                                </div>
                                <label class="field-file">
                                    <input class="field-file__input js-file" type="file" name="PHOTO[]"
                                           <?= $arResult["formLabelTech"]["PHOTO"]["req"]; ?>
                                           data-type="<?= $arResult["formLabelTech"]["PHOTO"]["file_type"]; ?>">
                                    <span class="field-file__title" data-default-title="Загрузить">Удалить файл</span>
                                </label>
                                <input type="hidden" name="photo_id[]" data-editfile value="<?= $photo['id']; ?>"/>
                            </div>

                        </div>
                        <? if ($arResult["formLabelTech"]["PHOTO"]["text"]): ?>
                            <div class="hint">
                                <div class="hint__message">
                                    <?= $arResult["formLabelTech"]["PHOTO"]["text"]; ?>
                                </div>
                            </div>
                        <? endif; ?>
                        <? if ($arResult["formLabelTech"]["PHOTO"]["req"]): ?>
                            <div class="tooltip js-error-tooltip">
                                Поле <?= $arResult["formLabelTech"]["PHOTO"]["name"]; ?> не может быть пустым
                            </div>
                        <? endif; ?>
                    </div>
                <? endforeach; ?>
            <? else: ?>
                <div class="lk__form-row js-field">
                    <div class="lk__form-field <?= $arResult["formLabelTech"]["PHOTO"]["req"]; ?>">
                        <div class="lk__form-file">
                            <div class="lk__download">
                                <div class="lk__download-title <?= $arResult["formLabelTech"]["PHOTO"]["req2"]; ?>">
                                    Фото
                                </div>
                                <? if (!empty($arResult["formLabelTech"]["PHOTO"]["dop_filds"]["UF_SAMPLE"])): ?>
                                    <a href="<?= CFile::GetPath($arResult["formLabelTech"]["PHOTO"]["dop_filds"]["UF_SAMPLE"]); ?>"
                                       class="lk__download-link" download>
                                        Скачать форму
                                    </a>
                                <? endif; ?>
                            </div>
                            <label class="field-file">
                                <input class="field-file__input js-file" type="file" name="PHOTO[]"
                                       <?= $arResult["formLabelTech"]["PHOTO"]["req"]; ?>
                                       data-type="<?= $arResult["formLabelTech"]["PHOTO"]["file_type"]; ?>">
                                <span class="field-file__title">Загрузить</span>
                            </label>
                        </div>
                    </div>
                    <? if ($arResult["formLabelTech"]["PHOTO"]["text"]): ?>
                        <div class="hint">
                            <div class="hint__message">
                                <?= $arResult["formLabelTech"]["PHOTO"]["text"]; ?>
                            </div>
                        </div>
                    <? endif; ?>
                    <? if ($arResult["formLabelTech"]["PHOTO"]["req"]): ?>
                        <div class="tooltip js-error-tooltip">
                            Поле <?= $arResult["formLabelTech"]["PHOTO"]["name"]; ?> не может быть пустым
                        </div>
                    <? endif; ?>
                </div>
            <? endif; ?>
            <div class="lk__form-row">
                <a class="lk__form-add js-lk-row-add" href="javascript:void(0)">Еще</a>
            </div>
            <br/>
            <div class="lk__form-row js-field">
                <div class="lk__form-field <?= $arResult["formLabelTech"]["PHONE_PERSONAL"]["req"]; ?>">
                    <label class="field <?= $arResult['infoEdit']['PHONE_PERSONAL'] ? 'focused' : ''; ?>">
                        <span class="field__title <?= $arResult["formLabelTech"]["PHONE_PERSONAL"]["req2"]; ?>"><?= $arResult["formLabelTech"]["PHONE_PERSONAL"]["name"]; ?></span>
                        <input class="field__input js-mask-phone" type="text" name="PHONE_PERSONAL"
                               <?= $arResult["formLabelTech"]["PHONE_PERSONAL"]["req"]; ?>
                               value="<?=!empty($arResult['infoEdit']["PHONE_PERSONAL"])?$arResult['infoEdit']["PHONE_PERSONAL"]:'';?>">
                    </label>
                </div>
                <? if ($arResult["formLabelTech"]["PHONE_PERSONAL"]["text"]): ?>
                    <div class="hint">
                        <div class="hint__message">
                            <?= $arResult["formLabelTech"]["PHONE_PERSONAL"]["text"]; ?>
                        </div>
                    </div>
                <? endif; ?>
                <? if ($arResult["formLabelTech"]["PHONE_PERSONAL"]["req"]): ?>
                    <div class="tooltip js-error-tooltip">
                        Поле <?= $arResult["formLabelTech"]["PHONE_PERSONAL"]["name"]; ?> не может
                        быть пустым
                    </div>
                <? endif; ?>
            </div>
            <div class="lk__form-req-text">
                <b>*</b> – поля, обязательные для заполнения
            </div>
            <? if (!empty($arResult['infoEdit'])): ?>
                <input type="hidden" name="edit" value="<?= $arResult['infoEdit']['id']; ?>">
            <? endif; ?>
            <button class="btn lk__form-submit" type="submit" data-click="upHref" data-url="/dealer/reclamation/">
                Отправить
            </button>
            <span class="mess"></span>
        </div>
    </form>
</div>