<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}

use Letsrock\Lib\Models\User;
use Letsrock\Lib\Models\Element;
use Letsrock\Lib\Models\HLBlock;

\Bitrix\Main\Loader::includeModule('letsrock.lib');

class MarketingComponent extends CBitrixComponent
{
    public function propIsDop($code, $iblock)
    {
        $prMarkHl = new HLBlock(HL_POPSDOP);
        $arMarkProp_db = $prMarkHl->get(["UF_CODE_PROP" => $code, "UF_IBLOCK_ID"=>$iblock]);
        if (!empty($arMarkProp_db)) {
            return $arMarkProp_db[0];
        } else {
            return [];
        }
    }

    public function onPrepareComponentParams($arParams)
    {
        $result = $arParams;

        return $result;
    }

    /**
     * @return array|mixed
     * @throws \Bitrix\Main\LoaderException
     */
    public function executeComponent()
    {
        global $USER;
        if ($this->startResultCache($this->arParams["CACHE_TIME"]) && $USER->GetID()) {
            $elemIbl = new Element;
            $prMarkHl = new HLBlock(HL_MARKPROPS);
            $arMarkProp_db = $prMarkHl->get();
            $aVidsProp = [];
            $searchPropVids = [];
            foreach ($arMarkProp_db as $item) {
                $aVidsProp[$item["UF_TYPE_PROP"]] = $item["UF_PROPS_PIC"];
                foreach ($item["UF_PROPS_PIC"] as $picp) {
                    $searchPropVids[$item["UF_TYPE_PROP"]][] = $picp;
                }
            }
            $searchPropVids[0][] = "TYPE_KOMP";
            $searchPropVids[1][] = "PHONE_PERSONAL";
            if (!empty($this->arParams["EDTI_ID"])) {
                $infoMarketing = $elemIbl->GetList(IB_MARKETING,
                    ["ID", "IBLOCK_ID", "NAME", "CODE", "DATE_ACTIVE_FROM", "PROPERTY_*"],
                    ["ACTIVE_DATE" => "Y", "ACTIVE" => "Y", "PROPERTY_DILER" => $USER->GetID(), "ID" => $this->arParams["EDTI_ID"]],
                    ["ID" => "desc"]
                );
            } else {
                $infoMarketing = $elemIbl->GetList(IB_MARKETING,
                    ["ID", "IBLOCK_ID", "NAME", "CODE", "DATE_ACTIVE_FROM", "PROPERTY_*"],
                    ["ACTIVE_DATE" => "Y", "ACTIVE" => "Y", "PROPERTY_DILER" => $USER->GetID()],
                    ["ID" => "desc"]
                );
            }
            $arMarketings = [];
            foreach ($infoMarketing["ELEMS"] as $info) {
                $arProps = $infoMarketing["PROPS"][$info["ID"]];
                $type = $arProps["TYPE_KOMP"]["~VALUE"];
                $status = $arProps["STATUS"]["~VALUE"];
                $type_id = $arProps["TYPE_KOMP"]["VALUE_ENUM_ID"];
                $status_id = $arProps["STATUS"]["VALUE_ENUM_ID"];
                unset($arProps["TYPE_KOMP"]);
                unset($arProps["STATUS"]);
                $arFiles = [];
                foreach ($searchPropVids[$type_id] as $searchPropVid) {
                    $itemPropVids = $arProps[$searchPropVid];
                    if (!empty($itemPropVids)) {
                        if ($itemPropVids["PROPERTY_TYPE"] == 'F') {
                            $src = [];
                            if (is_array($itemPropVids["VALUE"])) {
                                foreach ($itemPropVids["VALUE"] as $id) {
                                    $file = CFile::GetFileArray($id);
                                    if (!empty($file))
                                        $src[] = $file;
                                }
                            } else {
                                $file_one = CFile::GetFileArray($itemPropVids["VALUE"]);
                                if (!empty($file_one))
                                    $src = CFile::GetFileArray($itemPropVids["VALUE"]);
                            }
                            if (!empty($src))
                                $arFiles[$itemPropVids["CODE"]] = [
                                    "info" => $src,
                                    "name" => $itemPropVids["NAME"],
                                    "dop_filds" => $this->propIsDop($itemPropVids["CODE"], IB_MARKETING),
                                ];
                        }
                    }
                }
                $arMarketings[] = [
                    "id" => $info["ID"],
                    "number" => 'M' . $info["ID"],
                    "type" => $type,
                    "type_id" => $type_id,
                    "PHONE_PERSONAL" => $arProps["PHONE_PERSONAL"]["VALUE"],
                    "refusal" => $arProps["REFUSAL"]["VALUE"]["TEXT"],
                    "status" => $status,
                    "status_id" => $status_id,
                    "files" => $arFiles
                ];
            }
            $listSelect = [];
            $db_enum_list = CIBlockProperty::GetPropertyEnum("TYPE_KOMP", Array("sort" => "asc"), Array("IBLOCK_ID" => IB_MARKETING));
            while ($ar_enum_list = $db_enum_list->GetNext()) {
                if (!empty($aVidsProp[$ar_enum_list["ID"]]))
                    $listSelect[] = $ar_enum_list;
            }
            $arTextMark = [];
            foreach ($searchPropVids as $searchPropVid) {
                foreach ($searchPropVid as $propVid) {
                    $properties = CIBlockProperty::GetList(Array("sort" => "asc"), Array("ACTIVE" => "Y", "IBLOCK_ID" => IB_MARKETING, "CODE" => $propVid));
                    if ($prop_fields = $properties->GetNext()) {
                        $req = '';
                        $req_class2 = '';
                        if ($prop_fields["IS_REQUIRED"] == 'Y') {
                            $req = 'required';
                            $req_class2 = 'field__required';
                        }
                        if ($prop_fields["MULTIPLE"] == 'N') {
                            $arTextMark["NoMulti"][$prop_fields["CODE"]] = [
                                "name" => $prop_fields["NAME"],
                                "file_type" => $prop_fields["FILE_TYPE"],
                                "text" => $prop_fields["HINT"],
                                "req" => $req,
                                "multi" => "",
                                "req2" => $req_class2,
                                "dop_filds" => $this->propIsDop($prop_fields["CODE"], IB_MARKETING)
                            ];
                        } else {
                            $arTextMark["NoMulti"][$prop_fields["CODE"]] = [
                                "name" => $prop_fields["NAME"],
                                "file_type" => $prop_fields["FILE_TYPE"],
                                "text" => $prop_fields["HINT"],
                                "req" => $req,
                                "multi" => "Y",
                                "req2" => $req_class2,
                                "dop_filds" => $this->propIsDop($prop_fields["CODE"], IB_MARKETING)
                            ];
                        }
                    }
                }
            }
            $pagen = [];
            $arPage = [];
            if (!empty($arMarketings)) {
                $arPage = array_chunk($arMarketings, $this->arParams["PAGE_ELEMS"]);
                if (count($arPage) > 1) {
                    for ($i = 1; $i <= count($arPage); $i++) {
                        $pagen[] = $i;
                    }
                }
            }

            $arFullParams = [
                "listSelect" => $listSelect,
                "arTextMark" => $arTextMark,
                "propVisible" => $aVidsProp,
                "arMarketings" => $arPage[$_REQUEST["page"] ? $_REQUEST["page"] - 1 : 0],
                "pagination" => $pagen
            ];
            $this->arResult = $arFullParams;
            $this->includeComponentTemplate();
        }
        return $this->arResult;
    }
} ?>