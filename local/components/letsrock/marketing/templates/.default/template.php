<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}

if (!$arResult) {
    return null;
}
?>
<div class="lk__content-box">
    <section class="lk__tabs-section">
        <article class="lk__tabs-content-item active" data-tab-content="">
            <div class="lk__tabs-content-inner js-tabs-content-inner <?= $arParams["EDTI_ID"] ? '' : 'active'; ?>">
                <div class="lk__tabs-top">
                    <a class="btn btn_small lk__tabs-top-btn js-tabs-show-form <?= $arParams["EDTI_ID"] ? 'hide' : ''; ?>"
                       href="javascript:void(0)">Подать документы</a>
                </div>

                <div class="lk__table marketing">
                    <div class="lk__table-head">
                        <div class="lk__table-row">
                            <div class="lk__table-col">
                                Номер
                            </div>
                            <div class="lk__table-col">
                                Тип
                            </div>
                            <div class="lk__table-col">
                                Статус
                            </div>
                            <div class="lk__table-col">
                            </div>
                        </div>
                    </div>
                    <? if (!empty($arResult["arMarketings"])): ?>
                        <div class="lk__table-body">
                            <? foreach ($arResult["arMarketings"] as $itemM): ?>
                                <div class="lk__table-row-wrap js-table-detail">
                                    <div class="lk__table-row">
                                        <div class="lk__table-col">
                                            <div class="lk__table-col-head">
                                                Номер
                                            </div>
                                            <?= $itemM["number"]; ?>
                                        </div>
                                        <div class="lk__table-col">
                                            <div class="lk__table-col-head">
                                                Тип
                                            </div>
                                            <?= $itemM["type"]; ?>
                                        </div>
                                        <div class="lk__table-col">
                                            <div class="lk__table-col-head">
                                                Статус
                                            </div>
                                            <?= $itemM["status"]; ?>
                                        </div>
                                        <div class="lk__table-col">
                                            <a class="lk__link js-table-detail-link" href="javascript:void(0)"
                                               data-show-text="Свернуть">
                                                Подробнее
                                            </a>
                                        </div>
                                    </div>
                                    <? if (!empty($itemM["files"]) || $itemM["status_id"] == 25): ?>
                                        <div class="lk__table-row-data js-table-detail-data">
                                            <div class="lk__data">
                                                <? if (!empty($itemM["files"])): ?>
                                                    <? foreach ($itemM["files"] as $file): ?>
                                                        <? if (!empty($file["info"][0])): ?>
                                                            <div class="lk__data-separate"></div>
                                                        <? endif; ?>
                                                        <div class="lk__data-row">
                                                            <div class="lk__data-label">
                                                                <?= $file["name"]; ?>
                                                            </div>
                                                            <div class="lk__data-value">
                                                                <div class="lk__data-files">
                                                                    <? if (!empty($file["info"][0])): ?>
                                                                        <? foreach ($file["info"] as $infoFile): ?>
                                                                            <a class="link-file"
                                                                               href="<?= $infoFile["SRC"]; ?>"
                                                                               download><?= $infoFile["ORIGINAL_NAME"]; ?></a>
                                                                        <? endforeach; ?>
                                                                    <? else: ?>
                                                                        <a class="link-file"
                                                                           href="<?= $file["info"]["SRC"]; ?>"
                                                                           download><?= $file["info"]["ORIGINAL_NAME"]; ?></a>
                                                                    <? endif; ?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    <? endforeach; ?>
                                                <? endif; ?>
                                                <? if ($itemM["status_id"] == 25): ?>
                                                    <div class="lk__data-row">
                                                        <div class="lk__data-label">
                                                            <? if (!empty($itemM["refusal"])): ?>Причина отказа<? endif; ?>
                                                        </div>
                                                        <div class="lk__data-value">
                                                            <? if (!empty($itemM["refusal"])): ?>
                                                                <div class="lk__data-value">
                                                                    <div class="lk__data-box">
                                                                        <?= $itemM["refusal"]; ?>
                                                                    </div>
                                                                </div>
                                                            <? endif; ?>
                                                            <div class="btn-group">
                                                                <a class="btn"
                                                                   href="/dealer/marketing/?edit_mark=<?= $itemM["id"]; ?>"
                                                                   data-taskid="<?= $itemM["id"]; ?>"
                                                                   data-click="editTask">Редактировать</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                <? endif; ?>
                                            </div>
                                        </div>
                                    <? endif; ?>
                                </div>
                            <? endforeach; ?>
                        </div>
                    <? endif; ?>
                </div>
                <? $APPLICATION->IncludeComponent("letsrock:paginations",
                    ".default",
                    [
                        "CACHE_TIME" => 0,
                        "ELEMS_PAGE" => $arResult["pagination"],
                        "GET_PAGE" => "page",
                        "AJAX_BLOCK" => ""
                    ]
                ); ?>
            </div>
            <?
            $editElement = [];
            if (!empty($arParams["EDTI_ID"])) {
                $editElement = $arResult["arMarketings"][0];
            }
            ?>
            <div class="lk__tabs-content-form js-tabs-form <?= $arParams["EDTI_ID"] ? 'active' : ''; ?>">
                <a class="btn-arrow js-tabs-back" href="javascript:void(0)">
                    <i class="icon-back"></i>
                    <span>Назад</span>
                </a>
                <form action="/ajax/dealer/addMark/" method="POST" class="js-form-marketing">
                    <div class="lk__form-section">
                        <div class="lk__form-box">
                            <div class="lk__form-title">
                                Выберите <?= $arResult["arTextMark"]["NoMulti"]["TYPE_KOMP"]["name"]; ?>
                            </div>
                            <div class="lk__form-row js-field">
                                <div class="lk__form-field <?= $arResult["arTextMark"]["NoMulti"]["TYPE_KOMP"]["req"]; ?>">
                                    <select class="select js-select js-tabs-select"
                                            name="TYPE_KOMP"
                                            <?= $arResult["arTextMark"]["NoMulti"]["TYPE_KOMP"]["req"]; ?>>
                                        <option disabled selected
                                                hidden><?= $arResult["arTextMark"]["NoMulti"]["TYPE_KOMP"]["name"]; ?></option>
                                        <? foreach ($arResult["listSelect"] as $option): ?>
                                            <? if (!empty($editElement) && $editElement["type_id"] == $option["ID"]): ?>
                                                <option value="<?= $option["ID"]; ?>"
                                                        selected><?= $option["VALUE"]; ?></option>
                                            <? else: ?>
                                                <option value="<?= $option["ID"]; ?>"><?= $option["VALUE"]; ?></option>
                                            <? endif; ?>
                                        <? endforeach; ?>
                                    </select>
                                </div>
                                <? if ($arResult["arTextMark"]["NoMulti"]["TYPE_KOMP"]["text"]): ?>
                                    <div class="hint">
                                        <div class="hint__message">
                                            <?= $arResult["arTextMark"]["NoMulti"]["TYPE_KOMP"]["text"]; ?>
                                        </div>
                                    </div>
                                <? endif; ?>
                                <? if ($arResult["arTextMark"]["NoMulti"]["TYPE_KOMP"]["req"]): ?>
                                    <div class="tooltip js-error-tooltip">
                                        Поле <?= $arResult["arTextMark"]["NoMulti"]["TYPE_KOMP"]["name"]; ?> не может
                                        быть пустым
                                    </div>
                                <? endif; ?>
                            </div>
                            <br/>
                            <div class="lk__form-row js-field">
                                <div class="lk__form-field <?= $arResult["arTextMark"]["NoMulti"]["PHONE_PERSONAL"]["req"]; ?>">
                                    <label class="field <?= $editElement['PHONE_PERSONAL'] ? 'focused' : ''; ?>">
                                        <span class="field__title <?= $arResult["arTextMark"]["NoMulti"]["PHONE_PERSONAL"]["req2"]; ?>"><?= $arResult["arTextMark"]["NoMulti"]["PHONE_PERSONAL"]["name"]; ?></span>
                                        <input class="field__input js-mask-phone" type="text" name="PHONE_PERSONAL"
                                               <?= $arResult["arTextMark"]["NoMulti"]["PHONE_PERSONAL"]["req"]; ?>
                                               value="<?= !empty($editElement["PHONE_PERSONAL"]) ? $editElement["PHONE_PERSONAL"] : ''; ?>">
                                    </label>
                                </div>
                                <? if ($arResult["arTextMark"]["NoMulti"]["PHONE_PERSONAL"]["text"]): ?>
                                    <div class="hint">
                                        <div class="hint__message">
                                            <?= $arResult["arTextMark"]["NoMulti"]["PHONE_PERSONAL"]["text"]; ?>
                                        </div>
                                    </div>
                                <? endif; ?>
                                <? if ($arResult["arTextMark"]["NoMulti"]["PHONE_PERSONAL"]["req"]): ?>
                                    <div class="tooltip js-error-tooltip">
                                        Поле <?= $arResult["arTextMark"]["NoMulti"]["PHONE_PERSONAL"]["name"]; ?> не
                                        может
                                        быть пустым
                                    </div>
                                <? endif; ?>
                            </div>
                        </div>
                    </div>
                    <? if (!empty($arResult["propVisible"])): ?>
                        <div class="lk__form-fieldset-list js-tabs-select-content">
                            <? foreach ($arResult["propVisible"] as $id_type => $section): ?>
                                <fieldset <?= $editElement["type_id"] == $id_type ? '' : 'disabled'; ?>
                                          class="lk__form-fieldset" data-form-fieldset="<?= $id_type; ?>">
                                    <? if (!empty($arResult["arTextMark"]["NoMulti"])): ?>
                                        <div class="lk__form-title">
                                            Прикрепите необходимые документы
                                        </div>
                                        <? $firstOne = ''; ?>
                                        <? foreach ($section as $key => $props): ?>
                                            <?
                                            $elems = $arResult["arTextMark"]["NoMulti"][$props];
                                            $nextElem = $arResult["arTextMark"]["NoMulti"][$section[$key + 1]];
                                            ?>
                                            <? if (empty($elems)) continue; ?>
                                            <? if (!empty($elems["multi"])): ?>
                                                <div class="lk__form-section">
                                                    <? if (!empty($editElement["files"][$props])): ?>
                                                        <? foreach ($editElement["files"][$props]["info"] as $file): ?>
                                                            <div class="lk__form-row js-field"
                                                                 data-req="<?= $elems["req"] ? 'Y' : 'N'; ?>">
                                                                <div class="lk__form-field">
                                                                    <div class="lk__form-file selected">
                                                                        <div class="lk__download">
                                                                            <div class="lk__download-title"
                                                                                 data-default-title="<?= $elems["name"]; ?>">
                                                                                <?= $file['FILE_NAME']; ?>
                                                                            </div>
                                                                            <? if (!empty($editElement["files"][$props]["dop_filds"]["UF_SAMPLE"])): ?>
                                                                                <a href="<?= CFile::GetPath($editElement["files"][$props]["dop_filds"]["UF_SAMPLE"]); ?>"
                                                                                   class="lk__download-link" download>
                                                                                    Скачать форму
                                                                                </a>
                                                                            <? endif; ?>
                                                                        </div>
                                                                        <label class="field-file">
                                                                            <input class="field-file__input <?=!empty($elems["file_type"])?'js-file':'';?>"
                                                                                   type="file"
                                                                                   <?= $editElement["type_id"] == $id_type ? $elems["req"] : ''; ?>
                                                                                   name="<?= $props; ?>[]"
                                                                                   <?=!empty($elems["file_type"])?'data-type="'.$elems["file_type"].'"':'';?>>
                                                                            <span class="field-file__title"
                                                                                  data-default-title="Загрузить">Удалить файл</span>
                                                                        </label>
                                                                        <input type="hidden"
                                                                               name="<?= strtolower($props); ?>_id[]"
                                                                               data-editfile
                                                                               value="<?= $file['ID']; ?>"/>
                                                                    </div>
                                                                </div>
                                                                <? if ($elems["text"]): ?>
                                                                    <div class="hint">
                                                                        <div class="hint__message">
                                                                            <?= $elems["text"]; ?>
                                                                        </div>
                                                                    </div>
                                                                <? endif; ?>
                                                                <? if ($elems["req"]): ?>
                                                                    <div class="tooltip js-error-tooltip">
                                                                        Поле <?= $elems["name"]; ?> не может быть пустым
                                                                    </div>
                                                                <? endif; ?>
                                                            </div>
                                                        <? endforeach; ?>
                                                    <? else: ?>
                                                        <div class="lk__form-row js-field"
                                                             data-req="<?= $elems["req"] ? 'Y' : 'N'; ?>">
                                                            <div class="lk__form-field">
                                                                <div class="lk__form-file">
                                                                    <div class="lk__download">
                                                                        <div class="lk__download-title <?= $editElement["type_id"] == $id_type ? $elems["req2"] : ''; ?>">
                                                                            <?= $elems["name"]; ?>
                                                                        </div>
                                                                        <? if (!empty($elems["dop_filds"]["UF_SAMPLE"])): ?>
                                                                            <a href="<?= CFile::GetPath($elems["dop_filds"]["UF_SAMPLE"]); ?>"
                                                                               class="lk__download-link" download>
                                                                                Скачать форму
                                                                            </a>
                                                                        <? endif; ?>
                                                                    </div>
                                                                    <label class='field-file'>
                                                                        <input class="field-file__input <?=!empty($elems["file_type"])?'js-file':'';?>"
                                                                               type="file"
                                                                               <?= $editElement["type_id"] == $id_type ? $elems["req"] : ''; ?>
                                                                               name="<?= $props; ?>[]"
                                                                               <?=!empty($elems["file_type"])?'data-type="'.$elems["file_type"].'"':'';?>">
                                                                        <span class="field-file__title">Загрузить</span>
                                                                    </label>
                                                                </div>
                                                            </div>
                                                            <? if ($elems["text"]): ?>
                                                                <div class="hint">
                                                                    <div class="hint__message">
                                                                        <?= $elems["text"]; ?>
                                                                    </div>
                                                                </div>
                                                            <? endif; ?>
                                                            <? if ($elems["req"]): ?>
                                                                <div class="tooltip js-error-tooltip">
                                                                    Поле <?= $elems["name"]; ?> не может быть пустым
                                                                </div>
                                                            <? endif; ?>
                                                        </div>
                                                    <? endif; ?>
                                                    <div class="lk__form-row">
                                                        <a class="lk__form-add js-lk-row-add" href="javascript:void(0)">Еще</a>
                                                    </div>
                                                </div>
                                            <? else: ?>
                                                <? if (empty($firstOne)):$firstOne = 'Y'; ?>
                                                    <div class="lk__form-section">
                                                <? endif; ?>
                                                <div class="lk__form-row js-field"
                                                     data-req="<?= $elems["req"] ? 'Y' : 'N'; ?>">
                                                    <div class="lk__form-field">
                                                        <? if (!empty($editElement["files"][$props])): ?>
                                                            <div class="lk__form-file selected">
                                                                <div class="lk__download">
                                                                    <div class="lk__download-title"
                                                                         data-default-title="<?= $elems["name"]; ?>">
                                                                        <?= $editElement["files"][$props]["info"]['FILE_NAME']; ?>
                                                                    </div>
                                                                    <? if (!empty($editElement["files"][$props]["dop_filds"]["UF_SAMPLE"])): ?>
                                                                        <a href="<?= CFile::GetPath($editElement["files"][$props]["dop_filds"]["UF_SAMPLE"]); ?>"
                                                                           class="lk__download-link" download>
                                                                            Скачать форму
                                                                        </a>
                                                                    <? endif; ?>
                                                                </div>
                                                                <label class="field-file">
                                                                    <input class="field-file__input <?=!empty($elems["file_type"])?'js-file':'';?>"
                                                                           type="file"
                                                                           <?= $editElement["type_id"] == $id_type ? $elems["req"] : ''; ?>
                                                                           name="<?= $props; ?>"
                                                                        <?=!empty($elems["file_type"])?'data-type="'.$elems["file_type"].'"':'';?>>
                                                                    <span class="field-file__title"
                                                                          data-default-title="Загрузить">Удалить файл</span>
                                                                </label>
                                                                <input type="hidden"
                                                                       name="<?= strtolower($props); ?>_id"
                                                                       data-editfile
                                                                       value="<?= $editElement["files"][$props]["info"]['ID']; ?>"/>
                                                            </div>
                                                        <? else: ?>
                                                            <div class="lk__form-file">
                                                                <div class="lk__download">
                                                                    <div class="lk__download-title <?= $editElement["type_id"] == $id_type ? $elems["req2"] : ''; ?>">
                                                                        <?= $elems["name"]; ?>
                                                                    </div>
                                                                    <? if (!empty($elems["dop_filds"]["UF_SAMPLE"])): ?>
                                                                        <a href="<?= CFile::GetPath($elems["dop_filds"]["UF_SAMPLE"]); ?>"
                                                                           class="lk__download-link" download>
                                                                            Скачать форму
                                                                        </a>
                                                                    <? endif; ?>
                                                                </div>
                                                                <label class='field-file'>
                                                                    <input class="field-file__input <?=!empty($elems["file_type"])?'js-file':'';?>"
                                                                           type="file"
                                                                           <?= $editElement["type_id"] == $id_type ? $elems["req"] : ''; ?>
                                                                           name="<?= $props; ?>"
                                                                        <?=!empty($elems["file_type"])?'data-type="'.$elems["file_type"].'"':'';?>>
                                                                    <span class="field-file__title">Загрузить</span>
                                                                </label>
                                                            </div>
                                                        <? endif; ?>
                                                    </div>
                                                    <? if ($elems["text"]): ?>
                                                        <div class="hint">
                                                            <div class="hint__message">
                                                                <?= $elems["text"]; ?>
                                                            </div>
                                                        </div>
                                                    <? endif; ?>
                                                    <? if ($elems["req"]): ?>
                                                        <div class="tooltip js-error-tooltip">
                                                            Поле <?= $elems["name"]; ?> не может быть пустым
                                                        </div>
                                                    <? endif; ?>
                                                </div>
                                                <? if (!empty($firstOne) && !empty($nextElem["multi"])):$firstOne = ''; ?>
                                                    </div>
                                                <? endif; ?>
                                            <? endif; ?>
                                        <? endforeach; ?>
                                    <? endif; ?>
                                    <div class="lk__form-req-text">
                                        <b>*</b> – поля, обязательные для заполнения
                                    </div>
                                    <button class="btn lk__form-submit" type="submit" data-click="upHref"
                                            data-url="/dealer/marketing/">Подать компенсацию
                                    </button>
                                    <span class="mess"></span>
                                </fieldset>
                            <? endforeach; ?>
                        </div>
                    <? endif; ?>
                    <? if (!empty($editElement)): ?>
                        <input type="hidden" name="edit" value="<?= $editElement['id']; ?>">
                    <? endif; ?>
                </form>
            </div>
        </article>
    </section>
</div>