<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
$arComponentDescription = array(
    "NAME" => 'Маркетинговые компенсации',
    "DESCRIPTION" => 'Маркетинговые компенсации',
    "PATH" => array(
        "ID" => "dv_components",
        "CHILD" => array(
            "ID" => "marketing",
            "NAME" => "Маркетинговые компенсации"
        )
    ),
);
?>