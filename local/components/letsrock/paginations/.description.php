<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
$arComponentDescription = array(
    "NAME" => 'Покупатели',
    "DESCRIPTION" => 'Покупатели',
    "PATH" => array(
        "ID" => "dv_components",
        "CHILD" => array(
            "ID" => "buyers",
            "NAME" => "Покупатели"
        )
    ),
);
?>