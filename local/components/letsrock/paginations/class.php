<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}

use Letsrock\Lib\Models\User;
use Letsrock\Lib\Models\Element;
use Letsrock\Lib\Models\HLBlock;

\Bitrix\Main\Loader::includeModule('letsrock.lib');

class PaginationsComponent extends CBitrixComponent
{
    public function onPrepareComponentParams($arParams)
    {
        $result = $arParams;

        return $result;
    }

    /**
     * @return array|mixed
     * @throws \Bitrix\Main\LoaderException
     */
    public function executeComponent()
    {
        if ($this->startResultCache($this->arParams["CACHE_TIME"]) && !empty($this->arParams["ELEMS_PAGE"])) {
            $this->arResult = [
                "elems"=>$this->arParams["ELEMS_PAGE"],
                "get_page"=>$this->arParams["GET_PAGE"],
                "ajax_block"=>$this->arParams["AJAX_BLOCK"],
                "page_id" => $this->arParams["PAGE_ID"],
            ];
            $this->includeComponentTemplate();
        }
        return $this->arResult;
    }
} ?>