<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}

if (!$arResult) {
    return null;
}
?>
<?if(!empty($arResult["elems"])):?>
    <?if($arResult["ajax_block"]):?>
        <div class="pagination" data-pagenajax="<?=$arResult["ajax_block"];?>">
            <?if($arParams["PAGEN_HASH"]):?>
                <!--startAjaxPagination<?=$arParams["PAGEN_HASH"];?>-->
            <?else:?>
                <!--startAjaxPagination-->
            <?endif;?>
    <?else:?>
        <div class="pagination">
    <?endif;?>
        <?$req_page = $_REQUEST[$arResult["get_page"]]?$_REQUEST[$arResult["get_page"]]:1;?>
        <?if($req_page > 5):?>
            <a href="<?=$APPLICATION->GetCurPageParam($arResult["get_page"]."=1", array($arResult["get_page"]));?><?=$arResult["page_id"]?'#'.$arResult["page_id"]:'';?>" class="pagination__item">1</a>
            <span class="pagination__item">...</span>
        <?endif;?>
        <?foreach ($arResult["elems"] as $key=> $page):?>
            <?if($req_page == $page):?>
                <a href="<?=$APPLICATION->GetCurPageParam($arResult["get_page"]."=".$page, array($arResult["get_page"]));?><?=$arResult["page_id"]?'#'.$arResult["page_id"]:'';?>" class="pagination__item active"><?=$page;?></a>
            <?else:?>
                <?if(($key+3 < $req_page) && ($req_page > 3)):?>
                <?else:?>
                    <a href="<?=$APPLICATION->GetCurPageParam($arResult["get_page"]."=".$page, array($arResult["get_page"]));?><?=$arResult["page_id"]?'#'.$arResult["page_id"]:'';?>" class="pagination__item"><?=$page;?></a>
                <?endif;?>
            <?endif;?>
            <?if(($key > $req_page) && $arResult["elems"][count($arResult["elems"])-1] != $page):?>
                <span class="pagination__item">...</span>
                <a href="<?=$APPLICATION->GetCurPageParam($arResult["get_page"]."=".$arResult["elems"][count($arResult["elems"])-1], array($arResult["get_page"]));?><?=$arResult["page_id"]?'#'.$arResult["page_id"]:'';?>" class="pagination__item"><?=$arResult["elems"][count($arResult["elems"])-1];?></a>
            <?break;endif;?>
        <?endforeach;?>
    <?if($arResult["ajax_block"]):?>
        <?if($arParams["PAGEN_HASH"]):?>
            <!--endAjaxPagination<?=$arParams["PAGEN_HASH"];?>-->
        <?else:?>
            <!--endAjaxPagination-->
        <?endif;?>
    <?endif;?>
    </div>
<?endif;?>