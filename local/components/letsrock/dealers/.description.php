<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
$arComponentDescription = array(
    "NAME" => 'Список дилеров',
    "DESCRIPTION" => 'Список дилеров',
    "PATH" => array(
        "ID" => "dv_components",
        "CHILD" => array(
            "ID" => "dealers",
            "NAME" => "Список дилеров"
        )
    ),
);
?>