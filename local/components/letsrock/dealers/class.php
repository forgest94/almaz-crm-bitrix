<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}

use Letsrock\Lib\Models\User;
use Letsrock\Lib\Models\Element;
use Letsrock\Lib\Models\HLBlock;

\Bitrix\Main\Loader::includeModule('letsrock.lib');

class DealersComponent extends CBitrixComponent
{
    public function onPrepareComponentParams($arParams)
    {
        $result = $arParams;

        return $result;
    }

    public function getUser($id)
    {
        if ($id) {
            return CUser::GetByID($id)->Fetch();
        }
    }

    public function isTasks($id_dealer, $type_vid, $request = false)
    {
        global $USER;
        $isTask = 0;
        $arTasks = [];
        switch ($this->arParams["PANEL"]) {
            case 'manager':
                $groupManager = $USER->GetUserGroupArray();
                if (in_array(ID_MANAGER_SERVICES, $groupManager)) {
                    $visibleTask[] = IB_EXPLOITATION;
                    $visibleTask[] = IB_SERVCOMP;
                }
                if (in_array(ID_MANAGER_PARTS, $groupManager)) {
                    $visibleTask[] = IB_RECLAMATION_PARTS;
                }
                if (in_array(ID_MANAGER_TECH, $groupManager)) {
                    $visibleTask[] = IB_RECLAMATION_TECH;
                }
                if (in_array(ID_MANAGER_MARKETING, $groupManager)) {
                    $visibleTask[] = IB_MARKETING;
                }
                break;
            case 'administrator':
                $visibleTask = [IB_EXPLOITATION, IB_SERVCOMP, IB_RECLAMATION_PARTS, IB_RECLAMATION_TECH, IB_MARKETING];
                break;
        }
        if (!empty($visibleTask)) {
            $elemIbl = new Element;
            /**
             * Ввод в эксплуатацию
             */
            if (in_array(IB_EXPLOITATION, $visibleTask)) {
                $arExplotations_db = $elemIbl->GetList(IB_EXPLOITATION,
                    ["NAME", "ID", "IBLOCK_ID", "DATE_CREATE", "PROPERTY_*"],
                    [
                        "PROPERTY_STATUS_VALUE" => "На рассмотрении",
                        "!PROPERTY_DEALER" => false,
                        "PROPERTY_DEALER" => $id_dealer,
                        "!NAME" => "Техника не введена в эксплуатацию",
                    ],
                    ["created_date" => "desc"]
                );
                $typeTechHl = new HLBlock(HL_EQUIPMENT);
                foreach ($arExplotations_db["ELEMS"] as $item) {
                    $arProps = $arExplotations_db["PROPS"][$item["ID"]];
                    $status = $arProps["STATUS"]["VALUE"];
                    $isNew = '';
                    if ($status == 'На рассмотрении') {
                        $isTask++;
                        $isNew = 'Y';
                    }

                    if ($type_vid == "detail") {
                        $infoBuyer = $elemIbl->GetList(IB_BUYERS, ["NAME", "ID", "IBLOCK_ID", "PROPERTY_*"], ["ID" => $arProps["BUYER"]["VALUE"], "!PROPERTY_TYPE_VALUE" => false]);
                        $fildsBuyer = $infoBuyer["ELEMS"][0];
                        $porpsBuyer = $infoBuyer["PROPS"][$fildsBuyer["ID"]];
                        $infoModel = $elemIbl->getById(IB_DIRECTORY, $arProps["MODEL"]["VALUE"]);
                        $infoType = $typeTechHl->get(["UF_XML_ID" => $arProps["EQUIPMENT"]["VALUE"]])[0];
                        $infoRegionBuyer = $elemIbl->getById(IB_REGION, $porpsBuyer["REGION"]["VALUE"]);
                        $infoDistrictBuyer = $elemIbl->getById(IB_AREA, $porpsBuyer["DISTRICT"]["VALUE"]);
                        $akt_parse = [];
                        if (!empty($arProps["FILE"]["VALUE"])) {
                            $akt = CFile::GetFileArray($arProps["FILE"]["VALUE"]);
                            if (!empty($akt))
                                $akt_parse = ["name" => $akt["ORIGINAL_NAME"], "src" => $akt["SRC"]];
                        }
                        $arPhoto = [];
                        if (!empty($arProps["PHOTO_TECH"]["VALUE"])) {
                            foreach ($arProps["PHOTO_TECH"]["VALUE"] as $itemP) {
                                $photo = CFile::GetFileArray($itemP);
                                if ($photo)
                                    $arPhoto[] = ["name" => $photo["ORIGINAL_NAME"], "src" => $photo["SRC"]];
                            }
                        }
                        if (!empty($arProps["DATE_EXECUTION"]["VALUE"])) {
                            $due_date = $arProps["DATE_EXECUTION"]["VALUE"];
                        } else {
                            $deteProp = explode(' ', $item["DATE_CREATE"])[0];
                            $due_date = date('d.m.Y', strtotime($deteProp . DAY_EXECUTION));
                        }
                        $error = '';
                        if ((strtotime(date("d.m.Y")) > strtotime($due_date)) && !empty($isNew)) {
                            $error = 'Y';
                        }
                        $arTasks[] = [
                            "id" => $item["ID"],
                            "vid" => "expl",
                            "error" => $error,
                            "type" => $infoType["UF_NAME"],
                            "model" => $infoModel["NAME"],
                            "date" => $item["DATE_CREATE"],
                            "date_sort" => strtotime($item["DATE_CREATE"]),
                            "date_expl" => $arProps["DATE"]["VALUE"],
                            "status" => $status,
                            "status_arr" => ["reject_id" => 14, "confirm_id" => 15],
                            "akt" => $akt_parse,
                            "photo" => $arPhoto,
                            "phone" => $arProps["PHONE_PERSONAL"]["VALUE"],
                            "name" => $item["NAME"],
                            "due_date" => $due_date,
                            "new" => $isNew,
                            "status_id" => $arProps["STATUS_ID"]["VALUE"],
                            "number" => $arProps["NUMBER"]["VALUE"],
                            "dealer" => $this::getUser($arProps["DEALER"]["VALUE"]),
                            "buyer" => [
                                "name" => $item["NAME"],
                                "contact_people" => $porpsBuyer["CONTACT_PEOPLE"]["VALUE"],
                                "phone" => $porpsBuyer["PHONE"]["VALUE"],
                                "inn" => $porpsBuyer["INN"]["VALUE"],
                                "position" => $porpsBuyer["POSITION"]["VALUE"],
                                "region" => $infoRegionBuyer["NAME"],
                                "district" => $infoDistrictBuyer["NAME"],
                                "address" => $porpsBuyer["ADDRESS"]["VALUE"],
                                "mail" => $porpsBuyer["EMAIL"]["VALUE"],
                                "arProps" => $porpsBuyer
                            ]
                        ];
                    }
                }
            }
            /**
             * Рекламации "Техника"
             */
            if (in_array(IB_RECLAMATION_TECH, $visibleTask)) {
                $arReclamationTech_db = $elemIbl->GetList(IB_RECLAMATION_TECH,
                    ["NAME", "ID", "IBLOCK_ID", "DATE_CREATE", "PROPERTY_*"],
                    [
                        "PROPERTY_DEALER" => $id_dealer,
                        /*[
                            "LOGIC" => "OR",
                            ["PROPERTY_STATUS_VALUE" => "Отправлено"],
                            ["PROPERTY_STATUS_VALUE" => "В работе"],
                        ]*/
                    ],
                    ["created_date" => "desc"]
                );
                foreach ($arReclamationTech_db["ELEMS"] as $tech) {
                    $arProps = $arReclamationTech_db["PROPS"][$tech["ID"]];
                    $status = $arProps["STATUS"]["VALUE"];
                    $isNew = '';
                    if ($status == 'Отправлено' || $status == 'В работе') {
                        $isTask++;
                        $isNew = 'Y';
                    }

                    if ($type_vid == "detail") {
                        $arBuyerInfo = [];
                        if ($arProps["SERIAL_NUMBER"]["VALUE"]) {
                            $infoExpl = $elemIbl->GetList(IB_EXPLOITATION, ["NAME", "ID", "IBLOCK_ID", "DATE_CREATE", "PROPERTY_*"], ["PROPERTY_DEALER" => $id_dealer, "ID" => $arProps["SERIAL_NUMBER"]], array("created_date" => "desc"));
                            $arPropsEx = $infoExpl["PROPS"][$infoExpl["ELEMS"][0]["ID"]];
                            if (!$arProps["NOT_OPERATION"]["VALUE"]) {
                                $infoBuyer = $elemIbl->GetList(IB_BUYERS, ["NAME", "ID", "IBLOCK_ID", "PROPERTY_*"], ["ID" => $arPropsEx["BUYER"]["VALUE"], "!PROPERTY_TYPE_VALUE" => false]);
                                $fildsBuyer = $infoBuyer["ELEMS"][0];
                                $porpsBuyer = $infoBuyer["PROPS"][$fildsBuyer["ID"]];
                                $infoRegionBuyer = $elemIbl->getById(IB_REGION, $porpsBuyer["REGION"]["VALUE"]);
                                $infoDistrictBuyer = $elemIbl->getById(IB_AREA, $porpsBuyer["DISTRICT"]["VALUE"]);
                                $arBuyerInfo = [
                                    "name" => $infoExpl["ELEMS"][0]["NAME"],
                                    "contact_people" => $porpsBuyer["CONTACT_PEOPLE"]["VALUE"],
                                    "phone" => $porpsBuyer["PHONE"]["VALUE"],
                                    "inn" => $porpsBuyer["INN"]["VALUE"],
                                    "position" => $porpsBuyer["POSITION"]["VALUE"],
                                    "region" => $infoRegionBuyer["NAME"],
                                    "district" => $infoDistrictBuyer["NAME"],
                                    "address" => $porpsBuyer["ADDRESS"]["VALUE"],
                                    "mail" => $porpsBuyer["EMAIL"]["VALUE"],
                                    "arProps" => $porpsBuyer
                                ];
                            }
                        } else {
                            continue;
                        }


                        /*$akt_parse = [];
                    if (!empty($arProps["AKT"]["VALUE"])) {
                        $akt = CFile::GetFileArray($arProps["AKT"]["VALUE"]);
                        if ($akt)
                            $akt_parse = ["name" => $akt["ORIGINAL_NAME"], "src" => $akt["SRC"]];
                    }*/
                        $akt_parse = [];
                        if (!empty($arProps["AKT"]["VALUE"])) {
                            foreach ($arProps["AKT"]["VALUE"] as $akt2) {
                                $arFile = CFile::GetFileArray($akt2);
                                if ($arFile)
                                    $akt_parse[] = ["name" => $arFile["ORIGINAL_NAME"], "src" => $arFile["SRC"]];
                            }
                        }
                        $arPritenzy = [];
                        if (!empty($arProps["PRITENZY"]["VALUE"])) {
                            foreach ($arProps["PRITENZY"]["VALUE"] as $photo) {
                                $arFile = CFile::GetFileArray($photo);
                                if ($arFile)
                                    $arPritenzy[] = ["name" => $arFile["ORIGINAL_NAME"], "src" => $arFile["SRC"]];
                            }
                        }
                        $arPhoto = [];
                        if (!empty($arProps["PHOTO"]["VALUE"])) {
                            foreach ($arProps["PHOTO"]["VALUE"] as $photo) {
                                $pare_phot = CFile::GetFileArray($photo);
                                if (!empty($pare_phot["ORIGINAL_NAME"]))
                                    $arPhoto[] = ["name" => $pare_phot["ORIGINAL_NAME"], "src" => $pare_phot["SRC"]];
                            }
                        }

                        if (!empty($arProps["DATE_EXECUTION"]["VALUE"])) {
                            $due_date = $arProps["DATE_EXECUTION"]["VALUE"];
                        } else {
                            $deteProp = explode(' ', $tech["DATE_CREATE"])[0];
                            $due_date = date('d.m.Y', strtotime($deteProp . DAY_EXECUTION));
                        }
                        $error = '';
                        if ((strtotime(date("d.m.Y")) > strtotime($due_date)) && !empty($isNew)) {
                            $error = 'Y';
                        }
                        if (strpos('---- ' . $tech["NAME"], 'RT') === false) {
                            $numberTechRecl = $tech["NAME"];
                        } else {
                            $numberTechRecl = 'RT' . $tech["ID"];
                        }
                        $arTasks[] = [
                            "id" => $tech["ID"],
                            "vid" => "tech",
                            "error" => $error,
                            "date" => $tech["DATE_CREATE"],
                            "date_sort" => strtotime($tech["DATE_CREATE"]),
                            "status" => $status,
                            "status_id" => $arProps["STATUS_ID"]["VALUE"],
                            "status_arr" => ["reject_id" => 20, "confirm_id" => 19],
                            "akt" => $akt_parse,
                            "pritenzy" => $arPritenzy,
                            "photo" => $arPhoto,
                            "phone" => $arProps["PHONE_PERSONAL"]["VALUE"],
                            "new" => $isNew,
                            "name" => "Рекламации - техника",
                            "reason" => $arProps["REASON"]["VALUE"]["TEXT"],
                            "wishes" => $arProps["WISHES"]["VALUE"]["TEXT"],
                            "due_date" => $due_date,
                            "number" => $numberTechRecl,
                            "dealer" => $this::getUser($arProps["DEALER"]["VALUE"]),
                            "buyer" => $arBuyerInfo
                        ];
                    }
                }
            }
            /**
             * Рекламации "Запчасти"
             */
            if (in_array(IB_RECLAMATION_PARTS, $visibleTask)) {
                $arReclamationParts_db = $elemIbl->GetList(IB_RECLAMATION_PARTS,
                    ["NAME", "ID", "IBLOCK_ID", "DATE_CREATE", "PROPERTY_*"],
                    [
                        "PROPERTY_DEALER" => $id_dealer,
                        /*[
                            "LOGIC" => "OR",
                            ["PROPERTY_STATUS_VALUE" => "Отправлено"],
                            ["PROPERTY_STATUS_VALUE" => "В работе"],
                        ],*/
                    ],
                    ["created_date" => "desc"]
                );
                foreach ($arReclamationParts_db["ELEMS"] as $parts) {
                    $arProps = $arReclamationParts_db["PROPS"][$parts["ID"]];
                    $status = $arProps["STATUS"]["VALUE"];
                    $isNew = '';
                    if ($status == 'Отправлено' || $status == 'В работе') {
                        $isTask++;
                        $isNew = 'Y';
                    }

                    if ($type_vid == "detail") {
                        $akt_parse = [];
                        if (!empty($arProps["AKT"]["VALUE"])) {
                            $akt = CFile::GetFileArray($arProps["AKT"]["VALUE"]);
                            if ($akt)
                                $akt_parse = ["name" => $akt["ORIGINAL_NAME"], "src" => $akt["SRC"]];
                        }
                        $arPhoto = [];
                        if (!empty($arProps["PHOTO"]["VALUE"])) {
                            foreach ($arProps["PHOTO"]["VALUE"] as $photo) {
                                $pare_phot = CFile::GetFileArray($photo);
                                if ($pare_phot)
                                    $arPhoto[] = ["name" => $pare_phot["ORIGINAL_NAME"], "src" => $pare_phot["SRC"]];
                            }
                        }

                        if (!empty($arProps["DATE_EXECUTION"]["VALUE"])) {
                            $due_date = $arProps["DATE_EXECUTION"]["VALUE"];
                        } else {
                            $deteProp = explode(' ', $parts["DATE_CREATE"])[0];
                            $due_date = date('d.m.Y', strtotime($deteProp . DAY_EXECUTION));
                        }
                        $error = '';
                        if ((strtotime(date("d.m.Y")) > strtotime($due_date)) && !empty($isNew)) {
                            $error = 'Y';
                        }
                        if (strpos('---- ' . $parts["NAME"], 'RP') === false) {
                            $numberParts = $parts["NAME"];
                        } else {
                            $numberParts = 'RP' . $parts["ID"];
                        }
                        $arTasks[] = [
                            "id" => $parts["ID"],
                            "vid" => "parts",
                            "error" => $error,
                            "date" => $parts["DATE_CREATE"],
                            "date_sort" => strtotime($parts["DATE_CREATE"]),
                            "status" => $status,
                            "status_id" => $arProps["STATUS_ID"]["VALUE"],
                            "status_arr" => ["reject_id" => 24, "confirm_id" => 23],
                            "akt" => $akt_parse,
                            "photo" => $arPhoto,
                            "phone" => $arProps["PHONE_PERSONAL"]["VALUE"],
                            "new" => $isNew,
                            "name" => "Рекламации - запчасти",
                            "invoice" => $arProps["INVOICE"]["VALUE"],
                            "consignment" => $arProps["CONSIGNMENT"]["VALUE"],
                            "reason" => $arProps["REASON"]["VALUE"]["TEXT"],
                            "wishes" => $arProps["WISHES"]["VALUE"]["TEXT"],
                            "due_date" => $due_date,
                            "number" => $numberParts,
                            "dealer" => $this::getUser($arProps["DEALER"]["VALUE"]),
                        ];
                    }
                }
            }
            /**
             * Сервисные компенсации
             */
            if (in_array(IB_SERVCOMP, $visibleTask)) {
                $typeTechHl = new HLBlock(HL_EQUIPMENT);
                $infoServices = $elemIbl->GetList(IB_SERVCOMP,
                    ["ID", "IBLOCK_ID", "NAME", "DATE_CREATE", "CODE", "DATE_ACTIVE_FROM", "DATE_CREATE", "PROPERTY_*"],
                    [
                        "ACTIVE_DATE" => "Y",
                        "ACTIVE" => "Y",
                        "PROPERTY_DILER" => $id_dealer,
                        /*[
                            "LOGIC" => "OR",
                            ["PROPERTY_STATUS_VALUE" => "Отправлено"],
                            ["PROPERTY_STATUS_VALUE" => "В работе"],
                        ],*/
                    ],
                    ["ID" => "desc"]
                );
                foreach ($infoServices["ELEMS"] as $service) {
                    $arProps = $infoServices["PROPS"][$service["ID"]];
                    $status = $arProps["STATUS"]["~VALUE"];
                    $isNew = '';
                    if ($status == 'Отправлено' || $status == 'В работе') {
                        $isTask++;
                        $isNew = 'Y';
                    }

                    if ($type_vid == "detail") {
                        $type = $arProps["TYPE_SERVICE"]["~VALUE"];
                        unset($arProps["TYPE_SERVICE"]);
                        unset($arProps["STATUS"]);
                        $arFiles = [];
                        foreach ($arProps as $arProp) {
                            if ($arProp["PROPERTY_TYPE"] == 'F') {
                                if (empty($arProp["VALUE"])) continue;
                                $src = [];
                                if (is_array($arProp["VALUE"])) {
                                    foreach ($arProp["VALUE"] as $id) {
                                        $fileFind = CFile::GetFileArray($id);
                                        if (!empty($fileFind))
                                            $src[] = $fileFind;
                                    }
                                } else {
                                    $fileFind2 = CFile::GetFileArray($arProp["VALUE"]);
                                    if (!empty($fileFind2))
                                        $src = $fileFind2;
                                }
                                if (!empty($src))
                                    $arFiles[] = ["info" => $src, "name" => $arProp["NAME"]];
                            }
                        }
                        $arTech = [];
                        if (!empty($arProps["SERIAL_NUMBER"]["VALUE"])) {
                            foreach ($arProps["SERIAL_NUMBER"]["VALUE"] as $serialElem) {
                                if (!empty((int)$serialElem)) {
                                    $infoTech = $elemIbl->GetList(IB_EXPLOITATION, ["NAME", "ID", "PROPERTY_MODEL", "PROPERTY_EQUIPMENT", "PROPERTY_NUMBER"], ["ID" => (int)$serialElem, "!EQUIPMENT_VALUE" => false])["ELEMS"][0];
                                    if ($infoTech) {
                                        $infoModel = $elemIbl->getById(IB_DIRECTORY, $infoTech["PROPERTY_MODEL_VALUE"]);
                                        $infoType = $typeTechHl->get(["UF_XML_ID" => $infoTech["PROPERTY_EQUIPMENT_VALUE"]])[0];
                                        $numberTech = $infoTech["PROPERTY_NUMBER_VALUE"];
                                        $arTech[$infoTech["ID"]] = ["number" => $numberTech, "model" => $infoModel["NAME"], "type" => $infoType["UF_NAME"]];
                                    }
                                }
                            }
                        }
                        $arReclamationTech = [];
                        if (!empty($arProps["RECLAMATION_TECH"]["VALUE"])) {
                            foreach ($arProps["RECLAMATION_TECH"]["VALUE"] as $reclElem) {
                                if (!empty((int)$reclElem)) {
                                    $infoRecl = $elemIbl->GetList(IB_RECLAMATION_TECH, ["NAME", "ID", "IBLOCK_ID", "PROPERTY_*"], ["ID" => $reclElem]);
                                    $elem = $infoRecl["ELEMS"][0];
                                    $propsRecl = $infoRecl["PROPS"][$elem["ID"]];
                                    $infoTechRecl = [];
                                    if (!empty($propsRecl["SERIAL_NUMBER"]["VALUE"])) {
                                        if (is_array($propsRecl["SERIAL_NUMBER"]["VALUE"])) {
                                            foreach ($propsRecl["SERIAL_NUMBER"]["VALUE"] as $itemIdsTech) {
                                                $infoTechRecl = $arTech[$itemIdsTech];
                                            }
                                        } else {
                                            $infoTechRecl = $arTech[$propsRecl["SERIAL_NUMBER"]["VALUE"]];
                                        }
                                    }
                                    $akt_parse = [];
                                    if (!empty($propsRecl["AKT"]["VALUE"])) {
                                        foreach ($propsRecl["AKT"]["VALUE"] as $akt2) {
                                            $arFile = CFile::GetFileArray($akt2);
                                            if ($arFile)
                                                $akt_parse[] = ["name" => $arFile["ORIGINAL_NAME"], "src" => $arFile["SRC"]];
                                        }
                                    }
                                    $arPhoto = [];
                                    if (!empty($propsRecl["PHOTO"]["VALUE"])) {
                                        foreach ($propsRecl["PHOTO"]["VALUE"] as $photo) {
                                            $arFile = CFile::GetFileArray($photo);
                                            if ($arFile)
                                                $arPhoto[] = ["name" => $arFile["ORIGINAL_NAME"], "src" => $arFile["SRC"]];
                                        }
                                    }
                                    $arReclamationTech[] = [
                                        "number" => $elem["NAME"],
                                        "id" => $elem["ID"],
                                        "infoTech" => $infoTechRecl,
                                        "reason" => $propsRecl["REASON"]["VALUE"]["TEXT"],
                                        "wishes" => $propsRecl["WISHES"]["VALUE"]["TEXT"],
                                        "akt" => $akt_parse,
                                        "photo" => $arPhoto
                                    ];
                                }
                            }
                        }
                        if (!empty($arReclamationTech))
                            $arTech = [];

                        if (!empty($arProps["DATE_EXECUTION"]["VALUE"])) {
                            $due_date = $arProps["DATE_EXECUTION"]["VALUE"];
                        } else {
                            $deteProp = explode(' ', $service["DATE_CREATE"])[0];
                            $due_date = date('d.m.Y', strtotime($deteProp . DAY_EXECUTION));
                        }
                        $error = '';
                        if ((strtotime(date("d.m.Y")) > strtotime($due_date)) && !empty($isNew)) {
                            $error = 'Y';
                        }
                        $arTasks[] = [
                            "id" => $service["ID"],
                            "vid" => "services",
                            "error" => $error,
                            "date" => $service["DATE_CREATE"],
                            "date_sort" => strtotime($service["DATE_CREATE"]),
                            "due_date" => $due_date,
                            "number" => 'S' . $service["ID"],
                            "type" => $type,
                            "phone" => $arProps["PHONE_PERSONAL"]["VALUE"],
                            "status" => $status,
                            "status_id" => $arProps["STATUS_ID"]["VALUE"],
                            "new" => $isNew,
                            "status_arr" => ["reject_id" => 26, "confirm_id" => 12],
                            "name" => "Сервисные компенсации",
                            "files" => $arFiles,
                            "dealer" => $this::getUser($arProps["DILER"]["VALUE"]),
                            "arTech" => $arTech,
                            "arReclamationTech" => $arReclamationTech
                        ];
                    }
                }
            }
            /**
             * Маркетинговые компенсации
             */
            if (in_array(IB_MARKETING, $visibleTask)) {
                $elemIbl = new Element;
                $infoMarketing = $elemIbl->GetList(IB_MARKETING,
                    ["ID", "IBLOCK_ID", "NAME", "CODE", "DATE_CREATE", "DATE_ACTIVE_FROM", "PROPERTY_*"],
                    [
                        "ACTIVE_DATE" => "Y",
                        "ACTIVE" => "Y",
                        "PROPERTY_DILER" => $id_dealer,
                        /*[
                            "LOGIC" => "OR",
                            ["PROPERTY_STATUS_VALUE" => "Отправлено"],
                            ["PROPERTY_STATUS_VALUE" => "В работе"],
                        ],*/
                    ],
                    ["ID" => "desc"]
                );
                foreach ($infoMarketing["ELEMS"] as $info) {
                    $arProps = $infoMarketing["PROPS"][$info["ID"]];
                    $status = $arProps["STATUS"]["~VALUE"];
                    $isNew = '';
                    if ($status == 'Отправлено' || $status == 'В работе') {
                        $isTask++;
                        $isNew = 'Y';
                    }

                    if ($type_vid == "detail") {
                        $type = $arProps["TYPE_KOMP"]["~VALUE"];
                        unset($arProps["TYPE_KOMP"]);
                        unset($arProps["STATUS"]);
                        $arFiles = [];
                        foreach ($arProps as $arProp) {
                            if ($arProp["PROPERTY_TYPE"] == 'F') {
                                if (empty($arProp["VALUE"])) continue;
                                $src = [];
                                if (is_array($arProp["VALUE"])) {
                                    foreach ($arProp["VALUE"] as $id) {
                                        $fileFind = CFile::GetFileArray($id);
                                        if (!empty($fileFind))
                                            $src[] = $fileFind;
                                    }
                                } else {
                                    $fileFind2 = CFile::GetFileArray($arProp["VALUE"]);
                                    if (!empty($fileFind2))
                                        $src = $fileFind2;
                                }
                                if (!empty($src))
                                    $arFiles[] = ["info" => $src, "name" => $arProp["NAME"]];
                            }
                        }

                        if (!empty($arProps["DATE_EXECUTION"]["VALUE"])) {
                            $due_date = $arProps["DATE_EXECUTION"]["VALUE"];
                        } else {
                            $deteProp = explode(' ', $info["DATE_CREATE"])[0];
                            $due_date = date('d.m.Y', strtotime($deteProp . DAY_EXECUTION));
                        }
                        $error = '';
                        if ((strtotime(date("d.m.Y")) > strtotime($due_date)) && !empty($isNew)) {
                            $error = 'Y';
                        }
                        $arTasks[] = [
                            "id" => $info["ID"],
                            "vid" => "mark",
                            "error" => $error,
                            "date" => $info["DATE_CREATE"],
                            "date_sort" => strtotime($info["DATE_CREATE"]),
                            "due_date" => $due_date,
                            "number" => 'M' . $info["ID"],
                            "type" => $type,
                            "phone" => $arProps["PHONE_PERSONAL"]["VALUE"],
                            "status" => $status,
                            "status_id" => $arProps["STATUS_ID"]["VALUE"],
                            "new" => $isNew,
                            "status_arr" => ["reject_id" => 25, "confirm_id" => 3],
                            "name" => "Маркетинговые компенсации",
                            "files" => $arFiles,
                            "dealer" => $this::getUser($arProps["DILER"]["VALUE"]),
                        ];
                    }
                }
            }
        }
        //usort($arTasks, "usort_object_by_time_ms");
        $arTasks = sort_nested_arrays($arTasks, array('status_id' => 'asc', 'date_sort' => 'desc'));
        $pagen = [];
        $arPage = [];
        if (!empty($arTasks) && !empty($this->arParams["PAGE_ELEMS"])) {
            $arPage = array_chunk($arTasks, $this->arParams["PAGE_ELEMS"]);
            if (count($arPage) > 1) {
                for ($i = 1; $i <= count($arPage); $i++) {
                    $pagen[] = $i;
                }
            }
        }
        return ["check_task" => $isTask, "tasks" => ["arTasks" => $arPage[$request["page_task"] ? $request["page_task"] - 1 : 0], "pagination" => $pagen]];
    }

    /**
     * @return array|mixed
     * @throws \Bitrix\Main\LoaderException
     */
    public function executeComponent()
    {
        function sort_nested_arrays($array, $args = array('votes' => 'desc'))
        {
            usort($array, function ($a, $b) use ($args) {
                $res = 0;

                $a = (object)$a;
                $b = (object)$b;

                foreach ($args as $k => $v) {
                    if ($a->$k == $b->$k) continue;

                    $res = ($a->$k < $b->$k) ? -1 : 1;
                    if ($v == 'desc') $res = -$res;
                    break;
                }

                return $res;
            });

            return $array;
        }

        global $USER;
        if ($this->startResultCache($this->arParams["CACHE_TIME"]) && $USER->GetID()) {
            if (!empty($this->arParams["DETAIL_ID"])) {
                $this->arResult["tasks"] = $this::isTasks($this->arParams["DETAIL_ID"], "detail", $_REQUEST)["tasks"];
                $this->arResult["checkTask"] = $this::isTasks($this->arParams["DETAIL_ID"], "detail", $_REQUEST)["check_task"];
                $elemIbl = new Element;
                $dealerInfo = $this::getUser($this->arParams["DETAIL_ID"]);
                if (!empty($dealerInfo["UF_REGIONS_USER"])) {
                    $regArray = [];
                    foreach ($dealerInfo["UF_REGIONS_USER"] as $reg) {
                        $fildRegion = $elemIbl->getById(IB_REGION, $reg);
                        if (!empty($fildRegion["NAME"]))
                            $regArray[] = $fildRegion["NAME"];
                    }
                    if (!empty($regArray))
                        $dealerInfo["regions"] = implode(',', $regArray);
                }
                /*if(!empty($dealerInfo["UF_COUNTRY"])){
                    $infoCountry = $elemIbl->getById(IB_COUNTRY, $dealerInfo["UF_COUNTRY"]);
                    $dealerInfo["country"] = $infoCountry["NAME"];
                }*/
                $this->arResult["dealer"] = $dealerInfo;
                $typeTechHl = new HLBlock(HL_EQUIPMENT);
                if (!empty($_REQUEST["search_buyers"])) {
                    $searchVal = $_REQUEST["search_buyers"];
                    $arBuyers_db = $elemIbl->GetList(IB_BUYERS, ["NAME", "ID", "IBLOCK_ID", "PROPERTY_*"], [
                        "PROPERTY_DILER" => $this->arParams["DETAIL_ID"],
                        "?NAME" => $searchVal
                    ], ["ID" => "desc"]);
                } else {
                    $arBuyers_db = $elemIbl->GetList(IB_BUYERS, ["NAME", "ID", "IBLOCK_ID", "PROPERTY_*"],
                        ["PROPERTY_DILER" => $this->arParams["DETAIL_ID"]],
                        ["ID" => "desc"]);
                }
                $arBuyers = [];
                $arIdsBuyers = [];
                $newArBuyer = [];
                foreach ($arBuyers_db["ELEMS"] as $item) {
                    $arProps = $arBuyers_db["PROPS"][$item["ID"]];
                    $arIdsBuyers[] = $item["ID"];
                    $infoRegion = $elemIbl->GetList(IB_REGION, ["NAME"], ["ID" => $arProps["REGION"]["VALUE"]])["ELEMS"][0];
                    $infoDistrict = $elemIbl->GetList(IB_AREA, ["NAME"], ["ID" => $arProps["DISTRICT"]["VALUE"]])["ELEMS"][0];
                    if (!empty($arProps["COUNTRY"]["VALUE"])) {
                        $infoCountry = CIBlockElement::GetByID($arProps["COUNTRY"]["VALUE"])->GetNext();
                        $countryUser = $infoCountry;
                    }
                    $arBuyers[$item["ID"]] = [
                        "name" => $item["NAME"],
                        "id" => $item["ID"],
                        "contact_people" => $arProps["CONTACT_PEOPLE"]["VALUE"],
                        "phone" => $arProps["PHONE"]["VALUE"],
                        "inn" => (!empty($arProps["INN"]["VALUE"]) && $arProps["INN"]["VALUE"] != 0) ? $arProps["INN"]["VALUE"] : '',
                        "bin" => (!empty($arProps["BIN"]["VALUE"]) && $arProps["BIN"]["VALUE"] != 0) ? $arProps["BIN"]["VALUE"] : '',
                        "position" => $arProps["POSITION"]["VALUE"],
                        "country" => $countryUser,
                        "region" => $infoRegion["NAME"],
                        "district" => $infoDistrict["NAME"],
                        "address" => $arProps["ADDRESS"]["VALUE"],
                        "mail" => $arProps["EMAIL"]["VALUE"],
                        "arProps" => $arProps
                    ];
                }
                if (!empty($arBuyers)) {
                    $infoExploitation = $elemIbl->GetList(IB_EXPLOITATION, ["NAME", "ID", "IBLOCK_ID", "PROPERTY_*"], ["PROPERTY_BUYER" => $arIdsBuyers]);
                    foreach ($infoExploitation["ELEMS"] as $exploit) {
                        $arPropsExploitation = $infoExploitation["PROPS"][$exploit["ID"]];
                        $infoModel = $elemIbl->getById(IB_DIRECTORY, $arPropsExploitation["MODEL"]["VALUE"]);
                        $infoType = $typeTechHl->get(["UF_XML_ID" => $arPropsExploitation["EQUIPMENT"]["VALUE"]])[0];
                        $arBuyers[$arPropsExploitation["BUYER"]["VALUE"]]["arTech"][] = ["type" => $infoType["UF_NAME"], "model" => $infoModel["NAME"], "number" => $arPropsExploitation["NUMBER"]["VALUE"]];
                    }
                    foreach ($arBuyers as $item) {
                        $newArBuyer[] = $item;
                    }
                }
                $pagenBuyers = [];
                $arPageBuyers = [];
                if (!empty($newArBuyer)) {
                    $arPageBuyers = array_chunk($newArBuyer, $this->arParams["PAGE_ELEMS"]);
                    if (count($arPageBuyers) > 1) {
                        for ($i = 1; $i <= count($arPageBuyers); $i++) {
                            $pagenBuyers[] = $i;
                        }
                    }
                }
                $this->arResult["buyers"] = ["arBuyers" => $arPageBuyers[$_REQUEST["page_buyer"] ? $_REQUEST["page_buyer"] - 1 : 0], "pagination" => $pagenBuyers];

                if (!empty($_REQUEST["search_expl"])) {
                    $searchVal = $_REQUEST["search_expl"];
                    $modelIds = [];
                    $typeModelIds = [];
                    $buyerIds = [];
                    $searchModel = $elemIbl->GetList(IB_DIRECTORY, ["NAME", "ID", "IBLOCK_ID", "PROPERTY_*"], ["!PROPERTY_TYPE_VALUE" => false, "?NAME" => $searchVal]);
                    foreach ($searchModel["ELEMS"] as $modelItem) {
                        $arPropsModel = $searchModel["PROPS"][$modelItem["ID"]];
                        if ($modelItem["ID"])
                            $modelIds[] = $modelItem["ID"];
                    }
                    $searchType = $typeTechHl->get(["?UF_NAME" => $searchVal]);
                    foreach ($searchType as $typeItem) {
                        if ($typeItem["ID"])
                            $typeModelIds[] = $typeItem["UF_XML_ID"];
                    }
                    $searchBuyer = $elemIbl->GetList(IB_BUYERS, ["NAME", "ID", "IBLOCK_ID", "PROPERTY_*"], ["?NAME" => $searchVal, "PROPERTY_DILER" => $this->arParams["DETAIL_ID"]]);
                    foreach ($searchBuyer["ELEMS"] as $buyerItem) {
                        if ($buyerItem["ID"])
                            $buyerIds[] = $buyerItem["ID"];
                    }
                    $filterSearch["?PROPERTY_NUMBER"] = $searchVal;
                    if (!empty($filterSearch))
                        $arExplotations_db = $elemIbl->GetList(IB_EXPLOITATION, ["NAME", "ID", "IBLOCK_ID", "PROPERTY_*"],
                            [
                                "PROPERTY_DEALER" => $this->arParams["DETAIL_ID"],
                                "!PROPERTY_BUYER" => false,
                                [
                                    "LOGIC" => "OR",
                                    "?PROPERTY_NUMBER" => $searchVal,
                                    "PROPERTY_EQUIPMENT" => $typeModelIds,
                                    "PROPERTY_MODEL" => $modelIds,
                                    "PROPERTY_BUYER" => $buyerIds,
                                ]
                            ],
                            [
                                "ID" => "desc"
                            ],
                            $this->arParams["PAGE_ELEMS"],
                            false,
                            [
                                "pagination" => [
                                    "hash" => "#technics"
                                ],
                            ]
                        );
                } else {
                    $arExplotations_db = $elemIbl->GetList(IB_EXPLOITATION, ["NAME", "ID", "IBLOCK_ID", "PROPERTY_*"],
                        [
                            "PROPERTY_DEALER" => $this->arParams["DETAIL_ID"],
                            "!PROPERTY_BUYER" => false
                        ],
                        [
                            "ID" => "desc"
                        ],
                        $this->arParams["PAGE_ELEMS"],
                        false,
                        [
                            "pagination" => [
                                "hash" => "#technics"
                            ],
                        ]
                    );
                }
                $arExploitations = [];
                foreach ($arExplotations_db["ELEMS"] as $item) {
                    $file = '';
                    $arProps = $arExplotations_db["PROPS"][$item["ID"]];
                    $status = $arProps["STATUS"]["VALUE"];
                    $infoModel = $elemIbl->getById(IB_DIRECTORY, $arProps["MODEL"]["VALUE"]);
                    $infoType = $typeTechHl->get(["UF_XML_ID" => $arProps["EQUIPMENT"]["VALUE"]])[0];
                    if (empty($arProps["BUYER"]["VALUE"])) continue;
                    $infoBuyer = $elemIbl->GetList(IB_BUYERS,
                        ["NAME", "ID", "IBLOCK_ID", "PROPERTY_*"],
                        ["ID" => $arProps["BUYER"]["VALUE"], "PROPERTY_DILER" => $this->arParams["DETAIL_ID"]]
                    );
                    if ($arProps["FILE"]["VALUE"])
                        $file = CFile::GetFileArray($arProps["FILE"]["VALUE"]);

                    if ($arProps["STATUS"]["VALUE_ENUM_ID"] == 15)
                        $status = $arProps["DATE"]["VALUE"];

                    $idsDopHistory = [$item["ID"]];
                    $arReclamation_Tech_db = $elemIbl->GetList(IB_RECLAMATION_TECH,
                        ["NAME", "ID", "IBLOCK_ID", "PROPERTY_*", "DATE_CREATE"],
                        [
                            "PROPERTY_DEALER" => $this->arParams["DETAIL_ID"],
                            "PROPERTY_SERIAL_NUMBER" => $item["ID"]
                        ],
                        ["ID" => "desc"]
                    );
                    foreach ($arReclamation_Tech_db["ELEMS"] as $item2) {
                        $idsDopHistory[] = $item2["ID"];
                    }
                    $infoServices = $elemIbl->GetList(IB_SERVCOMP,
                        ["ID", "IBLOCK_ID", "NAME", "CODE", "DATE_ACTIVE_FROM", "DATE_CREATE", "PROPERTY_*"],
                        [
                            "PROPERTY_DILER" => $this->arParams["DETAIL_ID"],
                            "PROPERTY_SERIAL_NUMBER" => $item["ID"]
                        ],
                        ["ID" => "desc"]
                    );
                    foreach ($infoServices["ELEMS"] as $key => $service) {
                        $idsDopHistory[] = $item["ID"];
                    }

                    $historyHl = new HLBlock(HL_HISTORY);
                    $arHistory_db = $historyHl->get(
                        [
                            "UF_ID_ELEM" => $idsDopHistory,
                            [
                                "LOGIC" => "OR",
                                ["UF_WHO_USER" => $this->arParams["DETAIL_ID"]],
                                ["UF_THAT_USER" => $this->arParams["DETAIL_ID"]],
                            ]
                        ]
                    );

                    $arExploitations[] = [
                        "id" => $item["ID"],
                        "type" => $infoType["UF_NAME"],
                        "model" => $infoModel["NAME"],
                        "history" => $arHistory_db,
                        "refusal" => $arProps["REFUSAL"]["VALUE"]["TEXT"],
                        "status" => $status,
                        "status_id" => $arProps["STATUS"]["VALUE_ENUM_ID"],
                        "akt" => $file,
                        "number" => $arProps["NUMBER"]["VALUE"],
                        "buyer" => [
                            "name" => $infoBuyer["ELEMS"][0]["NAME"],
                            "id" => $infoBuyer["ELEMS"][0]["ID"],
                            "props" => $infoBuyer["PROPS"][$infoBuyer["ELEMS"][0]["ID"]]
                        ]
                    ];
                }
                /*$pagenTech = [];
                $arPageTech = [];
                if (!empty($arExploitations)) {
                    $arPageTech = array_chunk($arExploitations, $this->arParams["PAGE_ELEMS"]);
                    if (count($arPageTech) > 1) {
                        for ($i = 1; $i <= count($arPageTech); $i++) {
                            $pagenTech[] = $i;
                        }
                    }
                }
                $arPageTech[$_REQUEST["page_expl"] ? $_REQUEST["page_expl"] - 1 : 0]*/
                $this->arResult["tech"] = ["arTech" => $arExploitations, "pagination" => $arExplotations_db["pagination"]];
            } else {
                $arInfo = [];
                $arIdsDealer = [];
                $elemIbl = new Element;
                switch ($this->arParams["PANEL"]) {
                    case 'manager':
                        $filterDealer = [];
                        if (!empty($_REQUEST["search_dealer"])) {
                            $filterDealer = [
                                [
                                    "LOGIC" => "OR",
                                    "?WORK_COMPANY" => $_REQUEST["search_dealer"],
                                    "?WORK_ZIP" => $_REQUEST["search_dealer"],
                                ]
                            ];
                        }
                        $filterDealer["UF_MANAGER_NEW"] = $USER->GetID();
                        $rsDealers = \Bitrix\Main\UserTable::getList([
                            'select' => [
                                'ID',
                                'WORK_COMPANY',
                                'WORK_ZIP',
                                'UF_REGIONS_USER',
                                'UF_MANAGER_NEW',
                            ],
                            'filter' => $filterDealer,
                            'order' => ['ID' => 'asc'],
                        ]);
                        while ($dealer = $rsDealers->fetch()) {
                            $arIdsDealer[] = $dealer["ID"];
                            $regions = [];
                            if (!empty($dealer["UF_REGIONS_USER"])) {
                                foreach ($dealer["UF_REGIONS_USER"] as $regionId) {
                                    $fildRegion = $elemIbl->getById(IB_REGION, $regionId);
                                    $regions[] = $fildRegion["NAME"];
                                }
                            }
                            $arInfo[$dealer["ID"]] = [
                                "id" => $dealer["ID"],
                                "company" => $dealer["WORK_COMPANY"],
                                "regions" => implode(",", $regions),
                                "inn" => $dealer["WORK_ZIP"],
                                "tasks" => $this::isTasks($dealer["ID"], "list")["check_task"]
                            ];
                        }
                        /*$usersForManger = CUser::GetList(($by="ID"), ($order="ASC"),
                            [
                                "UF_MANAGER_NEW"=>array($USER->GetID()),
                            ], array("SELECT"=>array("UF_*")));
                        while ($arUser = $usersForManger->Fetch()) {
                            $arIdsDealer[] = $arUser["ID"];
                            $regions = [];
                            if(!empty($arUser["UF_REGIONS_USER"])){
                                foreach ($arUser["UF_REGIONS_USER"] as $regionId){
                                    $fildRegion = $elemIbl->getById(IB_REGION, $regionId);
                                    $regions[] = $fildRegion["NAME"];
                                }
                            }
                            $arInfo[$arUser["ID"]] = [
                                "id" => $arUser["ID"],
                                "company" => $arUser["WORK_COMPANY"],
                                "regions" => implode(",", $regions),
                                "inn" => $arUser["WORK_ZIP"],
                                "tasks" => $this::isTasks($arUser["ID"], "list")["check_task"]
                            ];
                        }*/
                        break;
                    case 'administrator':
                        $filterDealer = [];
                        if (!empty($_REQUEST["search_dealer"])) {
                            $filterDealer = [
                                [
                                    "LOGIC" => "OR",
                                    "?WORK_COMPANY" => $_REQUEST["search_dealer"],
                                    "?WORK_ZIP" => $_REQUEST["search_dealer"],
                                ]
                            ];
                        }
                        $usersForManger = CUser::GetList(($by = "ID"), ($order = "ASC"),
                            [
                                "UF_ADMIN_NEW" => $USER->GetID()
                            ], array("SELECT" => array("UF_*")));
                        while ($arUser = $usersForManger->Fetch()) {
                            if ($arUser["UF_ADMIN_NEW"]) {
                                $filterDealer["UF_MANAGER_NEW"] = $arUser["ID"];
                                $rsDealers = \Bitrix\Main\UserTable::getList([
                                    'select' => [
                                        'ID',
                                        'WORK_COMPANY',
                                        'WORK_ZIP',
                                        'UF_REGIONS_USER',
                                        'UF_MANAGER_NEW',
                                    ],
                                    'filter' => $filterDealer,
                                    'order' => ['ID' => 'asc'],
                                ]);
                                while ($dealer = $rsDealers->fetch()) {
                                    $arIdsDealer[] = $dealer["ID"];
                                    $regions = [];
                                    if (!empty($dealer["UF_REGIONS_USER"])) {
                                        foreach ($dealer["UF_REGIONS_USER"] as $regionId) {
                                            $fildRegion = $elemIbl->getById(IB_REGION, $regionId);
                                            $regions[] = $fildRegion["NAME"];
                                        }
                                    }
                                    $arInfo[$dealer["ID"]] = [
                                        "id" => $dealer["ID"],
                                        "company" => $dealer["WORK_COMPANY"],
                                        "regions" => implode(",", $regions),
                                        "inn" => $dealer["WORK_ZIP"],
                                        "tasks" => $this::isTasks($dealer["ID"], "list")["check_task"]
                                    ];
                                }
                            }
                        }
                        break;
                }
                if (!empty($_REQUEST["sort_name"])) {
                    $arSort = array('tasks' => 'desc', 'company' => $_REQUEST["sort_name"]);
                } elseif (!empty($_REQUEST["sort_region"])) {
                    $arSort = array('tasks' => 'desc', 'regions' => $_REQUEST["sort_region"]);
                } else {
                    $arSort = array('tasks' => 'desc', 'company' => 'desc');
                }
                $arInfo = sort_nested_arrays($arInfo, $arSort);
                $pagenDealers = [];
                $arPageDealers = [];
                if (!empty($arInfo) && !empty($this->arParams["PAGE_ELEMS"])) {
                    $arPageDealers = array_chunk($arInfo, $this->arParams["PAGE_ELEMS"]);
                    if (count($arPageDealers) > 1) {
                        for ($i = 1; $i <= count($arPageDealers); $i++) {
                            $pagenDealers[] = $i;
                        }
                    }
                }
                $this->arResult = [
                    "persons" => $arPageDealers[$_REQUEST["page_dealers"] ? $_REQUEST["page_dealers"] - 1 : 0],
                    "pagination" => $pagenDealers
                ];
            }
            $this->includeComponentTemplate();
        }
        return $this->arResult;
    }
} ?>