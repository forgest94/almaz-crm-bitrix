<div class="lk__table-row-wrap js-table-detail <?=$item["error"]?'failed':'';?>">
    <div class="lk__table-row">
        <div class="lk__table-col">
            <div class="lk__table-col-head">
                Дата и время
            </div>
            <span>
                            <?=$item["date"];?>
                        </span>
        </div>
        <div class="lk__table-col">
            <div class="lk__table-col-head">
                Номер
            </div>
            <?=$item["number"];?>
        </div>
        <div class="lk__table-col">
            <div class="lk__table-col-head">
                Инициатор
            </div>
            <span>
                               Дилер <br>
                              <b><?=$item["dealer"]["WORK_COMPANY"];?></b>
                            </span>
        </div>
        <div class="lk__table-col">
            <div class="lk__table-col-head">
                Задача
            </div>
            <?=$item["name"];?>
        </div>
        <div class="lk__table-col">
            <div class="lk__table-col-head">
                Срок выполнения
            </div>
            <?=$item["due_date"];?>
        </div>
        <div class="lk__table-col">
            <a class="lk__link js-table-detail-link" href="javascript:void(0)" data-show-text="Свернуть">
                Подробнее
            </a>
        </div>
    </div>
    <div class="lk__table-row-data js-table-detail-data">
        <div class="lk__data">
            <?if(!empty($item["phone"])):?>
                <div class="lk__data-row">
                    <div class="lk__data-label">
                        Контактный номер телефона
                    </div>
                    <div class="lk__data-value">
                        <div class="lk__data-box">
                            <?=$item["phone"]?>
                        </div>
                    </div>
                </div>
            <?endif;?>
            <?if(!empty($item["files"])):?>
                <?foreach ($item["files"] as $file):?>
                    <?if(!empty($file["info"][0])):?>
                        <div class="lk__data-separate"></div>
                    <?endif;?>
                    <div class="lk__data-row">
                        <div class="lk__data-label">
                            <?=$file["name"];?>
                        </div>
                        <div class="lk__data-value">
                            <div class="lk__data-files">
                                <?if(!empty($file["info"][0])):?>
                                    <?foreach ($file["info"] as $infoFile):?>
                                        <a class="link-file" href="<?=$infoFile["SRC"];?>" download><?=$infoFile["ORIGINAL_NAME"];?></a>
                                    <?endforeach;?>
                                <?else:?>
                                    <a class="link-file" href="<?=$file["info"]["SRC"];?>" download><?=$file["info"]["ORIGINAL_NAME"];?></a>
                                <?endif;?>
                            </div>
                        </div>
                    </div>
                <?endforeach;?>
            <?endif;?>
            <?if(!empty($item["arReclamationTech"])):?>
                <?foreach ($item["arReclamationTech"] as $recl):?>
                    <div class="lk__data-block-box js-table-block">
                        <div class="lk__data-row lk__data-row--margin">
                            <div class="lk__data-label">
                                Указанная рекламация <br>
                                № <b><?=$recl["number"];?></b>
                            </div>
                            <div class="lk__data-value">
                                <a class="lk__link js-table-block-link" href="javascript:void(0)" data-show-text="Свернуть">
                                    Развернуть
                                </a>
                            </div>
                        </div>
                        <div class="lk__data-block hide js-table-block-data" style="display: none;">
                            <div class="lk__data-row">
                                <div class="lk__data-label">
                                    Указанная техника
                                </div>
                                <div class="lk__data-value">
                                    <ul class="lk__data-grid">
                                        <li>
                                            <b><?=$recl["infoTech"]["number"];?></b> <?=$recl["infoTech"]["type"];?> / <?=$recl["infoTech"]["model"];?>                                                </li>
                                    </ul>
                                </div>
                            </div>
                            <?if(!empty($recl["reason"])):?>
                                <div class="lk__data-row">
                                    <div class="lk__data-label">
                                        Причина обращения
                                    </div>
                                    <div class="lk__data-value">
                                        <div class="lk__data-box">
                                            <?=$recl["reason"];?>
                                        </div>
                                    </div>
                                </div>
                            <?endif;?>
                            <?if(!empty($recl["wishes"])):?>
                                <div class="lk__data-row">
                                    <div class="lk__data-label">
                                        Пожелания по решению рекламации
                                    </div>
                                    <div class="lk__data-value">
                                        <div class="lk__data-box">
                                            <?=$recl["wishes"];?>
                                        </div>
                                    </div>
                                </div>
                            <?endif;?>
                            <?if(!empty($recl["akt"])):?>
                                <div class="lk__data-row">
                                    <div class="lk__data-label">
                                        Акт ввода в эксплуатацию
                                    </div>
                                    <div class="lk__data-value">
                                        <div class="lk__data-files">
                                            <?foreach ($recl["akt"] as $akt):?>
                                                <a class="link-file" href="<?=$akt["src"];?>" download><?=$akt["name"];?></a>
                                            <?endforeach;?>
                                        </div>
                                    </div>
                                </div>
                            <?endif;?>
                            <?if(!empty($recl["photo"])):?>
                                <div class="lk__data-row">
                                    <div class="lk__data-label">
                                        Фото
                                    </div>
                                    <div class="lk__data-value">
                                        <div class="lk__data-files">
                                            <?foreach ($recl["photo"] as $photo):?>
                                                <a class="link-file" href="<?=$photo["src"];?>" download><?=$photo["name"];?></a>
                                            <?endforeach;?>
                                        </div>
                                    </div>
                                </div>
                            <?endif;?>
                            <div class="lk__data-separate"></div>
                        </div>
                    </div>
                <?endforeach;?>
            <?endif;?>
            <?/*if(!empty($item["arReclamationTech"])):?>
                <div class="lk__data-row">
                    <div class="lk__data-label">
                        Указанная рекламация
                    </div>
                    <div class="lk__data-value">
                        <ul class="lk__data-grid">
                            <?foreach ($item["arReclamationTech"] as $recl):?>
                                <li>
                                    № <b><?=$recl;?></b>
                                </li>
                            <?endforeach;?>
                        </ul>
                    </div>
                </div>
            <?endif;*/?>
            <?if(!empty($item["arTech"])):?>
                <div class="lk__data-row">
                    <div class="lk__data-label">
                        Указанная техника
                    </div>
                    <div class="lk__data-value">
                        <ul class="lk__data-grid">
                            <?foreach ($item["arTech"] as $tech):?>
                                <li>
                                    <b><?=$tech["number"];?></b> <?=$tech["type"];?> <?=$tech["model"];?>
                                </li>
                            <?endforeach;?>
                        </ul>
                    </div>
                </div>
            <?endif;?>
            <div class="lk__data-row">
                <div class="lk__data-label">
                </div>
                <div class="lk__data-value">
                    <div class="btn-group">
                        <a class="btn btn-black" href="javascript:void(0);"
                           data-editTask="<?=$item["id"];?>"
                           data-dealer="<?=$item["dealer"]["ID"];?>"
                           data-status="<?=$item["status_arr"]["reject_id"];?>">Отклонить</a>
                        <a class="btn" href="javascript:void(0);"
                           data-editTask="<?=$item["id"];?>"
                           data-dealer="<?=$item["dealer"]["ID"];?>"
                           data-status="<?=$item["status_arr"]["confirm_id"];?>">Подтвердить</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>