<div class="lk__table-row-wrap js-table-detail <?=$item["error"]?'failed':'';?>">
    <div class="lk__table-row">
        <div class="lk__table-col">
            <div class="lk__table-col-head">
                Дата и время
            </div>
            <span>
                            <?=$item["date"];?>
                        </span>
        </div>
        <div class="lk__table-col">
            <div class="lk__table-col-head">
                Номер
            </div>
            <?=$item["number"];?>
        </div>
        <div class="lk__table-col">
            <div class="lk__table-col-head">
                Инициатор
            </div>
            <span>
                               Дилер <br>
                              <b><?=$item["dealer"]["WORK_COMPANY"];?></b>
                            </span>
        </div>
        <div class="lk__table-col">
            <div class="lk__table-col-head">
                Задача
            </div>
            <?=$item["name"];?>
        </div>
        <div class="lk__table-col">
            <div class="lk__table-col-head">
                Срок выполнения
            </div>
            <?=$item["due_date"];?>
        </div>
        <div class="lk__table-col">
            <a class="lk__link js-table-detail-link" href="javascript:void(0)" data-show-text="Свернуть">
                Подробнее
            </a>
        </div>
    </div>
    <div class="lk__table-row-data js-table-detail-data">
        <div class="lk__data">
            <?if(!empty($item["akt"])):?>
                <div class="lk__data-row">
                    <div class="lk__data-label">
                        Рекламация
                    </div>
                    <div class="lk__data-value">
                        <div class="lk__data-files">
                            <a class="link-file" href="<?=$item["akt"]["src"];?>" download><?=$item["akt"]["name"];?></a>
                        </div>
                    </div>
                </div>
            <?endif;?>
            <?if(!empty($item["photo"][0])):?>
                <div class="lk__data-row">
                    <div class="lk__data-label">
                        Фото
                    </div>
                    <div class="lk__data-value">
                        <div class="lk__data-files">
                            <?foreach ($item["photo"] as $photo):?>
                                <a class="link-file" href="<?=$photo["src"];?>" download><?=$photo["name"];?></a>
                            <?endforeach;?>
                        </div>
                    </div>
                </div>
            <?endif;?>
            <?if(!empty($item["invoice"]) && $item["invoice"][0] != 'N'):?>
                <div class="lk__data-row">
                    <div class="lk__data-label">
                        Счет-фактура
                    </div>
                    <div class="lk__data-value">
                        <div class="lk__data-box">
                            <?foreach ($item["invoice"] as $invoi):?>
                                <?=$invoi.'<br/>';?>
                            <?endforeach;?>
                        </div>
                    </div>
                </div>
            <?endif;?>
            <?if(!empty($item["consignment"]) && $item["consignment"][0] != 'N'):?>
                <div class="lk__data-row">
                    <div class="lk__data-label">
                        Товарно-транспортная накладная
                    </div>
                    <div class="lk__data-value">
                        <div class="lk__data-box">
                            <?foreach ($item["consignment"] as $cons):?>
                                <?=$cons.'<br/>';?>
                            <?endforeach;?>
                        </div>
                    </div>
                </div>
            <?endif;?>
            <?if(!empty($item["reason"])):?>
                <div class="lk__data-row">
                    <div class="lk__data-label">
                        Причина обращения
                    </div>
                    <div class="lk__data-value">
                        <div class="lk__data-box">
                            <?=$item["reason"];?>
                        </div>
                    </div>
                </div>
            <?endif;?>
            <?if(!empty($item["wishes"])):?>
                <div class="lk__data-row">
                    <div class="lk__data-label">
                        Пожелания по решению рекламации
                    </div>
                    <div class="lk__data-value">
                        <div class="lk__data-box">
                            <?=$item["wishes"];?>
                        </div>
                    </div>
                </div>
            <?endif;?>
            <?if(!empty($item["phone"])):?>
                <div class="lk__data-row">
                    <div class="lk__data-label">
                        Контактный номер телефона
                    </div>
                    <div class="lk__data-value">
                        <div class="lk__data-box">
                            <?=$item["phone"]?>
                        </div>
                    </div>
                </div>
            <?endif;?>
            <div class="lk__data-row">
                <div class="lk__data-label">
                </div>
                <div class="lk__data-value">
                    <div class="btn-group">
                        <a class="btn btn-black" href="javascript:void(0);"
                           data-editTask="<?=$item["id"];?>"
                           data-dealer="<?=$item["dealer"]["ID"];?>"
                           data-status="<?=$item["status_arr"]["reject_id"];?>">Отклонить</a>
                        <a class="btn" href="javascript:void(0);"
                           data-editTask="<?=$item["id"];?>"
                           data-dealer="<?=$item["dealer"]["ID"];?>"
                           data-status="<?=$item["status_arr"]["confirm_id"];?>">Подтвердить</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>