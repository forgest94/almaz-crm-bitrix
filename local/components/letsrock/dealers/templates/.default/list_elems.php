<div class="lk__content-box">
    <label class="field field-search">
        <input class="field__input" type="text" name="search_dealer" data-search="<?=$APPLICATION->GetCurPage();?>?search_dealer=" data-has="dealer" placeholder="Поиск по дилеру" value="<?=$_REQUEST["search_dealer"];?>">
    </label>
    <div class="lk__table dealer-list">
        <div class="lk__table-head">
            <div class="lk__table-row">
                <a class="lk__table-col" href="?sort_name=<?=$_REQUEST["sort_name"]=='asc'?'desc':'asc';?>">
                        Название
                    <?if(empty($_REQUEST["sort_region"]) || !empty($_REQUEST["sort_name"])):?>
                        <i class = "icon icon-sort-arrow js-lk-sort <?=$_REQUEST["sort_name"]=='asc'?'active':'';?>"> </i>
                    <?endif;?>
                </a>
                <a class="lk__table-col" href="?sort_region=<?=$_REQUEST["sort_region"]=='asc'?'desc':'asc';?>">
                    Регион
                    <?if(!empty($_REQUEST["sort_region"])):?>
                        <i class = "icon icon-sort-arrow js-lk-sort <?=$_REQUEST["sort_region"]=='asc'?'active':'';?>"> </i>
                    <?endif;?>
                </a>
                <div class="lk__table-col">
                    ИНН
                </div>
                <div class="lk__table-col">
                    Задачи от дилера
                </div>
                <div class="lk__table-col">
                </div>
            </div>
        </div>
        <div class="lk__table-body" data-blockajax="dealer">
            <?if(!empty($arResult["persons"])):?>
                <!--startAjaxPagedealer-->
                <?foreach ($arResult["persons"] as $id => $person):?>
                    <div class="lk__table-row-wrap">
                        <div class="lk__table-row">
                            <div class="lk__table-col">
                                <div class="lk__table-col-head">
                                    Название
                                </div>
                                <?=$person["company"];?>
                            </div>
                            <div class="lk__table-col">
                                <div class="lk__table-col-head">
                                    Регион
                                </div>
                                <?=$person["regions"];?>
                            </div>
                            <div class="lk__table-col">
                                <div class="lk__table-col-head">
                                    ИНН
                                </div>
                                <?=$person["inn"];?>
                            </div>
                            <div class="lk__table-col">
                                <div class="lk__table-col-head">
                                    Задачи от дилера
                                </div>
                                <?if($person["tasks"] > 0):?>
                                    <span class="warning" data-warning="<?=$person["tasks"];?>">
                                      Есть входящие задачи
                                    </span>
                                <?else:?>
                                    <span>
                                      Нет входящих задач
                                    </span>
                                <?endif;?>
                            </div>
                            <div class="lk__table-col">
                                <a class="lk__link" href="?person=<?=$person["id"];?>">
                                    Подробнее
                                </a>
                            </div>
                        </div>
                    </div>
                <?endforeach;?>
                <!--endAjaxPagedealer-->
                <?$APPLICATION->IncludeComponent("letsrock:paginations",
                    ".default",
                    [
                        "CACHE_TIME" => 0,
                        "ELEMS_PAGE"=>$arResult["pagination"],
                        "GET_PAGE"=>"page_dealers",
                        "AJAX_BLOCK"=>"dealer",
                        "PAGEN_HASH"=>"dealer"
                    ]
                );?>
            <?endif;?>
        </div>
    </div>
</div>