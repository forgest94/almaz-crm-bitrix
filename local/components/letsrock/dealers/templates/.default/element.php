<div class="lk__content-box">
    <section class="lk__tabs-section">
        <div class="lk__tabs-header">
            <div class="lk__tabs-title"><?=$arResult["dealer"]["WORK_COMPANY"];?></div>
            <a class="lk__tabs-link js-modal-open" data-modal-type="dealer-data" href="javascript:void(0)">Данные дилера</a>
            <div class="popup popup__dealer-data js-modal" data-modal-type="dealer-data">
                <div class="popup__inner">
                    <div class="close popup__close js-modal-close"></div>
                    <div class="popup__content">
                        <ul class="list-dotted js-modal-copy-area">
                            <li class="list-dotted__row">
                                <span class="list-dotted__name">Название организации:</span>
                                <span class="list-dotted__line"></span>
                                <span class="list-dotted__value"><?=$arResult["dealer"]["WORK_COMPANY"];?></span>
                            </li>
                            <li class="list-dotted__row">
                                <span class="list-dotted__name">Адрес:</span>
                                <span class="list-dotted__line"></span>
                                <span class="list-dotted__value"><?=$arResult["dealer"]["WORK_STATE"];?>, <?=$arResult["dealer"]["WORK_CITY"];?>, <?=$arResult["dealer"]["WORK_STREET"];?></span>
                            </li>
                            <? if (!empty($arResult["dealer"]["country"])): ?>
                                <li class="list-dotted__row">
                                    <span class="list-dotted__name">Страна:</span>
                                    <span class="list-dotted__line"></span>
                                    <span class="list-dotted__value"><?= $arResult["dealer"]["country"]["NAME"]; ?></span>
                                </li>
                            <? endif; ?>
                            <li class="list-dotted__row">
                                <span class="list-dotted__name">Закрепленные за дилером регионы:</span>
                                <span class="list-dotted__line"></span>
                                <span class="list-dotted__value"><?=$arResult["dealer"]["regions"];?></span>
                            </li>
                            <li class="list-dotted__row">
                                <span class="list-dotted__name">Контактное лицо:</span>
                                <span class="list-dotted__line"></span>
                                <span class="list-dotted__value"><?=$arResult["dealer"]["LAST_NAME"];?> <?=$arResult["dealer"]["NAME"];?> <?=$arResult["dealer"]["SECOND_NAME"];?></span>
                            </li>
                            <?if(!empty($arResult["dealer"]["WORK_ZIP"])):?>
                                <li class="list-dotted__row">
                                    <span class="list-dotted__name">ИНН:</span>
                                    <span class="list-dotted__line"></span>
                                    <span class="list-dotted__value"><?=$arResult["dealer"]["WORK_ZIP"];?></span>
                                </li>
                            <?endif;?>
                            <?if(!empty($arResult["dealer"]["BIN"])):?>
                                <li class="list-dotted__row">
                                    <span class="list-dotted__name">БИН:</span>
                                    <span class="list-dotted__line"></span>
                                    <span class="list-dotted__value"><?=$arResult["dealer"]["UF_BIN"];?></span>
                                </li>
                            <?endif;?>
                            <li class="list-dotted__row">
                                <span class="list-dotted__name">Телефон:</span>
                                <span class="list-dotted__line"></span>
                                <span class="list-dotted__value">
                                   <a href="tel:<?=$arResult["dealer"]["PERSONAL_PHONE"];?>"><?=$arResult["dealer"]["PERSONAL_PHONE"];?></a>
                                </span>
                            </li>
                            <li class="list-dotted__row">
                                <span class="list-dotted__name">Адрес электронной почты:</span>
                                <span class="list-dotted__line"></span>
                                <span class="list-dotted__value"><a href="<?=$arResult["dealer"]["EMAIL"];?>"><?=$arResult["dealer"]["EMAIL"];?></a></span>
                            </li>
                        </ul>
                        <div class="popup__btn-wrap popup__btn-wrap-m-t">
                            <a class="btn popup__btn js-modal-close" href="javascript:void(0)">Закрыть</a>
                            <a class="btn btn-black popup__btn js-modal-copy" data-copy-message="Скопировано" href="javascript:void(0)">Скопировать</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <nav class="lk__tabs-nav js-tabs-nav">
            <a class="lk__tabs-nav-link active" data-tab='tasks' href="javascript:void(0)">
                <?if($arResult["checkTask"] > 0):?>
                    <i class="icon-tasks"></i>
                <?endif;?>
                <span>Задачи от дилера</span>
            </a>
            <a class="lk__tabs-nav-link" data-tab='buyers' href="javascript:void(0)">
                <i class="icon-buyers"></i>
                <span>Покупатели</span>
            </a>
            <a class="lk__tabs-nav-link" data-tab='technics' href="javascript:void(0)">
                <i class="icon-technics"></i>
                <span>Техника</span>
            </a>
        </nav>
        <section class="lk__tabs-content js-tabs-content">
            <article class="lk__tabs-content-item active" data-tab-content='tasks'>
                <div class="lk__table tasks">
                    <div class="lk__table-head">
                        <div class="lk__table-row">
                            <div class="lk__table-col">
                                Дата подачи
                            </div>
                            <div class="lk__table-col">
                                Номер
                            </div>
                            <div class="lk__table-col">
                                Инициатор
                            </div>
                            <div class="lk__table-col">
                                Задача
                            </div>
                            <div class="lk__table-col">
                                Срок выполнения
                            </div>
                            <div class="lk__table-col">
                            </div>
                        </div>
                    </div>
                    <div class="lk__table-body">
                        <?if(!empty($arResult["tasks"]["arTasks"])):?>
                            <?
                            foreach ($arResult["tasks"]["arTasks"] as $item){
                                $vid = $pathComponent.'/templates/'.$item["vid"].'.php';
                                include($vid);
                            }
                            ?>
                        <?endif;?>
                    </div>
                </div>
                <?$APPLICATION->IncludeComponent("letsrock:paginations",
                    ".default",
                    [
                        "CACHE_TIME" => 0,
                        "ELEMS_PAGE"=>$arResult["tasks"]["pagination"],
                        "GET_PAGE"=>"page_task",
                        "AJAX_BLOCK"=>"",
                        "PAGEN_HASH"=>""
                    ]
                );?>
            </article>
            <article class="lk__tabs-content-item" data-tab-content='buyers'>
                <label class="field field-search">
                    <input class="field__input" type="text" name="search_buyer" data-search="/<?=$arParams["PANEL"];?>/dealers/?person=<?=$arParams["DETAIL_ID"];?>&search_buyers=" data-has="buyers" placeholder="Поиск по покупателю" value="<?=$_REQUEST["search_buyers"];?>">
                </label>
                <div class="lk__table buyers-dealer">
                    <div class="lk__table-head">
                        <div class="lk__table-row">
                            <div class="lk__table-col">
                                Название
                            </div>
                            <div class="lk__table-col">
                                Контактное лицо
                            </div>
                            <div class="lk__table-col">
                                Телефон
                            </div>
                            <div class="lk__table-col">
                                ИНН
                            </div>
                            <div class="lk__table-col">
                            </div>
                        </div>
                    </div>
                    <div class="lk__table-body" data-blockajax="buyers">
                            <!--startAjaxPagebuyers-->
                            <?foreach ($arResult["buyers"]["arBuyers"] as $id => $item):?>
                                <div class="lk__table-row-wrap js-table-detail">
                                    <div class="lk__table-row">
                                        <div class="lk__table-col">
                                            <?=$item["name"];?>
                                        </div>
                                        <div class="lk__table-col">
                                            <?=$item["contact_people"];?>
                                        </div>
                                        <div class="lk__table-col">
                                            <a href="tel:<?=$item["phone"];?>"><?=$item["phone"];?></a>
                                        </div>
                                        <div class="lk__table-col">
                                            <?=$item["inn"];?>
                                        </div>
                                        <div class="lk__table-col">
                                            <a class="lk__link js-table-detail-link" href="javascript:void(0)" data-show-text="<?=($arParams["BUYER_ID"]==$item["id"]?'Подробнее':'Свернуть');?>">
                                                <?=($arParams["BUYER_ID"]==$item["id"]?'Свернуть':'Подробнее');?>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="lk__table-row-data js-table-detail-data" <?=($arParams["BUYER_ID"]==$item["id"]?'style="display:block;"':'');?>>
                                        <div class="lk__data">
                                            <div class="lk__data-row">
                                                <div class="lk__data-label">
                                                    Подробная информация
                                                </div>
                                                <div class="lk__data-value">
                                                    <ul class="list-dotted">
                                                        <li class="list-dotted__row">
                                                            <span class="list-dotted__name">Организация:</span>
                                                            <span class="list-dotted__line"></span>
                                                            <span class="list-dotted__value"><?=$item["name"];?></span>
                                                        </li>
                                                        <li class="list-dotted__row">
                                                            <span class="list-dotted__name">Контактное лицо:</span>
                                                            <span class="list-dotted__line"></span>
                                                            <span class="list-dotted__value"><?=$item["contact_people"];?></span>
                                                        </li>
                                                        <li class="list-dotted__row">
                                                            <span class="list-dotted__name">Должность:</span>
                                                            <span class="list-dotted__line"></span>
                                                            <span class="list-dotted__value"><?=$item["position"];?></span>
                                                        </li>
                                                        <li class="list-dotted__row">
                                                            <span class="list-dotted__name">Телефон:</span>
                                                            <span class="list-dotted__line"></span>
                                                            <span class="list-dotted__value"><?=$item["phone"];?></span>
                                                        </li>
                                                        <?if(!empty($item["country"])):?>
                                                            <li class="list-dotted__row">
                                                                <span class="list-dotted__name">Страна:</span>
                                                                <span class="list-dotted__line"></span>
                                                                <span class="list-dotted__value"><?=$item["country"]["NAME"];?></span>
                                                            </li>
                                                        <?endif;?>
                                                        <?if(!empty($item["bin"])):?>
                                                            <li class="list-dotted__row">
                                                                <span class="list-dotted__name">БИН:</span>
                                                                <span class="list-dotted__line"></span>
                                                                <span class="list-dotted__value"><?=$item["bin"];?></span>
                                                            </li>
                                                        <?endif;?>
                                                        <?if(!empty($item["inn"])):?>
                                                            <li class="list-dotted__row">
                                                                <span class="list-dotted__name">ИНН:</span>
                                                                <span class="list-dotted__line"></span>
                                                                <span class="list-dotted__value"><?=$item["inn"];?></span>
                                                            </li>
                                                        <?endif;?>
                                                        <li class="list-dotted__row">
                                                            <span class="list-dotted__name">Адрес:</span>
                                                            <span class="list-dotted__line"></span>
                                                            <span class="list-dotted__value"><?=$item["region"];?>, <?=$item["district"];?>, <?=$item["address"];?></span>
                                                        </li>
                                                        <li class="list-dotted__row">
                                                            <span class="list-dotted__name">Электронная почта:</span>
                                                            <span class="list-dotted__line"></span>
                                                            <span class="list-dotted__value"><?=$item["mail"];?></span>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <?if(!empty($item["arTech"])):?>
                                                <div class="lk__data-row">
                                                    <div class="lk__data-label">
                                                        Техника, закрепленная за покупателем
                                                    </div>
                                                    <div class="lk__data-value">
                                                        <?foreach ($item["arTech"] as $tech):?>
                                                            <p><?=$tech["type"];?> / <?=$tech["model"];?></p>
                                                        <?endforeach;?>
                                                        <div class="btn-group">
                                                            <form action="/ajax/unloading/allbuyers/" method="POST" class="js-form-marketing">
                                                                <input type="hidden" name="id_dealer" value="<?=$arParams["DETAIL_ID"];?>">
                                                                <input type="hidden" name="id_buyer" value="<?=$item["id"];?>">
                                                                <button type="submit" class="btn">Выгрузка данных покупателя</button>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            <?endif;?>
                                        </div>
                                        <a class="lk__link lk__link-in-data js-table-detail-link" href="javascript:void(0)">
                                            Свернуть
                                        </a>
                                    </div>
                                </div>
                            <?endforeach;?>
                            <!--endAjaxPagebuyers-->
                    </div>
                </div>
                <?$APPLICATION->IncludeComponent("letsrock:paginations",
                    ".default",
                    [
                        "CACHE_TIME" => 0,
                        "ELEMS_PAGE"=>$arResult["buyers"]["pagination"],
                        "GET_PAGE"=>"page_buyer",
                        "AJAX_BLOCK"=>"buyers",
                        "PAGEN_HASH"=>"buyers",
                        "PAGE_ID" => "buyers",
                    ]
                );?>
                <form action="/ajax/unloading/allbuyers/" method="POST" class="js-form-marketing">
                    <input type="hidden" name="id_dealer" value="<?=$arParams["DETAIL_ID"];?>">
                    <button type="submit" class="btn">Выгрузка данных всех покупателей</button>
                </form>
            </article>
            <article class="lk__tabs-content-item" data-tab-content='technics'>
                <label class="field field-search">
                    <input class="field__input" type="text" name="search_expl" data-search="/<?=$arParams["PANEL"];?>/dealers/?person=<?=$arParams["DETAIL_ID"];?>&search_expl=" data-has="tech" placeholder="Поиск техники по названию / номеру / покупателю" value="<?=$_REQUEST["search_expl"];?>">
                </label>
                <div class="lk__table technics-dealer">
                    <div class="lk__table-head">
                        <div class="lk__table-row">
                            <div class="lk__table-col">
                                Название
                            </div>
                            <div class="lk__table-col">
                                Номер
                            </div>
                            <div class="lk__table-col">
                                Гарантия
                            </div>
                            <div class="lk__table-col">
                                Покупатель
                            </div>
                            <div class="lk__table-col">
                            </div>
                        </div>
                    </div>
                    <div class="lk__table-body" data-blockajax="tech">
                        <!--startAjaxPagetech-->
                        <?foreach ($arResult["tech"]["arTech"] as $exploitation):?>
                            <div class="lk__table-row-wrap js-table-detail">
                                <div class="lk__table-row">
                                    <div class="lk__table-col">
                                        <div class="lk__table-col-head">
                                            Название
                                        </div>
                                        <?=$exploitation["type"];?> / <?=$exploitation["model"];?>
                                    </div>
                                    <div class="lk__table-col">
                                        <div class="lk__table-col-head">
                                            Номер
                                            <div class="hint">
                                                <div class="hint__message">
                                                    Lorem, ipsum dolor sit amet consectetur adipisicing elit. Modi, repellat necessitatibus quam
                                                </div>
                                            </div>
                                        </div>
                                        <?=$exploitation["number"];?>
                                    </div>
                                    <div class="lk__table-col">
                                        <div class="lk__table-col-head">
                                            Гарантия
                                            <div class="hint">
                                                <div class="hint__message">
                                                    Lorem, ipsum dolor sit amet consectetur adipisicing elit. Modi, repellat necessitatibus quam
                                                </div>
                                            </div>
                                        </div>
                                        <?=$exploitation["status"];?>
                                    </div>
                                    <div class="lk__table-col">
                                        <div class="lk__table-col-head">
                                            Покупатель
                                        </div>
                                        <?=$exploitation["buyer"]["name"];?>
                                    </div>
                                    <div class="lk__table-col">
                                        <a class="lk__link js-table-detail-link" href="javascript:void(0)" data-show-text="Свернуть">
                                            Подробнее
                                        </a>
                                    </div>
                                </div>
                                <div class="lk__table-row-data js-table-detail-data">
                                    <div class="lk__data">
                                        <?if(!empty($exploitation["akt"]["SRC"])):?>
                                            <div class="lk__data-row">
                                                <div class="lk__data-label">
                                                    Акт ввода в эксплуатацию
                                                </div>
                                                <div class="lk__data-value">
                                                    <div class="lk__data-files">
                                                        <a class="link-file" href="<?=$exploitation["akt"]["SRC"];?>" target="_blank">Отчет.doc</a>
                                                    </div>
                                                </div>
                                            </div>
                                        <?endif;?>
                                        <div class="lk__data-row">
                                            <div class="lk__data-label">
                                                Информация о покупателе
                                            </div>
                                            <div class="lk__data-value">
                                                <ul class="list-dotted">
                                                    <li class="list-dotted__row">
                                                        <span class="list-dotted__name">Название организации:</span>
                                                        <span class="list-dotted__line"></span>
                                                        <span class="list-dotted__value"><?=$exploitation["buyer"]["name"];?></span>
                                                    </li>
                                                    <li class="list-dotted__row">
                                                        <span class="list-dotted__name">Контактное лицо:</span>
                                                        <span class="list-dotted__line"></span>
                                                        <span class="list-dotted__value"><?=$exploitation["buyer"]["props"]["CONTACT_PEOPLE"]["VALUE"];?></span>
                                                    </li>
                                                    <li class="list-dotted__row">
                                                        <span class="list-dotted__name">Телефон:</span>
                                                        <span class="list-dotted__line"></span>
                                                        <span class="list-dotted__value">
                                         <a href="tel:<?=$exploitation["buyer"]["props"]["PHONE"]["VALUE"];?>"><?=$exploitation["buyer"]["props"]["PHONE"]["VALUE"];?></a>
                                      </span>
                                                    </li>
                                                    <li class="list-dotted__row">
                                                        <span class="list-dotted__name">ИНН:</span>
                                                        <span class="list-dotted__line"></span>
                                                        <span class="list-dotted__value"><?=$exploitation["buyer"]["props"]["INN"]["VALUE"];?></span>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="lk__data-row">
                                            <div class="lk__data-label">
                                                История техники
                                            </div>
                                            <div class="lk__data-value">
                                                <ul class="lk__data-history">
                                                    <?foreach ($exploitation["history"] as $history):?>
                                                        <li>
                                                            <span><?=explode(' ', $history["UF_DATE_HISTORY"])[0];?></span>
                                                            <span><?=$history["UF_ACTION"];?></span>
                                                        </li>
                                                    <?endforeach;?>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="lk__data-row">
                                            <div class="lk__data-label">
                                                <? if (!empty($exploitation["refusal"]) && $exploitation["status_id"] == 14): ?>Причина отказа<? endif; ?>
                                            </div>
                                            <div class="lk__data-value">
                                                <? if (!empty($exploitation["refusal"]) && $exploitation["status_id"] == 14): ?>
                                                    <div class="lk__data-value">
                                                        <div class="lk__data-box">
                                                            <?= $exploitation["refusal"]; ?>
                                                        </div>
                                                    </div>
                                                <? endif; ?>
                                                <div class="btn-group">
                                                    <form action="/ajax/unloading/exploitation/dealer/" method="POST" class="js-form-marketing">
                                                        <input type="hidden" name="id_dealer" value="<?=$arParams["DETAIL_ID"];?>">
                                                        <input type="hidden" name="id_expl" value="<?=$exploitation["id"];?>">
                                                        <button type="submit" class="btn">Выгрузка данных техники</button>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <a class="lk__link lk__link-in-data js-table-detail-link" href="javascript:void(0)">
                                        Свернуть
                                    </a>
                                </div>
                            </div>
                        <?endforeach;?>
                        <!--endAjaxPagetech-->
                    </div>
                    <div class="pagination" data-pagenajax="tech">
                        <!--startAjaxPaginationtech-->
                        <?
                        echo $arResult["tech"]["pagination"];
                    /*$APPLICATION->IncludeComponent("letsrock:paginations",
                        ".default",
                        [
                            "CACHE_TIME" => 0,
                            "ELEMS_PAGE"=>$arResult["tech"]["pagination"],
                            "GET_PAGE"=>"page_expl",
                            "AJAX_BLOCK"=>"tech",
                            "PAGEN_HASH"=> "tech",
                            "PAGE_ID" => "technics",
                        ]
                    );*/?>
                        <!--endAjaxPaginationtech-->
                    </div>
                </div>
                <form action="/ajax/unloading/exploitation/dealer/" method="POST" class="js-form-marketing">
                    <input type="hidden" name="id_dealer" value="<?=$arParams["DETAIL_ID"];?>">
                    <button type="submit" class="btn">Выгрузка данных всей техники</button>
                </form>
            </article>
        </section>
    </section>
</div>