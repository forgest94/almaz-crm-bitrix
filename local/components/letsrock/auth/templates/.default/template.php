<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
?>
<section class="auth-section">
    <div class="container">
        <h2 class="auth-title">Вход в личный кабинет</h2>
        <div class="auth-wrap">
            <form action="/ajax/auth/" method="POST" class="auth-form js-form-auth" data-redirect="<?=$arParams["REDIRECT"]?>">
                <div class="auth-form__row js-field">
                    <div class="auth-form__field required">
                        <label class="field">
                            <span class="field__title field__required">Логин</span>
                            <input class="field__input" type="text" name="email" required>
                        </label>
                    </div>
                    <div class="tooltip">
                        Поле не должно быть пустым
                    </div>
                </div>
                <div class="auth-form__row js-field">
                    <div class="auth-form__field required">
                        <label class="field">
                            <span class="field__title field__required">Пароль</span>
                            <input class="field__input" type="password" name="password" required>
                        </label>
                    </div>
                    <div class="tooltip">
                        Поле не должно быть пустым
                    </div>
                </div>
                <button class="btn auth-form__submit" type="submit">Войти</button>
            </form>
        </div>
    </div>
</section>