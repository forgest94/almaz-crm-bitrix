<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}

use Letsrock\Lib\Models\User;
use Letsrock\Lib\Models\Element;
use Letsrock\Lib\Models\HLBlock;
use Letsrock\Lib\Models\Property;

\Bitrix\Main\Loader::includeModule('letsrock.lib');

class ReclamationPartsComponent extends CBitrixComponent
{
    public function propIsDop($code, $iblock)
    {
        $prMarkHl = new HLBlock(HL_POPSDOP);
        $arMarkProp_db = $prMarkHl->get(["UF_CODE_PROP" => $code, "UF_IBLOCK_ID"=>$iblock]);
        if (!empty($arMarkProp_db)) {
            return $arMarkProp_db[0];
        } else {
            return [];
        }
    }

    public function onPrepareComponentParams($arParams)
    {
        $result = $arParams;

        return $result;
    }


    /**
     * @return array|mixed
     * @throws \Bitrix\Main\LoaderException
     */
    public function executeComponent()
    {
        global $USER;
        if ($this->startResultCache($this->arParams["CACHE_TIME"]) && $USER->GetID()) {
            $elemIbl = new Element;
            /*startParts*/
            $arReclamation_Parts_db = $elemIbl->GetList(IB_RECLAMATION_PARTS,
                ["NAME", "ID", "IBLOCK_ID", "PROPERTY_*"],
                ["PROPERTY_DEALER" => $USER->GetID()],
                ["ID" => "desc"]
            );
            $arParts = [];
            foreach ($arReclamation_Parts_db["ELEMS"] as $item) {
                $arProps = $arReclamation_Parts_db["PROPS"][$item["ID"]];
                /*$akt_parse = [];
                if(!empty($arProps["AKT"]["VALUE"])){
                    $akt = CFile::GetFileArray($arProps["AKT"]["VALUE"]);
                    if($akt)
                        $akt_parse = ["name"=>$akt["ORIGINAL_NAME"], "src"=>$akt["SRC"]];
                }*/
                $akt_parse = [];
                if (!empty($arProps["AKT"]["VALUE"])) {
                    foreach ($arProps["AKT"]["VALUE"] as $akt2) {
                        $arFile = CFile::GetFileArray($akt2);
                        if ($arFile)
                            $akt_parse[] = ["name" => $arFile["ORIGINAL_NAME"], "src" => $arFile["SRC"]];
                    }
                }
                $arPhoto = [];
                if (!empty($arProps["PHOTO"]["VALUE"])) {
                    foreach ($arProps["PHOTO"]["VALUE"] as $photo) {
                        $arFile = CFile::GetFileArray($photo);
                        if ($arFile)
                            $arPhoto[] = ["name" => $arFile["ORIGINAL_NAME"], "src" => $arFile["SRC"]];
                    }
                }
                $arParts[] = [
                    "id" => $item["ID"],
                    "date" => $arProps["DATE"]["VALUE"],
                    "reason" => $arProps["REASON"]["VALUE"]["TEXT"],
                    "wishes" => $arProps["WISHES"]["VALUE"]["TEXT"],
                    "refusal" => $arProps["REFUSAL"]["VALUE"]["TEXT"],
                    "status" => $arProps["STATUS"]["VALUE"],
                    "status_id" => $arProps["STATUS"]["VALUE_ENUM_ID"],
                    "akt" => $akt_parse,
                    "photo" => $arPhoto
                ];
            }
            $propertiesParts = CIBlockProperty::GetList(Array("sort" => "asc", "name" => "asc"), Array("ACTIVE" => "Y", "IBLOCK_ID" => IB_RECLAMATION_PARTS));
            $arTextParts = [];
            while ($prop_fields = $propertiesParts->GetNext()) {
                if ($prop_fields["CODE"] == "STATUS")
                    unset($prop_fields);
                $req_class = '';
                $req_class2 = '';
                if ($prop_fields["IS_REQUIRED"] == 'Y') {
                    $req_class = 'required';
                    $req_class2 = 'field__required';
                }
                $arTextParts[$prop_fields["CODE"]] = [
                    "name" => $prop_fields["NAME"],
                    "file_type" => $prop_fields["FILE_TYPE"],
                    "text" => $prop_fields["HINT"],
                    "req" => $req_class,
                    "req2" => $req_class2,
                    "dop_filds" => $this->propIsDop($prop_fields["CODE"], IB_RECLAMATION_PARTS),
                ];
            }

            /*endParts*/

            $infoEdit = [];
            if (!empty($this->arParams["EDTI_ID"])) {
                $arReclamation_Parts_db2 = $elemIbl->GetList(IB_RECLAMATION_PARTS,
                    ["NAME", "ID", "IBLOCK_ID", "PROPERTY_*"],
                    [
                        "PROPERTY_DEALER" => $USER->GetID(),
                        "ID" => $this->arParams["EDTI_ID"]
                    ],
                    ["ID" => "desc"]
                );
                foreach ($arReclamation_Parts_db2["ELEMS"] as $item) {
                    $arProps = $arReclamation_Parts_db["PROPS"][$item["ID"]];
                    $aktInfo = [];
                    if (!empty($arProps["AKT"]["VALUE"])) {
                        $aktInfoParse = '';
                        foreach ($arProps["AKT"]["VALUE"] as $aktVal){
                            $aktInfoParse = CFile::GetFileArray($aktVal);
                            $aktInfo[] = ["name" => $aktInfoParse["ORIGINAL_NAME"], "src" => $aktInfoParse["SRC"], "id" => $aktVal];
                        }
                    }
                    $photoInfo = [];
                    if (!empty($arProps["PHOTO"]["VALUE"])) {
                        $fileInfo = '';
                        foreach ($arProps["PHOTO"]["VALUE"] as $file) {
                            $fileInfo = CFile::GetFileArray($file);
                            $photoInfo[] = ["name" => $fileInfo["ORIGINAL_NAME"], "src" => $fileInfo["SRC"], "id" => $file];
                        }
                    }
                    $arDocument = [];
                    if (!empty($arProps["INVOICE"]["VALUE"]) && $arProps["INVOICE"]["VALUE"][0] != 'N') {
                        $arDocument["invoice"] = $arProps["INVOICE"]["VALUE"];
                    }
                    if (!empty($arProps["CONSIGNMENT"]["VALUE"]) && $arProps["CONSIGNMENT"]["VALUE"][0] != 'N') {
                        $arDocument["consignment"] = $arProps["CONSIGNMENT"]["VALUE"];
                    }
                    $infoEdit = [
                        "id" => $item["ID"],
                        "arDocument" => $arDocument,
                        "reason" => $arProps["REASON"]["VALUE"]["TEXT"],
                        "wishes" => $arProps["WISHES"]["VALUE"]["TEXT"],
                        "PHONE_PERSONAL" => $arProps["PHONE_PERSONAL"]["VALUE"],
                        "akt" => $aktInfo,
                        "photo" => $photoInfo,
                    ];
                }
            }

            $pagen = [];
            $arPage = [];
            if (!empty($arParts)) {
                $arPage = array_chunk($arParts, $this->arParams["PAGE_ELEMS"]);
                if (count($arPage) > 1) {
                    for ($i = 1; $i <= count($arPage); $i++) {
                        $pagen[] = $i;
                    }
                }
            }

            $arFullParams = [
                "arParts" => $arPage[$this->arParams["page"] ? $this->arParams["page"] - 1 : 0],
                "formLabelParts" => $arTextParts,
                "pagination" => $pagen,
                "infoEdit" => $infoEdit
            ];
            $this->arResult = $arFullParams;
            $this->includeComponentTemplate();
        }
        return $this->arResult;
    }
} ?>