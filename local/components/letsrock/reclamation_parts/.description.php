<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
$arComponentDescription = array(
    "NAME" => 'Рекламации запчастей',
    "DESCRIPTION" => 'Рекламации запчастей',
    "PATH" => array(
        "ID" => "dv_components",
        "CHILD" => array(
            "ID" => "reclamation_parts",
            "NAME" => "Рекламации запчастей"
        )
    ),
);
?>