<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}

?>
<a class="btn btn_small lk__tabs-content-btn js-tabs-show-form <?= $arParams["EDTI_ID"] ? 'hide' : ''; ?>"
   href="javascript:void(0)">Подать рекламацию на запчасти</a>
<div class="lk__tabs-content-inner js-tabs-content-inner <?= $arParams["EDTI_ID"] ? '' : 'active'; ?>">
    <div class="lk__table spares">
        <div class="lk__table-head">
            <div class="lk__table-row">
                <div class="lk__table-col">
                    Дата подачи
                </div>
                <div class="lk__table-col">
                    Причина обращения
                </div>
                <div class="lk__table-col">
                    Статус
                </div>
                <div class="lk__table-col">
                </div>
            </div>
        </div>
        <div class="lk__table-body">
            <? if (!empty($arResult["arParts"])): ?>
                <? foreach ($arResult["arParts"] as $itemP): ?>
                    <div class="lk__table-row-wrap js-table-detail">
                        <div class="lk__table-row">
                            <div class="lk__table-col">
                                <div class="lk__table-col-head">
                                    Дата подачи
                                </div>
                                <?= $itemP["date"]; ?>
                            </div>
                            <div class="lk__table-col">
                                <div class="lk__table-col-head">
                                    Причина обращения
                                </div>
                                <?= $itemP["reason"]; ?>
                            </div>
                            <div class="lk__table-col">
                                <div class="lk__table-col-head">
                                    Статус
                                </div>
                                <?= $itemP["status"]; ?>
                            </div>
                            <div class="lk__table-col">
                                <a class="lk__link js-table-detail-link" href="javascript:void(0)"
                                   data-show-text="Свернуть">
                                    Подробнее
                                </a>
                            </div>
                        </div>
                        <div class="lk__table-row-data js-table-detail-data">
                            <div class="lk__data">
                                <? if (!empty($itemP["reason"])): ?>
                                    <div class="lk__data-row">
                                        <div class="lk__data-label">
                                            Причина обращения
                                        </div>
                                        <div class="lk__data-value">
                                            <div class="lk__data-box">
                                                <?= $itemP["reason"]; ?>
                                            </div>
                                        </div>
                                    </div>
                                <? endif; ?>
                                <? if (!empty($itemP["wishes"])): ?>
                                    <div class="lk__data-row">
                                        <div class="lk__data-label">
                                            Предложения по решению
                                        </div>
                                        <div class="lk__data-value">
                                            <div class="lk__data-box">
                                                <?= $itemP["wishes"]; ?>
                                            </div>
                                        </div>
                                    </div>
                                <? endif; ?>
                                <? /*<?if(!empty($itemP["akt"])):?>
                                                    <div class="lk__data-row">
                                                        <div class="lk__data-label">
                                                            Акт выхода из строя
                                                        </div>
                                                        <div class="lk__data-value">
                                                            <div class="lk__data-files">
                                                                <a class="link-file" href="<?=$itemP["akt"]["src"];?>" download><?=$itemP["akt"]["name"];?></a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                <?endif;?>*/ ?>
                                <? if (!empty($itemP["akt"])): ?>
                                    <div class="lk__data-row">
                                        <div class="lk__data-label">
                                            Рекламация
                                        </div>
                                        <div class="lk__data-value">
                                            <div class="lk__data-files">
                                                <? foreach ($itemP["akt"] as $photo): ?>
                                                    <a class="link-file" href="<?= $photo["src"]; ?>"
                                                       download><?= $photo["name"]; ?></a>
                                                <? endforeach; ?>
                                            </div>
                                        </div>
                                    </div>
                                <? endif; ?>
                                <? if (!empty($itemP["photo"])): ?>
                                    <div class="lk__data-row">
                                        <div class="lk__data-label">
                                            Фото
                                        </div>
                                        <div class="lk__data-value">
                                            <div class="lk__data-files">
                                                <? foreach ($itemP["photo"] as $photo): ?>
                                                    <a class="link-file" href="<?= $photo["src"]; ?>"
                                                       download><?= $photo["name"]; ?></a>
                                                <? endforeach; ?>
                                            </div>
                                        </div>
                                    </div>
                                <? endif; ?>
                                <? if ($itemP["status_id"] == 24): ?>
                                    <div class="lk__data-row">
                                        <div class="lk__data-label">
                                            <? if (!empty($itemP["refusal"])): ?>Причина отказа<? endif; ?>
                                        </div>
                                        <div class="lk__data-value">
                                            <? if (!empty($itemP["refusal"])): ?>
                                                <div class="lk__data-value">
                                                    <div class="lk__data-box">
                                                        <?= $itemP["refusal"]; ?>
                                                    </div>
                                                </div>
                                            <? endif; ?>
                                            <div class="btn-group">
                                                <a class="btn"
                                                   href="/dealer/reclamation/?edit_recp=<?= $itemP["id"]; ?>#spares"
                                                   data-taskid="<?= $itemP["id"]; ?>" data-click="editTask">Редактировать</a>
                                            </div>
                                        </div>
                                    </div>
                                <? endif; ?>
                            </div>
                            <?/*<a class="lk__link lk__link-in-data js-table-detail-link" href="javascript:void(0)">
                                Свернуть
                            </a>*/?>
                        </div>
                    </div>
                <? endforeach; ?>
            <? endif; ?>
        </div>
    </div>
    <? $APPLICATION->IncludeComponent("letsrock:paginations",
        ".default",
        [
            "CACHE_TIME" => 0,
            "ELEMS_PAGE" => $arResult["pagination"],
            "GET_PAGE" => "page_parts",
            "AJAX_BLOCK" => "reclamation_parts"
        ]
    ); ?>
</div>
<div class="lk__tabs-content-form js-tabs-form <?= $arParams["EDTI_ID"] ? 'active' : ''; ?>">
    <a class="btn-arrow js-tabs-back" href="javascript:void(0)">
        <i class="icon-back"></i>
        <span>Назад</span>
    </a>
    <form action="/ajax/dealer/addReclamation/parts" method="POST" class="js-form-marketing" novalidate="novalidate">
        <div class="lk__form-section">
            <div class="lk__form-title">
                Введите номер документа поступления (на выбор):
            </div>
            <? $icv = 0; ?>
            <? if (!empty($arResult["infoEdit"]["arDocument"])): ?>
                <? if (!empty($arResult["infoEdit"]["arDocument"]["invoice"])): ?>
                    <? foreach ($arResult["infoEdit"]["arDocument"]["invoice"] as $inv): ?>
                        <? $icv++; ?>
                        <div class="lk__form-radio js-lk-radio">
                            <div class="lk__form-radio-block">
                                <label class="field-radio">Счет-фактура
                                    <input class="field-radio__input js-lk-radio-btn" type="radio" checked="checked"
                                           name="document-type<?= $icv; ?>" value="invoice"
                                           data-req="<?= $arResult["formLabelParts"]["INVOICE"]["req"] ? 'Y' : 'N'; ?>">
                                    <span class="field-radio__checkmark"></span>
                                </label>
                                <label class="field-radio">Товарно-транспортная накладная
                                    <input class="field-radio__input js-lk-radio-btn" type="radio"
                                           name="document-type<?= $icv; ?>" value="waybill"
                                           data-req="<?= $arResult["formLabelParts"]["CONSIGNMENT"]["req"] ? 'Y' : 'N'; ?>">
                                    <span class="field-radio__checkmark"></span>
                                </label>
                            </div>
                            <div class="lk__form-radio-data">
                                <div class="lk__form-row js-field" data-radio-type="invoice">
                                    <div class="lk__form-field <?= $arResult["formLabelParts"]["INVOICE"]["req"]; ?>">
                                        <label class="field focused">
                                            <span class="field__title <?= $arResult["formLabelParts"]["INVOICE"]["req2"]; ?>">Введите номер счета-фактуры</span>
                                            <input class="field__input" type="text" name="INVOICE[]"
                                                   <?= $arResult["formLabelParts"]["INVOICE"]["req"]; ?>
                                                   value="<?= $inv; ?>">
                                        </label>
                                    </div>
                                    <? if ($arResult["formLabelParts"]["INVOICE"]["text"]): ?>
                                        <div class="hint">
                                            <div class="hint__message">
                                                <?= $arResult["formLabelParts"]["INVOICE"]["text"]; ?>
                                            </div>
                                        </div>
                                    <? endif; ?>
                                    <? if ($arResult["formLabelParts"]["INVOICE"]["req"]): ?>
                                        <div class="tooltip js-error-tooltip">
                                            Поле <?= $arResult["formLabelParts"]["INVOICE"]["name"]; ?> не может быть
                                            пустым
                                        </div>
                                    <? endif; ?>
                                </div>
                                <div class="lk__form-row hide js-field" data-radio-type="waybill">
                                    <div class="lk__form-field">
                                        <label class="field">
                                            <span class="field__title">Введите номер товарно-транспортной накладной</span>
                                            <input class="field__input" type="text" name="CONSIGNMENT[]">
                                        </label>
                                    </div>
                                    <? if ($arResult["formLabelParts"]["CONSIGNMENT"]["text"]): ?>
                                        <div class="hint">
                                            <div class="hint__message">
                                                <?= $arResult["formLabelParts"]["CONSIGNMENT"]["text"]; ?>
                                            </div>
                                        </div>
                                    <? endif; ?>
                                    <? if ($arResult["formLabelParts"]["CONSIGNMENT"]["req"]): ?>
                                        <div class="tooltip js-error-tooltip">
                                            Поле <?= $arResult["formLabelParts"]["CONSIGNMENT"]["name"]; ?> не может
                                            быть пустым
                                        </div>
                                    <? endif; ?>
                                </div>
                            </div>
                        </div>
                    <? endforeach; ?>
                <? endif; ?>
                <? if (!empty($arResult["infoEdit"]["arDocument"]["consignment"])): ?>
                    <? foreach ($arResult["infoEdit"]["arDocument"]["consignment"] as $key => $cons): ?>
                        <? $icv++; ?>
                        <div class="lk__form-radio js-lk-radio">
                            <div class="lk__form-radio-block">
                                <label class="field-radio">Счет-фактура
                                    <input class="field-radio__input js-lk-radio-btn" type="radio"
                                           name="document-type<?= $icv; ?>" value="invoice"
                                           data-req="<?= $arResult["formLabelParts"]["INVOICE"]["req"] ? 'Y' : 'N'; ?>">
                                    <span class="field-radio__checkmark"></span>
                                </label>
                                <label class="field-radio">Товарно-транспортная накладная
                                    <input class="field-radio__input js-lk-radio-btn" type="radio" checked="checked"
                                           name="document-type<?= $icv; ?>" value="waybill"
                                           data-req="<?= $arResult["formLabelParts"]["CONSIGNMENT"]["req"] ? 'Y' : 'N'; ?>">
                                    <span class="field-radio__checkmark"></span>
                                </label>
                            </div>
                            <div class="lk__form-radio-data">
                                <div class="lk__form-row hide js-field" data-radio-type="invoice">
                                    <div class="lk__form-field">
                                        <label class="field">
                                            <span class="field__title">Введите номер счета-фактуры</span>
                                            <input class="field__input" type="text" name="INVOICE[]">
                                        </label>
                                    </div>
                                    <? if ($arResult["formLabelParts"]["INVOICE"]["text"]): ?>
                                        <div class="hint">
                                            <div class="hint__message">
                                                <?= $arResult["formLabelParts"]["INVOICE"]["text"]; ?>
                                            </div>
                                        </div>
                                    <? endif; ?>
                                    <? if ($arResult["formLabelParts"]["INVOICE"]["req"]): ?>
                                        <div class="tooltip js-error-tooltip">
                                            Поле <?= $arResult["formLabelParts"]["INVOICE"]["name"]; ?> не может быть
                                            пустым
                                        </div>
                                    <? endif; ?>
                                </div>
                                <div class="lk__form-row js-field" data-radio-type="waybill">
                                    <div class="lk__form-field <?= $arResult["formLabelParts"]["CONSIGNMENT"]["req"]; ?>">
                                        <label class="field focused">
                                            <span class="field__title <?= $arResult["formLabelParts"]["CONSIGNMENT"]["req2"]; ?>">Введите номер товарно-транспортной накладной</span>
                                            <input class="field__input" type="text" name="CONSIGNMENT[]"
                                                   value="<?= $cons; ?>"
                                                   <?= $arResult["formLabelParts"]["CONSIGNMENT"]["req"]; ?>>
                                        </label>
                                    </div>
                                    <? if ($arResult["formLabelParts"]["CONSIGNMENT"]["text"]): ?>
                                        <div class="hint">
                                            <div class="hint__message">
                                                <?= $arResult["formLabelParts"]["CONSIGNMENT"]["text"]; ?>
                                            </div>
                                        </div>
                                    <? endif; ?>
                                    <? if ($arResult["formLabelParts"]["CONSIGNMENT"]["req"]): ?>
                                        <div class="tooltip js-error-tooltip">
                                            Поле <?= $arResult["formLabelParts"]["CONSIGNMENT"]["name"]; ?> не может
                                            быть пустым
                                        </div>
                                    <? endif; ?>
                                </div>
                            </div>
                        </div>
                    <? endforeach; ?>
                <? endif; ?>
            <? else: ?>
                <div class="lk__form-radio js-lk-radio">
                    <div class="lk__form-radio-block">
                        <label class="field-radio">Счет-фактура
                            <input class="field-radio__input js-lk-radio-btn" type="radio" checked="checked"
                                   name="document-type" value="invoice"
                                   data-req="<?= $arResult["formLabelParts"]["INVOICE"]["req"] ? 'Y' : 'N'; ?>">
                            <span class="field-radio__checkmark"></span>
                        </label>
                        <label class="field-radio">Товарно-транспортная накладная
                            <input class="field-radio__input js-lk-radio-btn" type="radio" name="document-type"
                                   value="waybill"
                                   data-req="<?= $arResult["formLabelParts"]["CONSIGNMENT"]["req"] ? 'Y' : 'N'; ?>">
                            <span class="field-radio__checkmark"></span>
                        </label>
                    </div>
                    <div class="lk__form-radio-data">
                        <div class="lk__form-row js-field" data-radio-type="invoice">
                            <div class="lk__form-field <?= $arResult["formLabelParts"]["INVOICE"]["req"]; ?>">
                                <label class="field">
                                    <span class="field__title <?= $arResult["formLabelParts"]["INVOICE"]["req2"]; ?>">Введите номер счета-фактуры</span>
                                    <input class="field__input" type="text" name="INVOICE[]"
                                           <?= $arResult["formLabelParts"]["INVOICE"]["req"]; ?>>
                                </label>
                            </div>
                            <? if ($arResult["formLabelParts"]["INVOICE"]["text"]): ?>
                                <div class="hint">
                                    <div class="hint__message">
                                        <?= $arResult["formLabelParts"]["INVOICE"]["text"]; ?>
                                    </div>
                                </div>
                            <? endif; ?>
                            <? if ($arResult["formLabelParts"]["INVOICE"]["req"]): ?>
                                <div class="tooltip js-error-tooltip">
                                    Поле <?= $arResult["formLabelParts"]["INVOICE"]["name"]; ?> не может быть пустым
                                </div>
                            <? endif; ?>
                        </div>
                        <div class="lk__form-row hide js-field" data-radio-type="waybill">
                            <div class="lk__form-field">
                                <label class="field">
                                    <span class="field__title">Введите номер товарно-транспортной накладной</span>
                                    <input class="field__input" type="text" name="CONSIGNMENT[]">
                                </label>
                            </div>
                            <? if ($arResult["formLabelParts"]["CONSIGNMENT"]["text"]): ?>
                                <div class="hint">
                                    <div class="hint__message">
                                        <?= $arResult["formLabelParts"]["CONSIGNMENT"]["text"]; ?>
                                    </div>
                                </div>
                            <? endif; ?>
                            <? if ($arResult["formLabelParts"]["CONSIGNMENT"]["req"]): ?>
                                <div class="tooltip js-error-tooltip">
                                    Поле <?= $arResult["formLabelParts"]["CONSIGNMENT"]["name"]; ?> не может быть пустым
                                </div>
                            <? endif; ?>
                        </div>
                    </div>
                </div>
            <? endif; ?>
            <div class="lk__form-add-wrap">
                <a class="lk__form-add js-lk-row-add" href="javascript:void(0)">Еще</a>
            </div>
        </div>
        <div class="lk__form-section">
            <div class="lk__form-box">
                <div class="lk__form-title">
                    Укажите причину обращения
                </div>
                <div class="lk__form-row">
                    <label class="field field-textarea <?= $arResult["infoEdit"]["reason"] ? 'focused' : ''; ?>">
                        <span class="field__title <?= $arResult["formLabelParts"]["REASON"]["req2"]; ?>">Кратко опишите проблему</span>
                        <textarea class="field__input" type="text" name="REASON"
                                  <?= $arResult["formLabelParts"]["REASON"]["req"]; ?>><?= $arResult["infoEdit"]["reason"]; ?></textarea>
                    </label>
                    <? if ($arResult["formLabelParts"]["REASON"]["text"]): ?>
                        <div class="hint">
                            <div class="hint__message">
                                <?= $arResult["formLabelParts"]["REASON"]["text"]; ?>
                            </div>
                        </div>
                    <? endif; ?>
                    <? if ($arResult["formLabelParts"]["REASON"]["req"]): ?>
                        <div class="tooltip js-error-tooltip">
                            Поле <?= $arResult["formLabelParts"]["REASON"]["name"]; ?> не может быть пустым
                        </div>
                    <? endif; ?>
                </div>
            </div>
            <div class="lk__form-box">
                <div class="lk__form-title">
                    Ваши пожелания по решению рекламации
                </div>
                <div class="lk__form-row">
                    <label class="field field-textarea <?= $arResult["infoEdit"]["wishes"] ? 'focused' : ''; ?>">
                        <span class="field__title <?= $arResult["formLabelParts"]["WISHES"]["req2"]; ?>">Допоставка, возврат средств и т.д.</span>
                        <textarea class="field__input" type="text" name="WISHES"
                                  <?= $arResult["formLabelParts"]["WISHES"]["req"]; ?>><?= $arResult["infoEdit"]["wishes"]; ?></textarea>
                    </label>
                    <? if ($arResult["formLabelParts"]["WISHES"]["text"]): ?>
                        <div class="hint">
                            <div class="hint__message">
                                <?= $arResult["formLabelParts"]["WISHES"]["text"]; ?>
                            </div>
                        </div>
                    <? endif; ?>
                    <? if ($arResult["formLabelParts"]["WISHES"]["req"]): ?>
                        <div class="tooltip js-error-tooltip">
                            Поле <?= $arResult["formLabelParts"]["WISHES"]["name"]; ?> не может быть пустым
                        </div>
                    <? endif; ?>
                </div>
            </div>
        </div>
        <div class="lk__form-section">
            <div class="lk__form-title">
                Прикрепите необходимые документы
            </div>
            <? /*<div class="lk__form-row js-field">
                                <div class="lk__form-field <?=$arResult["formLabelParts"]["AKT"]["req"];?>">
                                    <?if(!empty($arResult['infoEdit']['akt']['name'])):?>
                                        <div class="lk__form-file selected">
                                            <div class="lk__download">
                                                <div class="lk__download-title" data-default-title="Претензия">
                                                    <?=$arResult['infoEdit']['akt']['name'];?>
                                                </div>
                                                <?if(!empty($arResult["formLabelParts"]["AKT"]["dop_filds"]["UF_SAMPLE"])):?>
                                                    <a href="<?=CFile::GetPath($arResult["formLabelParts"]["AKT"]["dop_filds"]["UF_SAMPLE"]);?>" class="lk__download-link" download>
                                                        Скачать форму
                                                    </a>
                                                <?endif;?>
                                            </div>
                                            <label class="field-file">
                                                <input class="field-file__input js-file" type="file" name="AKT" <?=$arResult["formLabelParts"]["AKT"]["req"];?> data-type="<?=$arResult["formLabelParts"]["AKT"]["file_type"];?>" value="N">
                                                <span class="field-file__title" data-default-title="Загрузить">Удалить файл</span>
                                            </label>
                                            <input type="hidden" name="akt_id" data-editfile value="<?=$arResult['infoEdit']['akt']['id'];?>" />
                                        </div>
                                    <?else:?>
                                        <div class="lk__form-file">
                                            <div class="lk__download">
                                                <div class="lk__download-title <?=$arResult["formLabelParts"]["AKT"]["req2"];?>">
                                                    Акт выхода из строя
                                                </div>
                                                <?if(!empty($arResult["formLabelParts"]["AKT"]["dop_filds"]["UF_SAMPLE"])):?>
                                                    <a href="<?=CFile::GetPath($arResult["formLabelParts"]["AKT"]["dop_filds"]["UF_SAMPLE"]);?>" class="lk__download-link" download>
                                                        Скачать форму
                                                    </a>
                                                <?endif;?>
                                            </div>
                                            <label class="field-file">
                                                <input class="field-file__input js-file" type="file" name="AKT" <?=$arResult["formLabelParts"]["AKT"]["req"];?> data-type="<?=$arResult["formLabelParts"]["AKT"]["file_type"];?>">
                                                <span class="field-file__title">Загрузить</span>
                                            </label>
                                        </div>
                                    <?endif;?>
                                </div>
                                <?if($arResult["formLabelParts"]["AKT"]["text"]):?>
                                    <div class="hint">
                                        <div class="hint__message">
                                            <?=$arResult["formLabelParts"]["AKT"]["text"];?>
                                        </div>
                                    </div>
                                <?endif;?>
                                <?if($arResult["formLabelParts"]["AKT"]["req"]):?>
                                    <div class="tooltip js-error-tooltip">
                                        Поле <?=$arResult["formLabelParts"]["AKT"]["name"];?> не может быть пустым
                                    </div>
                                <?endif;?>
                            </div>*/ ?>
            <? if (!empty($arResult['infoEdit']['akt'])): ?>
                <? foreach ($arResult['infoEdit']['akt'] as $photo): ?>
                    <div class="lk__form-row js-field">
                        <div class="lk__form-field <?= $arResult["formLabelParts"]["AKT"]["req"]; ?>">
                            <div class="lk__form-file selected">
                                <div class="lk__download">
                                    <div class="lk__download-title <?= $arResult["formLabelParts"]["AKT"]["req2"]; ?>"
                                         data-default-title="Рекламация">
                                        <?= $photo['name']; ?>
                                    </div>
                                    <? if (!empty($arResult["formLabelParts"]["AKT"]["dop_filds"]["UF_SAMPLE"])): ?>
                                        <a href="<?= CFile::GetPath($arResult["formLabelParts"]["AKT"]["dop_filds"]["UF_SAMPLE"]); ?>"
                                           class="lk__download-link" download>
                                            Скачать форму
                                        </a>
                                    <? endif; ?>
                                </div>
                                <label class="field-file">
                                    <input class="field-file__input js-file" type="file" name="AKT[]"
                                           <?= $arResult["formLabelParts"]["AKT"]["req"]; ?>
                                           data-type="<?= $arResult["formLabelParts"]["AKT"]["file_type"]; ?>">
                                    <span class="field-file__title" data-default-title="Загрузить">Удалить файл</span>
                                </label>
                                <input type="hidden" name="akt_id[]" data-editfile value="<?= $photo['id']; ?>"/>
                            </div>

                        </div>
                        <? if ($arResult["formLabelParts"]["AKT"]["text"]): ?>
                            <div class="hint">
                                <div class="hint__message">
                                    <?= $arResult["formLabelParts"]["AKT"]["text"]; ?>
                                </div>
                            </div>
                        <? endif; ?>
                        <? if ($arResult["formLabelParts"]["AKT"]["req"]): ?>
                            <div class="tooltip js-error-tooltip">
                                Поле <?= $arResult["formLabelParts"]["AKT"]["name"]; ?> не может быть пустым
                            </div>
                        <? endif; ?>
                    </div>
                <? endforeach; ?>
            <? else: ?>
                <div class="lk__form-row js-field">
                    <div class="lk__form-field <?= $arResult["formLabelParts"]["AKT"]["req"]; ?>">
                        <div class="lk__form-file">
                            <div class="lk__download">
                                <div class="lk__download-title <?= $arResult["formLabelParts"]["AKT"]["req2"]; ?>">
                                    Рекламация
                                </div>
                                <? if (!empty($arResult["formLabelParts"]["AKT"]["dop_filds"]["UF_SAMPLE"])): ?>
                                    <a href="<?= CFile::GetPath($arResult["formLabelParts"]["AKT"]["dop_filds"]["UF_SAMPLE"]); ?>"
                                       class="lk__download-link" download>
                                        Скачать форму
                                    </a>
                                <? endif; ?>
                            </div>
                            <label class="field-file">
                                <input class="field-file__input js-file" type="file" name="AKT[]"
                                       <?= $arResult["formLabelParts"]["AKT"]["req"]; ?>
                                       data-type="<?= $arResult["formLabelParts"]["AKT"]["file_type"]; ?>">
                                <span class="field-file__title">Загрузить</span>
                            </label>
                        </div>
                    </div>
                    <? if ($arResult["formLabelParts"]["AKT"]["text"]): ?>
                        <div class="hint">
                            <div class="hint__message">
                                <?= $arResult["formLabelParts"]["AKT"]["text"]; ?>
                            </div>
                        </div>
                    <? endif; ?>
                    <? if ($arResult["formLabelParts"]["AKT"]["req"]): ?>
                        <div class="tooltip js-error-tooltip">
                            Поле <?= $arResult["formLabelParts"]["AKT"]["name"]; ?> не может быть пустым
                        </div>
                    <? endif; ?>
                </div>
            <? endif; ?>
            <div class="lk__form-row">
                <a class="lk__form-add js-lk-row-add" href="javascript:void(0)">Еще</a>
            </div>
            <? if (!empty($arResult['infoEdit']['photo'])): ?>
                <? foreach ($arResult['infoEdit']['photo'] as $photo): ?>
                    <div class="lk__form-row js-field">
                        <div class="lk__form-field <?= $arResult["formLabelParts"]["PHOTO"]["req"]; ?>">
                            <div class="lk__form-file selected">
                                <div class="lk__download">
                                    <div class="lk__download-title <?= $arResult["formLabelParts"]["PHOTO"]["req2"]; ?>"
                                         data-default-title="Фото">
                                        <?= $photo['name']; ?>
                                    </div>
                                    <? if (!empty($arResult["formLabelParts"]["PHOTO"]["dop_filds"]["UF_SAMPLE"])): ?>
                                        <a href="<?= CFile::GetPath($arResult["formLabelParts"]["PHOTO"]["dop_filds"]["UF_SAMPLE"]); ?>"
                                           class="lk__download-link" download>
                                            Скачать форму
                                        </a>
                                    <? endif; ?>
                                </div>
                                <label class="field-file">
                                    <input class="field-file__input js-file" type="file" name="PHOTO[]"
                                           <?= $arResult["formLabelParts"]["PHOTO"]["req"]; ?>
                                           data-type="<?= $arResult["formLabelParts"]["PHOTO"]["file_type"]; ?>">
                                    <span class="field-file__title" data-default-title="Загрузить">Удалить файл</span>
                                </label>
                                <input type="hidden" name="photo_id[]" data-editfile value="<?= $photo['id']; ?>"/>
                            </div>

                        </div>
                        <? if ($arResult["formLabelParts"]["PHOTO"]["text"]): ?>
                            <div class="hint">
                                <div class="hint__message">
                                    <?= $arResult["formLabelParts"]["PHOTO"]["text"]; ?>
                                </div>
                            </div>
                        <? endif; ?>
                        <? if ($arResult["formLabelParts"]["PHOTO"]["req"]): ?>
                            <div class="tooltip js-error-tooltip">
                                Поле <?= $arResult["formLabelParts"]["PHOTO"]["name"]; ?> не может быть пустым
                            </div>
                        <? endif; ?>
                    </div>
                <? endforeach; ?>
            <? else: ?>
                <div class="lk__form-row js-field">
                    <div class="lk__form-field <?= $arResult["formLabelParts"]["PHOTO"]["req"]; ?>">
                        <div class="lk__form-file">
                            <div class="lk__download">
                                <div class="lk__download-title <?= $arResult["formLabelParts"]["PHOTO"]["req2"]; ?>">
                                    Фото
                                </div>
                                <? if (!empty($arResult["formLabelParts"]["PHOTO"]["dop_filds"]["UF_SAMPLE"])): ?>
                                    <a href="<?= CFile::GetPath($arResult["formLabelParts"]["PHOTO"]["dop_filds"]["UF_SAMPLE"]); ?>"
                                       class="lk__download-link" download>
                                        Скачать форму
                                    </a>
                                <? endif; ?>
                            </div>
                            <label class="field-file">
                                <input class="field-file__input js-file" type="file" name="PHOTO[]"
                                       <?= $arResult["formLabelParts"]["PHOTO"]["req"]; ?>
                                       data-type="<?= $arResult["formLabelParts"]["PHOTO"]["file_type"]; ?>">
                                <span class="field-file__title">Загрузить</span>
                            </label>
                        </div>
                    </div>
                    <? if ($arResult["formLabelParts"]["PHOTO"]["text"]): ?>
                        <div class="hint">
                            <div class="hint__message">
                                <?= $arResult["formLabelParts"]["PHOTO"]["text"]; ?>
                            </div>
                        </div>
                    <? endif; ?>
                    <? if ($arResult["formLabelParts"]["PHOTO"]["req"]): ?>
                        <div class="tooltip js-error-tooltip">
                            Поле <?= $arResult["formLabelParts"]["PHOTO"]["name"]; ?> не может быть пустым
                        </div>
                    <? endif; ?>
                </div>
            <? endif; ?>
            <div class="lk__form-row">
                <a class="lk__form-add js-lk-row-add" href="javascript:void(0)">Еще</a>
            </div>
            <br/>
            <div class="lk__form-row js-field">
                <div class="lk__form-field <?= $arResult["formLabelParts"]["PHONE_PERSONAL"]["req"]; ?>">
                    <label class="field <?= $arResult['infoEdit']['PHONE_PERSONAL'] ? 'focused' : ''; ?>">
                        <span class="field__title <?= $arResult["formLabelParts"]["PHONE_PERSONAL"]["req2"]; ?>"><?= $arResult["formLabelParts"]["PHONE_PERSONAL"]["name"]; ?></span>
                        <input class="field__input js-mask-phone" type="text" name="PHONE_PERSONAL"
                               <?= $arResult["formLabelParts"]["PHONE_PERSONAL"]["req"]; ?>
                               value="<?=!empty($arResult['infoEdit']["PHONE_PERSONAL"])?$arResult['infoEdit']["PHONE_PERSONAL"]:'';?>">
                    </label>
                </div>
                <? if ($arResult["formLabelParts"]["PHONE_PERSONAL"]["text"]): ?>
                    <div class="hint">
                        <div class="hint__message">
                            <?= $arResult["formLabelParts"]["PHONE_PERSONAL"]["text"]; ?>
                        </div>
                    </div>
                <? endif; ?>
                <? if ($arResult["formLabelParts"]["PHONE_PERSONAL"]["req"]): ?>
                    <div class="tooltip js-error-tooltip">
                        Поле <?= $arResult["formLabelParts"]["PHONE_PERSONAL"]["name"]; ?> не может
                        быть пустым
                    </div>
                <? endif; ?>
            </div>
            <div class="lk__form-req-text">
                <b>*</b> – поля, обязательные для заполнения
            </div>
            <? if (!empty($arResult['infoEdit'])): ?>
                <input type="hidden" name="edit" value="<?= $arResult['infoEdit']['id']; ?>">
            <? endif; ?>
            <button class="btn lk__form-submit" type="submit" data-click="upHref"
                    data-url="/dealer/reclamation/#spares">Отправить
            </button>
            <span class="mess"></span>
        </div>
    </form>
</div>