<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}

if (!$arResult) {
    return null;
}
?>
<div class="lk__tabs-content-inner js-tabs-content-inner active">
    <label class="field field-search">
        <input class="field__input" type="text" name="search_buyer" data-search="/dealer/?search_buyers="
               data-has="buyers" placeholder="Поиск по покупателю" value="<?= $_REQUEST["search_buyers"]; ?>">
    </label>
    <div class="lk__table buyers lk__table guarantee">
        <div class="lk__table-head">
            <div class="lk__table-row">
                <div class="lk__table-col">
                    Название
                </div>
                <div class="lk__table-col">
                    Контактное лицо
                </div>
                <div class="lk__table-col">
                    Телефон
                </div>
                <div class="lk__table-col">
                    ИНН
                </div>
                <div class="lk__table-col">
                </div>
            </div>
        </div>
        <div class="lk__table-body" data-blockajax="buyers">
            <!--startAjaxPagebuyers-->
            <? foreach ($arResult["buyers"] as $id => $item): ?>
                <div class="lk__table-row-wrap js-table-detail">
                    <div class="lk__table-row">
                        <div class="lk__table-col">
                            <?= $item["name"]; ?>
                        </div>
                        <div class="lk__table-col">
                            <?= $item["contact_people"]; ?>
                        </div>
                        <div class="lk__table-col">
                            <a href="tel:<?= $item["phone"]; ?>"><?= $item["phone"]; ?></a>
                        </div>
                        <div class="lk__table-col">
                            <?= $item["inn"]; ?>
                        </div>
                        <div class="lk__table-col">
                            <a class="lk__link js-table-detail-link" href="javascript:void(0)"
                               data-show-text="<?= ($arParams["BUYER_ID"] == $item["id"] ? 'Подробнее' : 'Свернуть'); ?>">
                                <?= ($arParams["BUYER_ID"] == $item["id"] ? 'Свернуть' : 'Подробнее'); ?>
                            </a>
                        </div>
                    </div>
                    <div class="lk__table-row-data js-table-detail-data"
                         <?= ($arParams["BUYER_ID"] == $item["id"] ? 'style="display:block;"' : ''); ?>>
                        <div class="lk__data">
                            <div class="lk__data-row">
                                <div class="lk__data-label">
                                    Подробная информация
                                </div>
                                <div class="lk__data-value">
                                    <ul class="list-dotted">
                                        <li class="list-dotted__row">
                                            <span class="list-dotted__name">Организация:</span>
                                            <span class="list-dotted__line"></span>
                                            <span class="list-dotted__value"><?= $item["name"]; ?></span>
                                        </li>
                                        <li class="list-dotted__row">
                                            <span class="list-dotted__name">Контактное лицо:</span>
                                            <span class="list-dotted__line"></span>
                                            <span class="list-dotted__value"><?= $item["contact_people"]; ?></span>
                                        </li>
                                        <li class="list-dotted__row">
                                            <span class="list-dotted__name">Должность:</span>
                                            <span class="list-dotted__line"></span>
                                            <span class="list-dotted__value"><?= $item["position"]; ?></span>
                                        </li>
                                        <li class="list-dotted__row">
                                            <span class="list-dotted__name">Телефон:</span>
                                            <span class="list-dotted__line"></span>
                                            <span class="list-dotted__value"><?= $item["phone"]; ?></span>
                                        </li>
                                        <? if (!empty($item["country"])): ?>
                                            <li class="list-dotted__row">
                                                <span class="list-dotted__name">Страна:</span>
                                                <span class="list-dotted__line"></span>
                                                <span class="list-dotted__value"><?= $item["country"]["NAME"]; ?></span>
                                            </li>
                                        <? endif; ?>
                                        <? if (!empty($item["bin"])): ?>
                                            <li class="list-dotted__row">
                                                <span class="list-dotted__name">БИН:</span>
                                                <span class="list-dotted__line"></span>
                                                <span class="list-dotted__value"><?= $item["bin"]; ?></span>
                                            </li>
                                        <? endif; ?>
                                        <? if (!empty($item["inn"])): ?>
                                            <li class="list-dotted__row">
                                                <span class="list-dotted__name">ИНН:</span>
                                                <span class="list-dotted__line"></span>
                                                <span class="list-dotted__value"><?= $item["inn"]; ?></span>
                                            </li>
                                        <? endif; ?>
                                        <li class="list-dotted__row">
                                            <span class="list-dotted__name">Адрес:</span>
                                            <span class="list-dotted__line"></span>
                                            <span class="list-dotted__value"><?= $item["region"]; ?>, <?= $item["district"]; ?>, <?= $item["address"]; ?></span>
                                        </li>
                                        <li class="list-dotted__row">
                                            <span class="list-dotted__name">Электронная почта:</span>
                                            <span class="list-dotted__line"></span>
                                            <span class="list-dotted__value"><?= $item["mail"]; ?></span>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <? if (!empty($item["arTech"])): ?>
                                <div class="lk__data-row">
                                    <div class="lk__data-label">
                                        Техника, закрепленная за покупателем
                                    </div>
                                    <div class="lk__data-value">
                                        <? foreach ($item["arTech"] as $tech): ?>
                                            <p><?= $tech["type"]; ?> <?= $tech["model"]; ?></p>
                                        <? endforeach; ?>
                                        <a class="lk__form-add" href="/dealer/exploitation/">Добавить технику</a>
                                    </div>
                                </div>
                            <? endif; ?>
                        </div>
                        <a class="lk__link lk__link-in-data js-table-detail-link" href="javascript:void(0)">
                            Свернуть
                        </a>
                    </div>
                </div>
            <? endforeach; ?>
            <!--endAjaxPagebuyers-->
        </div>
    </div>
    <? $APPLICATION->IncludeComponent("letsrock:paginations",
        ".default",
        [
            "CACHE_TIME" => 0,
            "ELEMS_PAGE" => $arResult["pagination"],
            "GET_PAGE" => "page_buyer",
            "AJAX_BLOCK" => "buyers",
            "PAGEN_HASH" => "buyers"
        ]
    ); ?>
    <a class="lk__form-add lk__form-add_colored js-tabs-show-form" href="javascript:void(0)">Добавить покупателя</a>
</div>
<div class="lk__tabs-content-form js-tabs-form">
    <a class="btn-arrow js-tabs-back" href="javascript:void(0)">
        <i class="icon-back"></i>
        <span>Назад</span>
    </a>
    <form action="/ajax/dealer/addBuyer" method="POST" class="js-form-marketing">
        <div class="lk__form-section">
            <div class="lk__form-block lk__form-edit">
                <div class="lk__form-title">
                    Добавьте нового покупателя
                </div>
                <div class="lk__form-row js-field">
                    <div class="lk__form-field required">
                        <label class="field"><span class="field__title field__required">Название</span>
                            <input class="field__input" type="text" name="NAME" required>
                        </label>
                    </div>
                    <div class="hint">
                        <div class="hint__message">
                            Название организации
                        </div>
                    </div>
                    <div class="tooltip">
                        Название не должно быть пустым
                    </div>
                </div>
                <div class="lk__form-row js-field">
                    <div class="lk__form-field <?= $arResult["arText"]["COUNTRY"]["req"]; ?>">
                        <select class="select js-select js-tabs-select js-select-linked"
                                data-path="/ajax/region/get/"
                                data-linked=".js-select-region"
                                name="COUNTRY" <?= $arResult["arText"]["COUNTRY"]["req"]; ?>>
                            <option disabled selected hidden>Выбор страны</option>
                            <? foreach ($arResult['arCountry'] as $country): ?>
                                <option value="<?= $country['ID'] ?>"><?= $country['NAME'] ?></option>
                            <? endforeach; ?>
                        </select>
                    </div>
                    <? if ($arResult["arText"]["COUNTRY"]["text"]): ?>
                        <div class="hint">
                            <div class="hint__message">
                                <?= $arResult["arText"]["COUNTRY"]["text"]; ?>
                            </div>
                        </div>
                    <? endif; ?>
                    <div class="tooltip">
                        Страна не выбрана
                    </div>
                </div>
                <div class="lk__form-row js-field">
                    <div class="lk__form-field <?= $arResult["arText"]["REGION"]["req"]; ?>">
                        <select class="select js-select-search js-select-linked js-select-region"
                                name="REGION"
                                data-path="/ajax/area/get/"
                                data-linked=".js-select-area" <?= $arResult["arText"]["REGION"]["req"]; ?>>
                            <option disabled selected hidden>Выбор региона</option>
                            <? foreach ($arResult['arRegions'] as $region): ?>
                                <option value="<?= $region['ID'] ?>"><?= $region['NAME'] ?></option>
                            <? endforeach; ?>
                        </select>
                    </div>
                    <? if ($arResult["arText"]["REGION"]["text"]): ?>
                        <div class="hint">
                            <div class="hint__message">
                                <?= $arResult["arText"]["REGION"]["text"]; ?>
                            </div>
                        </div>
                    <? endif; ?>
                    <div class="tooltip">
                        Регион не выбран
                    </div>
                </div>
                <div class="lk__form-row js-field">
                    <div class="lk__form-field <?= $arResult["arText"]["DISTRICT"]["req"]; ?>">
                        <select class="select js-select-search js-select-area" name="DISTRICT"
                                <?= $arResult["arText"]["DISTRICT"]["req"]; ?>>
                            <option disabled selected hidden>Выбор района</option>
                        </select>
                    </div>
                    <? if ($arResult["arText"]["DISTRICT"]["text"]): ?>
                        <div class="hint">
                            <div class="hint__message">
                                <?= $arResult["arText"]["DISTRICT"]["text"]; ?>
                            </div>
                        </div>
                    <? endif; ?>
                    <div class="tooltip">
                        Район не выбран
                    </div>
                </div>
                <div class="lk__form-row js-field">
                    <div class="lk__form-field <?= $arResult["arText"]["ADDRESS"]["req"]; ?>">
                        <label class="field"><span class="field__title <?= $arResult["arText"]["ADDRESS"]["req2"]; ?>">Адрес</span>
                            <input class="field__input" type="text" name="ADDRESS"
                                   <?= $arResult["arText"]["ADDRESS"]["req"]; ?>>
                        </label>
                    </div>
                    <? if ($arResult["arText"]["ADDRESS"]["text"]): ?>
                        <div class="hint">
                            <div class="hint__message">
                                <?= $arResult["arText"]["ADDRESS"]["text"]; ?>
                            </div>
                        </div>
                    <? endif; ?>
                    <div class="tooltip">
                        Адрес не должн быть пустым
                    </div>
                </div>
                <br/>
                <div class="lk__form-fieldset-list js-tabs-select-content">
                    <fieldset class="lk__form-fieldset" data-form-fieldset="<?=$arResult["block_document"]["RUS"];?>">
                        <div class="lk__form-row js-field">
                            <div class="lk__form-field <?= $arResult["arText"]["INN"]["req"]; ?>">
                                <label class="field"><span
                                            class="field__title <?= $arResult["arText"]["INN"]["req2"]; ?>">ИНН</span>
                                    <input class="field__input" type="text" name="INN"
                                           <?= $arResult["arText"]["INN"]["req"]; ?>>
                                </label>
                            </div>
                            <? if ($arResult["arText"]["INN"]["text"]): ?>
                                <div class="hint">
                                    <div class="hint__message">
                                        <?= $arResult["arText"]["INN"]["text"]; ?>
                                    </div>
                                </div>
                            <? endif; ?>
                            <div class="tooltip">
                                ИНН не должно быть пустым
                            </div>
                        </div>
                    </fieldset>
                    <fieldset disabled
                              class="lk__form-fieldset" data-form-fieldset="<?=$arResult["block_document"]["KAZ"];?>">
                        <div class="lk__form-row js-field">
                            <div class="lk__form-field <?= $arResult["arText"]["BIN"]["req"]; ?>">
                                <label class="field"><span
                                            class="field__title <?= $arResult["arText"]["BIN"]["req2"]; ?>">БИН</span>
                                    <input class="field__input" type="text" name="BIN"
                                           <?= $arResult["arText"]["BIN"]["req"]; ?>>
                                </label>
                            </div>
                            <? if ($arResult["arText"]["BIN"]["text"]): ?>
                                <div class="hint">
                                    <div class="hint__message">
                                        <?= $arResult["arText"]["BIN"]["text"]; ?>
                                    </div>
                                </div>
                            <? endif; ?>
                            <div class="tooltip">
                                БИН не должно быть пустым
                            </div>
                        </div>
                    </fieldset>
                </div>
                <br/>
                <div class="lk__form-row js-field">
                    <div class="lk__form-field <?= $arResult["arText"]["PHONE"]["req"]; ?>">
                        <label class="field"><span class="field__title <?= $arResult["arText"]["PHONE"]["req2"]; ?>">Телефон</span>
                            <input class="field__input js-mask-phone" type="text" name="PHONE"
                                   <?= $arResult["arText"]["PHONE"]["req"]; ?>>
                        </label>
                    </div>
                    <? if ($arResult["arText"]["PHONE"]["text"]): ?>
                        <div class="hint">
                            <div class="hint__message">
                                <?= $arResult["arText"]["PHONE"]["text"]; ?>
                            </div>
                        </div>
                    <? endif; ?>
                    <div class="tooltip">
                        Телефон не должн быть пустым
                    </div>
                </div>
                <div class="lk__form-row js-field">
                    <div class="lk__form-field <?= $arResult["arText"]["EMAIL"]["req"]; ?>">
                        <label class="field"><span class="field__title <?= $arResult["arText"]["EMAIL"]["req2"]; ?>">Электронная почта</span>
                            <input class="field__input" type="text" name="EMAIL"
                                   <?= $arResult["arText"]["EMAIL"]["req"]; ?>>
                        </label>
                    </div>
                    <? if ($arResult["arText"]["EMAIL"]["text"]): ?>
                        <div class="hint">
                            <div class="hint__message">
                                <?= $arResult["arText"]["EMAIL"]["text"]; ?>
                            </div>
                        </div>
                    <? endif; ?>
                    <div class="tooltip">
                        Электронная почта не должна быть пустой
                    </div>
                </div>
                <div class="lk__form-row js-field">
                    <div class="lk__form-field <?= $arResult["arText"]["CONTACT_PEOPLE"]["req"]; ?>">
                        <label class="field"><span
                                    class="field__title <?= $arResult["arText"]["CONTACT_PEOPLE"]["req2"]; ?>">Контактное лицо</span>
                            <input class="field__input" type="text" name="CONTACT_PEOPLE"
                                   <?= $arResult["arText"]["CONTACT_PEOPLE"]["req"]; ?>>
                        </label>
                    </div>
                    <? if ($arResult["arText"]["CONTACT_PEOPLE"]["text"]): ?>
                        <div class="hint">
                            <div class="hint__message">
                                <?= $arResult["arText"]["CONTACT_PEOPLE"]["text"]; ?>
                            </div>
                        </div>
                    <? endif; ?>
                    <div class="tooltip">
                        Поле не должно быть пустым
                    </div>
                </div>
                <div class="lk__form-row js-field">
                    <div class="lk__form-field <?= $arResult["arText"]["POSITION"]["req"]; ?>">
                        <label class="field"><span class="field__title <?= $arResult["arText"]["POSITION"]["req2"]; ?>">Должность</span>
                            <input class="field__input" type="text" name="POSITION"
                                   <?= $arResult["arText"]["POSITION"]["req"]; ?>>
                        </label>
                    </div>
                    <? if ($arResult["arText"]["POSITION"]["text"]): ?>
                        <div class="hint">
                            <div class="hint__message">
                                <?= $arResult["arText"]["POSITION"]["text"]; ?>
                            </div>
                        </div>
                    <? endif; ?>
                    <div class="tooltip">
                        Поле не должно быть пустым
                    </div>
                </div>
            </div>

            <div class="lk__form-req-text">
                <b>*</b> – поля, обязательные для заполнения
            </div>
            <button class="btn lk__form-submit" type="submit" data-click="upHref" data-url="/dealer/#buyers">Создать
                покупателя
            </button>
        </div>
    </form>
</div>