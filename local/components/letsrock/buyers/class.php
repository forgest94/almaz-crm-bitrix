<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}

use Letsrock\Lib\Models\User;
use Letsrock\Lib\Models\Element;
use Letsrock\Lib\Models\HLBlock;

\Bitrix\Main\Loader::includeModule('letsrock.lib');

class BuyersComponent extends CBitrixComponent
{
    public function onPrepareComponentParams($arParams)
    {
        $result = $arParams;

        return $result;
    }

    /**
     * @return array|mixed
     * @throws \Bitrix\Main\LoaderException
     */
    public function executeComponent()
    {
        global $USER;
        if ($this->startResultCache($this->arParams["CACHE_TIME"]) && $USER->GetID()) {
            $elemIbl = new Element;
            $typeTechHl = new HLBlock(HL_EQUIPMENT);
            if(!empty($_REQUEST["search_buyers"])){
                $searchVal = $_REQUEST["search_buyers"];
                $arBuyers_db = $elemIbl->GetList(IB_BUYERS, ["NAME", "ID", "IBLOCK_ID", "PROPERTY_*"], [
                    "PROPERTY_DILER" => $USER->GetID(),
                    "?NAME" => $searchVal
                ], ["ID"=>"desc"]);
            }else{
                $arBuyers_db = $elemIbl->GetList(IB_BUYERS, ["NAME", "ID", "IBLOCK_ID", "PROPERTY_*"],
                    ["PROPERTY_DILER" => $USER->GetID()],
                    ["ID"=>"desc"]);
            }
            $arBuyers = [];
            $arIdsBuyers = [];
            $newArBuyer = [];
            foreach ($arBuyers_db["ELEMS"] as $item) {
                $arProps = $arBuyers_db["PROPS"][$item["ID"]];
                $arIdsBuyers[] = $item["ID"];
                $infoRegion = $elemIbl->getById(IB_REGION, $arProps["REGION"]["VALUE"]);
                $infoDistrict = $elemIbl->getById(IB_AREA, $arProps["DISTRICT"]["VALUE"]);
                $countryUser = [];
                if (!empty($arProps["COUNTRY"]["VALUE"])) {
                    $infoCountry = CIBlockElement::GetByID($arProps["COUNTRY"]["VALUE"])->GetNext();
                    $countryUser = $infoCountry;
                }
                $arBuyers[$item["ID"]] = [
                    "name"=>$item["NAME"],
                    "id"=>$item["ID"],
                    "contact_people"=>$arProps["CONTACT_PEOPLE"]["VALUE"],
                    "phone"=>$arProps["PHONE"]["VALUE"],
                    "inn"=>(!empty($arProps["INN"]["VALUE"]) && $arProps["INN"]["VALUE"] != 0)?$arProps["INN"]["VALUE"]:'',
                    "bin"=>(!empty($arProps["BIN"]["VALUE"]) && $arProps["BIN"]["VALUE"] != 0)?$arProps["BIN"]["VALUE"]:'',
                    "position"=>$arProps["POSITION"]["VALUE"],
                    "region"=>$infoRegion["NAME"],
                    "country" => $countryUser,
                    "district"=>$infoDistrict["NAME"],
                    "address"=>$arProps["ADDRESS"]["VALUE"],
                    "mail"=>$arProps["EMAIL"]["VALUE"],
                    "arProps"=>$arProps
                ];
            }
            if(!empty($arBuyers)){
                $infoExploitation = $elemIbl->GetList(IB_EXPLOITATION, ["NAME", "ID", "IBLOCK_ID", "PROPERTY_*"], ["PROPERTY_BUYER" => $arIdsBuyers]);
                foreach ($infoExploitation["ELEMS"] as $exploit) {
                    $arPropsExploitation = $infoExploitation["PROPS"][$exploit["ID"]];
                    $infoModel = $elemIbl->getById(IB_DIRECTORY, $arPropsExploitation["MODEL"]["VALUE"]);
                    $infoType = $typeTechHl->get(["UF_XML_ID" => $arPropsExploitation["EQUIPMENT"]["VALUE"]])[0];
                    $arBuyers[$arPropsExploitation["BUYER"]["VALUE"]]["arTech"][] = ["type"=>$infoType["UF_NAME"], "model"=>$infoModel["NAME"], "number"=>$arPropsExploitation["NUMBER"]["VALUE"]];
                }
                foreach ($arBuyers as $item){
                    $newArBuyer[] = $item;
                }
            }
            $properties = CIBlockProperty::GetList(Array("sort" => "asc", "name" => "asc"), Array("ACTIVE" => "Y", "IBLOCK_ID" => IB_BUYERS));
            $arText= [];
            while ($prop_fields = $properties->GetNext()) {
                if ($prop_fields["CODE"] == "STATUS")
                    unset($prop_fields);
                $req_class = '';
                $req_class2 = '';
                if ($prop_fields["IS_REQUIRED"] == 'Y') {
                    $req_class = 'required';
                    $req_class2 = 'field__required';
                }
                $arText[$prop_fields["CODE"]] = ["name" => $prop_fields["NAME"], "file_type" => $prop_fields["FILE_TYPE"], "text" => $prop_fields["HINT"], "req" => $req_class, "req2" => $req_class2];
            }

            $arRegions = Element::getList(
                IB_REGION,
                ['ID', 'NAME'],
                [
                    'ACTIVE' => 'Y',
                ],
                [
                    'NAME' => 'ASC'
                ]
            )["ELEMS"];

            $arCountry = Element::getList(
                IB_COUNTRY,
                ['ID', 'NAME', 'CODE'],
                [
                    'ACTIVE' => 'Y',
                ],
                [
                    'NAME' => 'ASC'
                ]
            )["ELEMS"];
            foreach ($arCountry as $countryInf) {
                if(!empty($countryInf["CODE"]))
                $arBlockDocument[$countryInf["CODE"]]=$countryInf["ID"];
            }

            $pagen = [];
            $arPage = [];
            if(!empty($newArBuyer)){
                $arPage = array_chunk($newArBuyer, $this->arParams["PAGE_ELEMS"]);
                if(count($arPage) > 1){
                    for ($i = 1; $i <= count($arPage); $i++) {
                        $pagen[] = $i;
                    }
                }
            }


            $this->arResult = [
                "buyers"=>$arPage[$_REQUEST["page_buyer"]?$_REQUEST["page_buyer"]-1:0],
                "arText"=>$arText,
                "arRegions"=>$arRegions,
                "arCountry"=>$arCountry,
                "block_document"=>$arBlockDocument,
                "pagination"=>$pagen
            ];
            $this->includeComponentTemplate();
        }
        return $this->arResult;
    }
} ?>