<div class="lk__table-row-wrap js-table-detail <?=$item["error"]?'failed':'';?>">
    <div class="lk__table-row">
        <div class="lk__table-col">
            <div class="lk__table-col-head">
                Дата и время
            </div>
            <span>
                            <?=$item["date"];?>
                        </span>
        </div>
        <div class="lk__table-col">
            <div class="lk__table-col-head">
                Номер
            </div>
            <?=$item["number"];?>
        </div>
        <div class="lk__table-col">
            <div class="lk__table-col-head">
                Инициатор
            </div>
            <span>
                               Дилер <br>
                              <b><?=$item["dealer"]["WORK_COMPANY"];?></b>
                            </span>
        </div>
        <div class="lk__table-col">
            <div class="lk__table-col-head">
                Задача
            </div>
            <?=$item["name"];?>
        </div>
        <div class="lk__table-col">
            <div class="lk__table-col-head">
                Срок выполнения
            </div>
            <?=$item["due_date"];?>
        </div>
        <div class="lk__table-col">
            <a class="lk__link js-table-detail-link" href="javascript:void(0)" data-show-text="Свернуть">
                Подробнее
            </a>
        </div>
    </div>
    <div class="lk__table-row-data js-table-detail-data">
        <div class="lk__data">
            <div class="lk__data-row">
                <div class="lk__data-label">
                    Техника
                </div>
                <div class="lk__data-value">
                    <div class="lk__data-box">
                        <?=$item["type"]?> / <?=$item["model"]?>
                    </div>
                </div>
            </div>
            <?if(!empty($item["akt"])):?>
                <div class="lk__data-row">
                    <div class="lk__data-label">
                        Акт ввода в эксплуатацию
                    </div>
                    <div class="lk__data-value">
                        <div class="lk__data-files">
                            <a class="link-file" href="<?=$item["akt"]["src"];?>" download><?=$item["akt"]["name"];?></a>
                        </div>
                    </div>
                </div>
            <?endif;?>
            <?if(!empty($item["photo"])):?>
                <div class="lk__data-row">
                    <div class="lk__data-label">
                        Фото
                    </div>
                    <div class="lk__data-value">
                        <div class="lk__data-files">
                            <?foreach ($item["photo"] as $photo):?>
                                <a class="link-file" href="<?=$photo["src"];?>" download><?=$photo["name"];?></a>
                            <?endforeach;?>
                        </div>
                    </div>
                </div>
            <?endif;?>
            <div class="lk__data-row">
                <div class="lk__data-label">
                    Дата постановки на гарантию
                </div>
                <div class="lk__data-value">
                    <div class="lk__data-box">
                        <?=$item["date_expl"]?>
                    </div>
                </div>
            </div>
            <?if(!empty($item["phone"])):?>
                <div class="lk__data-row">
                    <div class="lk__data-label">
                        Контактный номер телефона
                    </div>
                    <div class="lk__data-value">
                        <div class="lk__data-box">
                            <?=$item["phone"]?>
                        </div>
                    </div>
                </div>
            <?endif;?>
            <div class="lk__data-row">
                <div class="lk__data-label">
                    Информация о покупателе
                </div>
                <div class="lk__data-value">
                    <ul class="list-dotted">
                        <li class="list-dotted__row">
                            <span class="list-dotted__name">Название:</span>
                            <span class="list-dotted__line"></span>
                            <span class="list-dotted__value"><?=$item["buyer"]["name"];?></span>
                        </li>
                        <li class="list-dotted__row">
                            <span class="list-dotted__name">Контактное лицо:</span>
                            <span class="list-dotted__line"></span>
                            <span class="list-dotted__value"><?=$item["buyer"]["contact_people"];?></span>
                        </li>
                        <li class="list-dotted__row">
                            <span class="list-dotted__name">Должность:</span>
                            <span class="list-dotted__line"></span>
                            <span class="list-dotted__value"><?=$item["buyer"]["arProps"]["POSITION"]["VALUE"];?></span>
                        </li>
                        <li class="list-dotted__row">
                            <span class="list-dotted__name">Телефон:</span>
                            <span class="list-dotted__line"></span>
                            <span class="list-dotted__value">
                                       <a href="tel:<?=$item["buyer"]["phone"];?>"><?=$item["buyer"]["phone"];?></a>
                                    </span>
                        </li>
                        <li class="list-dotted__row">
                            <span class="list-dotted__name">ИНН:</span>
                            <span class="list-dotted__line"></span>
                            <span class="list-dotted__value"><?=$item["buyer"]["inn"];?></span>
                        </li>
                        <li class="list-dotted__row">
                            <span class="list-dotted__name">Адрес:</span>
                            <span class="list-dotted__line"></span>
                            <span class="list-dotted__value"><?=$item["buyer"]["region"];?>, <?=$item["buyer"]["district"];?>, <?=$item["buyer"]["address"];?></span>
                        </li>
                        <?if(!empty(item["buyer"]["mail"])):?>
                            <li class="list-dotted__row">
                                <span class="list-dotted__name">Электронная почта:</span>
                                <span class="list-dotted__line"></span>
                                <span class="list-dotted__value"><a href="<?=item["buyer"]["mail"];?>"><?=item["buyer"]["mail"];?></a></span>
                            </li>
                        <?endif;?>
                    </ul>
                    <div class="btn-group">
                        <a class="btn btn-black" href="javascript:void(0);"
                           data-editTask="<?=$item["id"];?>"
                           data-dealer="<?=$item["dealer"]["ID"];?>"
                           data-status="<?=$item["status_arr"]["reject_id"];?>">Отклонить</a>
                        <a class="btn" href="javascript:void(0);"
                           data-editTask="<?=$item["id"];?>"
                           data-dealer="<?=$item["dealer"]["ID"];?>"
                           data-status="<?=$item["status_arr"]["confirm_id"];?>">Подтвердить</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>