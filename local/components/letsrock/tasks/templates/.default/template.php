<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
$pathComponent = $_SERVER["DOCUMENT_ROOT"].$this->GetFolder();
?>
<div class="lk__content-box">
<div class="lk__table tasks">
    <div class="lk__table-head">
        <div class="lk__table-row">
            <div class="lk__table-col">
                Дата подачи
            </div>
            <div class="lk__table-col">
                Номер
            </div>
            <div class="lk__table-col">
                Инициатор
            </div>
            <div class="lk__table-col">
                Задача
            </div>
            <div class="lk__table-col">
                Срок выполнения
            </div>
            <div class="lk__table-col">
            </div>
        </div>
    </div>
    <div class="lk__table-body">
        <?if(!empty($arResult["elems"])):?>
            <?
            foreach ($arResult["elems"] as $item){
                $vid = $pathComponent.'/templates/'.$item["vid"].'.php';
                include($vid);
            }
            ?>
        <?endif;?>
    </div>
</div>
    <?$APPLICATION->IncludeComponent("letsrock:paginations",
        ".default",
        [
            "CACHE_TIME" => 0,
            "ELEMS_PAGE"=>$arResult["pagination"],
            "GET_PAGE"=>"page",
            "AJAX_BLOCK"=>""
        ]
    );?>
</div>