<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}

use Letsrock\Lib\Models\User;
use Letsrock\Lib\Models\Element;
use Letsrock\Lib\Models\HLBlock;

\Bitrix\Main\Loader::includeModule('letsrock.lib');

class BuyersComponent extends CBitrixComponent
{
    public function onPrepareComponentParams($arParams)
    {
        $result = $arParams;

        return $result;
    }


    /**
     * @return array|mixed
     * @throws \Bitrix\Main\LoaderException
     */
    public function executeComponent()
    {
        function cmp($a, $b)
        {
            $a_new = strtotime($a["date"]);
            $b_new = strtotime($b["date"]);

            return $b_new - $a_new;
        }

        global $USER;
        if ($this->startResultCache($this->arParams["CACHE_TIME"]) && $USER->GetID()) {
            $arIdsDealer = [];
            $arInfo = [];
            $chechNew = '';
            $visibleTask = [];
            switch ($this->arParams["PANEL"]) {
                case 'manager':
                    $groupManager = $USER->GetUserGroupArray();
                    if (in_array(ID_MANAGER_SERVICES, $groupManager)) {
                        $visibleTask[] = IB_EXPLOITATION;
                        $visibleTask[] = IB_SERVCOMP;
                    }
                    if (in_array(ID_MANAGER_PARTS, $groupManager)) {
                        $visibleTask[] = IB_RECLAMATION_PARTS;
                    }
                    if (in_array(ID_MANAGER_TECH, $groupManager)) {
                        $visibleTask[] = IB_RECLAMATION_TECH;
                    }
                    if (in_array(ID_MANAGER_MARKETING, $groupManager)) {
                        $visibleTask[] = IB_MARKETING;
                    }
                    $usersForManger = CUser::GetList(($by = "ID"), ($order = "ASC"),
                        [
                            "UF_MANAGER_NEW" => array($USER->GetID())
                        ], array("SELECT" => array("UF_*")));
                    while ($arUser = $usersForManger->Fetch()) {
                        $arIdsDealer[] = $arUser["ID"];
                        $arInfo[$arUser["ID"]] = $arUser;
                    }
                    break;
                case 'admin':
                    $visibleTask = [IB_EXPLOITATION, IB_SERVCOMP, IB_RECLAMATION_PARTS, IB_RECLAMATION_TECH, IB_MARKETING];
                    $usersForManger = CUser::GetList(($by = "ID"), ($order = "ASC"),
                        [
                            "UF_ADMIN_NEW" => $USER->GetID()
                        ], array("SELECT" => array("UF_*")));
                    while ($arUser = $usersForManger->Fetch()) {
                        if ($arUser["UF_ADMIN_NEW"]) {
                            $usersForManger2 = CUser::GetList(($by = "ID"), ($order = "ASC"), ["UF_MANAGER_NEW" => $arUser["ID"]], array("SELECT" => array("UF_*")));
                            while ($arUser2 = $usersForManger2->Fetch()) {
                                $arIdsDealer[] = $arUser2["ID"];
                                $arInfo[$arUser2["ID"]] = $arUser2;
                            }
                            $arManagerIds[] = $arUser["ID"];
                        }
                        $arInfo[$arUser["ID"]] = $arUser;
                    }
                    break;
            }

            if (empty($arInfo) || empty($arIdsDealer)) {
                $this->arResult = false;
                $this->includeComponentTemplate();
                exit();
            }
            $arTasks = [];
            $status = "";
            $elemIbl = new Element;


            /**
             * Ввод в эксплуатацию
             */
            if (in_array(IB_EXPLOITATION, $visibleTask)) {
                $arExplotations_db = $elemIbl->GetList(IB_EXPLOITATION,
                    ["NAME", "ID", "IBLOCK_ID", "DATE_CREATE", "PROPERTY_*"],
                    [
                        "PROPERTY_STATUS_VALUE" => "На рассмотрении",
                        "!PROPERTY_DEALER" => false,
                        "PROPERTY_DEALER" => $arIdsDealer,
                        "!NAME" => "Техника не введена в эксплуатацию"
                    ],
                    ["created_date" => "desc"]
                );
                $typeTechHl = new HLBlock(HL_EQUIPMENT);
                foreach ($arExplotations_db["ELEMS"] as $item) {
                    $arProps = $arExplotations_db["PROPS"][$item["ID"]];
                    $status = $arProps["STATUS"]["VALUE"];
                    $infoBuyer = $elemIbl->GetList(IB_BUYERS, ["NAME", "ID", "IBLOCK_ID", "PROPERTY_*"], ["ID" => $arProps["BUYER"]["VALUE"], "!PROPERTY_TYPE_VALUE" => false]);
                    $fildsBuyer = $infoBuyer["ELEMS"][0];
                    $porpsBuyer = $infoBuyer["PROPS"][$fildsBuyer["ID"]];
                    $infoModel = $elemIbl->getById(IB_DIRECTORY, $arProps["MODEL"]["VALUE"]);
                    $infoType = $typeTechHl->get(["UF_XML_ID" => $arProps["EQUIPMENT"]["VALUE"]])[0];
                    $infoRegionBuyer = $elemIbl->getById(IB_REGION, $porpsBuyer["REGION"]["VALUE"]);
                    $infoDistrictBuyer = $elemIbl->getById(IB_AREA, $porpsBuyer["DISTRICT"]["VALUE"]);
                    $akt_parse = [];
                    if (!empty($arProps["FILE"]["VALUE"])) {
                        $akt = CFile::GetFileArray($arProps["FILE"]["VALUE"]);
                        if ($akt)
                            $akt_parse = ["name" => $akt["ORIGINAL_NAME"], "src" => $akt["SRC"]];
                    }
                    $arPhoto = [];
                    if (!empty($arProps["PHOTO_TECH"]["VALUE"])) {
                        foreach ($arProps["PHOTO_TECH"]["VALUE"] as $itemP) {
                            $photo = CFile::GetFileArray($itemP);
                            if ($photo)
                                $arPhoto[] = ["name" => $photo["ORIGINAL_NAME"], "src" => $photo["SRC"]];
                        }
                    }
                    if ($status == 'На рассмотрении')
                        $chechNew = 'Y';

                    if (!empty($arProps["DATE_EXECUTION"]["VALUE"])) {
                        $due_date = $arProps["DATE_EXECUTION"]["VALUE"];
                    } else {
                        $deteProp = explode(' ', $item["DATE_CREATE"])[0];
                        $due_date = date('d.m.Y', strtotime($deteProp . DAY_EXECUTION));
                    }
                    $error = '';
                    if (strtotime(date("d.m.Y")) > strtotime($due_date)) {
                        $error = 'Y';
                    }
                    $arTasks[] = [
                        "id" => $item["ID"],
                        "vid" => "expl",
                        "error" => $error,
                        "type"=>$infoType["UF_NAME"],
                        "model"=>$infoModel["NAME"],
                        "date" => $item["DATE_CREATE"],
                        "date_expl" => $arProps["DATE"]["VALUE"],
                        "status" => $status,
                        "status_arr" => ["reject_id" => 14, "confirm_id" => 15],
                        "akt" => $akt_parse,
                        "photo" => $arPhoto,
                        "name" => $item["NAME"],
                        "phone" => $arProps["PHONE_PERSONAL"]["VALUE"],
                        "due_date" => $due_date,
                        "number" => $arProps["NUMBER"]["VALUE"],
                        "dealer" => $arInfo[$arProps["DEALER"]["VALUE"]],
                        "buyer" => [
                            "name" => $fildsBuyer["NAME"],
                            "contact_people" => $porpsBuyer["CONTACT_PEOPLE"]["VALUE"],
                            "phone" => $porpsBuyer["PHONE"]["VALUE"],
                            "inn" => $porpsBuyer["INN"]["VALUE"],
                            "position" => $porpsBuyer["POSITION"]["VALUE"],
                            "region" => $infoRegionBuyer["NAME"],
                            "district" => $infoDistrictBuyer["NAME"],
                            "address" => $porpsBuyer["ADDRESS"]["VALUE"],
                            "mail" => $porpsBuyer["EMAIL"]["VALUE"],
                            "arProps" => $porpsBuyer
                        ]
                    ];
                }
            }
            /**
             * Рекламации "Техника"
             */
            if (in_array(IB_RECLAMATION_TECH, $visibleTask)) {
                $arReclamationTech_db = $elemIbl->GetList(IB_RECLAMATION_TECH,
                    ["NAME", "ID", "IBLOCK_ID", "DATE_CREATE", "PROPERTY_*"],
                    [
                        "PROPERTY_DEALER" => $arIdsDealer,
                        [
                            "LOGIC" => "OR",
                            ["PROPERTY_STATUS_VALUE" => "Отправлено"],
                            ["PROPERTY_STATUS_VALUE" => "В работе"],
                        ]
                    ],
                    ["created_date" => "desc"]
                );
                $status = "";
                foreach ($arReclamationTech_db["ELEMS"] as $tech) {
                    $arProps = $arReclamationTech_db["PROPS"][$tech["ID"]];
                    $status = $arProps["STATUS"]["VALUE"];
                    $arBuyerInfo = [];
                    if ($arProps["SERIAL_NUMBER"]["VALUE"]) {
                        $infoExpl = $elemIbl->GetList(IB_EXPLOITATION, ["NAME", "ID", "IBLOCK_ID", "DATE_CREATE", "PROPERTY_*"], ["PROPERTY_DEALER" => $arIdsDealer, "ID" => $arProps["SERIAL_NUMBER"]], array("created_date" => "desc"));
                        $arPropsEx = $infoExpl["PROPS"][$infoExpl["ELEMS"][0]["ID"]];
                        if (!$arProps["NOT_OPERATION"]["VALUE"]) {
                            $infoBuyer = $elemIbl->GetList(IB_BUYERS, ["NAME", "ID", "IBLOCK_ID", "PROPERTY_*"], ["ID" => $arPropsEx["BUYER"]["VALUE"], "!PROPERTY_TYPE_VALUE" => false]);
                            $fildsBuyer = $infoBuyer["ELEMS"][0];
                            $porpsBuyer = $infoBuyer["PROPS"][$fildsBuyer["ID"]];
                            $infoRegionBuyer = $elemIbl->getById(IB_REGION, $porpsBuyer["REGION"]["VALUE"]);
                            $infoDistrictBuyer = $elemIbl->getById(IB_AREA, $porpsBuyer["DISTRICT"]["VALUE"]);
                            $arBuyerInfo[] = [
                                "name" => $infoExpl["ELEMS"][0]["NAME"],
                                "contact_people" => $porpsBuyer["CONTACT_PEOPLE"]["VALUE"],
                                "phone" => $porpsBuyer["PHONE"]["VALUE"],
                                "inn" => $porpsBuyer["INN"]["VALUE"],
                                "position" => $porpsBuyer["POSITION"]["VALUE"],
                                "region" => $infoRegionBuyer["NAME"],
                                "district" => $infoDistrictBuyer["NAME"],
                                "address" => $porpsBuyer["ADDRESS"]["VALUE"],
                                "mail" => $porpsBuyer["EMAIL"]["VALUE"],
                                "arProps" => $porpsBuyer
                            ];
                        }
                    } else {
                        continue;
                    }


                    /*$akt_parse = [];
                    if (!empty($arProps["AKT"]["VALUE"])) {
                        $akt = CFile::GetFileArray($arProps["AKT"]["VALUE"]);
                        if ($akt)
                            $akt_parse = ["name" => $akt["ORIGINAL_NAME"], "src" => $akt["SRC"]];
                    }*/
                    $akt_parse = [];
                    if (!empty($arProps["AKT"]["VALUE"])) {
                        foreach ($arProps["AKT"]["VALUE"] as $akt2) {
                            $arFile = CFile::GetFileArray($akt2);
                            if ($arFile)
                                $akt_parse[] = ["name" => $arFile["ORIGINAL_NAME"], "src" => $arFile["SRC"]];
                        }
                        /*$akt = CFile::GetFileArray($arProps["AKT"]["VALUE"]);
                        if($akt)
                        $akt_parse = ["name"=>$akt["ORIGINAL_NAME"], "src"=>$akt["SRC"]];*/
                    }
                    $arPritenzy = [];
                    if (!empty($arProps["PRITENZY"]["VALUE"])) {
                        foreach ($arProps["PRITENZY"]["VALUE"] as $photo) {
                            $arFile = CFile::GetFileArray($photo);
                            if ($arFile)
                                $arPritenzy[] = ["name" => $arFile["ORIGINAL_NAME"], "src" => $arFile["SRC"]];
                        }
                    }
                    $arPhoto = [];
                    if (!empty($arProps["PHOTO"]["VALUE"])) {
                        foreach ($arProps["PHOTO"]["VALUE"] as $photo) {
                            $pare_phot = CFile::GetFileArray($photo);
                            if ($pare_phot)
                                $arPhoto[] = ["name" => $pare_phot["ORIGINAL_NAME"], "src" => $pare_phot["SRC"]];
                        }
                    }
                    if ($status == 'Отправлено' || $status == 'В работе')
                        $chechNew = 'Y';

                    if (!empty($arProps["DATE_EXECUTION"]["VALUE"])) {
                        $due_date = $arProps["DATE_EXECUTION"]["VALUE"];
                    } else {
                        $deteProp = explode(' ', $tech["DATE_CREATE"])[0];
                        $due_date = date('d.m.Y', strtotime($deteProp . DAY_EXECUTION));
                    }
                    $error = '';
                    if (strtotime(date("d.m.Y")) > strtotime($due_date)) {
                        $error = 'Y';
                    }
                    $arTasks[] = [
                        "id" => $tech["ID"],
                        "vid" => "tech",
                        "error" => $error,
                        "date" => $tech["DATE_CREATE"],
                        "status" => $status,
                        "status_arr" => ["reject_id" => 20, "confirm_id" => 19],
                        "akt" => $akt_parse,
                        "pritenzy" => $arPritenzy,
                        "photo" => $arPhoto,
                        "phone" => $arProps["PHONE_PERSONAL"]["VALUE"],
                        "name" => "Рекламации - техника",
                        "reason" => $arProps["REASON"]["VALUE"]["TEXT"],
                        "wishes" => $arProps["WISHES"]["VALUE"]["TEXT"],
                        "due_date" => $due_date,
                        "number" => $tech["NAME"],
                        "dealer" => $arInfo[$arProps["DEALER"]["VALUE"]],
                        "buyer" => $arBuyerInfo
                    ];
                }
            }
            /**
             * Рекламации "Запчасти"
             */
            if (in_array(IB_RECLAMATION_PARTS, $visibleTask)) {
                $arReclamationParts_db = $elemIbl->GetList(IB_RECLAMATION_PARTS,
                    ["NAME", "ID", "IBLOCK_ID", "DATE_CREATE", "PROPERTY_*"],
                    [
                        "PROPERTY_DEALER" => $arIdsDealer,
                        [
                            "LOGIC" => "OR",
                            ["PROPERTY_STATUS_VALUE" => "Отправлено"],
                            ["PROPERTY_STATUS_VALUE" => "В работе"],
                        ],
                    ],
                    ["created_date" => "desc"]
                );
                $status = "";
                foreach ($arReclamationParts_db["ELEMS"] as $parts) {
                    $arProps = $arReclamationParts_db["PROPS"][$parts["ID"]];
                    $status = $arProps["STATUS"]["VALUE"];

                    $akt_parse = [];
                    if (!empty($arProps["AKT"]["VALUE"])) {
                        $akt = CFile::GetFileArray($arProps["AKT"]["VALUE"]);
                        if ($akt)
                            $akt_parse = ["name" => $akt["ORIGINAL_NAME"], "src" => $akt["SRC"]];
                    }
                    $arPhoto = [];
                    if (!empty($arProps["PHOTO"]["VALUE"])) {
                        foreach ($arProps["PHOTO"]["VALUE"] as $photo) {
                            $pare_phot = CFile::GetFileArray($photo);
                            if ($pare_phot)
                                $arPhoto[] = ["name" => $pare_phot["ORIGINAL_NAME"], "src" => $pare_phot["SRC"]];
                        }
                    }
                    if ($status == 'Отправлено' || $status == 'В работе')
                        $chechNew = 'Y';

                    if (!empty($arProps["DATE_EXECUTION"]["VALUE"])) {
                        $due_date = $arProps["DATE_EXECUTION"]["VALUE"];
                    } else {
                        $deteProp = explode(' ', $parts["DATE_CREATE"])[0];
                        $due_date = date('d.m.Y', strtotime($deteProp . DAY_EXECUTION));
                    }
                    $error = '';
                    if (strtotime(date("d.m.Y")) > strtotime($due_date)) {
                        $error = 'Y';
                    }
                    $arTasks[] = [
                        "id" => $parts["ID"],
                        "vid" => "parts",
                        "error" => $error,
                        "date" => $parts["DATE_CREATE"],
                        "status" => $status,
                        "status_arr" => ["reject_id" => 24, "confirm_id" => 23],
                        "akt" => $akt_parse,
                        "photo" => $arPhoto,
                        "phone" => $arProps["PHONE_PERSONAL"]["VALUE"],
                        "name" => "Рекламации - запчасти",
                        "invoice" => $arProps["INVOICE"]["VALUE"],
                        "consignment" => $arProps["CONSIGNMENT"]["VALUE"],
                        "reason" => $arProps["REASON"]["VALUE"]["TEXT"],
                        "wishes" => $arProps["WISHES"]["VALUE"]["TEXT"],
                        "due_date" => $due_date,
                        "number" => $parts["NAME"],
                        "dealer" => $arInfo[$arProps["DEALER"]["VALUE"]],
                    ];
                }
            }
            /**
             * Сервисные компенсации
             */
            if (in_array(IB_SERVCOMP, $visibleTask)) {
                $typeTechHl = new HLBlock(HL_EQUIPMENT);
                $infoServices = $elemIbl->GetList(IB_SERVCOMP,
                    ["ID", "IBLOCK_ID", "NAME", "DATE_CREATE", "CODE", "DATE_ACTIVE_FROM", "DATE_CREATE", "PROPERTY_*"],
                    [
                        "ACTIVE_DATE" => "Y",
                        "ACTIVE" => "Y",
                        "PROPERTY_DILER" => $arIdsDealer,
                        [
                            "LOGIC" => "OR",
                            ["PROPERTY_STATUS_VALUE" => "Отправлено"],
                            ["PROPERTY_STATUS_VALUE" => "В работе"],
                        ],
                    ],
                    ["ID" => "desc"]
                );
                foreach ($infoServices["ELEMS"] as $key => $service) {
                    $arProps = $infoServices["PROPS"][$service["ID"]];
                    $type = $arProps["TYPE_SERVICE"]["~VALUE"];
                    $status = $arProps["STATUS"]["~VALUE"];
                    unset($arProps["TYPE_SERVICE"]);
                    unset($arProps["STATUS"]);
                    $arFiles = [];
                    foreach ($arProps as $arProp) {
                        if ($arProp["PROPERTY_TYPE"] == 'F') {
                            if (empty($arProp["VALUE"])) continue;
                            $src = [];
                            if (is_array($arProp["VALUE"])) {
                                foreach ($arProp["VALUE"] as $id) {
                                    $file = CFile::GetFileArray($id);
                                    if (!empty($file))
                                        $src[] = $file;
                                }
                            } else {
                                $file_one = CFile::GetFileArray($arProp["VALUE"]);
                                if (!empty($file_one))
                                    $src = $file_one;
                            }
                            if (!empty($src))
                                $arFiles[] = ["info" => $src, "name" => $arProp["NAME"]];
                        }
                    }
                    $arTech = [];
                    if (!empty($arProps["SERIAL_NUMBER"]["VALUE"])) {
                        foreach ($arProps["SERIAL_NUMBER"]["VALUE"] as $serialElem) {
                            if (!empty((int)$serialElem)) {
                                $infoTech = $elemIbl->GetList(IB_EXPLOITATION, ["ID", "NAME", "PROPERTY_MODEL", "PROPERTY_EQUIPMENT", "PROPERTY_DATE", "PROPERTY_FILE", "PROPERTY_NUMBER"], ["ID" => (int)$serialElem, "!EQUIPMENT_VALUE" => false])["ELEMS"][0];
                                if ($infoTech) {
                                    $infoModel = $elemIbl->GetList(IB_DIRECTORY, ["NAME"], ["ID" => $infoTech["PROPERTY_MODEL_VALUE"], "!PROPERTY_TYPE_VALUE" => false])["ELEMS"][0];
                                    $infoType = $typeTechHl->get(["UF_XML_ID" => $infoTech["PROPERTY_EQUIPMENT_VALUE"]])[0];
                                    $numberTech = $infoTech["PROPERTY_NUMBER_VALUE"];
                                    $parse_akt = CFile::GetFileArray($infoTech["PROPERTY_FILE_VALUE"]);

                                    $arTech[$infoTech["ID"]] = [
                                        "number" => $numberTech,
                                        "model" => $infoModel["NAME"],
                                        "type" => $infoType["UF_NAME"],
                                        "akt" => ["name" => $parse_akt["ORIGINAL_NAME"], "src" => $parse_akt["SRC"]],
                                        "date" => $infoTech["PROPERTY_DATE_VALUE"],
                                    ];
                                }
                            }
                        }
                    }
                    $arReclamationTech = [];
                    if (!empty($arProps["RECLAMATION_TECH"]["VALUE"]) && $arProps["RECLAMATION_TECH"]["VALUE"][0] != 'N') {
                        foreach ($arProps["RECLAMATION_TECH"]["VALUE"] as $reclElem) {
                            if (!empty((int)$reclElem)) {
                                $infoRecl = $elemIbl->GetList(IB_RECLAMATION_TECH, ["NAME", "ID", "IBLOCK_ID", "PROPERTY_*"], ["ID" => $reclElem]);
                                $elem = $infoRecl["ELEMS"][0];
                                $propsRecl = $infoRecl["PROPS"][$elem["ID"]];
                                $infoTechRecl = [];
                                if (!empty($propsRecl["SERIAL_NUMBER"]["VALUE"])) {
                                    if (is_array($propsRecl["SERIAL_NUMBER"]["VALUE"])) {
                                        foreach ($propsRecl["SERIAL_NUMBER"]["VALUE"] as $itemIdsTech) {
                                            $infoTechRecl = $arTech[$itemIdsTech];
                                        }
                                    } else {
                                        $infoTechRecl = $arTech[$propsRecl["SERIAL_NUMBER"]["VALUE"]];
                                    }
                                }
                                $akt_parse = [];
                                if (!empty($propsRecl["AKT"]["VALUE"])) {
                                    foreach ($propsRecl["AKT"]["VALUE"] as $akt2) {
                                        $arFile = CFile::GetFileArray($akt2);
                                        if ($arFile)
                                            $akt_parse[] = ["name" => $arFile["ORIGINAL_NAME"], "src" => $arFile["SRC"]];
                                    }
                                }
                                $arPhoto = [];
                                if (!empty($propsRecl["PHOTO"]["VALUE"])) {
                                    foreach ($propsRecl["PHOTO"]["VALUE"] as $photo) {
                                        $arFile = CFile::GetFileArray($photo);
                                        if ($arFile)
                                            $arPhoto[] = ["name" => $arFile["ORIGINAL_NAME"], "src" => $arFile["SRC"]];
                                    }
                                }
                                $arReclamationTech[] = [
                                    "number" => $elem["NAME"],
                                    "id" => $elem["ID"],
                                    "infoTech" => $infoTechRecl,
                                    "reason" => $propsRecl["REASON"]["VALUE"]["TEXT"],
                                    "wishes" => $propsRecl["WISHES"]["VALUE"]["TEXT"],
                                    "akt" => $akt_parse,
                                    "photo" => $arPhoto
                                ];
                            }
                        }
                    }
                    if (!empty($arProps["RECLAMATION_TECH"]["VALUE"]) && $arProps["RECLAMATION_TECH"]["VALUE"][0] != 'N')
                        $arTech = [];

                    if ($status == 'Отправлено' || $status == 'В работе')
                        $chechNew = 'Y';

                    if (!empty($arProps["DATE_EXECUTION"]["VALUE"])) {
                        $due_date = $arProps["DATE_EXECUTION"]["VALUE"];
                    } else {
                        $deteProp = explode(' ', $service["DATE_CREATE"])[0];
                        $due_date = date('d.m.Y', strtotime($deteProp . DAY_EXECUTION));
                    }
                    $error = '';
                    if (strtotime(date("d.m.Y")) > strtotime($due_date)) {
                        $error = 'Y';
                    }
                    $arTasks[] = [
                        "id" => $service["ID"],
                        "vid" => "services",
                        "error" => $error,
                        "date" => $service["DATE_CREATE"],
                        "due_date" => $due_date,
                        "number" => $service["NAME"],
                        "phone" => $arProps["PHONE_PERSONAL"]["VALUE"],
                        "type" => $type,
                        "status" => $status,
                        "status_arr" => ["reject_id" => 26, "confirm_id" => 12],
                        "name" => "Сервисные компенсации",
                        "files" => $arFiles,
                        "dealer" => $arInfo[$arProps["DILER"]["VALUE"]],
                        "arTech" => $arTech,
                        "arReclamationTech" => $arReclamationTech
                    ];
                }
            }

            /**
             * Маркетинговые компенсации
             */
            if (in_array(IB_MARKETING, $visibleTask)) {
                $elemIbl = new Element;
                $infoMarketing = $elemIbl->GetList(IB_MARKETING,
                    ["ID", "IBLOCK_ID", "NAME", "CODE", "DATE_CREATE", "DATE_ACTIVE_FROM", "PROPERTY_*"],
                    [
                        "ACTIVE_DATE" => "Y",
                        "ACTIVE" => "Y",
                        "PROPERTY_DILER" => $arIdsDealer,
                        [
                            "LOGIC" => "OR",
                            ["PROPERTY_STATUS_VALUE" => "Отправлено"],
                            ["PROPERTY_STATUS_VALUE" => "В работе"],
                        ],
                    ],
                    ["ID" => "desc"]
                );
                foreach ($infoMarketing["ELEMS"] as $info) {
                    $arProps = $infoMarketing["PROPS"][$info["ID"]];
                    $type = $arProps["TYPE_KOMP"]["~VALUE"];
                    $status = $arProps["STATUS"]["~VALUE"];
                    unset($arProps["TYPE_KOMP"]);
                    unset($arProps["STATUS"]);
                    $arFiles = [];
                    foreach ($arProps as $arProp) {
                        if ($arProp["PROPERTY_TYPE"] == 'F') {
                            $src = [];
                            if (is_array($arProp["VALUE"])) {
                                foreach ($arProp["VALUE"] as $id) {
                                    $isfile = CFile::GetFileArray($id);
                                    if(!empty($isfile["SRC"]))
                                    $src[] = $isfile;
                                }
                            } else {
                                $isfile = CFile::GetFileArray($arProp["VALUE"]);
                                if(!empty($isfile["SRC"]))
                                $src = $isfile;
                            }
                            if (!empty($src))
                                $arFiles[] = ["info" => $src, "name" => $arProp["NAME"]];
                        }
                    }
                    if ($status == 'Отправлено' || $status == 'В работе')
                        $chechNew = 'Y';

                    if (!empty($arProps["DATE_EXECUTION"]["VALUE"])) {
                        $due_date = $arProps["DATE_EXECUTION"]["VALUE"];
                    } else {
                        $deteProp = explode(' ', $info["DATE_CREATE"])[0];
                        $due_date = date('d.m.Y', strtotime($deteProp . DAY_EXECUTION));
                    }
                    $error = '';
                    if (strtotime(date("d.m.Y")) > strtotime($due_date)) {
                        $error = 'Y';
                    }
                    $arTasks[] = [
                        "id" => $info["ID"],
                        "vid" => "mark",
                        "error" => $error,
                        "date" => $info["DATE_CREATE"],
                        "due_date" => $due_date,
                        "number" => $info["NAME"],
                        "phone" => $arProps["PHONE_PERSONAL"]["VALUE"],
                        "type" => $type,
                        "status" => $status,
                        "status_arr" => ["reject_id" => 25, "confirm_id" => 3],
                        "name" => "Маркетинговые компенсации",
                        "files" => $arFiles,
                        "dealer" => $arInfo[$arProps["DILER"]["VALUE"]],
                    ];
                }
            }

            if ($chechNew) {
                $_SESSION["newTask"] = 'Y';
            } else {
                $_SESSION["newTask"] = '';
            }

            usort($arTasks, "cmp");

            $pagen = [];
            $arPage = [];
            if (!empty($arTasks)) {
                $arPage = array_chunk($arTasks, $this->arParams["PAGE_ELEMS"]);
                if (count($arPage) > 1) {
                    for ($i = 1; $i <= count($arPage); $i++) {
                        $pagen[] = $i;
                    }
                }
            }

            $this->arResult = ["elems" => $arPage[$_REQUEST["page"] ? $_REQUEST["page"] - 1 : 0], "pagination" => $pagen];
            $this->includeComponentTemplate();
        }
        return $this->arResult;
    }
} ?>