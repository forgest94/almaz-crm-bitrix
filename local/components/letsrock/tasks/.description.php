<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
$arComponentDescription = array(
    "NAME" => 'История действий',
    "DESCRIPTION" => 'История действий',
    "PATH" => array(
        "ID" => "dv_components",
        "CHILD" => array(
            "ID" => "history",
            "NAME" => "История действий"
        )
    ),
);
?>