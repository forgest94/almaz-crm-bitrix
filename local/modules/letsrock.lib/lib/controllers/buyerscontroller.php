<?

namespace Letsrock\Lib\Controllers;

/*
 * Class BuyersController
 * Контроллер покупателей
 */

use Bitrix\Main\Loader;
use Bitrix\Main\Mail\Event;
use CIBlockElement;
use CIBlock;
use CIBlockProperty;
use CUser;
use Letsrock\Lib\Controllers\MailController;
use Letsrock\Lib\Models;
use Letsrock\Lib\Models\HLBlock;
use Letsrock\Lib\Controllers\PyrusController;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Style\{Font, Border, Alignment};
use Letsrock\Lib\Controllers\HistoryController;

class BuyersController extends Controller
{
    public function addBuyer($request){
        if (Loader::includeModule('iblock')) {
            global $USER;
            $result = false;
            $resultAddUser = false;

            if(!$USER->GetID())
                return Controller::sendError('Произошла ошибка');

            $arFields['PROPERTY'] = [
                'DILER' => $USER->GetID(),
                'INN' => !empty($request["INN"])?$request["INN"]:0,
                'BIN' => !empty($request["BIN"])?$request["BIN"]:0,
                'REGION' => $request["REGION"],
                'COUNTRY' => $request["COUNTRY"],
                'DISTRICT' => $request["DISTRICT"],
                'ADDRESS' => $request["ADDRESS"],
                'PHONE' => $request["PHONE"],
                'EMAIL' => $request["EMAIL"],
                'CONTACT_PEOPLE' => $request["CONTACT_PEOPLE"],
                'POSITION' => $request["POSITION"],
            ];

            if($arFields)
                $result = Models\Element::add(IB_BUYERS, $request["NAME"], $arFields, true);

            if ($result) {
                $arHistory = [
                    "id_who"=>$USER->GetID(),
                    "id_that"=>$USER->GetID(),
                    "id_iblock"=>IB_BUYERS,
                    "id_elem"=>$result,
                    "type"=>"add",
                    "action"=>"Добавлен покупатель"
                ];
                $history = new HistoryController();
                $history_add = $history->addHistory($arHistory);
                if($history_add){
                    return Controller::sendAnswer([]);
                }else{
                    return Controller::sendError('Произошла ошибка');
                }
            } else {
                return Controller::sendError('Произошла ошибка');
            }
        }
    }
    public function unloadBuyers($request){
        header('Content-Type: application/json');
        if (!function_exists('array_key_first')) {
            function array_key_first($arr=false) {
                if(!empty($arr))
                    foreach($arr as $key => $unused) {
                        return $key;
                    }
                return NULL;
            }
        }
        global $USER;
        if(!empty($request["id_dealer"])){
            $arBuyers = [];
            $arIdsBuyers = [];
            $newArBuyer = [];
            $typeTechHl = new HLBlock(HL_EQUIPMENT);
            if($request["id_buyer"]){
                $arBuyers_db = Models\Element::GetList(IB_BUYERS, ["NAME", "ID", "IBLOCK_ID", "PROPERTY_*"],
                    [
                        "ID" => $request["id_buyer"],
                        "PROPERTY_DILER" => $request["id_dealer"]
                    ],
                    ["ID"=>"desc"]);
            }else{
                $arBuyers_db = Models\Element::GetList(IB_BUYERS, ["NAME", "ID", "IBLOCK_ID", "PROPERTY_*"],
                        ["PROPERTY_DILER" => $request["id_dealer"]],
                        ["ID"=>"desc"]);
            }
            foreach ($arBuyers_db["ELEMS"] as $item) {
                $arProps = $arBuyers_db["PROPS"][$item["ID"]];
                $arIdsBuyers[] = $item["ID"];
                $infoRegion = Models\Element::GetList(IB_REGION, ["NAME"], ["ID" => $arProps["REGION"]["VALUE"]])["ELEMS"][0];
                $infoDistrict = Models\Element::GetList(IB_AREA, ["NAME"], ["ID" => $arProps["DISTRICT"]["VALUE"]])["ELEMS"][0];
                $arBuyers[$item["ID"]] = [
                    "name"=>$item["NAME"],
                    "id"=>$item["ID"],
                    "contact_people"=>$arProps["CONTACT_PEOPLE"]["VALUE"],
                    "phone"=>$arProps["PHONE"]["VALUE"],
                    "inn"=>$arProps["INN"]["VALUE"],
                    "position"=>$arProps["POSITION"]["VALUE"],
                    "region"=>$infoRegion["NAME"],
                    "district"=>$infoDistrict["NAME"],
                    "address"=>$arProps["ADDRESS"]["VALUE"],
                    "mail"=>$arProps["EMAIL"]["VALUE"],
                    "arProps"=>$arProps
                ];
            }
            if(!empty($arBuyers)){
                $infoExploitation = Models\Element::GetList(IB_EXPLOITATION, ["NAME", "ID", "IBLOCK_ID", "PROPERTY_*"], ["PROPERTY_BUYER" => $arIdsBuyers]);
                $arIdsTech = [];
                $arTechInfo = [];
                foreach ($infoExploitation["ELEMS"] as $exploit) {
                    $arPropsExploitation = $infoExploitation["PROPS"][$exploit["ID"]];
                    $infoModel = Models\Element::GetList(IB_DIRECTORY, ["NAME"], ["ID" => $arPropsExploitation["MODEL"]["VALUE"], "!PROPERTY_TYPE_VALUE" => false])["ELEMS"][0];
                    $infoType = $typeTechHl->get(["UF_XML_ID" => $arPropsExploitation["EQUIPMENT"]["VALUE"]])[0];
                    $arBuyers[$arPropsExploitation["BUYER"]["VALUE"]]["arTech"][$exploit["ID"]] = [
                        "type"=>$infoType["UF_NAME"],
                        "model"=>$infoModel["NAME"],
                        "number"=>$arPropsExploitation["NUMBER"]["VALUE"],
                        "date"=>$arPropsExploitation["DATE"]["VALUE"]
                    ];
                    $arIdsTech[] = $exploit["ID"];
                    $arTechInfo[$exploit["ID"]] = ["buyer_id"=>$arPropsExploitation["BUYER"]["VALUE"]];
                }
                if(!empty($arIdsTech)){
                    $arReclamation_Tech_db = Models\Element::GetList(IB_RECLAMATION_TECH,
                        ["NAME", "ID", "IBLOCK_ID", "PROPERTY_*", "DATE_CREATE"],
                        [
                            "PROPERTY_DEALER" => $request["id_dealer"],
                            "PROPERTY_SERIAL_NUMBER" => $arIdsTech
                        ],
                        ["ID"=>"desc"]
                    );
                    $arIdsReclamation = [];
                    foreach ($arReclamation_Tech_db["ELEMS"] as $item){
                        $arProps = $arReclamation_Tech_db["PROPS"][$item["ID"]];
                        if(is_array($arProps["SERIAL_NUMBER"]["VALUE"]) && !empty($arProps["SERIAL_NUMBER"]["VALUE"])){
                            foreach ($arProps["SERIAL_NUMBER"]["VALUE"] as $IdExp) {
                                $buyer_id=$arTechInfo[$IdExp]["buyer_id"];
                                if($arBuyers[$buyer_id]["arTech"][$IdExp])
                                $arBuyers[$buyer_id]["arTech"][$IdExp]["reclamation"][$item["ID"]] = ["text"=>$item["NAME"].' от '.explode(' ', $item["DATE_CREATE"])[0]];
                            }
                        }elseif(!empty($arProps["SERIAL_NUMBER"]["VALUE"])){
                            $buyer_id=$arTechInfo[$arProps["SERIAL_NUMBER"]["VALUE"]]["buyer_id"];
                            if($arBuyers[$buyer_id]["arTech"][$arProps["SERIAL_NUMBER"]["VALUE"]])
                            $arBuyers[$buyer_id]["arTech"][$arProps["SERIAL_NUMBER"]["VALUE"]]["reclamation"][$item["ID"]] = ["text"=>$item["NAME"].' от '.explode(' ', $item["DATE_CREATE"])[0]];
                        }
                        $arIdsReclamation[] = $item["ID"];
                    }
                    $infoServices = Models\Element::GetList(IB_SERVCOMP,
                        ["ID", "IBLOCK_ID", "NAME", "CODE", "DATE_ACTIVE_FROM", "DATE_CREATE", "PROPERTY_*"],
                        [
                            "PROPERTY_DILER" => $request["id_dealer"],
                            "PROPERTY_SERIAL_NUMBER" => $arIdsTech,
                            "PROPERTY_RECLAMATION_TECH" => $arIdsReclamation
                        ],
                        ["ID"=>"desc"]
                    );
                    foreach ($infoServices["ELEMS"] as $key => $service) {
                        $arProps = $infoServices["PROPS"][$service["ID"]];
                        $recl = $arProps["RECLAMATION_TECH"]["VALUE"];
                        $expl = $arProps["SERIAL_NUMBER"]["VALUE"];
                        $arServ = [];
                        if(is_array($expl) && is_array($recl)){
                            foreach ($expl as $index => $idExpl){
                                if(!empty($recl[$index]) && $recl[$index] != 'N' && !empty($idExpl) && $idExpl != 'N')
                                    $arServ[] = ["expl"=>$idExpl, "reclamation"=>$recl[$index]];
                            }
                        }
                        if (!empty($arServ)) {
                            foreach ($arServ as $itemServ) {
                                $buyer_id=$arTechInfo[$itemServ["expl"]]["buyer_id"];
                                if($arBuyers[$buyer_id]["arTech"][$itemServ["expl"]])
                                    $arBuyers[$buyer_id]["arTech"][$itemServ["expl"]]["reclamation"][$itemServ["reclamation"]]["service"] = $service["NAME"] . ' от ' . explode(' ', $service["DATE_CREATE"])[0];
                            }
                        } elseif (!empty($expl) && !empty($arTechInfo[$expl]) && !empty($arBuyers[$buyer_id]["arTech"][$expl]["reclamation"][$recl])) {
                            $buyer_id=$arTechInfo[$expl]["buyer_id"];
                            if($arBuyers[$buyer_id]["arTech"][$expl])
                                $arBuyers[$buyer_id]["arTech"][$expl]["reclamation"][$recl]["service"] = $service["NAME"] . ' от ' . explode(' ', $service["DATE_CREATE"])[0];
                        } elseif (!empty($expl) && is_array($expl)) {
                            foreach ($expl as $index => $IdExp) {
                                $arTechInfo[$expl]["service"][] = $service["NAME"] . ' от ' . explode(' ', $service["DATE_CREATE"])[0];
                            }
                        } elseif (!empty($expl) && !empty($arTechInfo[$expl])) {
                            $arTechInfo[$expl]["service"][] = $service["NAME"] . ' от ' . explode(' ', $service["DATE_CREATE"])[0];
                        }
                    }
                }
                foreach ($arBuyers as $item){
                    $newArBuyer[] = $item;
                }
            }
            $_SESSION["count_check"] = '';
            if(!empty($newArBuyer)){
                $spreadsheet = new Spreadsheet();
                $sheet = $spreadsheet->getActiveSheet();
                $arrayData = [
                    ["Покупатель", "Техника", "Номер", "Гарантия до", "Рекламации", "Сервисные компенсации"]
                ];
                $a = 1;
                foreach ($newArBuyer as $item){
                    $a++;
                    $techFirstKey = array_key_first($item["arTech"]);
                    $reclamationFirstKey = array_key_first($item["arTech"][$techFirstKey]["reclamation"]);

                    $arrayData[] = [
                        $item["name"].PHP_EOL.$item["contact_people"].PHP_EOL.'ИНН: '.$item["inn"].PHP_EOL.$item["phone"].PHP_EOL.$item["region"].', '.$item["district"].', '.$item["address"].PHP_EOL.$item["mail"],
                        $item["arTech"][$techFirstKey]["type"].' '.$item["arTech"][$techFirstKey]["model"],
                        $item["arTech"][$techFirstKey]["number"],
                        $item["arTech"][$techFirstKey]["date"],
                        $item["arTech"][$techFirstKey]["reclamation"][$reclamationFirstKey]["text"],
                        $item["arTech"][$techFirstKey]["reclamation"][$reclamationFirstKey]["service"],
                    ];
                    unset($item["arTech"][array_key_first($item["arTech"])]);
                    $arrayData2 = [];
                    $arrayData3 = [];
                    foreach ($item["arTech"] as $arTech) {
                        $arrayData[] = [
                            false,
                            $arTech["type"].' '.$arTech["model"],
                            $arTech["number"],
                            $arTech["date"],
                            $arTech["reclamation"][array_key_first($arTech["reclamation"])]["text"],
                            $arTech["reclamation"][array_key_first($arTech["reclamation"])]["service"],
                        ];
                        unset($arTech["reclamation"][array_key_first($arTech["reclamation"])]);
                        if(!empty($arTech["reclamation"])){
                            foreach ($arTech["reclamation"] as $reclamation) {
                                if(!empty($reclamation["text"])){
                                    $arrayData3[] = $reclamation;
                                    $arrayData[] = [
                                        false,
                                        false,
                                        false,
                                        false,
                                        $reclamation["text"],
                                        $reclamation["service"]
                                    ];
                                }
                            }
                        }
                        if (!empty($arTech["service"])) {
                            foreach ($arTech["service"] as $service) {
                                if (!empty($service)) {
                                    $arrayData3[] = $service;
                                    $arrayData[] = [
                                        false,
                                        false,
                                        false,
                                        false,
                                        false,
                                        $service
                                    ];
                                }
                            }
                        }
                                $arrayData2[] = $arTech;
                    }
                    $count_dop = count($arrayData2);
                    if($a == 2){
                        $b = $a+$count_dop;
                        $_SESSION["count_check"] = $b;
                    }elseif($_SESSION["count_check"]){
                        $a = $_SESSION["count_check"]+1;
                        $b = $a+$count_dop;
                        $_SESSION["count_check"] = $b;
                    }
                    if(!empty($arrayData3)){
                        $count_dop2 = count($arrayData3)+1;
                        if($count_dop2 >= 0){
                            $b2 = $a+$count_dop2;
                            $b = $b+$count_dop2;
                        }else{
                            $b2 = $a;
                        }
                        $sheet->mergeCells('B'.$a.':B'.$b2);
                        $sheet->mergeCells('C'.$a.':C'.$b2);
                        $sheet->mergeCells('D'.$a.':D'.$b2);
                    }
                    $sheet->mergeCells('A'.$a.':A'.$b);
                    $sheet->getStyle('A'.$a)->applyFromArray([
                        'alignment' => [
                            'horizontal' => Alignment::HORIZONTAL_LEFT,
                            'vertical' => Alignment::VERTICAL_CENTER,
                            'wrapText' => true,
                        ]
                    ]);
                    $sheet->getStyle('B'.$a)->applyFromArray([
                        'alignment' => [
                            'horizontal' => Alignment::HORIZONTAL_LEFT,
                            'vertical' => Alignment::VERTICAL_CENTER,
                            'wrapText' => true,
                        ]
                    ]);
                    $sheet->getStyle('C'.$a)->applyFromArray([
                        'alignment' => [
                            'horizontal' => Alignment::HORIZONTAL_LEFT,
                            'vertical' => Alignment::VERTICAL_CENTER,
                            'wrapText' => true,
                        ]
                    ]);
                    $sheet->getStyle('D'.$a)->applyFromArray([
                        'alignment' => [
                            'horizontal' => Alignment::HORIZONTAL_LEFT,
                            'vertical' => Alignment::VERTICAL_CENTER,
                            'wrapText' => true,
                        ]
                    ]);
                }
                if(!empty($arrayData)){
                    $sheet->fromArray(
                        $arrayData,
                        NULL
                    );
                    $writer = new Xlsx($spreadsheet);
                    $writer->save($_SERVER["DOCUMENT_ROOT"].'/upload/testXlsBuyers.xlsx');
                    if($writer){
                        $_SESSION["count_check"] = '';
                        ob_start();
                        $writer->save('php://output');
                        $xlsData = ob_get_contents();
                        ob_end_clean();
                        return Controller::sendAnswer([
                            'file' => 'data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64,'.base64_encode($xlsData)
                        ]);
                    }
                }
            }else{
                return Controller::sendError('Данные пусты');
            }
        }else{
            return Controller::sendError('Не найлен дилер');
        }
    }
}