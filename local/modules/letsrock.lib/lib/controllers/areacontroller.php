<?php

namespace Letsrock\Lib\Controllers;

/*
 * Class AreaController
 * Контроллер районов
 */

use Bitrix\Main\Loader;
use CIBlockElement;

class AreaController extends Controller
{
    public function get($request)
    {
        if (!empty($request['sign']) && Loader::includeModule('iblock')) {
            $arSelect = [
                "ID",
                "NAME",
                "PROPERTY_REGION"
            ];

            $arFilter = [
                "IBLOCK_ID" => IntVal(IB_AREA),
                "ACTIVE" => "Y",
                "PROPERTY_REGION" => $request['sign']
            ];

            $res = CIBlockElement::GetList(["NAME" => "ASC"], $arFilter, false, false, $arSelect);
            $arFields = [];

            while ($ob = $res->GetNextElement()) {
                $arFields[] = $ob->GetFields();
            }

            return Controller::sendAnswer([
                'DATA' => $arFields
            ]);
        }
    }
    public function getRegion($request){
        if (!empty($request['sign']) && Loader::includeModule('iblock')) {
            $arSelect = [
                "ID",
                "NAME",
                "PROPERTY_REGION"
            ];

            $arFilter = [
                "IBLOCK_ID" => IntVal(IB_REGION),
                "ACTIVE" => "Y",
                "PROPERTY_COUNTRY" => $request['sign']
            ];

            $res = CIBlockElement::GetList(["NAME" => "ASC"], $arFilter, false, false, $arSelect);
            $arFields = [
                [
                    "ID"=>"disabled",
                    "NAME"=>"Выбор региона"
                ]
            ];

            while ($ob = $res->GetNextElement()) {
                $arFields[] = $ob->GetFields();
            }

            return Controller::sendAnswer([
                'DATA' => $arFields
            ]);
        }
    }
}