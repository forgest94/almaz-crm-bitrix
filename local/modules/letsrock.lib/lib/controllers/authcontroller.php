<?php

namespace Letsrock\Lib\Controllers;

/*
 * Class authController
 * Контроллер авторизации
 */

class authController extends Controller
{
    public function auth($request)
    {
        global $USER;
        $arAuthResult = $USER->Login($request['email'], $request['password'], "N");

        if(isset($arAuthResult['TYPE']) && $arAuthResult['TYPE'] == 'ERROR')
        {
            return Controller::sendError($arAuthResult['MESSAGE']);
        }
        else
        {
            return Controller::sendAnswer([
                'msg' => 'Вы успешно авторизованы!'
            ]);
        }
    }
}