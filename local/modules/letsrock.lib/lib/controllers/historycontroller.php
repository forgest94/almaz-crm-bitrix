<?

namespace Letsrock\Lib\Controllers;

/*
 * Class HistoryController
 * Контроллер историй
 */
use Letsrock\Lib\Models;
use Letsrock\Lib\Models\HLBlock;
use Letsrock\Lib\Controllers\MailController;
use CIBlock;
use CUser;

class HistoryController extends Controller
{
    function addHistory($arFilds){
        if(!empty($arFilds)){
            $historyHl = new HLBlock(HL_HISTORY);
            $data = [];
            switch ($arFilds["type"]){
                case 'add':
                    $infoIblock = [];
                    if($arFilds["id_iblock"])
                        $infoIblock = CIBlock::GetByID($arFilds["id_iblock"])->GetNext();

                    if($infoIblock){
                        $data = [
                            "UF_WHO_USER" => $arFilds["id_who"],
                            "UF_THAT_USER" => $arFilds["id_that"],
                            "UF_SECTION" => $infoIblock["NAME"],
                            "UF_ACTION" => $arFilds["action"],
                            "UF_ID_RAZ" => $arFilds["id_iblock"],
                            "UF_ID_ELEM" => $arFilds["id_elem"],
                            "UF_DATE_HISTORY" => date("d.m.Y H:i")
                        ];
                    }
                    break;
                case 'edit':
                    break;
            }
            if($data){
                $his = $historyHl->add($data);
                if($his){
                    $textFtoEdit = '';
                    if($data["UF_WHO_USER"] == $data["UF_THAT_USER"]){
                        $infoUserWho = CUser::GetByID($data["UF_WHO_USER"])->Fetch();
                        $textFtoEdit = 'Кто совершил действие: Дилер - '.$infoUserWho["WORK_COMPANY"];
                    }else{
                        $infoGroupWho = CUser::GetUserGroup($data["UF_WHO_USER"]);
                        $infoUserWho = CUser::GetByID($data["UF_WHO_USER"])->Fetch();
                        if(
                            in_array(ID_MANAGER_SERVICES, $infoGroupWho) ||
                            in_array(ID_MANAGER_PARTS, $infoGroupWho) ||
                            in_array(ID_MANAGER_TECH, $infoGroupWho) ||
                            in_array(ID_MANAGER_MARKETING, $infoGroupWho)){
                            $name_person = '';
                            $secondName_person = '';
                            if($infoUserWho["NAME"])
                                $name_person = mb_substr($infoUserWho["NAME"],0,1).". ";
                            if($infoUserWho["SECOND_NAME"])
                                $secondName_person = mb_substr($infoUserWho["SECOND_NAME"],0,1).".";

                            $name_parse = $infoUserWho["LAST_NAME"]." ".$name_person.$secondName_person;
                            $textFtoEdit = 'Кто совершил действие: Менеджер - '.$name_parse;
                        }else if(in_array(ID_ADMIN, $infoGroupWho)){
                            $textFtoEdit = 'Кто совершил действие: Администратор - '.$infoUserWho["NAME"];
                        }
                    }
                    if($textFtoEdit){
                        $mailer = new MailController();
                        if($arFilds["subject_mail"]){
                            $arMail["subject"] = $arFilds["subject_mail"];
                        }else{
                            $arMail["subject"] = $data["UF_SECTION"];
                        }
                        if($arFilds["html_mail"]){
                            $arMail["html"] = $arFilds["html_mail"];
                        }else{
                            $arMail["html"] = '<p>'.$textFtoEdit.'</p><p>'.$data["UF_ACTION"].'</p>';
                        }
                        if($arFilds["to_mail"]){
                            $arMail["to_mail"] = $arFilds["to_mail"];
                        }else{
                            $arMail["to_mail"] = "kkv@almaztd.ru";
                        }
                        if(!empty($arMail))
                        $mailer->send($arMail);
                    }
                    return $his;
                }
            }
            return false;
        }
    }
}
