<?php

namespace Letsrock\Lib\Controllers;

/*
 * Class EquipmentsController
 * Контроллер техники
 */

use Bitrix\Main\Loader;
use CIBlockElement;

class EquipmentsController extends Controller
{
    public function get($request)
    {
        if (!empty($request['sign']) && Loader::includeModule('iblock')) {
            $arSelect = [
                "ID",
                "NAME",
                "PROPERTY_TYPE"
            ];

            $arFilter = [
                "IBLOCK_ID" => IntVal(IB_DIRECTORY),
                "ACTIVE" => "Y",
                "PROPERTY_TYPE" => $request['sign']
            ];

            $res = CIBlockElement::GetList(["SORT" => "ASC"], $arFilter, false, ["nPageSize" => 50], $arSelect);
            $arFields = [];

            while ($ob = $res->GetNextElement()) {
                $arFields[] = $ob->GetFields();
            }

            return Controller::sendAnswer([
                'DATA' => $arFields
            ]);
        }
    }

}