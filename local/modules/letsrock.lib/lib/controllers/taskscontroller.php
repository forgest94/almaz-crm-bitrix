<?php

namespace Letsrock\Lib\Controllers;

/*
 * Class TasksController
 * Контроллер входящих задач для личного кабинета
 */

use Bitrix\Main\Loader;
use Bitrix\Main\Mail\Event;
use CIBlockElement;
use CIBlockProperty;
use CUser;
use Letsrock\Lib\Models;
use Letsrock\Lib\Models\HLBlock;

class TasksController extends Controller
{
    public function getUser($id)
    {
        if ($id) {
            return CUser::GetByID($id)->Fetch();
        }
    }

    public function uploadedPyrusProderty($arFields, $iblock, $pyrus)
    {
        $arParseProp = [];
        $properties = CIBlockProperty::GetList(Array("sort" => "asc", "name" => "asc"), Array("ACTIVE" => "Y", "IBLOCK_ID" => $iblock));
        while ($prop_fields = $properties->GetNext()) {
            if (!empty($arFields["PROPERTY"][$prop_fields["CODE"]]["VALUE"])) {
                if ($prop_fields["PROPERTY_TYPE"] == 'F') {
                    $idPyrusProp = '';
                    switch ($prop_fields["CODE"]) {
                        case 'OTCHET_1':
                            $idPyrusProp = 9;
                            break;
                        case 'OTCHET_3':
                            $idPyrusProp = 11;
                            break;
                        case 'COMPENSATION':
                            $idPyrusProp = 14;
                            break;
                        case 'AKT':
                            $idPyrusProp = 15;
                            break;
                        case 'INVOICE':
                            $idPyrusProp = 16;
                            break;
                        case 'PHOTO1':
                        case 'PHOTO2':
                        case 'PHOTO':
                            $idPyrusProp = 10;
                            break;
                        case 'CONTRACTS':
                            $idPyrusProp = 12;
                            break;
                        case 'CONTRACTS_2':
                            $idPyrusProp = 13;
                            break;
                        case 'DOCS_F':
                            $idPyrusProp = 17;
                            break;
                    }
                    if ($idPyrusProp) {
                        if ($prop_fields["MULTIPLE"] == 'N') {
                            $fileUnloadMore = [];
                            $fileArray = \CFile::GetFileArray($arFields["PROPERTY"][$prop_fields["CODE"]]["VALUE"]);
                            $fileFind = $pyrus->uploadFile($fileArray);
                            if ($fileFind["success"] && $fileFind["guid"]) {
                                $fileUnloadMore[] = $fileFind;
                            }
                            if (!empty($fileUnloadMore))
                                $arParseProp[] = ["id" => $idPyrusProp, "value" => $fileUnloadMore];
                        } else {
                            $fileUnloadMore = [];
                            foreach ($arFields["PROPERTY"][$prop_fields["CODE"]]["VALUE"] as $file) {
                                $fileArray = \CFile::GetFileArray($file);
                                $fileFind = $pyrus->uploadFile($fileArray);
                                if ($fileFind["success"] && $fileFind["guid"]) {
                                    $fileUnloadMore[] = $fileFind;
                                }
                            }
                            if (!empty($fileUnloadMore))
                                $arParseProp[] = ["id" => $idPyrusProp, "value" => $fileUnloadMore];
                        }
                    }
                }
            }
        }
        return $arParseProp;
    }

    public function editTask($request)
    {
        if (Loader::includeModule('iblock')) {
            global $USER;
            $result = false;
            $arHistoryDop = [];
            $resp_pyrus = [];
            if ($request["status"] && $request["id_elem"] && $request["id_dealer"]) {
                $dealer_info = $this->getUser($request["id_dealer"]);
                $infoGroup = CUser::GetUserGroup($USER->GetID());
                $infoElem_db = CIBlockElement::GetByID($request["id_elem"]);
                if ($infoElem = $infoElem_db->GetNext()) {
                    $textAction = "";
                    $status_elem = CIBlockElement::GetProperty($infoElem["IBLOCK_ID"], $infoElem["ID"], array("sort" => "asc"), Array("CODE" => "STATUS"))->Fetch();
                    $new_stataus = CIBlockProperty::GetPropertyEnum("STATUS", Array(), Array("IBLOCK_ID" => $infoElem["IBLOCK_ID"], "ID" => $request["status"]))->GetNext();
                    if (!empty($request["inperr"])) {
                        CIBlockElement::SetPropertyValueCode($infoElem["ID"], "REFUSAL", $request["inperr"]);
                    }
                    switch ($infoElem["IBLOCK_ID"]) {
                        case IB_EXPLOITATION:
                            $infoElemDop = Models\Element::getById(IB_EXPLOITATION, $request["id_elem"], ["*"], true);
                            $textAction = "Постановке на гарантию " . $infoElemDop["PROPERTY"]["NUMBER"]["VALUE"] . " изменен статус с " . $status_elem["VALUE_ENUM"] . " на " . $new_stataus["VALUE"];
                            $statusMess = '';
                            switch ($request["status"]) {
                                case 14:
                                    $statusMess = 'отклонена';
                                    break;
                                case 15:
                                    $statusMess = 'подтверждена';
                                    break;
                            }
                            $infoBuyer = [];
                            $arPropsBuyer = [];
                            if (!empty($statusMess) && !empty($dealer_info) && $infoElemDop) {
                                if ($infoElemDop["PROPERTY"]["BUYER"]["VALUE"]) {
                                    $infoBuyer = Models\Element::getById(IB_BUYERS, $infoElemDop["PROPERTY"]["BUYER"]["VALUE"], ["*"], true);
                                    $arPropsBuyer = $infoBuyer["PROPERTY"];
                                    $infoRegion = Models\Element::getById(IB_REGION, $arPropsBuyer["REGION"]["VALUE"]);
                                    $infoDistrict = Models\Element::getById(IB_AREA, $arPropsBuyer["DISTRICT"]["VALUE"]);
                                    $infoModel = Models\Element::getById(IB_DIRECTORY, $infoElemDop["PROPERTY"]['MODEL']["VALUE"]);
                                    $typeTechHl = new HLBlock(HL_EQUIPMENT);
                                    $infoType = $typeTechHl->get(["UF_XML_ID" => $infoElemDop["PROPERTY"]['EQUIPMENT']["VALUE"]])[0];

                                    $arHistoryDop["subject_mail"] = "Личный кабинет дилера Алмаз";
                                    $arHistoryDop["to_mail"] = $dealer_info["EMAIL"];
                                    $arHistoryDop["html_mail"] = "
                                        <p>
                                        В личном кабинете дилера " . $statusMess . " постановка на гарантию следующей техники:<br/>
                                        Дилер: " . $dealer_info["WORK_COMPANY"] . "<br/>
                                        Техника: " . $infoType["UF_NAME"] . " " . $infoModel["NAME"] . ", номер " . $infoElemDop["PROPERTY"]["NUMBER"]["VALUE"] . "<br/>
                                        Дата ввода в эксплуатацию: " . $infoElemDop["PROPERTY"]["DATE"]["VALUE"] . "</p>
                                        
                                        <p>Покупатель: " . $infoBuyer["NAME"] . ", " . $infoRegion["NAME"] . ", " . $infoDistrict["NAME"] . ", " . $arPropsBuyer["ADDRESS"]["VALUE"] . "<br/>
                                        ИНН: " . $arPropsBuyer["INN"]["VALUE"] . "<br/>
                                        Контактное лицо: " . $arPropsBuyer["CONTACT_PEOPLE"]["VALUE"] . ", " . $arPropsBuyer["PHONE"]["VALUE"] . ", " . $arPropsBuyer["EMAIL"]["VALUE"] . "</p>
                                    ";
                                    if (!empty($request["inperr"])) {
                                        $arHistoryDop["html_mail"] .= "<p>Причина отклонения: " . $request["inperr"] . "</p>";
                                    }
                                }
                            }
                            if (in_array(ID_MANAGER_SERVICES, $infoGroup) && !empty($infoBuyer) && !empty($arPropsBuyer)) {
                                $infoManagerTech = [];
                                if ($dealer_info["UF_MANAGER_NEW"]) {
                                    foreach ($dealer_info["UF_MANAGER_NEW"] as $manager) {
                                        $infoGroup2 = \CUser::GetUserGroup($manager);
                                        if (in_array(ID_MANAGER_TECH, $infoGroup2)) {
                                            $userInfo = \CUser::GetByID($manager)->GetNext();
                                            if (!empty($userInfo))
                                                $infoManagerTech = $userInfo;
                                        } else {
                                            continue;
                                        }
                                    }
                                }
                                if (!empty($infoManagerTech)) {
                                    $arMail = [];
                                    if (empty($arPropsBuyer['BIN']["VALUE"])) {
                                        $arSelect = [
                                            "ID",
                                            "NAME",
                                            "PROPERTY_INN"
                                        ];
                                        $arFilter = [
                                            "IBLOCK_ID" => IntVal(IB_REGION),
                                            "ACTIVE" => "Y",
                                            "ID" => $dealer_info["UF_REGIONS_USER"],
                                            "PROPERTY_INN" => substr(IntVal($arPropsBuyer['INN']["VALUE"]), 0, 2)
                                        ];
                                        $res = CIBlockElement::GetList(["SORT" => "ASC"], $arFilter, false, ["nPageSize" => 50], $arSelect);
                                        $indfoRegionInn = [];
                                        while ($ob = $res->GetNextElement()) {
                                            $indfoRegionInn[] = $ob->GetFields();
                                        }
                                    } else {
                                        $indfoRegionInn = true;
                                    }
                                    if (!empty($indfoRegionInn)) {
                                        $textMail = '';
                                        switch ($request["status"]) {
                                            case 14:
                                                $arMail["to_mail"] = $infoManagerTech["EMAIL"];
                                                $arMail["subject"] = "Постановка техники на гарантию, отказ";
                                                $textMail = 'В личном кабинете дилера отклонена постановка на гарантию следующей техники';
                                            case 15:
                                                $arMail["to_mail"] = $infoManagerTech["EMAIL"];
                                                $arMail["subject"] = "Постановка техники на гарантию";
                                                $textMail = 'Поступила заявка на постановку техники на гарантию';
                                                break;
                                        }
                                        if (!empty($textMail))
                                            $arMail["html"] = "
                                                    <p>
                                                    " . $textMail . ":<br/>
                                                    Дилер: " . $dealer_info["WORK_COMPANY"] . "<br/>
                                                    Техника: " . $infoType["UF_NAME"] . " " . $infoModel["NAME"] . ", номер " . $infoElemDop["PROPERTY"]["NUMBER"]["VALUE"] . "<br/>
                                                    Дата ввода в эксплуатацию: " . $infoElemDop["PROPERTY"]["DATE"]["VALUE"] . "</p>
                                                    
                                                    <p>Покупатель: " . $infoBuyer["NAME"] . ", " . $infoRegion["NAME"] . ", " . $infoDistrict["NAME"] . ", " . $arPropsBuyer["ADDRESS"]["VALUE"] . "<br/>
                                                    ИНН: " . $arPropsBuyer["INN"]["VALUE"] . "<br/>
                                                    Контактное лицо: " . $arPropsBuyer["CONTACT_PEOPLE"]["VALUE"] . ", " . $arPropsBuyer["PHONE"]["VALUE"] . ", " . $arPropsBuyer["EMAIL"]["VALUE"] . "</p>
                                                ";
                                        if (!empty($request["inperr"])) {
                                            $arMail["html"] .= "<p>Причина отклонения: " . $request["inperr"] . "</p>";
                                        }
                                    } else {
                                        $textMail = '';
                                        switch ($request["status"]) {
                                            case 14:
                                                $arMail["to_mail"] = $infoManagerTech["EMAIL"];
                                                $arMail["subject"] = "Постановка техники на гарантию, отказ, ИНН не совпадает";
                                                $textMail = 'В личном кабинете дилера отклонена постановка на гарантию следующей техники';
                                            case 15:
                                                $arMail["to_mail"] = $infoManagerTech["EMAIL"];
                                                $arMail["subject"] = "Постановка техники на гарантию, ИНН не совпадает";
                                                $textMail = 'В личном кабинете дилера поставлена на гарантию следующая техника';
                                                break;
                                        }
                                        if (!empty($textMail))
                                            $arMail["html"] = '
                                                    <p>
                                                    ' . $textMail . ':<br/>
                                                    Дилер: ' . $dealer_info["WORK_COMPANY"] . '<br/>
                                                    Техника: ' . $infoType["UF_NAME"] . ' ' . $infoModel["NAME"] . ', номер ' . $infoElemDop["PROPERTY"]["NUMBER"]["VALUE"] . '<br/>
                                                    Дата ввода в эксплуатацию: ' . $infoElemDop["PROPERTY"]["DATE"]["VALUE"] . '</p>
                                                    
                                                    <p>Покупатель: ' . $infoBuyer["NAME"] . ', ' . $infoRegion["NAME"] . ', ' . $infoDistrict["NAME"] . ', ' . $arPropsBuyer["ADDRESS"]["VALUE"] . '<br/>
                                                    ИНН: ' . $arPropsBuyer["INN"]["VALUE"] . '<br/>
                                                    Контактное лицо: ' . $arPropsBuyer["CONTACT_PEOPLE"]["VALUE"] . ', ' . $arPropsBuyer["PHONE"]["VALUE"] . ', ' . $arPropsBuyer["EMAIL"]["VALUE"] . '</p>
                                                    <p  style="background-color: #ffe9bd;">*** Внимание! Указанный ИНН покупателя не совпадает с закрепленным за дилером регионом! Пожалуйста, проверьте фактическое расположение покупателя.</p>
                                                ';
                                    }
                                    if (!empty($arMail)) {
                                        $mailer = new MailController();
                                        $mailto = $mailer->send($arMail);
                                    }
                                }
                            }
                            break;
                        case IB_RECLAMATION_TECH:
                            if (strpos('---- ' . $infoElem["NAME"], 'RT') === false) {
                                $numberTechRecl = $infoElem["NAME"];
                            } else {
                                $numberTechRecl = 'RT' . $infoElem["ID"];
                            }
                            $textAction = "Рекламации техника " . $numberTechRecl . " изменен статус с " . $status_elem["VALUE_ENUM"] . " на " . $new_stataus["VALUE"];
                            switch ($request["status"]) {
                                case 20:
                                    $statusMess = 'отклонена';
                                    break;
                                case 19:
                                    $statusMess = 'одобрена';
                                    break;
                            }
                            if (!empty($statusMess) && !empty($dealer_info)) {
                                $infoElemDop = Models\Element::getById(IB_RECLAMATION_TECH, $request["id_elem"], ["*"], true);
                                $arHistoryDop["subject_mail"] = "Личный кабинет дилера Алмаз";
                                $arHistoryDop["to_mail"] = $dealer_info["EMAIL"];
                                $arHistoryDop["html_mail"] = "
                                        <p>
                                            Рекламация номер " . $numberTechRecl . " " . $statusMess . "<br/>
                                            Дата подачи рекламации: " . $infoElemDop["PROPERTY"]["DATE"]["VALUE"] . "
                                        </p>
                                ";
                                if (!empty($request["inperr"])) {
                                    $arHistoryDop["html_mail"] .= "<p>Причина отклонения: " . $request["inperr"] . "</p>";
                                }
                            }
                            break;
                        case IB_RECLAMATION_PARTS:
                            if (strpos('---- ' . $infoElem["NAME"], 'RP') === false) {
                                $numberParts = $infoElem["NAME"];
                            } else {
                                $numberParts = 'RP' . $infoElem["ID"];
                            }
                            $textAction = "Рекламации запчасти " . $numberParts . " изменен статус с " . $status_elem["VALUE_ENUM"] . " на " . $new_stataus["VALUE"];
                            switch ($request["status"]) {
                                case 24:
                                    $statusMess = 'отклонена';
                                    break;
                                case 23:
                                    $statusMess = 'одобрена';
                                    break;
                            }
                            if (!empty($statusMess) && !empty($dealer_info)) {
                                $infoElemDop = Models\Element::getById(IB_RECLAMATION_PARTS, $request["id_elem"], ["*"], true);
                                $arHistoryDop["subject_mail"] = "Личный кабинет дилера Алмаз";
                                $arHistoryDop["to_mail"] = $dealer_info["EMAIL"];
                                $arHistoryDop["html_mail"] = "
                                        <p>
                                            Рекламация номер " . $numberParts . " " . $statusMess . "<br/>
                                            Дата подачи рекламации: " . $infoElemDop["PROPERTY"]["DATE"]["VALUE"] . "
                                        </p>
                                ";
                                if (!empty($request["inperr"])) {
                                    $arHistoryDop["html_mail"] .= "<p>Причина отклонения: " . $request["inperr"] . "</p>";
                                }
                            }
                            break;
                        case IB_MARKETING:
                            $textAction = "Маркетинговые компенсации M" . $infoElem["ID"] . " изменен статус с " . $status_elem["VALUE_ENUM"] . " на " . $new_stataus["VALUE"];
                            switch ($request["status"]) {
                                case 25:
                                    $statusMess = 'отклонена';
                                    break;
                                case 3:
                                    $statusMess = 'подтверждена';
                                    break;
                            }
                            if (!empty($statusMess) && !empty($dealer_info)) {
                                $infoElemDop = Models\Element::getById(IB_MARKETING, $request["id_elem"], ["*"], true);
                                /* pyrus */
                                if ($statusMess == 'подтверждена') {
                                    $arPyrus = [];
                                    $pyrus = new PyrusController;
                                    if (!empty($dealer_info["WORK_COMPANY"])) {
                                        $dealerText = $dealer_info["WORK_COMPANY"] . ' - ' . $dealer_info["LAST_NAME"] . " " . $dealer_info["NAME"] . " " . $dealer_info["SECOND_NAME"] . ", " . $dealer_info["PERSONAL_PHONE"];
                                        $arFildsForm = $this->uploadedPyrusProderty($infoElemDop, IB_MARKETING, $pyrus);
                                        $arFildsForm[] = ["id" => 3, "value" => $dealerText];
                                        //pr($arFildsForm);
                                        /*
                                            Контрагент ID-3
                                            Отчет о проведенном мероприятии (для выставок) ID-9
                                            Фото, макеты ID-10
                                            Смета затрат ID-11
                                            Договоры дилера с контрагентами ID-12
                                            Документы, подтверждающие затраты дилера ID-13
                                            Счет на компенсацию затрат ID-14
                                            Акт выполненных работ ID-15
                                            Счет-фактура ID-16
                                            Прочие документы (не обязательно) ID-17
                                            Id формы t#et766026
                                         */
                                        $arPyrus = [
                                            "form_id" => 766026,
                                            "fields" => $arFildsForm
                                        ];
                                    }
                                    if ($arPyrus) {
                                        $resp_pyrus = $pyrus->AddTaskForm($arPyrus);
                                        //pr($resp_pyrus);
                                        if (!empty($resp_pyrus) && $resp_pyrus["success"]) {
                                            Models\Element::update($infoElemDop["ID"], ["NAME" => $resp_pyrus["id_task"]]);
                                        }
                                    }
                                }
                                /* endpyrus */
                                $arHistoryDop["subject_mail"] = "Личный кабинет дилера Алмаз";
                                $arHistoryDop["to_mail"] = $dealer_info["EMAIL"];
                                $arHistoryDop["html_mail"] = "
                                        <p>
                                            В личном кабинете дилера " . $statusMess . " компенсация маркетинговых затрат<br/>
                                            Дата подачи документов: " . $infoElemDop["PROPERTY"]["DATE"]["VALUE"] . "
                                        </p>
                                ";
                                if (!empty($request["inperr"])) {
                                    $arHistoryDop["html_mail"] .= "<p>Причина отклонения: " . $request["inperr"] . "</p>";
                                }
                            }
                            break;
                        case IB_SERVCOMP:
                            $textAction = "Сервисные компенсации S" . $infoElem["ID"] . " изменен статус с " . $status_elem["VALUE_ENUM"] . " на " . $new_stataus["VALUE"];
                            switch ($request["status"]) {
                                case 26:
                                    $statusMess = 'отклонена';
                                    break;
                                case 12:
                                    $statusMess = 'подтверждена';
                                    break;
                            }
                            if (!empty($statusMess) && !empty($dealer_info)) {
                                $infoElemDop = Models\Element::getById(IB_SERVCOMP, $request["id_elem"], ["*"], true);
                                /* pyrus */
                                if ($statusMess == 'подтверждена') {
                                    $arPyrus = [];
                                    $pyrus = new PyrusController;
                                    if (!empty($dealer_info["WORK_COMPANY"])) {
                                        //$dealerText = $dealer_info["WORK_COMPANY"] . ' - ' . $dealer_info["LAST_NAME"] . " " . $dealer_info["NAME"] . " " . $dealer_info["SECOND_NAME"] . ", " . $dealer_info["PERSONAL_PHONE"];
                                        //$arFildsForm[] = ["id" => 2, "value" => $dealerText];
                                        if (!empty($infoElemDop["PROPERTY"]["INVOICE"]["VALUE"])) {
                                            $fileUnloadMore = [];
                                            $fileArray = \CFile::GetFileArray($infoElemDop["PROPERTY"]["INVOICE"]["VALUE"]);
                                            $fileFind = $pyrus->uploadFile($fileArray);
                                            if ($fileFind["success"] && $fileFind["guid"]) {
                                                $fileUnloadMore[] = $pyrus->uploadFile($fileArray);
                                            }
                                            if (!empty($fileUnloadMore))
                                                $arFildsForm[] = ["id" => 16, "value" => $fileUnloadMore];
                                        }
                                        if (!empty($infoElemDop["PROPERTY"]["AKT"]["VALUE"])) {
                                            $fileUnloadMore = [];
                                            $fileArray = \CFile::GetFileArray($infoElemDop["PROPERTY"]["AKT"]["VALUE"]);
                                            $fileFind = $pyrus->uploadFile($fileArray);
                                            if ($fileFind["success"] && $fileFind["guid"]) {
                                                $fileUnloadMore[] = $pyrus->uploadFile($fileArray);
                                            }
                                            if (!empty($fileUnloadMore))
                                                $arFildsForm[] = ["id" => 14, "value" => $fileUnloadMore];
                                        }
                                        if (!empty($infoElemDop["PROPERTY"]["INVOICE_PAYMENT"]["VALUE"])) {
                                            $fileUnloadMore = [];
                                            foreach ($infoElemDop["PROPERTY"]["INVOICE_PAYMENT"]["VALUE"] as $fileGet) {
                                                $fileArray = \CFile::GetFileArray($fileGet);
                                                $fileFind = $pyrus->uploadFile($fileArray);
                                                if ($fileFind["success"] && $fileFind["guid"]) {
                                                    $fileUnloadMore[] = $pyrus->uploadFile($fileArray);
                                                }
                                            }
                                            if (!empty($fileUnloadMore))
                                                $arFildsForm[] = ["id" => 15, "value" => $fileUnloadMore];
                                        }
                                        if (!empty($infoElemDop["PROPERTY"]["PUT_LIST"]["VALUE"])) {
                                            $fileUnloadMore = [];
                                            $fileArray = \CFile::GetFileArray($infoElemDop["PROPERTY"]["PUT_LIST"]["VALUE"]);
                                            $fileFind = $pyrus->uploadFile($fileArray);
                                            if ($fileFind["success"] && $fileFind["guid"]) {
                                                $fileUnloadMore[] = $pyrus->uploadFile($fileArray);
                                            }
                                            if (!empty($fileUnloadMore))
                                                $arFildsForm[] = ["id" => 17, "value" => $fileUnloadMore];
                                        }
                                        if (!empty($infoElemDop["PROPERTY"]['SERIAL_NUMBER']["VALUE"])) {
                                            $isExpl = Models\Element::getList(IB_EXPLOITATION, ['ID', 'IBLOCK_ID', 'PROPERTY_*'], ['ID' => $infoElemDop["PROPERTY"]['SERIAL_NUMBER']["VALUE"], 'PROPERTY_DEALER' => $dealer_info["ID"]]);
                                            $fileUnloadMore = [];
                                            $fileUnloadMore2 = [];
                                            foreach ($isExpl["ELEMS"] as $itemExpl) {
                                                $arProps = $isExpl["PROPS"][$itemExpl["ID"]];
                                                if (!empty($arProps["FILE"]["VALUE"])) {
                                                    $fileArray = \CFile::GetFileArray($arProps["FILE"]["VALUE"]);
                                                    $fileFind = $pyrus->uploadFile($fileArray);
                                                    if ($fileFind["success"] && $fileFind["guid"]) {
                                                        $fileUnloadMore[] = $pyrus->uploadFile($fileArray);
                                                    }
                                                }
                                                if (!empty($arProps["PUT_LIST"]["VALUE"])) {
                                                    $fileArray = \CFile::GetFileArray($arProps["PUT_LIST"]["VALUE"]);
                                                    $fileFind = $pyrus->uploadFile($fileArray);
                                                    if ($fileFind["success"] && $fileFind["guid"]) {
                                                        $fileUnloadMore2[] = $pyrus->uploadFile($fileArray);
                                                    }
                                                }
                                            }
                                            if (!empty($fileUnloadMore))
                                                $arFildsForm[] = ["id" => 8, "value" => $fileUnloadMore];
                                            if (!empty($fileUnloadMore2))
                                                $arFildsForm[] = ["id" => 17, "value" => $fileUnloadMore2];

                                            $typeTechHl = new HLBlock(HL_EQUIPMENT);
                                            $arTech = [];
                                            if (!empty($infoElemDop["PROPERTY"]['SERIAL_NUMBER']["VALUE"])) {
                                                foreach ($infoElemDop["PROPERTY"]['SERIAL_NUMBER']["VALUE"] as $serialElem) {
                                                    if (!empty((int)$serialElem)) {
                                                        $infoTech = Models\Element::GetList(IB_EXPLOITATION, ["ID", "NAME", "PROPERTY_MODEL", "PROPERTY_EQUIPMENT", "PROPERTY_DATE", "PROPERTY_FILE", "PROPERTY_NUMBER"], ["ID" => (int)$serialElem, "!EQUIPMENT_VALUE" => false])["ELEMS"][0];
                                                        if ($infoTech) {
                                                            $infoModel = Models\Element::GetList(IB_DIRECTORY, ["NAME"], ["ID" => $infoTech["PROPERTY_MODEL_VALUE"], "!PROPERTY_TYPE_VALUE" => false])["ELEMS"][0];
                                                            $infoType = $typeTechHl->get(["UF_XML_ID" => $infoTech["PROPERTY_EQUIPMENT_VALUE"]])[0];
                                                            $numberTech = $infoTech["PROPERTY_NUMBER_VALUE"];
                                                            $parse_akt = \CFile::GetFileArray($infoTech["PROPERTY_FILE_VALUE"]);

                                                            $arTech[$infoTech["ID"]] = [
                                                                "number" => $numberTech,
                                                                "model" => $infoModel["NAME"],
                                                                "type" => $infoType["UF_NAME"],
                                                                "akt" => ["name" => $parse_akt["ORIGINAL_NAME"], "src" => $parse_akt["SRC"]],
                                                                "date" => $infoTech["PROPERTY_DATE_VALUE"],
                                                            ];
                                                        }
                                                    }
                                                }
                                            }
                                            $arReclamationTech = [];
                                            if (!empty($infoElemDop["PROPERTY"]["RECLAMATION_TECH"]["VALUE"]) && $infoElemDop["PROPERTY"]["RECLAMATION_TECH"]["VALUE"][0] != 'N') {
                                                foreach ($infoElemDop["PROPERTY"]["RECLAMATION_TECH"]["VALUE"] as $reclElem) {
                                                    if (!empty((int)$reclElem)) {
                                                        $infoRecl = Models\Element::GetList(IB_RECLAMATION_TECH, ["NAME", "ID", "IBLOCK_ID", "PROPERTY_*"], ["ID" => $reclElem]);
                                                        $elem = $infoRecl["ELEMS"][0];
                                                        $propsRecl = $infoRecl["PROPS"][$elem["ID"]];
                                                        $infoTechRecl = [];
                                                        if (!empty($propsRecl["SERIAL_NUMBER"]["VALUE"])) {
                                                            if (is_array($propsRecl["SERIAL_NUMBER"]["VALUE"])) {
                                                                foreach ($propsRecl["SERIAL_NUMBER"]["VALUE"] as $itemIdsTech) {
                                                                    $infoTechRecl = $arTech[$itemIdsTech];
                                                                }
                                                            } else {
                                                                $infoTechRecl = $arTech[$propsRecl["SERIAL_NUMBER"]["VALUE"]];
                                                            }
                                                        }
                                                        $akt_parse = [];
                                                        if (!empty($propsRecl["AKT"]["VALUE"])) {
                                                            foreach ($propsRecl["AKT"]["VALUE"] as $akt2) {
                                                                $arFile = \CFile::GetFileArray($akt2);
                                                                if ($arFile)
                                                                    $akt_parse[] = ["name" => $arFile["ORIGINAL_NAME"], "src" => $arFile["SRC"]];
                                                            }
                                                        }
                                                        $arPhoto = [];
                                                        if (!empty($propsRecl["PHOTO"]["VALUE"])) {
                                                            foreach ($propsRecl["PHOTO"]["VALUE"] as $photo) {
                                                                $arFile = \CFile::GetFileArray($photo);
                                                                if ($arFile)
                                                                    $arPhoto[] = ["name" => $arFile["ORIGINAL_NAME"], "src" => $arFile["SRC"]];
                                                            }
                                                        }
                                                        $arReclamationTech[] = [
                                                            "number" => $elem["NAME"],
                                                            "id" => $elem["ID"],
                                                            "infoTech" => $infoTechRecl,
                                                            "reason" => $propsRecl["REASON"]["VALUE"]["TEXT"],
                                                            "wishes" => $propsRecl["WISHES"]["VALUE"]["TEXT"],
                                                            "akt" => $akt_parse,
                                                            "photo" => $arPhoto
                                                        ];
                                                    }
                                                }
                                            }
                                            //pr($infoTechRecl);
                                            $textTech = '';
                                            if (!empty($arTech)) {
                                                foreach ($arTech as $tech) {
                                                    $textTech .= $tech["type"] . ' ' . $tech["model"] . ' №' . $tech["number"] . ', дата ввода в эксплуатацию ' . $tech["date"] . ',';
                                                }
                                            }
                                            if (!empty($arReclamationTech)) {
                                                foreach ($arReclamationTech as $reclam) {
                                                    $textTech .= $textTech . ' рекламация №' . $reclam["number"] . ', ' . $reclam["reason"];
                                                }
                                            }

                                            if ($textTech)
                                                $arFildsForm[] = [
                                                    "id" => 7,
                                                    "value" => "Компенсация за " . $infoElemDop["PROPERTY"]["TYPE_SERVICE"]["~VALUE"] . ", " . $textTech];
                                        }
                                        //$infoManager = \Letsrock\Lib\Models\User::getWork();
                                        /*
                                            id формы t#et736409,
                                            контрагент 2,
                                            номер счет фактуры -3,
                                            дата поступления счет фактуры 4,
                                            сумма по документы - 5,
                                            Акт ввода техники в эксплуатацию-8,
                                            Акт выполненных работ-14, Счет на компенсацию сервисных затрат-15,
                                            Счет-фактура-16, Путевой лист-17
                                         */
                                        /*if (!empty($infoManager["UF_PYRUS_ID"])) {
                                            $arFildsForm[] = ["id" => 19, "value" =>
                                                [
                                                    "id" => $infoManager["UF_PYRUS_ID"]
                                                ]
                                            ];
                                        }*/
                                        $arPyrus = [
                                            "form_id" => 736409,
                                            "fields" => $arFildsForm
                                        ];
                                    }
                                    if ($arPyrus) {
                                        $resp_pyrus = $pyrus->AddTaskForm($arPyrus);
                                        //pr($arPyrus);
                                        //pr($resp_pyrus);
                                        if (!empty($resp_pyrus) && $resp_pyrus["success"]) {
                                            Models\Element::update($infoElemDop["ID"], ["NAME" => $resp_pyrus["id_task"]]);
                                        }
                                    }
                                }
                                /* endpyrus */
                                $arHistoryDop["subject_mail"] = "Личный кабинет дилера Алмаз";
                                $arHistoryDop["to_mail"] = $dealer_info["EMAIL"];
                                $arHistoryDop["html_mail"] = "
                                        <p>
                                            В личном кабинете дилера " . $statusMess . " компенсация сервисных затрат<br/>
                                            Дата подачи документов: " . $infoElemDop["PROPERTY"]["DATE"]["VALUE"] . "
                                        </p>
                                ";
                                if (!empty($request["inperr"])) {
                                    $arHistoryDop["html_mail"] .= "<p>Причина отклонения: " . $request["inperr"] . "</p>";
                                }
                            }
                            break;
                    }
                    $history_add = false;
                    if ($textAction) {
                        if (($infoElem["IBLOCK_ID"] == IB_RECLAMATION_TECH || $infoElem["IBLOCK_ID"] == IB_RECLAMATION_PARTS) &&
                            strpos('---- ' . $infoElem["NAME"], 'RP') === false &&
                            strpos('---- ' . $infoElem["NAME"], 'RT') === false
                        ) {
                            if ($infoGroup) {
                                $textEditPy = '';
                                $infoUserWho = CUser::GetByID($USER->GetID())->Fetch();
                                if (
                                    in_array(ID_MANAGER_SERVICES, $infoGroup) ||
                                    in_array(ID_MANAGER_PARTS, $infoGroup) ||
                                    in_array(ID_MANAGER_TECH, $infoGroup) ||
                                    in_array(ID_MANAGER_MARKETING, $infoGroup)) {
                                    $textEditPy = 'Задача изменена менеджером ' . $infoUserWho["NAME"];
                                } else if (in_array(ID_ADMIN, $infoGroup)) {
                                    $textEditPy = 'Задача изменена администратором ' . $infoUserWho["NAME"];
                                }
                                if (!empty($textEditPy)) {
                                    $arFildsForm = [];
                                    if ($request["status"] == 24 || $request["status"] == 20) {
                                        $arFildsForm[] = ["id" => 95, "value" => [
                                            "choice_ids" => [2],
                                        ]];
                                    } elseif ($request["status"] == 23 || $request["status"] == 19) {
                                        $arFildsForm[] = ["id" => 95, "value" => [
                                            "choice_ids" => [3],
                                        ]];
                                    }
                                    if ($arFildsForm && empty($request["inperr"])) {
                                        $pyrus = new PyrusController;
                                        $editArrPy = [
                                            "id" => $infoElem["NAME"],
                                            "text" => $textEditPy,
                                            "field_updates" => $arFildsForm
                                        ];
                                        $epit_py = $pyrus->editTaskComment($editArrPy);
                                    }
                                }
                            }
                        }
                        CIBlockElement::SetPropertyValueCode($infoElem["ID"], "STATUS", $request["status"]);
                        CIBlockElement::SetPropertyValueCode($infoElem["ID"], "STATUS_ID", 2);
                        $arHistory = [
                            "id_who" => $USER->GetID(),
                            "id_that" => $request["id_dealer"],
                            "id_iblock" => $infoElem["IBLOCK_ID"],
                            "id_elem" => $infoElem["ID"],
                            "type" => "add",
                            "action" => $textAction
                        ];
                        if (!empty($arHistoryDop)) {
                            foreach ($arHistoryDop as $key => $dop) {
                                $arHistory[$key] = $dop;
                            }
                        }
                        $history = new HistoryController();
                        $history_add = $history->addHistory($arHistory);
                    }
                    if ($history_add) {
                        return Controller::sendAnswer(["pyrus" => $resp_pyrus]);
                    } else {
                        return Controller::sendError('Произошла ошибка');
                    }
                } else {
                    return Controller::sendError('Произошла ошибка');
                }
            } else {
                return Controller::sendError('Произошла ошибка');
            }
        }
    }

    public function confirmTask($request)
    {
        if (Loader::includeModule('iblock')) {
            global $USER;
            $result = false;
            if ($request["status"] && $request["id_elem"]) {
                $infoElem_db = CIBlockElement::GetByID($request["id_elem"]);
                if ($infoElem = $infoElem_db->GetNext()) {
                    $textAction = "";
                    $status_elem = CIBlockElement::GetProperty($infoElem["IBLOCK_ID"], $infoElem["ID"], array("sort" => "asc"), Array("CODE" => "STATUS"))->Fetch();
                    $new_stataus = CIBlockProperty::GetPropertyEnum("STATUS", Array(), Array("IBLOCK_ID" => $infoElem["IBLOCK_ID"], "ID" => $request["status"]))->GetNext();
                    if ($infoElem["IBLOCK_ID"] == IB_EXPLOITATION) {
                        $textAction = "Компенсации " . $infoElem["ID"] . " изменен статус с " . $status_elem["VALUE_ENUM"] . " на " . $new_stataus["VALUE"];
                    } elseif ($infoElem["IBLOCK_ID"] == IB_RECLAMATION_TECH) {
                        $textAction = "Рекламации техника " . $infoElem["NAME"] . " изменен статус с " . $status_elem["VALUE_ENUM"] . " на " . $new_stataus["VALUE"];
                    } elseif ($infoElem["IBLOCK_ID"] == IB_RECLAMATION_PARTS) {
                        $textAction = "Рекламации запчасти " . $infoElem["NAME"] . " изменен статус с " . $status_elem["VALUE_ENUM"] . " на " . $new_stataus["VALUE"];
                    }
                    $history_add = false;
                    if ($textAction) {
                        CIBlockElement::SetPropertyValueCode($infoElem["ID"], "STATUS_ID", 2);
                        CIBlockElement::SetPropertyValueCode($infoElem["ID"], "STATUS", $request["stastus"]);
                        $arHistory = [
                            "id_who" => $USER->GetID(),
                            "id_that" => $USER->GetID(),
                            "id_iblock" => $request["iblock"],
                            "id_elem" => $result,
                            "type" => "add",
                            "action" => $textAction
                        ];
                        $history = new HistoryController();
                        $history_add = $history->addHistory($arHistory);
                    }
                    if ($history_add) {
                        return Controller::sendAnswer([]);
                    } else {
                        return Controller::sendError('Произошла ошибка');
                    }
                } else {
                    return Controller::sendError('Произошла ошибка');
                }
            } else {
                return Controller::sendError('Произошла ошибка');
            }
        }
    }

    public function rejectTask($request)
    {
        if (Loader::includeModule('iblock')) {
            global $USER;
            $result = false;
        }
    }

    public function editDateTask($request)
    {
        if ($request["date"] && $request["id_elem"]) {
            $infoElem_db = CIBlockElement::GetByID($request["id_elem"]);
            if ($infoElem = $infoElem_db->GetNext()) {
                CIBlockElement::SetPropertyValueCode($infoElem["ID"], "DATE_EXECUTION", $request["date"]);
                return Controller::sendAnswer([
                    "edit_fild" => "datetext_" . $request["id_elem"],
                    "type" => "text",
                    "value" => $request["date"]
                ]);
            } else {
                return Controller::sendError('Произошла ошибка');
            }
        } else {
            return Controller::sendError('Произошла ошибка');
        }
    }
}
