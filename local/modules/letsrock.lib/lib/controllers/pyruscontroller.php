<?php

namespace Letsrock\Lib\Controllers;

/*
 * Class PyrusController
 * Контроллер api pyrus
 */

use Bitrix\Main\Loader;
use Bitrix\Main\Mail\Event;
use CIBlockElement;

class PyrusController extends Controller
{
    public function curlParse($arInitPerson, $url, $dop_header = '', $type = 'POST')
    {
        $data_string = json_encode($arInitPerson);

        $curl = curl_init($url);

        if ($dop_header) {
            $header = array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($data_string),
                $dop_header
            );
        } else {
            $header = array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($data_string)
            );
        }

        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $type);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            return ['success' => false, 'error' => $err, 'method' => 'token'];
        } elseif ($response) {
            $resp = json_decode($response);
            return ['success' => true, 'info' => $resp];
        }
    }

    public function getToken()
    {
        $arInitPerson = [
            "Login" => "#",
            "SecurityKey" => "#",
            "ClientId" => "#"
        ];
        $url_api = 'https://pyrus.com/restapi/getaccesstoken';

        $curl_api = $this->curlParse($arInitPerson, $url_api);

        if ($curl_api["success"]) {
            if ($curl_api["info"]->AccessToken)
                return ["success" => true, "token" => $curl_api["info"]->AccessToken];
        }
        return $curl_api;
    }

    public function AddTask($arFilds = false)
    {
        $token = $this::getToken();
        if ($token["success"] && $arFilds) {
            $arInitPerson = [
                "Text" => $arFilds["Text"],
                "Subject" => $arFilds["Subject"],
                "AttachmentUrls" => $arFilds["Photo"],
                "ResponsibleId" => "233149",
                "DueDate" => gmdate("Y-m-d")
            ];
            $url_api = 'https://pyrus.com/restapi/addtask';

            $curl_api = $this->curlParse($arInitPerson, $url_api, 'Authorization: Bearer ' . $token["token"], "PUT");

            if ($curl_api["success"]) {
                if (!empty($curl_api["info"]->TaskWithNotes->Id)) {
                    $this->AddComment(["id" => $curl_api["info"]->TaskWithNotes->Id]);
                } else {
                    return ["success" => false];
                }
            }
            return $curl_api;
        } else {
            echo $token;
        }
    }

    public function getTokenV4()
    {
        $arInitPerson = [
            "login" => "#",
            "security_key" => "#",
        ];
        $url_api = 'https://api.pyrus.com/v4/auth';

        $curl_api = $this->curlParse($arInitPerson, $url_api, false, 'GET');

        //pr($curl_api);
        if ($curl_api["success"]) {
            if ($curl_api["info"]->access_token)
                return ["success" => true, "token" => $curl_api["info"]->access_token];
        }
        return $curl_api;
    }

    public function AddTaskForm($arFilds = false)
    {
        $token = $this::getToken();
        if ($token["success"] && $arFilds) {
            $arInitPerson = [
                "form_id" => $arFilds["form_id"],
                "fields" => $arFilds["fields"]
            ];
            if (!empty($arFilds["responsible"]))
                $arInitPerson["responsible"] = $arFilds["responsible"];
            if (!empty($arFilds["participants"]))
                $arInitPerson["participants"] = $arFilds["participants"];

            $url_api = 'https://api.pyrus.com/v4/tasks';

            $curl_api = $this->curlParse($arInitPerson, $url_api, 'Authorization: Bearer ' . $token["token"], "POST");

            //pr($curl_api);
            if ($curl_api["success"]) {
                if (!empty($curl_api["info"]->task->id)) {
                    return ["success" => true, "id_task" => $curl_api["info"]->task->id];
                } else {
                    return ["success" => false, "error" => $curl_api["info"]->error];
                }
            }
            return $curl_api;
        } else {
            echo $token;
        }
    }

    public function uploadFile($fileArray)
    {
        $token = $this::getToken();
        $upload = $_SERVER['DOCUMENT_ROOT'] . $fileArray["SRC"];
        $mydata = array('name' => $fileArray["ORIGINAL_NAME"], 'upload' => "@" . $upload);
        //pr($mydata);

        $header = array(
            'Content-Type: multipart/form-data',
            'content-type: multipart/form-data; boundary=----8d2f42366b41441',
            'Authorization: Bearer ' . $token["token"]
        );
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://api.pyrus.com/v4/files/upload');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $mydata);

        curl_setopt($ch, CURLOPT_POSTFIELDS, [
                'name' => $fileArray["ORIGINAL_NAME"],
                'file' => curl_file_create($upload, mime_content_type($upload), basename($upload))
            ]
        );
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        $response = curl_exec($ch);
        $err = curl_error($ch);
        curl_close($ch);
        if ($err) {
            return ['success' => false, 'error' => $err, 'method' => 'token'];
        } elseif ($response) {
            $resp = json_decode($response);
            //pr($resp);
            return ['success' => true, 'guid' => $resp->guid, 'md5_hash' => $resp->md5_hash];
        }
    }

    public function AddComment($arFilds = false)
    {
        $token = $this::getToken();
        if ($token["success"] && $arFilds["id"]) {
            $arInitPerson = [
                "TaskId" => $arFilds["id"],
                "Text" => "Добавлена задача"
            ];
            $url_api = 'https://pyrus.com/restapi/addcomment';

            $curl_api = $this->curlParse($arInitPerson, $url_api, 'Authorization: Bearer ' . $token["token"], "PUT");

            if ($curl_api["success"]) {
                return ["success" => true, "id_task" => $arFilds["id"]];
            }
            return $curl_api;
        } else {
            echo $token;
        }
    }

    public function editTaskComment($arFilds = false)
    {
        $token = $this::getToken();
        if ($token["success"] && $arFilds["id"]) {
            $arInitPerson = [
                "text" => $arFilds["text"],
                "field_updates" => $arFilds["field_updates"]
            ];
            $url_api = 'https://api.pyrus.com/v4/tasks/' . $arFilds["id"] . '/comments';

            $curl_api = $this->curlParse($arInitPerson, $url_api, 'Authorization: Bearer ' . $token["token"], "POST");
            if ($curl_api["success"]) {
                return ["success" => true, "id_task" => $curl_api["info"]->task->id];
            }
            return $curl_api;
        } else {
            echo $token;
        }
    }

    public function getTask($id)
    {
        $token = $this::getToken();
        if ($token["success"]) {
            $arInitPerson = [
            ];
            $url_api = 'https://pyrus.com/restapi/v3/task/' . $id;
            //$url_api = 'https://api.pyrus.com/v4/tasks/'.$id;
            $curl_api = $this->curlParse($arInitPerson, $url_api, 'Authorization: Bearer ' . $token["token"], "GET");
            //pr($curl_api);
            if ($curl_api["success"]) {
                $status = 0;
                if (!empty($curl_api["info"])) {
                    foreach ($curl_api["info"]->TaskWithNotes->Form as $filds) {
                        if ($filds->Field->Id == 95) {
                            $status = $filds->Value;
                        }
                    }
                }
                return ["success" => true, "taskInfo" => $curl_api["info"], "status" => $status];
            }
        } else {
            echo $token;
        }
    }
}