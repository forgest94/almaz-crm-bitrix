<?php

namespace Letsrock\Lib\Controllers;

use Bitrix\Main\Loader;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

// Load Composer's autoloader
require $_SERVER["DOCUMENT_ROOT"].'/local/vendor/autoload.php';

Loader::includeModule('iblock');

/*
 * Class MailController
 * Контроллер отправки почты
 */

class MailController
{
    public function send($fildsMail)
    {
        if(!empty($fildsMail) && !empty($fildsMail["to_mail"])){
                $mail = new PHPMailer();
                $mail->IsSMTP();
                $mail->Host       = "mail.almaztd.ru";
                $mail->SMTPDebug  = 0;
                $mail->SMTPAuth   = true;
                $mail->SMTPSecure = "ssl";
                $mail->Port       = 25;
                $mail->Priority    = 3;
                $mail->CharSet     = 'UTF-8';
                $mail->ContentType = "text/html; charset=utf-8\r\n";
                $mail->Username   = "no-reply@almaztd.ru";
                $mail->Password   = 'no-reply';
                $mail->setFrom('no-reply@almaztd.ru', 'almaztd.ru');
                $mail->Subject     = $fildsMail["subject"];
                $mail->isHTML(true);
                $mail->Body = $fildsMail["html"];
                $mail->AddAddress($fildsMail["to_mail"]);
                if($mail->send()){
                    return ['success'=>true];
                }
        }
    }
}