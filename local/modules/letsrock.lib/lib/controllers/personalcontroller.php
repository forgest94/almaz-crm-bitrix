<?php

namespace Letsrock\Lib\Controllers;

use Bitrix\Main\Loader;
use Bitrix\Main\Mail\Event;
use CIBlock;
use CIBlockElement;
use CIBlockProperty;
use CUser;
use Letsrock\Lib\Models;
use Letsrock\Lib\Models\HLBlock;

/*
 * Class PersonalController
 * Контроллер личного кабинета
 */

class PersonalController extends Controller
{
    public function addProperty($iblock, $arFields, $request, $files)
    {
        $properties = CIBlockProperty::GetList(Array("sort" => "asc", "name" => "asc"), Array("ACTIVE" => "Y", "IBLOCK_ID" => $iblock));
        $infoElem = [];
        if (!empty($request['edit']))
            $infoElem = Models\Element::getById($iblock, $request['edit'], array('*'), true);
        while ($prop_fields = $properties->GetNext()) {
            if (!empty($arFields['PROPERTY'][$prop_fields["CODE"]])) continue;
            if ($prop_fields["PROPERTY_TYPE"] == 'F') {
                if (!empty($files[$prop_fields["CODE"]])) {
                    $arImage = [];
                    if ($prop_fields["MULTIPLE"] == 'N') {
                        if (!empty($files[$prop_fields["CODE"]]["name"])) {
                            $arImage = $files[$prop_fields["CODE"]];
                            $arImage['del'] = "N";
                            $arImage['MODULE_ID'] = "iblock";
                            if ($arImage)
                                $arFields['PROPERTY'][$prop_fields["CODE"]] = \CFile::SaveFile($arImage, "vote");
                        } else {
                            if (!empty($request[strtolower($prop_fields["CODE"]) . '_id'])) {
                                $arFields['PROPERTY'][$prop_fields["CODE"]] = $request[strtolower($prop_fields["CODE"]) . '_id'];
                            } else {
                                $arFields['PROPERTY'][$prop_fields["CODE"]] = 1;
                            }
                        }
                    } else {
                        $arImage = $files[$prop_fields["CODE"]];
                        /*$arImage['del'] = "N";
                        $arImage['MODULE_ID'] = "iblock";*/
                        if ($arImage) {
                            $file_parse = [];
                            $arPropsMiltyplay = [];
                            foreach ($arImage["name"] as $key => $file_name) {
                                $arNewf = [];
                                if (!empty($arImage["name"][$key])) {
                                    $arNewf = [
                                        "name" => $arImage["name"][$key],
                                        "type" => $arImage["type"][$key],
                                        "tmp_name" => $arImage["tmp_name"][$key],
                                        "size" => $arImage["size"][$key],
                                        "error" => $arImage["error"][$key]
                                    ];
                                    $arFields['PROPERTY'][$prop_fields["CODE"]][$key] = \CFile::SaveFile($arNewf, "vote");
                                } elseif (!empty($request[strtolower($prop_fields["CODE"]) . '_id'][$key])) {
                                    $arPropsMiltyplay[$key] = $request[strtolower($prop_fields["CODE"]) . '_id'][$key];
                                } else {
                                    $arPropsMiltyplay[$key] = 0;
                                }
                            }
                            if (!empty($infoElem) && !empty($arPropsMiltyplay)) {
                                $arPhotoElem = [];
                                foreach ($infoElem["PROPERTY"][$prop_fields["CODE"]]["PROPERTY_VALUE_ID"] as $key => $item) {
                                    $arPhotoElem[$item] = $infoElem["PROPERTY"][$prop_fields["CODE"]]["VALUE"][$key];
                                }
                                $resultDiff = array_diff($arPhotoElem, $arPropsMiltyplay);
                                if (!empty($resultDiff)) {
                                    foreach ($resultDiff as $key => $itemDiff) {
                                        $arFields['PROPERTY'][$prop_fields["CODE"]][$key] = ["VALUE" => ["MODULE_ID" => "iblock", "del" => "Y"]];
                                    }
                                }
                            }
                        } else {
                            if (!empty($request[strtolower($prop_fields["CODE"]) . '_id'])) {
                                foreach ($request[strtolower($prop_fields["CODE"]) . '_id'] as $idfile) {
                                    $arFields['PROPERTY'][$prop_fields["CODE"]][] = $idfile;
                                }
                            } else {
                                $arFields['PROPERTY'][$prop_fields["CODE"]] = 1;
                            }
                        }
                    }
                } elseif ($prop_fields["IS_REQUIRED"] == 'Y') {
                    if (!empty($request[strtolower($prop_fields["CODE"]) . '_id'])) {
                        $arFields['PROPERTY'][$prop_fields["CODE"]] = $request[strtolower($prop_fields["CODE"]) . '_id'];
                    } else {
                        $arFields['PROPERTY'][$prop_fields["CODE"]] = 1;
                    }
                }
            } else {
                if (!empty($request[$prop_fields["CODE"]])) {
                    $arFields['PROPERTY'][$prop_fields["CODE"]] = $request[$prop_fields["CODE"]];
                } elseif ($prop_fields["IS_REQUIRED"] == 'Y') {
                    $arFields['PROPERTY'][$prop_fields["CODE"]] = 'N';
                }
            }
        }
        return $arFields;
    }

    public function set($request)
    {
        if (Loader::includeModule('iblock')) {
            global $USER;
            $result = false;
            $resultAddUser = false;
            $actionText = '';
            $errorInn = '';
            $workUser = \Letsrock\Lib\Models\User::getWork();
            if (!empty($request['edit'])) {
                $isNumber = Models\Element::getList(IB_EXPLOITATION, ['ID', 'IBLOCK_ID', 'PROPERTY_*'],
                    [
                        'PROPERTY_NUMBER' => $request['number'],
                        'PROPERTY_DEALER' => $USER->GetID(),
                        '!ID' => $request['edit'],
                        '!NAME' => 'Техника не введена в эксплуатацию'
                    ]);
            } else {
                $isNumber = Models\Element::getList(IB_EXPLOITATION, ['ID', 'IBLOCK_ID', 'PROPERTY_*'],
                    [
                        'PROPERTY_NUMBER' => $request['number'],
                        'PROPERTY_DEALER' => $USER->GetID(),
                        '!NAME' => 'Техника не введена в эксплуатацию'
                    ]);
            }
            if (!empty($isNumber["ELEMS"][0])) {
                $errorNumb = [
                    $isNumber,
                    $request
                ];
                file_put_contents($_SERVER["DOCUMENT_ROOT"] . '/csv/error_number.txt', print_r($errorNumb, true), FILE_APPEND);
                return Controller::sendError('Заводской номер уже используется');
            }
            if (!empty($request['name']) && empty($isNumber)) {
                $name = $request['name'];
                $arFields['PROPERTY'] = [
                    'INN' => !empty(intval($request['inn'])) ? intval($request['inn']) : 0,
                    'DILER' => $USER->GetID(),
                    'BIN' => !empty($request['BIN']) ? $request['BIN'] : 0,
                    'COUNTRY' => $request['COUNTRY'],
                    'REGION' => $request['region'],
                    'DISTRICT' => $request['area'],
                    'ADDRESS' => $request['address'],
                    'PHONE' => $request['phone'],
                    'EMAIL' => $request['email'],
                    'CONTACT_PEOPLE' => $request['contact_person'],
                    'POSITION' => $request['position']
                ];

                $resultAddUser = Models\Element::add(IB_BUYERS, $name, $arFields, true);

                if (empty($request['BIN'])) {
                    //Проверка ИНН и региона
                    //При несоответствии уведомление менеджеру
                    $arRegionsIds = [];
                    foreach ($workUser["UF_REGIONS_USER"] as $region) {
                            $fildRegion = Models\Element::getById(IB_REGION, $region);
                            if (!empty($fildRegion["ID"])) {
                                $arRegionsIds[] = $fildRegion["ID"];
                            }
                    }

                    $arSelect = [
                        "ID",
                        "NAME",
                        "PROPERTY_INN"
                    ];

                    $arFilter = [
                        "IBLOCK_ID" => IntVal(IB_REGION),
                        "ACTIVE" => "Y",
                        "ID" => $arRegionsIds,
                        "PROPERTY_INN" => substr(intval($request['inn']), 0, 2)
                    ];

                    $res = CIBlockElement::GetList(["SORT" => "ASC"], $arFilter, false, ["nPageSize" => 50], $arSelect);
                    $arFields = [];

                    while ($ob = $res->GetNextElement()) {
                        $arFields[] = $ob->GetFields();
                    }


                    if (empty($arFields) || $arFields[0]['ID'] != $request['region']) {
                        $name = "Несоответствие";
                        $arFields['PROPERTY'] = [
                            'USER' => $USER->GetID(),
                            'REGION' => $request['region'],
                            'INN' => $request['inn'],
                            'BUYER' => $resultAddUser ? $resultAddUser : $request['buyer']
                        ];

                        $result = Models\Element::add(IB_WARNING, $name, $arFields, true);

                        $regionName = !empty($arFields) ? $arFields[0]['NAME'] : 'Неизвестный регион';

                        /*$res = Event::send([
                            "EVENT_NAME" => "INN_WRONG",
                            "LID" => "s1",
                            "C_FIELDS" => [
                                "REGION" => $regionName,
                                "INN" => $request['inn'],
                                "USER" => $USER->GetFormattedName() . '(' . $USER->GetID() . ')'
                            ],
                        ]);*/
                        $errorInn = 'Y';
                    }
                }
            }

            if (($resultAddUser || $request['buyer']) && empty($isNumber)) {
                $name = "Ввод техники в эксплуатацию";
                $arFields['PROPERTY'] = [
                    'EQUIPMENT' => $request['type'],
                    'MODEL' => $request['model'],
                    'NUMBER' => $request['number'],
                    'DATE' => $request['date'],
                    'PHONE_PERSONAL' => $request['PHONE_PERSONAL'],
                    'BUYER' => $resultAddUser ? $resultAddUser : $request['buyer'],
                    'DEALER' => $USER->GetID(),
                    'STATUS' => 13,
                    'STATUS_ID' => 1
                ];
                $arFields = $this->addProperty(IB_EXPLOITATION, $arFields, $request, $_FILES);
                if (!empty($request['edit'])) {
                    $arFields["NAME"] = $name;
                    $result = Models\Element::update($request['edit'], $arFields);
                    $actionText = 'Изменен элемент №' . $request['edit'];
                } else {
                    $result = Models\Element::add(IB_EXPLOITATION, $name, $arFields, true);
                    $actionText = 'Техника введена в эксплуатацию';
                }
            }

            if ($result && $actionText) {
                $infoManager = [];
                if ($workUser["UF_MANAGER_NEW"]) {
                    foreach ($workUser["UF_MANAGER_NEW"] as $manager) {
                        $infoGroup = \CUser::GetUserGroup($manager);
                        if (in_array(ID_MANAGER_SERVICES, $infoGroup)) {
                            $userInfo = \CUser::GetByID($manager)->GetNext();
                            if (!empty($userInfo))
                                $infoManager = $userInfo;
                        } else {
                            continue;
                        }
                    }
                }
                $idBuyer = $resultAddUser ? $resultAddUser : $request['buyer'];
                $arBuyers_db = Models\Element::GetList(IB_BUYERS, ["NAME", "ID", "IBLOCK_ID", "PROPERTY_*"], [
                    "PROPERTY_DILER" => $USER->GetID(),
                    "ID" => $idBuyer
                ], ["ID" => "desc"]);
                if (!empty($arBuyers_db["ELEMS"][0])) {
                    $fildBuyer = $arBuyers_db["ELEMS"][0];
                    $arPropsBuyer = $arBuyers_db["PROPS"][$fildBuyer["ID"]];
                    $infoRegion = Models\Element::getById(IB_REGION, $arPropsBuyer["REGION"]["VALUE"]);
                    $infoDistrict = Models\Element::getById(IB_AREA, $arPropsBuyer["DISTRICT"]["VALUE"]);
                    $infoModel = Models\Element::getById(IB_DIRECTORY, $request['model']);
                    $typeTechHl = new HLBlock(HL_EQUIPMENT);
                    $infoType = $typeTechHl->get(["UF_XML_ID" => $request['type']])[0];
                }
                $arHistory = [
                    "id_who" => $USER->GetID(),
                    "id_that" => $USER->GetID(),
                    "id_iblock" => IB_EXPLOITATION,
                    "id_elem" => $result,
                    "type" => "add",
                    "action" => $actionText,
                ];
                /*if(!empty($errorInn) && !empty($arBuyers_db["ELEMS"][0]) && $infoManager["EMAIL"]){
                    $mailer = new MailController();
                    $arMail = [
                        "to_mail" => $infoManager["EMAIL"],//kkv@almaztd.ru
                        "subject" => "Постановка техники на гарантию, ИНН не совпадает",
                        "html" => '
                            <p>В личном кабинете дилера поставлена на гарантию следующая техника:<br/>
                            Дилер: '.$workUser["WORK_COMPANY"].'<br/>
                            Техника: '.$infoType["UF_NAME"].' '.$infoModel["NAME"].', номер '.$request['number'].'<br/>
                            Дата ввода в эксплуатацию: '.$request['date'].'</p>

                            <p>Покупатель: '.$fildBuyer["NAME"].', '.$infoRegion["NAME"].', '.$infoDistrict["NAME"].', '.$arPropsBuyer["ADDRESS"]["VALUE"].'<br/>
                            ИНН: '.$arPropsBuyer["INN"]["VALUE"].'<br/>
                            Контактное лицо: '.$arPropsBuyer["CONTACT_PEOPLE"]["VALUE"].', '.$arPropsBuyer["PHONE"]["VALUE"].', '.$arPropsBuyer["EMAIL"]["VALUE"].'</p>

                            <p style="background-color: #ffe9bd;">*** Внимание! Указанный ИНН покупателя не совпадает с закрепленным за дилером регионом! Пожалуйста, проверьте фактическое расположение покупателя.</p>
                            ',

                    ];
                    if(!empty($arMail))
                        $mailer->send($arMail);
                }*/
                if ($infoManager["EMAIL"] && !empty($arBuyers_db["ELEMS"][0])) {
                    $arHistory["subject_mail"] = "Постановка техники на гарантию";
                    $arHistory["to_mail"] = $infoManager["EMAIL"];
                    $arHistory["html_mail"] = "
                            <p>
                            Поступила заявка на постановку техники на гарантию<br/>
                            Дилер: " . $workUser["WORK_COMPANY"] . "<br/>
                            Техника: " . $infoType["UF_NAME"] . " " . $infoModel["NAME"] . ", номер " . $request['number'] . "<br/>
                            Дата ввода в эксплуатацию: " . $request['date'] . "</p>

                            <p>Покупатель: " . $fildBuyer["NAME"] . ", " . $infoRegion["NAME"] . ", " . $infoDistrict["NAME"] . ", " . $arPropsBuyer["ADDRESS"]["VALUE"] . "<br/>
                            ИНН: " . $arPropsBuyer["INN"]["VALUE"] . "<br/>
                            Контактное лицо: " . $arPropsBuyer["CONTACT_PEOPLE"]["VALUE"] . ", " . $arPropsBuyer["PHONE"]["VALUE"] . ", " . $arPropsBuyer["EMAIL"]["VALUE"] . "</p>

                            <p>Пожалуйста, войдите в личный кабинет по адресу my.almaztd.ru и проверьте приложенные дилером документы.</p>
                        ";
                }
                $history = new HistoryController();
                $history_add = $history->addHistory($arHistory);
                if ($history_add) {
                    return Controller::sendAnswer([]);
                } else {
                    return Controller::sendError('Ошибка записи в историю');
                }
            } else {
                return Controller::sendError('Ошибка добавления элемента');
            }
        }
    }


    public function addMark($request)
    {
        if (Loader::includeModule('iblock')) {
            global $USER;
            $result = false;
            $resultAddUser = false;

            if (!$USER->GetID())
                return Controller::sendError('Произошла ошибка');

            $arFields['PROPERTY'] = [
                'TYPE_KOMP' => $request['TYPE_KOMP'],
                'PHONE_PERSONAL' => $request['PHONE_PERSONAL'],
                'DILER' => $USER->GetID(),
                'STATUS' => 1,
                'STATUS_ID' => 1,
                'DATE' => date('d.m.Y'),
            ];
            $arFields = $this->addProperty(IB_MARKETING, $arFields, $request, $_FILES);
            /*$arPropsMiltyplay = [];
            $properties = CIBlockProperty::GetList(Array("sort" => "asc", "name" => "asc"), Array("ACTIVE" => "Y", "IBLOCK_ID" => IB_MARKETING));
            while ($prop_fields = $properties->GetNext()) {
                if ($prop_fields["PROPERTY_TYPE"] == 'F') {
                    if (!empty($_FILES[$prop_fields["CODE"]])) {
                        $arImage = [];
                        if ($prop_fields["MULTIPLE"] == 'N') {
                            $arImage = $_FILES[$prop_fields["CODE"]];
                            $arImage['del'] = "N";
                            $arImage['MODULE_ID'] = "iblock";
                            if ($arImage)
                                $arFields['PROPERTY'][$prop_fields["CODE"]] = \CFile::SaveFile($arImage, "vote");
                        } else {
                            $arImage = $_FILES[$prop_fields["CODE"]];
                            $arImage['del'] = "N";
                            $arImage['MODULE_ID'] = "iblock";
                            if ($arImage) {
                                $file_parse = [];
                                foreach ($arImage["name"] as $key => $file_name) {
                                    if (!empty($arImage["name"][$key]))
                                        $file_parse[] = array(
                                            'VALUE' => [
                                                "name" => $arImage["name"][$key],
                                                "type" => $arImage["type"][$key],
                                                "tmp_name" => $arImage["tmp_name"][$key],
                                                "size" => $arImage["error"][$key]
                                            ],
                                            'DESCRIPTION' => '');
                                }
                                if (!empty($file_parse))
                                    $arFields['PROPERTY'][$prop_fields["CODE"]] = $file_parse;
                            }
                        }
                    } elseif ($prop_fields["IS_REQUIRED"] == 'Y') {
                        $arFields['PROPERTY'][$prop_fields["CODE"]] = 1;
                    }
                    if (!empty($request[strtolower($prop_fields["CODE"]) . '_id'])) {
                        if ($prop_fields["MULTIPLE"] == 'N') {
                            $arFields['PROPERTY'][$prop_fields["CODE"]] = $request[strtolower($prop_fields["CODE"]) . '_id'];
                        } else {
                            $arPropsMiltyplay[$prop_fields["CODE"]] = $request[strtolower($prop_fields["CODE"]) . '_id'];
                        }
                    }
                } else {
                    if (!empty($request[$prop_fields["CODE"]])) {
                        $arFields['PROPERTY'][$prop_fields["CODE"]] = $request[$prop_fields["CODE"]];
                    } elseif ($prop_fields["IS_REQUIRED"] == 'Y') {
                        $arFields['PROPERTY'][$prop_fields["CODE"]] = 'N';
                    }
                }
            }*/
            $actionText = '';
            if ($arFields) {
                if (!empty($request['edit'])) {
                    $infoElem = Models\Element::getById(IB_MARKETING, $request['edit'], array('*'), true);
                    if ($infoElem) {
                        if (!empty($arPropsMiltyplay)) {
                            foreach ($arPropsMiltyplay as $code => $ids) {
                                $arPhotoElem = [];
                                foreach ($infoElem["PROPERTY"][$code]["PROPERTY_VALUE_ID"] as $key => $item) {
                                    $arPhotoElem[$item] = $infoElem["PROPERTY"][$code]["VALUE"][$key];
                                }
                                $resultDiff = array_diff($arPhotoElem, $ids);
                                if (!empty($resultDiff)) {
                                    foreach ($resultDiff as $key => $itemDiff) {
                                        $arFields['PROPERTY'][$code][$key] = ["VALUE" => ["MODULE_ID" => "iblock", "del" => "Y"]];
                                    }
                                }
                            }
                        }
                        $arFields["NAME"] = $infoElem["NAME"];
                        $result = Models\Element::update($request['edit'], $arFields);
                        $actionText = 'Изменен элемент №' . $request['edit'];
                    }
                } else {
                    $number = "M" . rand();

                    $isElem = Models\Element::getList(IB_MARKETING, array(), array("NAME" => $number));

                    if ($isElem["ID"]) {
                        $number = $number + 1;
                    }
                    $result = Models\Element::add(IB_MARKETING, $number, $arFields, true);
                    $actionText = 'Документы предоставлены';
                }
            }

            if ($result && $actionText) {
                $infoManager = [];
                $workUser = \Letsrock\Lib\Models\User::getWork();
                if ($workUser["UF_MANAGER_NEW"]) {
                    foreach ($workUser["UF_MANAGER_NEW"] as $manager) {
                        $infoGroup = \CUser::GetUserGroup($manager);
                        if (in_array(ID_MANAGER_MARKETING, $infoGroup)) {
                            $userInfo = \CUser::GetByID($manager)->GetNext();
                            if (!empty($userInfo))
                                $infoManager = $userInfo;
                        } else {
                            continue;
                        }
                    }
                }
                $arHistory = [
                    "id_who" => $USER->GetID(),
                    "id_that" => $USER->GetID(),
                    "id_iblock" => IB_MARKETING,
                    "id_elem" => $result,
                    "type" => "add",
                    "action" => $actionText
                ];
                if (!empty($infoManager)) {
                    $arHistory["subject_mail"] = "Личный кабинет дилера Алмаз";
                    $arHistory["to_mail"] = $infoManager["EMAIL"];
                    $arHistory["html_mail"] = "
                                <p>
                                В личном кабинете дилера поданы документы на компенсацию маркетинговых затрат<br/>
                                Дилер: " . $workUser["WORK_COMPANY"] . "
                                </p>
                    ";
                }
                $history = new HistoryController();
                $history_add = $history->addHistory($arHistory);
                if ($history_add) {
                    return Controller::sendAnswer([]);
                } else {
                    return Controller::sendError('Произошла ошибка');
                }
            } else {
                return Controller::sendError('Произошла ошибка');
            }
        }
    }

    public function addService($request)
    {
        if (Loader::includeModule('iblock')) {
            global $USER;
            $result = false;
            $resultAddUser = false;

            if (!$USER->GetID())
                return Controller::sendError('Произошла ошибка');

            if (empty($request['id_explotation'][0]))
                return Controller::sendError('Произошла ошибка', 'SERIAL_NUMBER[]');

            if ($request['TYPE_SERVICE'] == 8 && empty($request['edit'])) {
                $isService = [];
                foreach ($request['id_explotation'] as $expl) {
                    $isService = Models\Element::getList(IB_SERVCOMP, ['*'], [
                        'PROPERTY_DILER' => $USER->GetID(),
                        'PROPERTY_SERIAL_NUMBER' => $expl,
                        'PROPERTY_TYPE_SERVICE' => $request['TYPE_SERVICE']
                    ]);
                }
                if (!empty($isService["ELEMS"]))
                    return Controller::sendError('Компенсация по технике уже подавалась');
            }

            $arFields['PROPERTY'] = [
                'TYPE_SERVICE' => $request['TYPE_SERVICE'],
                'PHONE_PERSONAL' => $request['PHONE_PERSONAL'],
                'DILER' => $USER->GetID(),
                'STATUS' => 10,
                'STATUS_ID' => 1,
                'DATE' => date('d.m.Y'),
                'SERIAL_NUMBER' => $request['id_explotation'],
                'RECLAMATION_TECH' => $request['id_reclamation'] ? $request['id_reclamation'] : 'N'
            ];
            $arFields = $this->addProperty(IB_SERVCOMP, $arFields, $request, $_FILES);
            $actionText = '';
            if ($arFields) {
                if (!empty($request['edit'])) {
                    $infoElem = Models\Element::getById(IB_SERVCOMP, $request['edit'], array('*'), true);
                    if ($infoElem) {
                        $arPhotoElem = [];
                        foreach ($infoElem["PROPERTY"]["INVOICE_PAYMENT"]["PROPERTY_VALUE_ID"] as $key => $item) {
                            $arPhotoElem[$item] = $infoElem["PROPERTY"]["INVOICE_PAYMENT"]["VALUE"][$key];
                        }
                        $arPh = [];
                        if (!empty($request["invoice_payment_id"])) {
                            foreach ($request["invoice_payment_id"] as $ph) {
                                $arPh[] = $ph;
                            }
                        }
                        $resultDiff = array_diff($arPhotoElem, $arPh);
                        if (!empty($resultDiff)) {
                            foreach ($resultDiff as $key => $itemDiff) {
                                $arFields['PROPERTY']['INVOICE_PAYMENT'][$key] = ["VALUE" => ["MODULE_ID" => "iblock", "del" => "Y"]];
                            }
                        }
                        $arFields["NAME"] = $infoElem["NAME"];
                        $result = Models\Element::update($request['edit'], $arFields);
                        $actionText = 'Изменен элемент №' . $request['edit'];
                    }
                } else {
                    $number = "S" . rand();

                    $isElem = Models\Element::getList(IB_SERVCOMP, array(), array("NAME" => $number));

                    if ($isElem["ID"]) {
                        $number = $number + 1;
                    }
                    $result = Models\Element::add(IB_SERVCOMP, $number, $arFields, true);
                    $actionText = 'Документы предоставлены';
                }
            }


            if ($result && $actionText) {
                $infoManager = [];
                $workUser = \Letsrock\Lib\Models\User::getWork();
                if ($workUser["UF_MANAGER_NEW"]) {
                    foreach ($workUser["UF_MANAGER_NEW"] as $manager) {
                        $infoGroup = \CUser::GetUserGroup($manager);
                        if (in_array(ID_MANAGER_SERVICES, $infoGroup)) {
                            $userInfo = \CUser::GetByID($manager)->GetNext();
                            if (!empty($userInfo))
                                $infoManager = $userInfo;
                        } else {
                            continue;
                        }
                    }
                }
                $arHistory = [
                    "id_who" => $USER->GetID(),
                    "id_that" => $USER->GetID(),
                    "id_iblock" => IB_SERVCOMP,
                    "id_elem" => $result,
                    "type" => "add",
                    "action" => $actionText
                ];
                if (!empty($infoManager)) {
                    $arHistory["subject_mail"] = "Личный кабинет дилера Алмаз";
                    $arHistory["to_mail"] = $infoManager["EMAIL"];
                    $arHistory["html_mail"] = "
                                <p>
                                В личном кабинете дилера поданы документы на компенсацию сервисных затрат<br/>
                                Дилер: " . $workUser["WORK_COMPANY"] . "
                                </p>
                    ";
                }
                $history = new HistoryController();
                $history_add = $history->addHistory($arHistory);
                if ($history_add) {
                    return Controller::sendAnswer([]);
                } else {
                    return Controller::sendError('Произошла ошибка');
                }
            } else {
                return Controller::sendError('Произошла ошибка');
            }
        }
    }

    public function addParts($request)
    {
        if (Loader::includeModule('iblock')) {
            global $USER;
            $result = false;
            $resultAddUser = false;

            if (!$USER->GetID())
                return Controller::sendError('Произошла ошибка');

            $arFields['PROPERTY'] = [
                'DEALER' => $USER->GetID(),
                'PHONE_PERSONAL' => $request['PHONE_PERSONAL'],
                'STATUS' => 21,
                'STATUS_ID' => 1,
                'REASON' => $request["REASON"],
                'WISHES' => $request["WISHES"],
                'DATE' => date('d.m.Y'),
            ];
            if (!empty($request["INVOICE"])) {
                foreach ($request["INVOICE"] as $inv) {
                    if (!empty($inv))
                        $arFields['PROPERTY']['INVOICE'][] = $inv;
                }
                if (empty($arFields['PROPERTY']['INVOICE']))
                    $arFields['PROPERTY']['INVOICE'] = 'N';
            } else {
                $arFields['PROPERTY']['INVOICE'] = 'N';
            }
            if (!empty($request["CONSIGNMENT"])) {
                foreach ($request["CONSIGNMENT"] as $cons) {
                    if (!empty($cons))
                        $arFields['PROPERTY']['CONSIGNMENT'][] = $cons;
                }
                if (empty($arFields['PROPERTY']['CONSIGNMENT']))
                    $arFields['PROPERTY']['CONSIGNMENT'] = 'N';
            } else {
                $arFields['PROPERTY']['CONSIGNMENT'] = 'N';
            }

            $arFields = $this->addProperty(IB_RECLAMATION_PARTS, $arFields, $request, $_FILES);
            $actionText = '';
            if ($arFields) {
                if (!empty($request['edit'])) {
                    $infoElem = Models\Element::getById(IB_RECLAMATION_PARTS, $request['edit'], array('*'), true);
                    if ($infoElem) {
                        $arPhotoElem = [];
                        foreach ($infoElem["PROPERTY"]["PHOTO"]["PROPERTY_VALUE_ID"] as $key => $item) {
                            $arPhotoElem[$item] = $infoElem["PROPERTY"]["PHOTO"]["VALUE"][$key];
                        }
                        $arPh = [];
                        if (!empty($request["photo_id"])) {
                            foreach ($request["photo_id"] as $ph) {
                                $arPh[] = $ph;
                            }
                        }
                        $resultDiff = array_diff($arPhotoElem, $arPh);
                        if (!empty($resultDiff)) {
                            foreach ($resultDiff as $key => $itemDiff) {
                                $arFields['PROPERTY']['PHOTO'][$key] = ["VALUE" => ["MODULE_ID" => "iblock", "del" => "Y"]];
                            }
                        }
                        $arFields["NAME"] = $infoElem["NAME"];
                        $result = Models\Element::update($request['edit'], $arFields);
                        $actionText = 'Изменен элемент №' . $request['edit'];
                    }
                } else {

                    $number = "RP" . rand();

                    $isElem = Models\Element::getList(IB_RECLAMATION_PARTS, array(), array("NAME" => $number));

                    if ($isElem["ID"]) {
                        $number = $number + 1;
                    }

                    $result = Models\Element::add(IB_RECLAMATION_PARTS, $number, $arFields, true);
                    $actionText = 'Подана рекламация';
                }
            }

            if ($result && $actionText) {
                /* pyrus */
                $infoManager = [];
                $workUser = \Letsrock\Lib\Models\User::getWork();
                if ($workUser["UF_MANAGER_NEW"]) {
                    foreach ($workUser["UF_MANAGER_NEW"] as $manager) {
                        $userInfo = \CUser::GetByID($manager)->GetNext();
                        $infoGroup = \CUser::GetUserGroup($manager);
                        if ($userInfo && in_array(ID_MANAGER_SERVICES, $infoGroup)) {
                            $infoManager = $userInfo;
                        } else {
                            continue;
                        }
                    }
                }
                if (!empty($infoManager)) {
                    $elemIbl = new Models\Element;
                    $typeTechHl = new HLBlock(HL_EQUIPMENT);
                    $protocol = stripos($_SERVER['SERVER_PROTOCOL'], 'https') === true ? 'https://' : 'http://';
                    $site_addr = $protocol . $_SERVER['HTTP_HOST'];
                    $buyer = '';
                    $id_model = '';
                    $id_type = '';
                    $number = '';
                    $text = '';
                    $arFildsForm = [];

                    $sf = '';
                    if (!empty($request["INVOICE"][0])) {
                        foreach ($request["INVOICE"] as $invo) {
                            $sf .= 'С-ф: ' . $invo . '; ';
                        }
                    }
                    $ttn = '';
                    if (!empty($request["CONSIGNMENT"][0])) {
                        foreach ($request["CONSIGNMENT"] as $consig) {
                            $ttn .= 'ТТН: ' . $consig . '; ';
                        }
                    }
                    if (!empty($ttn) || !empty($sf)) {
                        $arFildsForm[] = ["id" => 92, "value" => $sf . $ttn];
                    }
                    if (!empty($request["REASON"])) {
                        $arFildsForm[] = ["id" => 86, "value" => $request["REASON"]];
                    }
                    if (!empty($request["WISHES"])) {
                        $arFildsForm[] = ["id" => 11, "value" => $request["WISHES"]];
                    }
                    $arAkt = [];
                    /*if (!empty($_FILES["AKT"])) {
                        $propAkt_db = CIBlockElement::GetProperty(IB_RECLAMATION_PARTS, $result, "sort", "asc", array("CODE" => "AKT"));
                        if ($atk_find = $propAkt_db->GetNext())
                        {
                            $arAkt = \CFile::GetFileArray($atk_find["VALUE"]);
                        }
                    }*/
                    if (!empty($_FILES["AKT"])) {
                        $propPhoto_db = CIBlockElement::GetProperty(IB_RECLAMATION_PARTS, $result, "sort", "asc", array("CODE" => "AKT"));
                        while ($elem = $propPhoto_db->GetNext()) {
                            $arAkt[] = \CFile::GetFileArray($elem["VALUE"]);
                        }
                    }
                    $arPhoto = [];
                    if (!empty($_FILES["PHOTO"])) {
                        $propPhoto_db = CIBlockElement::GetProperty(IB_RECLAMATION_PARTS, $result, "sort", "asc", array("CODE" => "PHOTO"));
                        while ($elem = $propPhoto_db->GetNext()) {
                            $arPhoto[] = \CFile::GetFileArray($elem["VALUE"]);
                        }
                    }
                    $arPyrus = [];
                    $pyrus = new PyrusController;
                    if ($workUser["WORK_COMPANY"] && !empty($arFildsForm)) {
                        $dealerText = $workUser["WORK_COMPANY"] . ' - ' . $workUser["LAST_NAME"] . " " . $workUser["NAME"] . " " . $workUser["SECOND_NAME"] . ", " . $workUser["PERSONAL_PHONE"];
                        $arFildsForm[] = ["id" => 87, "value" => $dealerText];
                        $arFildsForm[] = ["id" => 2, "value" => date('Y-m-d')];
                        if (!empty($arAkt)) {
                            $fileUnloadMore = [];
                            foreach ($arAkt as $item) {
                                $fileFind = $pyrus->uploadFile($item);
                                if ($fileFind["success"] && $fileFind["guid"]) {
                                    $fileUnloadMore[] = $pyrus->uploadFile($item);
                                }
                            }
                            if (!empty($fileUnloadMore))
                                $arFildsForm[] = ["id" => 13, "value" => $fileUnloadMore];
                        }
                        if (!empty($arPhoto)) {
                            $fileUnloadMore = [];
                            foreach ($arPhoto as $item) {
                                $fileFind = $pyrus->uploadFile($item);
                                if ($fileFind["success"] && $fileFind["guid"]) {
                                    $fileUnloadMore[] = $pyrus->uploadFile($item);
                                }
                            }
                            if (!empty($fileUnloadMore))
                                $arFildsForm[] = ["id" => 99, "value" => $fileUnloadMore];
                        }

                        if (!empty($infoManager["UF_PYRUS_ID"])) {
                            $arFildsForm[] = ["id" => 77, "value" =>
                                [
                                    "id" => $infoManager["UF_PYRUS_ID"]
                                ]
                            ];
                        }
                        $arPyrus = [
                            "form_id" => 695567,
                            "fields" => $arFildsForm
                        ];
                    }
                    if ($arPyrus) {
                        $resp_pyrus = [];
                        if (!empty($request['edit'])) {
                            $elemEdit = Models\Element::getList(IB_RECLAMATION_PARTS, array("NAME"), array("ID" => $result))["ELEMS"][0];
                            if ($elemEdit && strpos('---- ' . $elemEdit["NAME"], 'RP') === false &&
                                strpos('---- ' . $elemEdit["NAME"], 'RT') === false) {
                                $arFildsForm[] = ["id" => 95, "value" => [
                                    "choice_ids" => [1],
                                ]];
                                $editArrPy = [
                                    "id" => $elemEdit["NAME"],
                                    "text" => "Задача изменена дилером " . $workUser["WORK_COMPANY"],
                                    "field_updates" => $arFildsForm
                                ];
                                $resp_pyrus = $pyrus->editTaskComment($editArrPy);
                            }
                        } else {
                            $resp_pyrus = $pyrus->AddTaskForm($arPyrus);
                        }

                        if (!empty($resp_pyrus) && $resp_pyrus["success"]) {
                            CIBlockElement::SetPropertyValueCode($result, "STATUS", 22);
                            Models\Element::update($result, ["NAME" => $resp_pyrus["id_task"]]);
                        }
                    }
                }
                /* endpyrus */

                $arHistory = [
                    "id_who" => $USER->GetID(),
                    "id_that" => $USER->GetID(),
                    "id_iblock" => IB_RECLAMATION_PARTS,
                    "id_elem" => $result,
                    "type" => "add",
                    "action" => $actionText
                ];
                $history = new HistoryController();
                $history_add = $history->addHistory($arHistory);
                if ($history_add) {
                    return Controller::sendAnswer([]);
                } else {
                    return Controller::sendError('Произошла ошибка');
                }
            } else {
                return Controller::sendError('Произошла ошибка');
            }
        }
    }

    public function addTech($request)
    {
        if (Loader::includeModule('iblock')) {
            global $USER;
            $result = false;
            $resultAddUser = false;
            $arFields2 = false;

            if (!$USER->GetID())
                return Controller::sendError('Произошла ошибка');

            $arFields['PROPERTY'] = [
                'DEALER' => $USER->GetID(),
                'STATUS' => 17,
                'STATUS_ID' => 1,
                'REASON' => $request["REASON"],
                'PHONE_PERSONAL' => $request['PHONE_PERSONAL'],
                'SERIAL_NUMBER' => $request["id_explotation"],
                'WISHES' => $request["WISHES"],
                'DATE' => date('d.m.Y'),
            ];
            if (!empty($request["id_explotation"][0])) {
                $arFields['PROPERTY']['SERIAL_NUMBER'] = $request["id_explotation"][0];
            } else {
                $nameTech = "Техника не введена в эксплуатацию";
                $arFields2['PROPERTY'] = [
                    'EQUIPMENT' => $request['TYPE_TECH'],
                    'MODEL' => $request['MODELS_TECH'],
                    'NUMBER' => $request['NUMBER_TECH'],
                    'DEALER' => $USER->GetID(),
                    'STATUS' => 13
                ];
                $result = Models\Element::add(IB_EXPLOITATION, $nameTech, $arFields2, true);
                if ($result) {
                    $arFields['PROPERTY']['SERIAL_NUMBER'] = $result;
                }
                $arFields['PROPERTY']['NOT_OPERATION'] = 16;
            }

            $arFields = $this->addProperty(IB_RECLAMATION_TECH, $arFields, $request, $_FILES);
            $actionText = '';
            if ($arFields) {
                if (!empty($request['edit'])) {
                    $infoElem = Models\Element::getById(IB_RECLAMATION_TECH, $request['edit'], array('*'), true);
                    if ($infoElem) {
                        $arPhotoElem = [];
                        foreach ($infoElem["PROPERTY"]["PHOTO"]["PROPERTY_VALUE_ID"] as $key => $item) {
                            $arPhotoElem[$item] = $infoElem["PROPERTY"]["PHOTO"]["VALUE"][$key];
                        }
                        $arPh = [];
                        if (!empty($request["photo_id"])) {
                            foreach ($request["photo_id"] as $ph) {
                                $arPh[] = $ph;
                            }
                        }
                        $resultDiff = array_diff($arPhotoElem, $arPh);
                        if (!empty($resultDiff)) {
                            foreach ($resultDiff as $key => $itemDiff) {
                                $arFields['PROPERTY']['PHOTO'][$key] = ["VALUE" => ["MODULE_ID" => "iblock", "del" => "Y"]];
                            }
                        }
                        $arFields["NAME"] = $infoElem["NAME"];
                        $result = Models\Element::update($request['edit'], $arFields);
                        $actionText = 'Изменен элемент №' . $request['edit'];
                    }
                } else {

                    $number = "RT" . rand();

                    $isElem = Models\Element::getList(IB_RECLAMATION_TECH, array(), array("NAME" => $number));

                    if ($isElem["ID"]) {
                        $number = $number + 1;
                    }
                    $result = Models\Element::add(IB_RECLAMATION_TECH, $number, $arFields, true);
                    $actionText = 'Подана рекламация';
                }
            }
            if ($result && $actionText) {

                /* pyrus */
                $infoManager = [];
                $workUser = \Letsrock\Lib\Models\User::getWork();
                if ($workUser["UF_MANAGER_NEW"]) {
                    foreach ($workUser["UF_MANAGER_NEW"] as $manager) {
                        $userInfo = \CUser::GetByID($manager)->GetNext();
                        $infoGroup = CUser::GetUserGroup($manager);
                        if ($userInfo && in_array(ID_MANAGER_SERVICES, $infoGroup)) {
                            $infoManager = $userInfo;
                        } else {
                            continue;
                        }
                    }
                }
                if (!empty($infoManager)) {
                    $elemIbl = new Models\Element;
                    $typeTechHl = new HLBlock(HL_EQUIPMENT);
                    $protocol = stripos($_SERVER['SERVER_PROTOCOL'], 'https') === true ? 'https://' : 'http://';
                    $site_addr = $protocol . $_SERVER['HTTP_HOST'];
                    $buyer = '';
                    $buyer2 = '';
                    $id_model = '';
                    $id_type = '';
                    $number = '';
                    $text = '';
                    $techText = '';
                    $arFildsForm = [];
                    if ($request["id_explotation"][0]) {
                        $arExplotation = $elemIbl->GetList(IB_EXPLOITATION, ["NAME", "ID", "IBLOCK_ID", "PROPERTY_*"], ["ID" => $request["id_explotation"][0]]);
                        $arPropsEx = $arExplotation["PROPS"][$arExplotation["ELEMS"][0]["ID"]];
                        $infoBuyer = $elemIbl->GetList(IB_BUYERS, ["NAME", "ID", "IBLOCK_ID", "PROPERTY_*"], ["ID" => $arPropsEx["BUYER"]["VALUE"], "!PROPERTY_TYPE_VALUE" => false]);
                        $arPropsBuyer = $infoBuyer["PROPS"][$infoBuyer["ELEMS"][0]["ID"]];
                        $id_model = $arPropsEx["MODEL"]["VALUE"];
                        $id_type = $arPropsEx["EQUIPMENT"]["VALUE"];
                        $number = $arPropsEx["NUMBER"]["VALUE"];
                        //$buyer = 'Информация о покупателе:'.PHP_EOL;
                        $buyer = 'Название организации: ' . $infoBuyer["ELEMS"][0]["NAME"] . '; ';
                        $buyer .= 'ИНН: ' . $arPropsBuyer["INN"]["VALUE"] . '; ';
                        $buyer2 = 'Контактное лицо: ' . $arPropsBuyer["CONTACT_PEOPLE"]["VALUE"] . '; ';
                        $buyer2 .= 'Телефон: ' . $arPropsBuyer["PHONE"]["VALUE"] . '; ';
                        if ($arPropsBuyer["REGION"]["VALUE"]) {
                            $arRegions = $elemIbl->GetList(
                                IB_REGION,
                                ['ID', 'NAME'],
                                [
                                    'ACTIVE' => 'Y',
                                    'ID' => $arPropsBuyer["REGION"]["VALUE"]
                                ],
                                [
                                    'NAME' => 'ASC'
                                ]
                            )["ELEMS"];
                            $buyer .= 'Регион: ' . $arRegions[0]["NAME"] . '; ';
                        }
                        if ($arPropsBuyer["DISTRICT"]["VALUE"]) {
                            $arRegions = $elemIbl->GetList(
                                IB_AREA,
                                ['ID', 'NAME'],
                                [
                                    'ACTIVE' => 'Y',
                                    'ID' => $arPropsBuyer["DISTRICT"]["VALUE"]
                                ],
                                [
                                    'NAME' => 'ASC'
                                ]
                            )["ELEMS"];
                            $buyer .= 'Район: ' . $arRegions[0]["NAME"] . '; ';
                        }
                        if ($arPropsBuyer["ADDRESS"]["VALUE"])
                            $buyer .= 'Адрес: ' . $arPropsBuyer["ADDRESS"]["VALUE"] . '; ';
                    } elseif ($arFields2) {
                        $id_model = $request['MODELS_TECH'];
                        $id_type = $request['TYPE_TECH'];
                        $number = $request['NUMBER_TECH'];
                    }
                    if ($id_model && $id_type && $number) {
                        $infoModel = $elemIbl->GetList(IB_DIRECTORY, ["NAME"], ["ID" => $id_model, "!PROPERTY_TYPE_VALUE" => false])["ELEMS"][0];
                        $infoType = $typeTechHl->get(["UF_XML_ID" => $id_type])[0];
                        $techText = $infoType["UF_NAME"] . ", " . $infoModel["NAME"];
                    }
                    if ($buyer) {
                        $arFildsForm[] = ["id" => 79, "value" => $buyer];
                    }
                    if ($buyer2) {
                        $arFildsForm[] = ["id" => 6, "value" => $buyer2];
                    }
                    if ($techText && $number) {
                        $arFildsForm[] = ["id" => 84, "value" => $number];
                        $arFildsForm[] = ["id" => 85, "value" => $techText];
                    }
                    if ($request["REASON"]) {
                        $arFildsForm[] = ["id" => 86, "value" => $request["REASON"]];
                    }
                    if ($request["WISHES"]) {
                        $arFildsForm[] = ["id" => 11, "value" => $request["WISHES"]];
                    }
                    $arAkt = [];
                    if (!empty($_FILES["AKT"])) {
                        $propPhoto_db = CIBlockElement::GetProperty(IB_RECLAMATION_TECH, $result, "sort", "asc", array("CODE" => "AKT"));
                        while ($elem = $propPhoto_db->GetNext()) {
                            $arAkt[] = \CFile::GetFileArray($elem["VALUE"]);
                        }
                    }
                    $arPhoto = [];
                    if (!empty($_FILES["PHOTO"])) {
                        $propPhoto_db = CIBlockElement::GetProperty(IB_RECLAMATION_TECH, $result, "sort", "asc", array("CODE" => "PHOTO"));
                        while ($elem = $propPhoto_db->GetNext()) {
                            $arPhoto[] = \CFile::GetFileArray($elem["VALUE"]);
                        }
                    }
                    if (!empty($_FILES["PRITENZY"])) {
                        $propPhoto_db = CIBlockElement::GetProperty(IB_RECLAMATION_TECH, $result, "sort", "asc", array("CODE" => "PRITENZY"));
                        while ($elem = $propPhoto_db->GetNext()) {
                            $arAkt[] = \CFile::GetFileArray($elem["VALUE"]);
                        }
                    }
                    $arPyrus = [];
                    $pyrus = new PyrusController;
                    if ($workUser["WORK_COMPANY"] && !empty($arFildsForm)) {
                        $dealerText = $workUser["WORK_COMPANY"] . " - " . $workUser["LAST_NAME"] . " " . $workUser["NAME"] . " " . $workUser["SECOND_NAME"] . ", " . $workUser["PERSONAL_PHONE"];
                        $arFildsForm[] = ["id" => 87, "value" => $dealerText];
                        $arFildsForm[] = ["id" => 2, "value" => date('Y-m-d')];
                        if (!empty($arAkt)) {
                            $fileUnloadMore = [];
                            foreach ($arAkt as $item) {
                                $fileFind = $pyrus->uploadFile($item);
                                if ($fileFind["success"] && $fileFind["guid"]) {
                                    $fileUnloadMore[] = $pyrus->uploadFile($item);
                                }
                            }
                            if (!empty($fileUnloadMore))
                                $arFildsForm[] = ["id" => 13, "value" => $fileUnloadMore];
                        }
                        if (!empty($arPhoto)) {
                            $fileUnloadMore = [];
                            foreach ($arPhoto as $item) {
                                $fileFind = $pyrus->uploadFile($item);
                                if ($fileFind["success"] && $fileFind["guid"]) {
                                    $fileUnloadMore[] = $pyrus->uploadFile($item);
                                }
                            }
                            if (!empty($fileUnloadMore))
                                $arFildsForm[] = ["id" => 99, "value" => $fileUnloadMore];
                        }

                        if (!empty($infoManager["UF_PYRUS_ID"])) {
                            $arFildsForm[] = ["id" => 77, "value" =>
                                [
                                    "id" => $infoManager["UF_PYRUS_ID"]
                                ]
                            ];
                        }
                        $arPyrus = [
                            "form_id" => 695567,
                            "fields" => $arFildsForm
                        ];
                    }
                    if ($arPyrus) {
                        $resp_pyrus = [];
                        if (!empty($request['edit'])) {
                            $elemEdit = Models\Element::getList(IB_RECLAMATION_TECH, array("ID", "NAME"), array("ID" => $result))["ELEMS"][0];
                            if ($elemEdit && strpos('---- ' . $elemEdit["NAME"], 'RP') === false &&
                                strpos('---- ' . $elemEdit["NAME"], 'RT') === false) {
                                $arFildsForm[] = ["id" => 95, "value" => [
                                    "choice_ids" => [1],
                                ]];
                                $editArrPy = [
                                    "id" => $elemEdit["NAME"],
                                    "text" => "Задача изменена дилером " . $workUser["WORK_COMPANY"],
                                    "field_updates" => $arFildsForm
                                ];
                                $resp_pyrus = $pyrus->editTaskComment($editArrPy);
                            }
                        } else {
                            $resp_pyrus = $pyrus->AddTaskForm($arPyrus);
                        }
                        if (!empty($resp_pyrus) && $resp_pyrus["success"]) {
                            CIBlockElement::SetPropertyValueCode($result, "STATUS", 18);
                            Models\Element::update($result, ["NAME" => $resp_pyrus["id_task"]]);
                        }
                    }
                }
                /* endpyrus */

                $arHistory = [
                    "id_who" => $USER->GetID(),
                    "id_that" => $USER->GetID(),
                    "id_iblock" => IB_RECLAMATION_TECH,
                    "id_elem" => $result,
                    "type" => "add",
                    "action" => $actionText
                ];
                $history = new HistoryController();
                $history_add = $history->addHistory($arHistory);
                if ($history_add) {
                    return Controller::sendAnswer([]);
                } else {
                    return Controller::sendError('Произошла ошибка');
                }
            } else {
                return Controller::sendError('Произошла ошибка проверьте ваши данные');
            }
        }
    }

    public function editPersonalPage($request)
    {
        global $USER;
        $user = new CUser;
        $name = [];
        $addr = [];
        if ($request["NAME"]) {
            $name = explode(' ', $request["NAME"]);
        }
        if ($request["WORK_STATE"]) {
            $addr = explode(',', $request["WORK_STATE"]);
        }
        $fields = Array(
            "WORK_COMPANY" => $request["WORK_COMPANY"],
            "WORK_STATE" => $addr[0],
            "WORK_CITY" => $addr[1],
            "WORK_STREET" => $addr[2],
            "UF_REGIONS_USER" => $request["region"],
            "LAST_NAME" => $name[0],
            "NAME" => $name[1],
            "UF_COUNTRY" => $request["country"],
            "UF_BIN" => $request["UF_BIN"],
            "SECOND_NAME" => $name[3],
            "WORK_ZIP" => $request["WORK_ZIP"],
            "PERSONAL_PHONE" => $request["PERSONAL_PHONE"],
            "EMAIL" => $request["EMAIL"],
        );
        if ($user->Update($USER->GetID(), $fields)) {
            return Controller::sendAnswer([]);
        } else {
            return Controller::sendError('Произошла ошибка');
        }
    }
}
