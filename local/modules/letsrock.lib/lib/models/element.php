<?php

namespace Letsrock\Lib\Models;

use Bitrix\Main\Loader;

Loader::includeModule('iblock');



/*
 * Class Element
 * Класс для работы с элементами инфоблока
 */

class Element
{

    /**
     * Метод вывода списка элементов инфоблока
     * @param $iblock
     * @param array $select
     * @param array $filter
     * @param array $sort
     * @param bool $nav
     * @param bool $groupBy
     * @return array|string
     */
    public static function getList(
        $iblock,
        $select = array('*'),
        $filter = array('ACTIVE_DATE' => 'Y', 'ACTIVE' => 'Y'),
        $sort = array('SORT' => 'DESC'),
        $nav = false,
        $groupBy = false,
        $dopparams = []
    )
    {
        if (empty($iblock))
            return 'Ошибка! В вызове ' . __METHOD__ . ' отсутствует обязательный параметр iblock';

        $filter[] = array('IBLOCK_ID' => $iblock);

        if(!empty($nav))
            $nav = array('nPageSize'=>$nav);

        $res = \CIBlockElement::GetList($sort, $filter, $groupBy, $nav, $select);
        if(!empty($nav))
            $res->NavStart(0);

        $data = array();
        $props = array();
        while ($ob = $res->GetNextElement()) {
            $fildsElem = $ob->GetFields();
            $data["ELEMS"][] = $fildsElem;
            $data["PROPS"][$fildsElem["ID"]] = $ob->GetProperties();
        }

        if(!empty($nav)){
            $navStr = $res->GetPageNavStringEx($navComponentObject, "", "", true, 0, $dopparams["pagination"]?$dopparams["pagination"]:[]);
            $data["pagination"] = $navStr;
        }

        return $data;
    }

    /**
     * Метод получения элемента по ID
     * @param int $iblock
     * @param int $id
     * @param array $select
     * @param bool $props
     * @return array
     */
    public static function getById($iblock, $id, $select = array('*'), $props = false){

        if (empty($iblock) || empty($id))
            return 'Ошибка! В вызове ' . __METHOD__ . ' отсутствуют обязательные параметр iblock и/или id';

        $filter = Array("IBLOCK_ID" => $iblock, "ID" => $id);
        $res = \CIBlockElement::GetList(array(), $filter, false, array('nPageSize' => 1), $select);
        if($ob = $res->GetNextElement()){
            $data = $ob->GetFields();
            if($props)
                $data['PROPERTY'] = $ob->GetProperties();

            return $data;
        }else{
            return [];
        }
    }

    /**
     * Метод-алиас getById
     * @param int $iblock
     * @param int $id
     * @param array $select
     * @param array $props
     * @return array
     */
    public static function getId($iblock, $id, $select, $props){
        return self::getById($iblock, $id, $select, $props);
    }

    /**
     * Метод добавления нового элемента инфоблока
     * @param int $iblock
     * @param array $array
     * @param bool $active
     * @return bool
     */
    public static function add($iblock, $name, $array = array(), $active = false){

        global $USER;

        if (empty($iblock) || empty($name))
            return 'Ошибка! В вызове ' . __METHOD__ . ' отсутствуют обязательные параметр iblock и/или name';

        $el = new \CIBlockElement;

        $data = Helper::dataFilter($array, false);

        $data['IBLOCK_ID'] = $iblock;
        $data['NAME'] = trim(strip_tags($name));
        $data['ACTIVE'] = $active ? 'Y' : 'N';
        $data['MODIFIED_BY'] = $USER->GetID();

        if(!empty($data["PROPERTY"])){
            $data['PROPERTY_VALUES'] = $data['PROPERTY'];
            unset($data['PROPERTY']);
        }

        if($productId = $el->Add($data))
            return $productId;
        else {
            //pr($el->LAST_ERROR);
            return false;
        }

    }
    public static function update($elem, $array = array()){
        global $USER;

        $el = new \CIBlockElement;

        $data = Helper::dataFilter($array, false);

        if(!empty($data["PROPERTY"]))
            $data['PROPERTY_VALUES'] = $data['PROPERTY'];


        if($productId = $el->Update($elem, $data))
            return $elem;
        else {
            //pr($el->LAST_ERROR);
            return false;
        }
    }
}
