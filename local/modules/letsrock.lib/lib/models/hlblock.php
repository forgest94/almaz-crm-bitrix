<?php

namespace Letsrock\Lib\Models;

use Bitrix\Main\Loader;
use Bitrix\Highloadblock as HL;

Loader::includeModule('iblock');

class HLBlock {
    private $hlblock;
    private $entity;
    private $entityClass;

    function __construct($id) {
        Loader::IncludeModule('highloadblock');
        $this->hlblock = HL\HighloadBlockTable::getById($id)->fetch(); // id highload блока
        $this->entity = HL\HighloadBlockTable::compileEntity($this->hlblock);
        $this->entityClass = $this->entity->getDataClass();
    }

    function getSingleItemById($itemId) {
        $res = $this->entityClass::getList([
            'select' => ['*'],
            'filter' => ['ID' => $itemId],
        ]);

        $item = $res->fetch();

        if (empty($item)) {
            return false;
        }

        return $item;
    }

    function get($filter = [], $select = ['*'], $order = ['ID'=>'desc']) {
        $res = $this->entityClass::getList([
            'select' => $select,
            'filter' => $filter,
            'order' => $order,
        ]);

        $list = [];

        while ($ob = $res->fetch()) {
            $list[] = $ob;
        }

        if (count($list) < 1) {
            return false;
        }

        return $list;
    }
    function add(
        $data = array() // Массив полей для добавления
    )
    {
        $entity_data_class = $this->entityClass;

        /*$data = array(
           "UF_TYPE"=>'33',
           "UF_COUNT"=>'1',
           "UF_DATA"=>date("d.m.Y")
        );*/
        if(!empty($data)){
            $result = $entity_data_class::add($data);
            return $result;
        }else{
            return false;
        }
    }
}