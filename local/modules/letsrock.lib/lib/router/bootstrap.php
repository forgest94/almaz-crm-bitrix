<?php

namespace Letsrock\Lib\Models;
require_once($_SERVER['DOCUMENT_ROOT'] . "/bitrix/modules/main/include/prolog_before.php");

use FastRoute;

\Bitrix\Main\Loader::includeModule('letsrock.lib');

/*
 * Router
 */

$dispatcher = FastRoute\simpleDispatcher(function (FastRoute\RouteCollector $r) {
    $r->addRoute(['POST'], '/ajax/auth/', 'Letsrock\Lib\Controllers\AuthController/auth');
    $r->addRoute(['POST'], '/ajax/equipments/get/', 'Letsrock\Lib\Controllers\EquipmentsController/get');
    $r->addRoute(['POST'], '/ajax/personal/set/', 'Letsrock\Lib\Controllers\PersonalController/set');
    $r->addRoute(['POST'], '/ajax/dealer/addMark/', 'Letsrock\Lib\Controllers\PersonalController/addMark');
    $r->addRoute(['POST'], '/ajax/dealer/addService/', 'Letsrock\Lib\Controllers\PersonalController/addService');
    $r->addRoute(['POST'], '/ajax/dealer/getExplotationSerial/', 'Letsrock\Lib\Controllers\ExploitationController/getSerialNumber');
    $r->addRoute(['POST'], '/ajax/dealer/getExplotationSerialRecl/', 'Letsrock\Lib\Controllers\ExploitationController/getExplotationSerialRecl');
    $r->addRoute(['POST'], '/ajax/area/get/', 'Letsrock\Lib\Controllers\AreaController/get');
    $r->addRoute(['POST'], '/ajax/region/get/', 'Letsrock\Lib\Controllers\AreaController/getRegion');
    $r->addRoute(['POST'], '/ajax/dealer/addReclamation/parts', 'Letsrock\Lib\Controllers\PersonalController/addParts');
    $r->addRoute(['POST'], '/ajax/dealer/addReclamation/tech', 'Letsrock\Lib\Controllers\PersonalController/addTech');
    $r->addRoute(['POST'], '/ajax/dealer/addBuyer', 'Letsrock\Lib\Controllers\BuyersController/addBuyer');
    $r->addRoute(['POST'], '/ajax/task/edit', 'Letsrock\Lib\Controllers\TasksController/editTask');
    $r->addRoute(['POST'], '/ajax/dealer/editPersonalPage', 'Letsrock\Lib\Controllers\PersonalController/editPersonalPage');
    $r->addRoute(['GET'], '/ajax/api/get-token/', 'Letsrock\Lib\Controllers\PyrusController/getTask');
    $r->addRoute(['POST'], '/ajax/unloading/', 'Letsrock\Lib\Controllers\ExploitationController/unloadExplotations');
    $r->addRoute(['POST'], '/ajax/unloading/allbuyers/', 'Letsrock\Lib\Controllers\BuyersController/unloadBuyers');
    $r->addRoute(['POST'], '/ajax/unloading/exploitation/dealer/', 'Letsrock\Lib\Controllers\ExploitationController/unloadExplotationsDealer');
    $r->addRoute(['POST'], '/ajax/task/editDate/', 'Letsrock\Lib\Controllers\TasksController/editDateTask');
});

//___________________________

$httpMethod = $_SERVER['REQUEST_METHOD'];
$uri = $_SERVER['REQUEST_URI'];

if (false !== $pos = strpos($uri, '?')) {
    $uri = substr($uri, 0, $pos);
}

$uri = rawurldecode($uri);
$routeInfo = $dispatcher->dispatch($httpMethod, $uri);

switch ($routeInfo[0]) {
    case FastRoute\Dispatcher::NOT_FOUND:
        // ... 404 Not Found
        break;
    case FastRoute\Dispatcher::METHOD_NOT_ALLOWED:
        $allowedMethods = $routeInfo[1];
        // ... 405 Method Not Allowed
        break;
    case FastRoute\Dispatcher::FOUND:
        $handler = $routeInfo[1];
        $vars = $routeInfo[2];
        $vars['POST'] = $_POST;
        list($class, $method) = explode("/", $handler, 2);
        call_user_func_array(array(new $class, $method), $vars);
        break;
}
