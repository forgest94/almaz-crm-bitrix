<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Личный кабинет (дилер)");
if ($USER->IsAuthorized()) {
    $arGroups = $USER->GetUserGroupArray();
    $isGroup = array_intersect($arGroups, LK_IDS);
    $idUrl = $isGroup[key($isGroup)];
    if(!empty($idUrl))
        LocalRedirect('/'.LK_URLS[$idUrl].'/');
}else {
    LocalRedirect('/login/');
}
?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>